-- sale accounts in inpmast
select imsact, count(imsact)
from inpmast
group by imsact

select imstk#, imdoc#, imstat, b.bnsnam
from inpmast i
left join bopname b on b.bnkey = i.imkey
where trim(imsact) in ('144800', '145200') -- used car ws, used truck ws

-- sale types in bopmast
select bmwhsl, count(bmwhsl)
from bopmast 
group by bmwhsl

-- deals from bopmast with sale acct of 144800 or 145200
-- result includes many whith BMWHSL = R
select b.bmco#, b.bmstk#, b.bmvin, b.bmpric, b.bmsact, b.bmwhsl, b.bmsnam, i.imsact, i.imstat
from bopmast b
left join inpmast i on trim(bmstk#) = trim(imstk#)
where trim(bmsact) in ('144800', '145200')
and trim(bmco#) = 'RY1'

-- deals from bopmast with sale type of W
-- result includes a couple with non wholesale sale acct
select b.bmco#, b.bmstk#, b.bmvin, b.bmpric, b.bmsact, b.bmwhsl, i.imsact, i.imstat
from bopmast b
left join inpmast i on trim(bmstk#) = trim(imstk#)
where bmwhsl in ('W')
and bmco# = 'RY1'

-- where are all the cash for clunker trade ins ?
select b.bmstk#, b.bmvin, b.bmpric, b.bmsact, b.bmwhsl, b.bmsnam, i.imsact, i.imstat
from bopmast b
left join inpmast i on trim(bmstk#) = trim(imstk#)
where bmstk# like '%G%'