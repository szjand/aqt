/*
-- what are the new car inventory accounts
Select distinct gmco#, gmyear, gmacct, gmdesc 
from RYDEDATA.GLPMAST
--where left(trim(gmacct),1) = '1'
--and gmdesc like '%VENTO%'
--where gmacct like '124%'
where gmdesc like 'INV%'
--and gmyear in (2011, 2012)
order by gmacct
*/



select 
  trim(i.imstk#) as stocknumber, i.imvin, 
  case 
    when length(trim(imdinv)) = 8 then
      substr(imdinv, 5,2) ||'/' || right(imdinv,2) || '/' || left(imdinv,4)
    else
      'Unknown'
  end as "Date in Inv",
  imsact, imiact
-- select count(*)
from rydedata.inpmast i
left join ( -- eliminate inventory with no cost (Honda, cancelled dealer trades)
  select trim(gtctl#) as stocknumber, sum(gttamt) as Net
  from rydedata.glptrns
  where trim(gtacct) in (-- Inventory accounts only
    '123100', -- Chev
    '123101', -- Cad
    '123105', -- Buick
    '123700', -- Chev Lt Trk
    '123701', -- Cad Trk
    '123705', -- Buick Trk
    '123706', -- GMC Trk
    '223000', -- Honda Trk
    '223100', -- Honda
    '223105', -- Nissan
    '223700', -- Nissan Trk
    '323102', -- Buick
    '323702', -- Buick Trk
    '323703') -- GMC Trk) 
    and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by gtctl#
    having sum(gttamt) > 0) c on trim(i.imstk#) = c.stocknumber
where i.imstat = 'I'
and i.imtype = 'N'
and (net is null or net < 1)

-- current tool query
-- need to eliminate those with no inventory amt
SELECT IMSTK# as StockNumber, IMPRIC as ListPrice, IMYear as Yr, IMMake as Make, 
  IMMODL as Model, IMBody as Bdy, IMCOLR || ' / ' || IMTRIM as Color,
  IMODOM as Miles, 
  days(curdate()) - days(cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date)) as Days, 
  IMVIN as Vin 
-- select count(*)
FROM INPMAST i
WHERE IMSTAT = 'I'
and IMType = 'N'
and SUBSTRING(TRIM(IMSTK#), 1, 1) BETWEEN '1' and '9'


-- a vehicle in inventory will have a positive amount in the relevant inventory acct

  select gtctl#, sum(gttamt)as net
  from rydedata.glptrns
  where trim(gtacct) in (-- Inventory accounts only
    '123100', -- Chev
    '123101', -- Cad
    '123105', -- Buick
    '123700', -- Chev Lt Trk
    '123701', -- Cad Trk
    '123705', -- Buick Trk
    '123706', -- GMC Trk
    '223000', -- Honda Trk
    '223100', -- Honda
    '223105', -- Nissan
    '223700', -- Nissan Trk
    '323102', -- Buick
    '323702', -- Buick Trk
    '323703') -- GMC Trk) 
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by gtctl#
    having sum(coalesce(gttamt,0)) > 1


-- this is one that was being displayed that should not be
-- a cancelled dealer trade
select *
from glptrns
where trim(gtctl#) = 'H3437'
order by gtacct


SELECT trim(IMSTK#) as StockNumber, IMPRIC as ListPrice, IMYear as Yr, IMMake as Make, 
  IMMODL as Model, IMBody as Bdy, IMCOLR || ' / ' || IMTRIM as Color,
  IMODOM as Miles, 
  days(curdate()) - days(cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date)) as Days, 
  IMVIN as Vin 
-- select count(*)
FROM INPMAST i
inner join (
  select gtctl#, sum(gttamt)as net
  from rydedata.glptrns
  where trim(gtacct) in (-- Inventory accounts only
    '123100', -- Chev
    '123101', -- Cad
    '123105', -- Buick
    '123700', -- Chev Lt Trk
    '123701', -- Cad Trk
    '123705', -- Buick Trk
    '123706', -- GMC Trk
    '223000', -- Honda Trk
    '223100', -- Honda
    '223105', -- Nissan
    '223700', -- Nissan Trk
    '323102', -- Buick
    '323702', -- Buick Trk
    '323703') -- GMC Trk) 
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by gtctl#
    having sum(coalesce(gttamt,0)) > 1) ia on trim(i.imstk#) = trim(ia.gtctl#)
WHERE IMSTAT = 'I'
and IMType = 'N'
and SUBSTRING(TRIM(IMSTK#), 1, 1) = 'C'


-- 4/28/11: generating fewer vehicles than arkona inventory extract
-- C4249
-- hmm, offsetting entries with different GTREF#
select * from glptrns where trim(gtacct) = '323703' and trim(gtctl#) = 'C4249'


-- H3472
select * from inpmast where trim(imstk#) = 'H3472'

select * from glptrns where trim(gtacct) = '223700' and trim(gtctl#) = 'H3472' 

-- ok, for these 2, Marlee is fixing them in accounting
-- try to generate a list programatically
-- maybe in inpmast but no inventory account balance

SELECT trim(IMSTK#) as StockNumber,  imiact as"Inv Acct"
from rydedata.Inpmast i
left join (
  select gtctl#, sum(gttamt)as net
  from rydedata.glptrns
  where trim(gtacct) in (-- Inventory accounts only
    '123100', -- Chev
    '123101', -- Cad
    '123105', -- Buick
    '123700', -- Chev Lt Trk
    '123701', -- Cad Trk
    '123705', -- Buick Trk
    '123706', -- GMC Trk
    '223000', -- Honda Trk
    '223100', -- Honda
    '223105', -- Nissan
    '223700', -- Nissan Trk
    '323102', -- Buick
    '323702', -- Buick Trk
    '323703') -- GMC Trk) 
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by gtctl#
    having sum(coalesce(gttamt,0)) > 1) ia on trim(i.imstk#) = trim(ia.gtctl#)
where imstat = 'I'
and imtype = 'N'
and ia.gtctl# is null 
order by imiact, imstk#


select immake, count(*)
from rydedata.inpmast
group by immake
order by immake



-- 1/16/12
-- this is what's currently in the tool
-- what's the join to INPOPTF for: Internet Price
-- 
SELECT ' +

      'f.IONVAL as BestPrice, ' +
      'IMPRIC as ListPrice, ' +
      'IMYear as Year, ' +

      'IMMODL as Model, ' +
      'IMBody as Body, ' +
      'IMCOLR || '' / '' || IMTRIM as Color, ' +
      'IMODOM as Miles, ' +
      'days(curdate()) - days(cast(left(trim(imdinv),4)||''-''||substr(trim(imdinv),5,2)||''-''||right(trim(imdinv),2) as date)) as Days, ' +

      'FROM INPMAST i ' +
      'inner join (select gtctl#, sum(gttamt)as net from rydedata.glptrns where trim(gtacct) in (' +
      '''123100'',''123101'',''123105'',''123700'',''123701'',''123705'',''123706'', ' +
      '''223000'',''223100'',''223105'',''223700'',''323102'',''323702'',''323703'')' +
      'and trim(gtpost) <> ''V'' group by gtctl# having sum(coalesce(gttamt,0)) > 1) ia ' +
      'on trim(i.imstk#) = trim(ia.gtctl#) ' +
      'left outer join INPOPTF f on f.IOVIN = i.IMVIN and f.IOSEQ# = ''9'' ' +
      'WHERE IMSTAT = ''I'' and IMType = ''N''



-- 1/16/12
-- for nightly scrape into dpsvseries.arkonainpmastnightly
select imstk#, imvin, imyear, immake, immodl, imbody, imcolr, imtrim, imodom, impric, f.ionval 
--select count(*) 
from rydedata.inpmast i
inner join (
  select gtctl#, sum(gttamt) as net
  from rydedata.glptrns
  where trim(gtacct) in (
      '123100', -- Chev
      '123101', -- Cad
      '123105', -- Buick
      '123700', -- Chev Lt Trk
      '123701', -- Cad Trk
      '123705', -- Buick Trk
      '123706', -- GMC Trk
      '123707', -- MV-1
      '223000', -- Honda Trk
      '223100', -- Honda
      '223105', -- Nissan
      '223700', -- Nissan Trk
      '323102', -- Buick
      '323702', -- Buick Trk
      '323703') -- GMC Trk) 
    and trim(gtpost) <> 'V'
  group by gtctl#
      having sum(coalesce(gttamt, 0)) > 1) ia  on trim(i.imstk#) = trim(ia.gtctl#)
left join rydedata.inpoptf f on f.iovin = i.imvin and f.ioseq# = '9'
where imstat = 'I'
  and imtype = 'N'


-- get away from hard coded list of accounts
select imstk#, imvin, imyear, immake, immodl, imbody, imcolr, imtrim, imodom, impric, f.ionval 
--select count(*) 
from rydedata.inpmast i
inner join (
  select gtacct, gtctl#, sum(gttamt) as net
  from rydedata.glptrns
  where  trim(gtpost) <> 'V'
  group by gtacct, gtctl#
      having sum(coalesce(gttamt, 0)) > 1) ia  on trim(i.imstk#) = trim(ia.gtctl#)
inner join (
  select distinct gmacct
  from rydedata.glpmast
  where (
    gmdesc like 'INV N/%'
    or
    gmdesc like 'INV-NEW%'
    or
    gmdesc like 'INV NEW%')) a on trim(ia.gtacct) = trim(a.gmacct)
left join rydedata.inpoptf f on f.iovin = i.imvin and f.ioseq# = '9'
where imstat = 'I'
  and imtype = 'N'



