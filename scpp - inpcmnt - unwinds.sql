Select * 
from RYDEDATA.INPCMNT a
inner join rydedata.inpmast b on a.vin = b.imvin
where transaction_Date > 20160000 
and (
  upper(comment) like '%WOUND%'
  or upper(comment) like '%BACK%')
order by trim(b.inpmast_stock_number)  


-- 2/16/16  27273 is currently an unwind, sold 2/12, unwound 2/16
select *
from rydedata.glptrns a
inner join rydedata.glpmast b on a.gtacct = b.gmacct
  and b.gmyear = 2015
  and b.account_type = '4'
where trim(a.gtctl#) = '27273'
  and a.gtjrnl = 'VSN'

-- as opposed to 26500, capped on 1/6/16, unwound and recapped on 1/7/16
select *
from rydedata.glptrns a
inner join rydedata.glpmast b on a.gtacct = b.gmacct
  and b.gmyear = 2015
  and b.account_type = '4'
where trim(a.gtctl#) = '26500'
  and a.gtjrnl = 'VSN'


select *
from rydedata.glptrns a
inner join rydedata.glpmast b on a.gtacct = b.gmacct
  and b.gmyear = 2015
  and b.account_type in ('1', '4')
where trim(a.gtctl#) = '26500'
  and a.gtjrnl = 'VSN'


select *
from rydedata.glptrns a
where gtjrnl in ('VSN', 'VSU')
  and gtdate between '01/01/2016' and '01/31/2016'
  and exists (
    select 1 
    from rydedata.glptrns
    where gtjrnl in ('VSN', 'VSU')
      and gtdate between '01/01/2016' and '01/31/2016'   
      and gtpost = 'V'
      and gtctl# = a.gtctl#)
order by gtctl#, gtdate    

-- sale acct, veh sale journal w void entry with higher transaction #
-- valid unwind 27379, 27273, 
select *
from rydedata.glptrns a
inner join rydedata.glpmast b on trim(a.gtacct) = trim(b.gmacct)
  and b.gmyear = 2015
  and b.account_type = '4'
  and b.department in ('NC','UC')
where a.gtdate > '12/31/2015'
  and a.gtjrnl in ('VSN','VSU')
  and exists (
    select 1
    from rydedata.glptrns
    where gtdate > '12/31/2015'
      and trim(gtctl#) = trim(a.gtctl#)
      and trim(gtacct) = trim(a.gtacct)
      and gtpost = 'V')
--      and gttrn# > a.gttrn#)
order by gtctl#, gttrn#

-- sale acct, veh sale journal w void entry and no other entry
-- valid unwind 27379, 27273, 
select *
from rydedata.glptrns a
inner join rydedata.glpmast b on trim(a.gtacct) = trim(b.gmacct)
  and b.gmyear = 2015
  and b.account_type = '4'
  and b.department in ('NC','UC')
where a.gtdate > '12/31/2015'
  and a.gtjrnl in ('VSN','VSU')
  and a.gtpost = 'V'
  and not exists (
    select 1
    from rydedata.glptrns
    where gtdate > '12/31/2015'
      and trim(gtctl#) = trim(a.gtctl#)
      and trim(gtacct) = trim(a.gtacct)
      and gtpost = 'Y')
--      and gttrn# > a.gttrn#)
order by gtctl#, gttrn#


-- wtf, 27504, 2 different sales accounts?, 
-- same thing with 26717a OTHER unwound, Certified Sold
select *
from rydedata.glptrns a
inner join rydedata.glpmast b on trim(a.gtacct) = trim(b.gmacct)
  and b.gmyear = 2015
  and b.account_type = '4'
  and b.department in ('NC','UC')
where a.gtdate > '12/31/2015'
  and a.gtjrnl in ('VSN','VSU')
  and trim(a.gtctl#) = '26717A'
  and trim(a.gtctl#) = '27504'

select * from rydedata.bopmast where trim(bopmast_stock_number) in ('27379','27273')
27273 is in ext_bopmast_0214

-- another possible approach, an inventory amount on a vehicle previously sold
-- hmmm gtdate greater than when inentory was relieved




      