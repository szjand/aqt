Select suco#, suuser, suname, 
  trim(left(suname, position(' ' in suname))) as firstname,
  substring(suname,position(' ' in suname)+1,20) as lastname
from RYDEDATA.SEPUSER 
where suname <> ''
  and suco# in ('RY1', 'RY2')
  and suuser not like '*%'