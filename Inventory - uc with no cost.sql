-- the sum per vehicle of transactions against accts '124000', '124100', '224000','224100'
select 
  case 
    when right(trim(i.imstk#),1) = 'X' then 'Purchase'
    else 'Trade'
  end as Source,
  trim(i.imstk#) as stocknumber, i.imvin, 
  case 
    when length(trim(imdinv)) = 8 then
      substr(imdinv, 5,2) ||'/' || right(imdinv,2) || '/' || left(imdinv,4)
    else
      'Unknown'
  end as "Date in Inv",
  --cast(c.net as integer) as net,
  --cast(r.recon as integer) as recon,
  imsact, imiact
from inpmast i
left join (
  select trim(gtctl#) as stocknumber, sum(gttamt) as Net
  from rydedata.glptrns
  where trim(gtacct) in ('124000', '124100', '224000','224100') -- Inventory accounts only
    and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by gtctl#
    having sum(gttamt) > 0) c on trim(i.imstk#) = c.stocknumber
left join (	
  select trim(gtctl#) as stocknumber, sum(gttamt) as Recon
  from rydedata.glptrns
  where trim(gtacct) in ('124000', '124100', '224000','224100') -- Inventory accounts only
    and trim(gtpost) <> 'V'  -- Ignore VOIDS
	and trim(gtjrnl) in ('SVI', 'SWA', 'SCA') -- Journals = Service Sales Internal, Service Sales Warranty, Service Sales Retail
  group by gtctl#
    having sum(gttamt) > 0) r on trim(i.imstk#) = r.stocknumber	
where i.imstat = 'I'
  and i.imtype = 'U'
  and left(trim(i.imstk#), 1) <> 'C'
  and trim(i.imstk#) not in ('HTEST','22002VB')
and (net is null or net < 1)


