select *
from connect
where id > 4100000

select *
from connect
order by connecttime desc
limit 10000


select min(connecttime), max(connecttime)
from (
select *
from connect
order by connecttime desc
limit 500) a


select *
from connect
where connecttime > (
select min(connecttime)
from (
select *
from connect
order by connecttime desc
limit 500) a)


select id, calltableid, connecttime, disconnecttime, 
  timestampdiff(second, connecttime, disconnecttime),
  talktime, talktimeseconds, holdtime, ringtime, duration
from connect
order by id desc
limit 100


-- nick leaves me a voice mail
select * from `call` where id = 1662544
select * from connect where calltableid = 1662544 order by connecttime

-- called myself from ext 5815 but hung before ring
select *
from `call`
where dialednumber like '%2187793664'
order by starttime desc
limit 100

select * from connect where calltableid = 1664321


select holdtime, 
from connect order by talktime desc limit 500





-- calls that do not exist in connect
-- there are none
-- call to connect is one to many
select * from `call` a
where not exists (
  select 1
  from connect
  where calltableid = a.id
  limit 1000)
order by starttime desc limit 1000


-- nick leaves me a voice mail
select * from `call` where id = 1662544

select * from `call` where dialednumber = '5815' and callerid = 6004

select *
from `call`
order by starttime desc
limit 100

-- called myself from ext 5815 but hung before ring
-- disputing the notion th
select *
from `call`
where dialednumber like '%2187793664'
order by starttime desc
limit 100

select * from connect where calltableid = 1664096

select * from `call` order by id desc limit 1000

select duration, time(duration) from `call` order by id desc limit 1000

select id, callid, sipcallid, starttime, endtime, locked, extension, time(duration) as duration, calltype, workgroupcall,
  dialednumber, callerid
from `call`  
order by id desc limit 1000



-- does a row get created before a connected call is ended, ie, what is the endtime of a call in progress
-- stayed connected for at least a minute
-- stayed connected for 4:06
-- no row in call until i hung up
select * from (
  select * 
  from `call` 
  order by id desc  limit 1000) a
where callerid = '5815' 



select * from (
  select * 
  from `call` 
  order by id desc  limit 1000) a
where dialednumber like '%2187793664' 
-- what about connected

select * from (
  select *
  from connect
  order by id desc limit 1000) a
where callerid = '5815'  


-- call to connect is one to many, these are the base essential fields for call
-- limit 10,000 takes me back 2 days
-- limit 100000 takes me back a month, let's start with that
select id, callid, sipcallid, starttime, endtime, locked, extension, time_to_sec(time(duration)) as duration, calltype, workgroupcall,
  dialednumber, callerid
from `call`  
order by id desc limit 50000

select min(starttime) from (
select id, callid, sipcallid, starttime, endtime, locked, extension, time(duration) as duration, calltype, workgroupcall,
  dialednumber, callerid
from `call`  
order by id desc limit 50000) a


select distinct callnote from (
  select * from `call` order by id desc limit 100000) a

-- the relevant connect columns
-- and the connect rows with a calltableid from above
select a.id, a.partytype, a.calltableid, a.lineid, a.switchid, a.portnumber, a.portid, a.portname, a.groupid,
  a.groupname, a.connecttime, a.disconnecttime, a.connectreason, a.disconnectreason, a.partyidflags, 
  a.partyid, a.partyidname, a.partyidlastname, a.ctrlpartyidflags, a.ctrlpartyid, a.ctrlpartyidname, ctrlpartyidlastname, 
 a. mailboxid, a.talktimeseconds, time_to_sec(time(a.holdtime)) as holdtime, time_to_sec(time(a.ringtime)) as ringtime, 
  time_to_sec(time(a.duration)) as duration
from connect a
inner join (
  select id
  from `call`  
  order by id desc limit 50000) b on a.calltableid = b.id


select a.id, a.partytype, a.calltableid, a.lineid, a.switchid, a.portnumber, a.portid, a.portname, a.groupid,
  a.groupname, a.connecttime, a.disconnecttime, a.connectreason, a.disconnectreason, a.partyidflags, 
  a.partyid, a.partyidname, a.partyidlastname, a.ctrlpartyidflags, a.ctrlpartyid, a.ctrlpartyidname, ctrlpartyidlastname,
 a. mailboxid, a.talktimeseconds, time(a.holdtime) as holdtime, time(a.ringtime) as ringtime, 
  time(a.duration) as duration
from connect a
where calltableid = 1665354


select * from callType

select * from connectReason;

select * from disconnectreason;

select * from partyidflag;

select * from partytype

select * from queuecall order by starttime desc limit 500



select id, callid, sipcallid, starttime, endtime, locked, extension, time_to_sec(time(duration)) as duration, calltype, workgroupcall,
  dialednumber, callerid
-- select count(*)  
from `call`  
where id > 1665643
order by id asc limit 50000


select a.id, a.partytype, a.calltableid, a.lineid, a.switchid, a.portnumber, a.portid, a.portname, a.groupid,
  a.groupname, a.connecttime, a.disconnecttime, a.connectreason, a.disconnectreason, a.partyidflags, 
  a.partyid, a.partyidname, a.partyidlastname, a.ctrlpartyidflags, a.ctrlpartyid, a.ctrlpartyidname, ctrlpartyidlastname, 
 a. mailboxid, a.talktimeseconds, time_to_sec(time(a.holdtime)) as holdtime, time_to_sec(time(a.ringtime)) as ringtime, 
  time_to_sec(time(a.duration)) as duration
select count(*)  
from connect a
where calltableid > 1615640


select id, callid, sipcallid, 
  starttime, endtime, locked, extension, 
  time_to_sec(time(duration)) as durationSeconds, 
  calltype, workgroupcall,
  dialednumber, callerid
-- select count(*) 
-- select min(starttime) 
from `call` 
--where id > 1665643  
order by id desc limit 50000


select id, partytype,calltableid,lineid, switchid, 
  portnumber, portid, portname, groupid,
  groupname, connecttime, disconnecttime, 
  connectreason, disconnectreason, partyidflags, 
  partyid, partyidname, partyidlastname, 
  ctrlpartyidflags, ctrlpartyid, ctrlpartyidname, 
  ctrlpartyidlastname, mailboxid, talktimeseconds, 
  time_to_sec(time(holdtime)) as holdtime, 
  time_to_sec(time(ringtime)) as ringtime, 
  time_to_sec(time(duration)) as duration
-- select count(*)  
from connect 
where calltableid > 1665643

-- 4/27
-- initial load

select count(*) from (
select id, callid, sipcallid, 
  starttime, endtime, locked, extension, 
  time_to_sec(time(duration)) as durationSeconds, 
  calltype, workgroupcall,
  dialednumber, callerid
-- select count(*) 
from `call` 
--where id > 1665643  
order by id desc limit 50000) a


select min(starttime) from (
select id, callid, sipcallid, 
  starttime, endtime, locked, extension, 
  time_to_sec(time(duration)) as durationSeconds, 
  calltype, workgroupcall,
  dialednumber, callerid
-- select count(*) 
from `call` 
--where id > 1665643  
order by id desc limit 50000) a

-- initial load of call, 50000 rows that go back to 4/10/14
-- export query as insert statement
select id, callid, sipcallid, 
  starttime, endtime, locked, extension, 
  time_to_sec(time(duration)) as durationSeconds, 
  calltype, workgroupcall,
  dialednumber, right(trim(dialednumber), 7) as dialedNumberRight7,
  callerid, right(trim(callerid), 7) as callerIdRight7
from `call` 
order by id desc limit 50000


select a.id, a.partytype, a.calltableid, a.lineid, 
  a.switchid, a.portnumber, a.portid, a.portname,
  a.groupid, a.groupname, a.connecttime, 
  a.disconnecttime, a.connectreason, a.disconnectreason, 
  a.partyidflags, a.partyid, a.partyidname, 
  a.partyidlastname, a.ctrlpartyidflags, a.ctrlpartyid,
  a.ctrlpartyidname, ctrlpartyidlastname, 
  a.mailboxid, a.talktimeseconds, 
  time_to_sec(time(a.holdtime)) as holdtimeseconds, 
  time_to_sec(time(a.ringtime)) as ringtimeseconds, 
  time_to_sec(time(a.duration)) as durationseconds
from connect a
inner join (
  select id
  from `call`  
  order by id desc limit 50000) b on a.calltableid = b.id
  
-- and subsequent update scrapes  
select id, partytype, calltableid, lineid, 
  switchid, portnumber, portid, portname,
  groupid, groupname, connecttime, 
  disconnecttime, connectreason, disconnectreason, 
  partyidflags, partyid, partyidname, 
  partyidlastname, ctrlpartyidflags, ctrlpartyid,
  ctrlpartyidname, ctrlpartyidlastname, 
  mailboxid, talktimeseconds, 
  time_to_sec(time(holdtime)) as holdtimeseconds, 
  time_to_sec(time(ringtime)) as ringtimeseconds, 
  time_to_sec(time(duration)) as durationseconds
from connect a
where calltableid > 1671851


  
select count(*) from `call` where id > 1671913 

select * from connect  where calltableid > 1671917


select * from `call` order by id desc

select * from `call` where id = 1672130

select * from `call` where year(endtime) < 2000

select *
from connect
where calltableid in (
  select id
  from `call` 
  where year(endtime) < 2000)
  
select *
from `call`
order by id desc limit 100

select *
from connect
where calltableid = 1672540

select * from `call` where id > 1672129 and year(endtime) > 2000

select * from connect a
where not exists (
  select 1
  from `call`
  where id = a.calltableid)
  
  
select * from `call` where id = 1675039

select * from connect where calltableid = 1672937

select * from (
select * from `call` order by starttime desc limit 100
) x where callerid = '5815'


select * from `call` order by starttime desc limit 100

select count(*) from (
select * from `call` order by id desc limit 100
) x where id > 1678651

-- 4/30
-- uh oh, have i forgotten about the possibility of gaps in call.id
-- fuck yeah, none of the "missing" call.id rows even fucking exist
select * from `call` where id in (
1636785,    
1636818,    
1643842,    
1645590,    
1645994,    
1646228,    
1646315,    
1646554,    
1654227,    
1655274,    
1655832,    
1657522,    
1658075,    
1660334,    
1661430,    
1662754,    
1662987,    
1663464,    
1663829,    
1665579,    
1665954,    
1666657,    
1667897,    
1668040,    
1668562,    
1669760,    
1669764,    
1669897,    
1669916,    
1670456)

select * from `call` where id between 1636785 and 1670456 order by id

SELECT *
FROM `call`
WHERE right(trim(dialedNumber), 7) = '8932485'

select * from (
SELECT *
FROM `call`
order by id desc
limit 5000
) x where extension = '5914'
order by starttime desc


-- 5/1
according to the admin guide, a row is created in call when the call begins 
and is updated when the call ends, setting the locked field to true
but i am not seeing that, 
  select * 
  from `call` 
  order by id desc  limit 100
  
  select *
  from `call`
  where locked <> 1
order by id limit 1001  
Call Table: 
StartTime Date/Time 
  For an inbound call, this is when the trunk has been seized. For an outbound
  call, this is when the user has completed dialing. (8-byte date/time, required)
  
EndTime Date/Time 
  Time when the call terminates (either by the near end hanging up or when the
  end external to the system hangs up) and the ShoreTel switch receives the
  notification of the disconnect. (8-byte date/time)
  

The Connect table contains a record for each party in a call. There are many different types of parties
that can be reflected
  
ConnectTime Date/Time
Time when party was added to call.
Initial parties on inbound call, value indicates time trunk was seized.
Initial parties on outbound call, value indicates time dialing was complete.  

PartyIDFlags Number Caller ID flags that specifies the data available in ID and Name fields
Internal party: number, name and last name from system address book.
External party: data corresponds to caller ID field provided by the PSTN.
Refer to Table 139. (6-bit integer � required)

PartyID Text Number of party. Refer to PartyIDFlags field. (50 characters)

PartyIDName Text Name of party. Refer to PartyIDFlags field. (50 characters)


-- compare call.in/out vs connect trunk direction
-- initially looks like trunk direction is of no use 
  select a.id, b.name, a.starttime, c.connecttime, d.name
  from `call` a
  left join calltype b on a.calltype = b.calltype
  left join connect c on a.id = c.calltableid
  left join trunkdirectionlut d on c.trunkdirection = d.trunkdirection
  order by a.id desc
  limit 1000

-- are calls 1687274 - 1687277  all the same call? extensin the same start/endtime the same,
--   all workgroup calls, to 4 different extensions
--   workgroup agent is in the connect table
select a.id, a.starttime, a.endtime, a.extension, time_to_sec(a.duration) as duratin, b.name as CallType,
  a.workgroupcall, a.dialednumber, a.callerid 
from `call` a
left join calltype b on a.calltype = b.calltype
order by a.id desc
limit 1000

select a.id, a.starttime, a.endtime, a.extension, time_to_sec(a.duration) as duratin, b.name as CallType,
  a.workgroupcall, a.dialednumber, a.callerid, 
  c.*
from `call` a
left join calltype b on a.calltype = b.calltype
left join connect c on a.id = c.calltableid
where a.id between 1687274 and 1687277


select *
from `call`
where id > 1715874
order by starttime desc 



select a.id as "call.id", callID, sipCallId, a.startTime, a.endTime, time_to_sec(time(a.duration)) as duration,
  a.dialedNumber, b.id as "connect.id", b.portName, b.groupName, b.connectTime, b.disconnectTime,
  time_to_sec(time(b.talkTime)) as talkTime, 
  time_to_sec(time(b.holdTime)) as holdTime,
  time_to_sec(time(b.ringTime)) as ringTime, 
  time_to_sec(time(b.duration)) as "connect.Duration"
-- select *  
FROM `call` a
LEFT JOIN Connect b on a.id = b.callTableId
WHERE DATE(a.StartTime) = '2014-05-14'
  AND a.callType = 3
  AND a.extension = '5914'
ORDER BY a.startTime



select *  
FROM `call` a
LEFT JOIN Connect b on a.id = b.callTableId
where a.id in (1724486, 1725625, 1726381)

-- 5/22 rethinking scrape, instead of max call.id, do entire day into tmp (like i did for call copy)
-- looks slow
select date(starttime), curdatE(), a.*
from `call` a
order by startTime desc
limit 500


select *
from `call`
where date(starttime) = curdate()
limit 5000

select *
from connect
where calltableid in (
  select id
  from `call`
  where date(starttime) = curdate())

-- this isn't too bad, speed wise
select a.*
-- select count(*)
from connect a
inner join `call` b on a.callTableId = b.id
where date(b.starttime) = curdate() -1

