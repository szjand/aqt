
select *
from `call` a
order by a.starttime desc 
limit 1000
    
    
select distinct date(starttime), dayname(starttime), dayofweek(starttime)
from `call` a
where  year(date(starttime)) = 2015    
limit 100

select starttime, date(starttime), dayname(starttime), dayofweek(starttime)
from `call` a
where  date(starttime) = '2015-01-28'  
order by starttime
limit 100
    
select date(starttime), dayname(starttime), count(*)
from `call` a
where date(starttime) > '2015-01-04'
group by date(starttime), dayname(starttime)


select hour(time(a.starttime)) as theHour, -- round(count(*)/18, 0) as avgCalls,
--  sum(case when date(a.starttime) = '2015-01-12' then 1 else 0 end) as calls0112,
--  sum(case when date(a.starttime) = '2015-01-15' then 1 else 0 end) as calls0115,
--  sum(case when date(a.starttime) = '2015-01-19' then 1 else 0 end) as calls0119,
--  sum(case when date(a.starttime) = '2015-01-20' then 1 else 0 end) as calls0120,
  sum(case when date(a.starttime) = '2015-06-11' then 1 else 0 end) as calls0611,
  sum(case when date(a.starttime) = '2015-06-10' then 1 else 0 end) as calls0610,
  sum(case when date(a.starttime) = '2015-06-09' then 1 else 0 end) as calls0609,
  sum(case when date(a.starttime) = '2015-06-08' then 1 else 0 end) as calls0608,
  sum(case when date(a.starttime) = '2015-06-07' then 1 else 0 end) as calls0607,
  sum(case when date(a.starttime) = '2015-06-06' then 1 else 0 end) as calls0606
from `call` a
where  year(date(a.starttime)) = 2015  
  and dayofweek(a.starttime) between 2 and 6 
  and hour(time(a.starttime)) between 7 and 20
group by hour(time(a.starttime))


