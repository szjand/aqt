
-- 12/16/15 queuecall queries from stackoverflow

select a.id, a.callid, a.connecttableid, a.starttime, a.durationseconds, 
  a.queuename, a.queuedn, a.targetdn, a.targettype,
  a.targetfirstname,a.targetlastname, 
  b.name as target_name, c.name as queue_exit_reason, 
  d.*, e.*
-- select *  
from queuecall a
left join targettypelut b on a.targettype = b.targettype
left join queueexitreasonlut c on a.exitreason = c.exitreason
left join queuestep d on a.id = d.qcalltableid
left join stepexitreasonlut e on d.exitreason = e.exitreason
-- where a.id = 594537
WHERE DATE(a.StartTime) = '2015-01-16'



select a.id, a.callid, a.connecttableid, a.starttime, a.durationseconds, 
  a.queuename, a.queuedn, a.targetdn, a.targettype,
  a.targetfirstname,a.targetlastname, 
  b.name as target_name, c.name as queue_exit_reason, 
    (select count(*) from queuestep where qcalltableid = a.id) as queue_steps
-- select *  
from queuecall a
left join targettypelut b on a.targettype = b.targettype
left join queueexitreasonlut c on a.exitreason = c.exitreason
--left join queuestep d on a.id = d.qcalltableid
--left join stepexitreasonlut e on d.exitreason = e.exitreason
-- where a.id = 594537
WHERE DATE(a.StartTime) = '2015-01-16'


-- http://stackoverflow.com/questions/20405898/multiple-select-case-staments-in-query-for-shoretel-reports
-- removed the parens in joins
SELECT  queuecall1.StartTime, 
        queuecall1.QueueName,
        CASE WHEN ExitReason = 7 THEN 1 ELSE 0 END AS CallsAbandoned,
        CASE WHEN ExitReason = 1 THEN 1 ELSE 0 END AS CallsAgent,
        CASE WHEN calltype = 1 THEN 1 ELSE 0 END AS CallsInternal,
        CASE WHEN calltype = 2 THEN 1 ELSE 0 END AS CallsExternal
FROM queuecall queuecall1
INNER JOIN connect connect1 ON queuecall1.ConnectTableID=connect1.ID
INNER JOIN `call` call1 ON connect1.CallTableID=call1.ID
WHERE DATE(queuecall1.StartTime) = '2015-01-16'
-- join to connect -> call is to get the call type
--   the labeling of exit reasons, meh

where i am fuzzy is, what calls go into quecall, what makes these calls relevant:
from manual:
  each time a call is processed by the workgroup server, an entry is made in the QueueCall table.
  A workgroup is a call queuing mechanism, the the name "QueueCall" in the CDR database.
  
which then makes me ask, what calls are handled by the workgroup server and why  


-- 12/19/15
-- of the calls with multiple connections, what does QueueCall look like
-- first, for the mysqlworkbench model, let's add some type enumerations

select calltype as type, name
from calltype

select exitreason as code, name, description
from queueexitreasonlut

select a.id, count(*)
from `call` a
inner join connect b on a.id = b.calltableid
where a.calltype = 2 -- incoming calls
  and date(a.starttime) = '2015-01-15'
group by a.id
having count(*) > 5
order by count(*) desc  


select d.id as call_id, e.id as conn_id, h.id as q_id, j.id as q_step_id,
  time(d.duration) as call_dur, time(e.connecttime) as conn_time, 
  time(e.disconnecttime) as disconn_time, time(e.ringtime) as conn_ring_time, 
  time(e.holdtime) as conn_hold_time, time(e.talktime) as conn_talk_time,  
  time(e.duration) as conn_duration, f.name as conn_reason, 
  g.name as disconn_reason,
  h.queuename, h.targetlastname,  
  time(h.starttime) as q_start_time, time(h.duration) as q_duration,
  i.name as q_exit_reason,
  j.stepnumber as q_step_num, time(j.starttime) as q_step_start, time(j.duration) as q_step_duration,
  k.name as q_step_exit, k.description as q_step_exit_des
from ( -- incoming calls with more than 5 connections
  select a.id
  from `call` a
  inner join connect b on a.id = b.calltableid
  where a.calltype = 2 -- incoming calls
    and date(a.starttime) = '2015-01-15'
  group by a.id
  having count(*) > 3) c 
inner join `call` d on c.id = d.id 
inner join connect e on d.id = e.calltableid 
inner join connectreason f on e.connectreason = f.connectreason
inner join disconnectreason g on e.disconnectreason = g.disconnectreason
left join queuecall h on e.id = h.connecttableid
left join queueexitreasonlut i on h.exitreason = i.exitreason 
left join queuestep j on h.id = j.qcalltableid
left join stepexitreasonlut k on j.exitreason = k.exitreason
order by c.id, e.id, h.id, j.id



