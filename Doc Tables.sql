select *
from rydedata.glpfaaa
where g3co# = 'RY1'
  and g3acct in ('1400001', '1600001')

rpt 3 Jeri ry1 new veh dept
glpfaaa has sales acct, but not cost acct
is it decoding from glpmast

select *
from rydedata.glpfaaa
where g3co# = 'RY1'
  and g3rpt# = 3

select *
from rydedata.glptrns
where trim(gtdoc#) = 'H5049'

select count(*)
from rydedata.glptrns
where gtdate = '2012-09-13'

select gtdate, count(*)
from rydedata.glptrns
where year(gtdate) = 2012
  and month(gtdate) = 9
  and gtpost = 'Y'
group by gtdate

select *
from rydedata.glptrns
where gtdate = '2012-09-21'
  and gtpost = 'Y'

select month(gtdate), count(*)
from rydedata.glptrns
where gtpost = 'Y'
  and year(gtdate) = 2012
group by month(gtdate)

-- do transactions span multiple days


SELECT a.g1rpt#, a.g1titl, b.g2desc, c.g3acct, 

  g2lky1, g2lky2, g2lky3, g2lky4, g2lky5,
  g2lky6, g2lky7, g2lky8, g2lky9, g2lky10, g2lky11, g2lky12
FROM rydedata.glpfard a 
LEFT JOIN rydedata.GLPFAFD b ON a.g1rpt# = b.g2rpt#
LEFT JOIN (
  SELECT g3rpt#, g3acct, 
  cast(LEFT(g3lkey, 3) AS integer) AS level1,
  cast(substring(g3lkey,4,3) AS integer) AS level2,
  cast(substring(g3lkey,7,3) AS integer) AS level3,
  cast(substring(g3lkey,10,3) AS integer) AS level4,
  cast(substring(g3lkey,13,3) AS integer) AS level5,
  cast(substring(g3lkey,16,3) AS integer) AS level6,
  cast(substring(g3lkey,19,3) AS integer) AS level7,
  cast(substring(g3lkey,22,3) AS integer) AS level8,
  cast(substring(g3lkey,25,3) AS integer) AS level9,
  cast(substring(g3lkey,28,3) AS integer) AS level10,
  cast(substring(g3lkey,31,3) AS integer) AS level11,
  cast(substring(g3lkey,34,3) AS integer) AS level12
  FROM rydedata.GLPFAAA) c ON a.g1rpt# = c.g3rpt#
    AND c.level1 = b.g2lky1  
    AND c.level2 = b.g2lky2 
    AND c.level3 = b.g2lky3 
    AND c.level4 = b.g2lky4
    AND c.level5 = b.g2lky5 
    AND c.level6 = b.g2lky6 
    AND c.level7 = b.g2lky7 
    AND c.level8 = b.g2lky8 
    AND c.level9 = b.g2lky9 
    AND c.level10 = b.g2lky10 
    AND c.level11 = b.g2lky11
    AND c.level12 = b.g2lky12
--LEFT JOIN dimAccount d ON g3acct = d.glAccount
--where trim(g3acct) = '12324'
WHERE g1rpt# = 65 AND g2lky1 = 10 AND g2lky2 = 20 AND g2lky3 = 10



-- RY1 PDQ
report_number: 66

select * from rydedata.glpfafd where report_number = 66

select * from rydedata.glpfaaa where report_number = 66

SELECT a.g1rpt#, a.g1titl, b.g2desc, c.g3acct, 

  g2lky1, g2lky2, g2lky3, g2lky4, g2lky5,
  g2lky6, g2lky7, g2lky8, g2lky9, g2lky10, g2lky11, g2lky12
FROM rydedata.glpfard a 
LEFT JOIN rydedata.GLPFAFD b ON a.g1rpt# = b.g2rpt#
LEFT JOIN (
  SELECT g3rpt#, g3acct, 
  cast(LEFT(g3lkey, 3) AS integer) AS level1,
  cast(substring(g3lkey,4,3) AS integer) AS level2,
  cast(substring(g3lkey,7,3) AS integer) AS level3,
  cast(substring(g3lkey,10,3) AS integer) AS level4,
  cast(substring(g3lkey,13,3) AS integer) AS level5,
  cast(substring(g3lkey,16,3) AS integer) AS level6,
  cast(substring(g3lkey,19,3) AS integer) AS level7,
  cast(substring(g3lkey,22,3) AS integer) AS level8,
  cast(substring(g3lkey,25,3) AS integer) AS level9,
  cast(substring(g3lkey,28,3) AS integer) AS level10,
  cast(substring(g3lkey,31,3) AS integer) AS level11,
  cast(substring(g3lkey,34,3) AS integer) AS level12
  FROM rydedata.GLPFAAA) c ON a.g1rpt# = c.g3rpt#
    AND c.level1 = b.g2lky1  
    AND c.level2 = b.g2lky2 
    AND c.level3 = b.g2lky3 
    AND c.level4 = b.g2lky4
    AND c.level5 = b.g2lky5 
    AND c.level6 = b.g2lky6 
    AND c.level7 = b.g2lky7 
    AND c.level8 = b.g2lky8 
    AND c.level9 = b.g2lky9 
    AND c.level10 = b.g2lky10 
    AND c.level11 = b.g2lky11
    AND c.level12 = b.g2lky12
--LEFT JOIN dimAccount d ON g3acct = d.glAccount
--where trim(g3acct) = '12324'
WHERE g1rpt# = 77 AND g2lky1 = 10 AND g2lky2 = 20 AND g2lky3 = 10


select * from rydedata.glpfaaa where trim(g3acct) = '147701'


select * from rydedata.glpfard

select * from rydedata.glpfarv

select * from rydedata.GLPFAFD where report_number = 77

select * from rydedata.GLPFAaa where report_number = 77


