Select a.bopmast_stock_number, a.bopmast_vin, b.year, b.make, b.model, a.retail_price, 
  a.term, a.payment, a.down_payment, date_capped,
  a.record_Status, a.record_type, a.sale_type, a.lease_price, a.lease_payment
-- select a.*  
from RYDEDATA.BOPMAST a
left join rydedata.inpmast b on a.bopmast_vin = b.inpmast_vin
where trim(bopmast_stock_number) = 'G34692'
where date_capped > '2016-12-31'
  and record_status = 'U'
  and term > 1
  and record_type = 'L'


Lease: record_type: L sale_type: L
Retail Finance record_type: F  sale_type: R


select distinct sale_type from rydedata.bopmast


-- lease deals
-- for leases, downpayment is cash_received (which = cash CR + Upfront (in UI) in data cash_upfront + cash_capitalized)
-- lease term: lease_term
Select trim(a.bopmast_stock_number) as stock_number, b.year, b.make, b.model, 
  a.lease_price, a.lease_term, a.lease_payment, a.cash_received, date_capped
-- select a.*  
from RYDEDATA.BOPMAST a
left join rydedata.inpmast b on a.bopmast_vin = b.inpmast_vin
--where trim(bopmast_stock_number) = '32010'
where date_capped > '2016-12-31'
  and record_status = 'U'
  and record_type = 'L'
  and sale_type = 'L'
order by lease_payment desc
 
-- finance deals   
Select trim(a.bopmast_stock_number) as stock_number, b.year, b.make, b.model, a.retail_price, 
  a.term, a.payment, a.down_payment, date_capped
-- select a.*  
from RYDEDATA.BOPMAST a
left join rydedata.inpmast b on a.bopmast_vin = b.inpmast_vin
--where trim(bopmast_stock_number) = '30609'
where date_capped > '2016-12-31'
  and record_status = 'U'
--  and term > 1
  and record_type = 'F'
  and sale_type = 'R' 
order by payment desc   
  