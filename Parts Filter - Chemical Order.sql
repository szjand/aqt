-- Filter Order
-- much improved yearly sales 11/24/09
select pmpart as "Part #", pmdesc as Description,
--  pmstat as Status,
   pmonhd as "On Hand", pmordr as "Qty on Order",
  pmbcko as "Qty on Back Order",
	(SELECT COALESCE(SUM(PTQTY), 0) 
	FROM rydedata.PDPTDET 
	WHERE (PTPART = PM.PMPART OR PTPART IN ((SELECT PFOPRT FROM rydedata.PDPMRPL WHERE PFAPRT = PM.PMPART))) 
	  AND PTCODE IN (
	    'CP', /*RO CUSTOMER PAY SALE*/
	    'IS', /*RO INTERNAL SALE*/ 
	    'SA', /*COUNTER SALE*/
	    'WS', /*RO WARRANTY SALE*/
	    'SC', /*RO SERVICE CONTRACT SALE*/ 
	    'SR', /*RO RETURN*/ 
	    'CR', /*RO CORRECTION*/ 
	    'RT', /*COUNTER RETURN*/ 
	    'FR') /*FACTORY RETURN*/ 
	  AND PTCO# = PM.PMCO#
	  AND PTDATE > (YEAR(CURDATE()) - 1) * 10000 + MONTH(CURDATE()) * 100 + DAY(CURDATE())) AS "YEARLY SALES", 
  pmbinl as Bin, pmoprt as "Old Part #"
from rydedata.pdpmast pm
where trim(pmgrpc) in ('01835', '01836', '03410')
 and pmco# = 'RY1'
 and pmstat = 'A'

-- Chemicals
-- much improved yearly sales 11/24/09
 
select pmpart as "Part #", pmpack, pmdesc as Description,
--  pmstat as Status, 
  pmonhd as "On Hand", pmordr as "Qty on Order",
  pmbcko as "Qty on Back Order",
	(SELECT COALESCE(SUM(PTQTY), 0) 
	FROM PDPTDET 
	WHERE (PTPART = PM.PMPART OR PTPART IN ((SELECT PFOPRT FROM PDPMRPL WHERE PFAPRT = PM.PMPART))) 
	  AND PTCODE IN (
	    'CP', /*RO CUSTOMER PAY SALE*/
	    'IS', /*RO INTERNAL SALE*/ 
	    'SA', /*COUNTER SALE*/
	    'WS', /*RO WARRANTY SALE*/
	    'SC', /*RO SERVICE CONTRACT SALE*/ 
	    'SR', /*RO RETURN*/ 
	    'CR', /*RO CORRECTION*/ 
	    'RT', /*COUNTER RETURN*/ 
	    'FR') /*FACTORY RETURN*/ 
	  AND PTCO# = PM.PMCO#
	  AND PTDATE > (YEAR(CURDATE()) - 1) * 10000 + MONTH(CURDATE()) * 100 + DAY(CURDATE())) AS "YEARLY SALES", 
  pmbinl as Bin, pmoprt as "Old Part #"
from pdpmast pm
where trim(pmgrpc) = '08800' 
 and pmco# = 'RY1'
 and pmstat = 'A'

-- Grills and Fascias
-- this is ok but WAY TOO SLOW
select  pmgrpc as Group, pmpart as "Part #",pmdesc as Description,
  pmonhd as "On Hand", pmordr as "Qty on Order",
  pmbcko as "Qty on Back Order",  
	(SELECT COALESCE(SUM(PTQTY), 0) 
	FROM PDPTDET 
	WHERE (PTPART = PM.PMPART OR PTPART IN ((SELECT PFOPRT FROM PDPMRPL WHERE PFAPRT = PM.PMPART))) 
	  AND PTCODE IN (
	    'CP', /*RO CUSTOMER PAY SALE*/
	    'IS', /*RO INTERNAL SALE*/ 
	    'SA', /*COUNTER SALE*/
	    'WS', /*RO WARRANTY SALE*/
	    'SC', /*RO SERVICE CONTRACT SALE*/ 
	    'SR', /*RO RETURN*/ 
	    'CR', /*RO CORRECTION*/ 
	    'RT', /*COUNTER RETURN*/ 
	    'FR') /*FACTORY RETURN*/ 
	  AND PTCO# = PM.PMCO#
	  AND PTDATE > (YEAR(CURDATE()) - 1) * 10000 + MONTH(CURDATE()) * 100 + DAY(CURDATE())) AS "YEARLY SALES", 
  pmbinl as Bin, pmoprt as "Old Part #"
from pdpmast pm
where trim(pmgrpc) in ('01266', '07831') 
and pmco# = 'RY1'
and pmstat = 'A'
