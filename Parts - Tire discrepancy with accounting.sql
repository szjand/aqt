SELECT distinct b.thedate, e.fullname AS customer, a.ro-- , c.opcode AS opcode, d.opcode AS corcode
--SELECT COUNT(*)
FROM factRepairOrder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dimopcode d on a.corcodekey = d.opcodekey
INNER JOIN dimCustomer e on a.customerkey = e.customerkey
WHERE a.storecode = 'RY1'
  AND b.theyear = 2013
  AND (
    c.opcode IN ('1NT','2NT','3NT','4NT')
    OR
    d.opcode IN ('1NT','2NT','3NT','4NT')) 

select *
from (
  select distinct a.ptco#, a.ptro#, a.ptline
  from rydedata.sdprdet a
  where ptdate > 20130000
    AND (
      trim(a.ptlopc) IN  ('1NT','2NT','3NT','4NT')
      or
      trim(a.ptcrlo) IN  ('1NT','2NT','3NT','4NT'))) c


select distinct a.ptco#, a.ptro#, a.ptline
from rydedata.sdprdet a
where ptdate > 20130000
  and ptco# = 'RY1'
  AND (
    trim(a.ptlopc) IN  ('1NT','2NT','3NT','4NT')
    or
    trim(a.ptcrlo) IN  ('1NT','2NT','3NT','4NT'))
  and not exists (
    select 1 
    from rydedata.pdptdet
    where trim(ptinv#) = trim(a.ptro#))

      and ptline = a.ptline)


select distinct a.ptco#, a.ptro#
from rydedata.sdprdet a
where ptdate > 20130000
  and ptco# = 'RY1'
  AND (
    trim(a.ptlopc) IN  ('1NT','2NT','3NT','4NT')
    or
    trim(a.ptcrlo) IN  ('1NT','2NT','3NT','4NT'))
  and not exists (
    select 1 
    from rydedata.glptrns
    where trim(gtdoc#) = trim(a.ptro#)
      and trim(gtacct) = '124300')


select '''' || trim(a.ptro#) || '''' ||','
from rydedata.sdprdet a
where ptdate > 20130000
  and ptco# = 'RY1'
  AND (
    trim(a.ptlopc) IN  ('1NT','2NT','3NT','4NT')
    or
    trim(a.ptcrlo) IN  ('1NT','2NT','3NT','4NT'))
  and not exists (
    select 1 
    from rydedata.pdptdet
    where trim(ptinv#) = trim(a.ptro#))


select * from rydedata.GLPTRNS where trim(gtdoc#) = '16122895'

select * from rydedata.glptrns WHERE gttrn# between 2256800  AND 2256900 

SELECT gtdate, COUNT(*)
FROM rydedata.GLPTRNS
WHERE year(gtdate) = 2013
AND month(gtdate) = 6
GROUP BY gtdate
