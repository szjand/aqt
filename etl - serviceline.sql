-- there are no detail records without a matching (RO) header row
select *
from rydedata.sdprdet d
where not exists ( 
  select 1
  from rydedata.sdprhdr
  where ptco# = d.ptco#
    and trim(ptro#) = trim(d.ptro#))
  and d.ptro# in ('RY1', 'RY2','RY3')


select max(ptcdat) from rydedata.sdprhdr

select *
from rydedata.sdprhdr
where ptcdat = 0


select *
from rydedata.sdprdet d
where trim(ptro#) like '16078%'

select*
from rydedata.sdprtxt
where trim(sxro#) = '16078005'


/** 3/13/12  ** look at all 6 tables for ro's in each status **********************************************/
-- greg: where and when does warranty info show up


-- 18014537: status = open
select * -- 0
from rydedata.glptrns
where trim(gtdoc#) = '18014537'

select * --0
from rydedata.sdprhdr
where trim(ptro#) = '18014537'

select * --0
from rydedata.sdprdet
where trim(ptro#) = '18014537'

select * -- 1
from rydedata.pdpphdr
where trim(ptdoc#) = '18014537'

select * -- 1
from rydedata.pdppdet
where ptpkey = 426232

select * -- 0 records
from rydedata.pdpthdr
where trim(ptinv#) = '18014537'

select * -- 0 records
from rydedata.pdptdet
where trim(ptinv#) = '18014537'



-- 16083382: status = Cashr/D
select * -- 0
from rydedata.glptrns
where trim(gtdoc#) = '16083382'

select * --0
from rydedata.sdprhdr
where trim(ptro#) = '16083382'

select * --0
from rydedata.sdprdet
where trim(ptro#) = '16083382'

select * -- 1
from rydedata.pdpphdr
where trim(ptdoc#) = '16083382'

select * -- 58
from rydedata.pdppdet
where ptpkey = 443313

select * -- 0 records
from rydedata.pdpthdr
where trim(ptinv#) = '16083382'

select * -- 0 records
from rydedata.pdptdet
where trim(ptinv#) = '16083382'

-- 16083506: status = Parts-A
select * -- 0
from rydedata.glptrns
where trim(gtdoc#) = '16083506'

select * --0
from rydedata.sdprhdr
where trim(ptro#) = '16083506'

select * --0
from rydedata.sdprdet
where trim(ptro#) = '16083506'

select * -- 1
from rydedata.pdpphdr
where trim(ptdoc#) = '16083506'

select * -- 23
from rydedata.pdppdet
where ptpkey = 443859

select * -- 0 records
from rydedata.pdpthdr
where trim(ptinv#) = '16083506'

select * -- 0 records
from rydedata.pdptdet
where trim(ptinv#) = '16083506'

-- 16083787: status = Cashier (green)
select * -- 0
from rydedata.glptrns
where trim(gtdoc#) = '16083787'

select * --0
from rydedata.sdprhdr
where trim(ptro#) = '16083787'

select * --0
from rydedata.sdprdet
where trim(ptro#) = '16083787'

select * -- 1
from rydedata.pdpphdr
where trim(ptdoc#) = '16083787'

select * -- 20
from rydedata.pdppdet
where ptpkey = 445648

select * -- 0 records
from rydedata.pdpthdr
where trim(ptinv#) = '16083787'

select * -- 2 records
from rydedata.pdptdet
where trim(ptinv#) = '16083787'

-- 16083555: status = Cashier (white)
select * -- 21
from rydedata.glptrns
where trim(gtdoc#) = '16083555'

select * --1
from rydedata.sdprhdr
where trim(ptro#) = '16083555'

select * --41
from rydedata.sdprdet
where trim(ptro#) = '16083555'

select * -- 1
from rydedata.pdpphdr
where trim(ptdoc#) = '16083555'

select * -- 44
from rydedata.pdppdet
where ptpkey = 444106

select * -- 0 records
from rydedata.pdpthdr
where trim(ptinv#) = '16083555'

select * -- 13 records
from rydedata.pdptdet
where trim(ptinv#) = '16083555'

-- 18014466: status = Appr-PD (green)
select * -- 0
from rydedata.glptrns
where trim(gtdoc#) = '18014466'

select * -- 0
from rydedata.sdprhdr
where trim(ptro#) = '18014466'

select * -- 0
from rydedata.sdprdet
where trim(ptro#) = '18014466'

select * -- 1
from rydedata.pdpphdr
where trim(ptdoc#) = '18014466'

select * -- 4
from rydedata.pdppdet
where ptpkey = 424440

select * -- 0 records
from rydedata.pdpthdr
where trim(ptinv#) = '18014466'

select * -- 3 records
from rydedata.pdptdet
where trim(ptinv#) = '18014466'

-- 18014575: status = Appr-PD (white)
select * -- 15
from rydedata.glptrns
where trim(gtdoc#) = '18014575'

select * -- 1
from rydedata.sdprhdr
where trim(ptro#) = '18014575'

select * -- 5
from rydedata.sdprdet
where trim(ptro#) = '18014575'

select * -- 1
from rydedata.pdpphdr
where trim(ptdoc#) = '18014575'

select * -- 7
from rydedata.pdppdet
where ptpkey = 426783

select * -- 0 records
from rydedata.pdpthdr
where trim(ptinv#) = '18014575'

select * -- 3 records
from rydedata.pdptdet
where trim(ptinv#) = '18014575'

-- 16078029: status = In-Proc (green)
select * -- 0
from rydedata.glptrns
where trim(gtdoc#) = '16078029'

select * -- 0
from rydedata.sdprhdr
where trim(ptro#) = '16078029'

select * -- 0
from rydedata.sdprdet
where trim(ptro#) = '16078029'

select * -- 1
from rydedata.pdpphdr
where trim(ptdoc#) = '16078029'

select * -- 34
from rydedata.pdppdet
where ptpkey = 412697

select * -- 0 records
from rydedata.pdpthdr
where trim(ptinv#) = '16078029'

select * -- 0 records
from rydedata.pdptdet
where trim(ptinv#) = '16078029'

-- 16079837: status = In-Proc (white)
select * -- 1 GTPOST = V
from rydedata.glptrns
where trim(gtdoc#) = '16079837'

select * -- 1
from rydedata.sdprhdr
where trim(ptro#) = '16079837'

select * -- 18
from rydedata.sdprdet
where trim(ptro#) = '16079837'

select * -- 1
from rydedata.pdpphdr
where trim(ptdoc#) = '16079837'

select * -- 60
from rydedata.pdppdet
where ptpkey = 423141

select * -- 0 records
from rydedata.pdpthdr
where trim(ptinv#) = '16079837'

select * -- 1 records
from rydedata.pdptdet
where trim(ptinv#) = '16079837'

select 
  (select count(*) from rydedata.glptrns where trim(gtdoc#) = '16085113') as glptrns,
  (select count(*) from rydedata.sdprhdr where trim(ptro#) = '16085113') as sdprhdr,
  (select count(*) from rydedata.sdprdet where trim(ptro#) = '16085113') as sdprdet,
  (select count(*) from rydedata.pdpphdr where trim(ptdoc#) = '16085113') as pdpphdr,
  (select count(*) from rydedata.pdppdet where trim(ptpkey) = 451851) as pdppdet
from SYSIBM.SYSDUMMY1



/************ 3/21/12 ****************/
/***************** PDPPDET ****************************/
PTCODE - Transaction Code
AP = Add Part
BO = Back Ordered
CM = Comment
CN = Cancelled
CP = RO Customer Pay Sale
CQ = Converted Quantity
CR = RO Correction
CS = RO Cause
DC = Discounts
DP = Delete Part
FR = Factory Return
GC = Stock Group Change
IA = Manual Inventory Adjustment
IS = RO Internal Sale
LA = Lifo Adjust
LS = Lost Sale
MP = Merged Part
OF = Fees
OR = Ordered
PA = Physical Inventory Adjust
PC = Part count from last physical inventory
PO = Purchase order
RC = Special Order Receipt
RC = Received
RO = Reorder
RS = Restocking charge
RT = Counter Return
SA = Counter Sale
SC = RO Service Contract Sale
SH = Shipping
SL = Sublet
SR = RO Return
TT = RO Tech Time
WS = RO Warranty sale
ZA = assign core part
ZR = remove core part

-- ptlsts: line status
select ptlsts, count(*)
from rydedata.pdppdet
group by ptlsts

select ptcode, count(*)
from rydedata.pdppdet
where ptlsts = 'C'
group by ptcode

select ptcode, count(*)
from rydedata.pdppdet
where ptlsts = 'I'
group by ptcode

-- one question, what are TT lines with no ptcode?
select ptcode, count(*)
from rydedata.pdppdet
where ptlsts = ''
group by ptcode

-- one question, what are TT lines with no ptcode?
select *
from (
select (select ptdoc# from rydedata.pdpphdr where ptpkey = p.ptpkey) as RO, p.*
from rydedata.pdppdet p
where ptcode = 'TT'
  and ptlsts = '') x
where trim(ro) like '16%'
order by RO


select *
from rydedata.pdppdet
where ptpkey = (
  select ptpkey
  from rydedata.pdpphdr
  where trim(ptdoc#) = '16061936')

-- 4/28
-- PDPPHDR with no #doc
-- oh duh, no doc, it's not a fucking ro
select *
from rydedata.PDPPHDR

select count(*) -- 1211
from rydedata.PDPPHDR

select count(*) -- 476
from rydedata.PDPPHDR
where trim(ptdtyp) = 'RO'

select * -- 18
from rydedata.PDPPHDR
where ptdtyp = 'RO'
  and trim(ptdoc#) = ''

select distinct ptsvco
from rydedata.sdprhdr d
where trim(ptro#) like '16078%'
and ptsvco <> 0

select *
from rydedata.sdprdet
where trim(ptro#) like '16078%'

--4/29
-- pricing
select *
from rydedata.sdpprice
where spco# = 'RY1'
order by spsvctyp, splpym, spfran


select h.ptco#, h.ptro#, c.* 
from rydedata.sdprhdr h
inner join rydedata.sdpsvch c on h.ptco# = c.stco#
  and trim(h.ptro#) = trim(c.stro#)
where trim(h.ptro#) like '16078%'

select bmsrvc, count(*)
from rydedata.bopmast
group by bmsrvc



