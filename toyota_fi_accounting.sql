select distinct 
--					  case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code, 
                      case
                        when b.g_l_company_ = 'RY1' and b.consolidation_grp = '2' then 'RY2'
                        when b.g_l_company_ = 'RY1' and b.consolidation_grp = '' then 'RY1'
                        when b.g_l_company_ = 'RY8' and b.consolidation_grp = '' then 'RY8'
                      end as store_code,
                      a.fxmpge as the_page, INTEGER(a.fxmlne) as line, trim(b.g_l_acct_number) as gl_account
                    from eisglobal.sypffxmst a
                    inner join rydedata.ffpxrefdta b on a.fxmact = b.factory_account
                      and a.fxmcyy = b.factory_financial_year
                    where a.fxmcyy = 2022
                      and trim(a.fxmcde) in ('TOY', 'GM')
                      and b. consolidation_grp <> '3'
                      and b.factory_code in ('TOY', 'GM')
                      and trim(b.factory_account) <> '331A'
                      and b.company_number in ( 'RY1','RY8')
                      and b.g_l_acct_number <> ''
                      and (
                        (a.fxmpge between 5 and 15) or
                        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
                        -- exclude fin comp 
                        (a.fxmpge = 17 and a.fxmlne in (1,2,3,6,7,11,12,13,16,17)))

select distinct company_number, consolidation_grp, g_l_company_ from rydedata.ffpxrefdta


select distinct a.fxmcde, b.company_number, b.consolidation_grp, b.g_l_company_
from eisglobal.sypffxmst a
inner join rydedata.ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where trim(a.fxmcde) in ('GM','TOY','HON','NIS')
order by a.fxmcde, b.company_number




-- getting a bunch of weird page numbers for toyota
-- page numbers for GM as well as TOY

select 
  case
    when b.g_l_company_ = 'RY1' and b.consolidation_grp = '2' then 'RY2'
    when b.g_l_company_ = 'RY1' and b.consolidation_grp = '' then 'RY1'
    when b.g_l_company_ = 'RY8' and b.consolidation_grp = '' then 'RY8'
  end as store_code,
  a.*--, b.* -- a.fxmpge as the_page, INTEGER(a.fxmlne) as line, trim(b.g_l_acct_number) as gl_account
from eisglobal.sypffxmst a
inner join rydedata.ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy = 2022
  and trim(a.fxmcde) in ('TOY', 'GM')
  and b. consolidation_grp <> '3'
  and b.factory_code in ('TOY', 'GM')
  and trim(b.factory_account) <> '331A'
  and b.company_number in ( 'RY1','RY8')
  and b.g_l_acct_number <> ''
  and (
    (a.fxmpge between 5 and 15) or
    (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
    -- exclude fin comp 
    (a.fxmpge = 17 and a.fxmlne in (1,2,3,6,7,11,12,13,16,17)))
and trim(b.g_l_acct_number) = '6271C'
-- which resulted in this output: ffxmcde for RY8 with GM & TOY
RY8	GM	 2022	850	17	3.0	3	51
RY8	TOY	2022	6270	7	1.0	4	25


select 
  case
    when b.g_l_company_ = 'RY1' and b.consolidation_grp = '2' then 'RY2'
    when b.g_l_company_ = 'RY1' and b.consolidation_grp = '' then 'RY1'
    when b.g_l_company_ = 'RY8' and b.consolidation_grp = '' then 'RY8'
  end as store_code,
  a.*--, b.* -- a.fxmpge as the_page, INTEGER(a.fxmlne) as line, trim(b.g_l_acct_number) as gl_account
from eisglobal.sypffxmst a
inner join rydedata.ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy = 2022
  and trim(a.fxmcde) in ('TOY', 'GM')
  and b. consolidation_grp <> '3'
  and b.factory_code in ('TOY', 'GM')
  and trim(b.factory_account) <> '331A'
  and b.company_number in ( 'RY1','RY8')
  and b.g_l_acct_number <> ''
  and (
    (a.fxmpge between 5 and 15) or
    (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
    -- exclude fin comp 
    (a.fxmpge = 17 and a.fxmlne in (1,2,3,6,7,11,12,13,16,17)))
and trim(b.g_l_acct_number) = '6271C' 

select store_code, fxmpge as the_page, INTEGER(fxmlne) as line, trim(g_l_acct_number) as gl_account
from (
select 
  case
    when b.g_l_company_ = 'RY1' and b.consolidation_grp = '2' then 'RY2'
    when b.g_l_company_ = 'RY1' and b.consolidation_grp = '' then 'RY1'
    when b.g_l_company_ = 'RY8' and b.consolidation_grp = '' then 'RY8'
  end as store_code,
  a.*, b.* -- a.fxmpge as the_page, INTEGER(a.fxmlne) as line, trim(b.g_l_acct_number) as gl_account
from eisglobal.sypffxmst a
inner join rydedata.ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy = 2022
  and trim(a.fxmcde) in ('TOY', 'GM')
  and b. consolidation_grp <> '3'
  and b.factory_code in ('TOY', 'GM')
  and trim(b.factory_account) <> '331A'
  and b.company_number in ( 'RY1','RY8')
  and b.g_l_acct_number <> ''
  and (
    (a.fxmpge between 5 and 15) or
    (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
    -- exclude fin comp 
    (a.fxmpge = 17 and a.fxmlne in (1,2,3,6,7,11,12,13,16,17)))
and ( -- this was necessary to get toyota
  (b.g_l_company_ = 'RY1' and b.consolidation_grp = '2')
  or
  (b.g_l_company_ = 'RY1' and b.consolidation_grp = '')
  or
  (b.g_l_company_ = 'RY8' and b.consolidation_grp =  '' and trim(a.fxmcde) = 'TOY'))) c
	

-- whats up with lines 8 & 9 in toyota
-- don't know
select store_code, fxmpge as the_page, INTEGER(fxmlne) as line, trim(g_l_acct_number) as gl_account
from (
select 
  case
    when b.g_l_company_ = 'RY1' and b.consolidation_grp = '2' then 'RY2'
    when b.g_l_company_ = 'RY1' and b.consolidation_grp = '' then 'RY1'
    when b.g_l_company_ = 'RY8' and b.consolidation_grp = '' then 'RY8'
  end as store_code,
  a.*, b.* -- a.fxmpge as the_page, INTEGER(a.fxmlne) as line, trim(b.g_l_acct_number) as gl_account
from eisglobal.sypffxmst a
inner join rydedata.ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy = 2022
  and trim(a.fxmcde) in ('TOY', 'GM')
  and b. consolidation_grp <> '3'
  and b.factory_code in ('TOY', 'GM')
  and trim(b.factory_account) <> '331A'
  and b.company_number in ( 'RY1','RY8')
  and b.g_l_acct_number <> ''
  and (
    (a.fxmpge between 5 and 15) or
    (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
    -- exclude fin comp 
    (a.fxmpge = 17 and a.fxmlne in (1,2,3,6,7,11,12,13,16,17)))
and ( -- this was necessary to get toyota
  (b.g_l_company_ = 'RY1' and b.consolidation_grp = '2')
  or
  (b.g_l_company_ = 'RY1' and b.consolidation_grp = '')
  or
  (b.g_l_company_ = 'RY8' and b.consolidation_grp =  '' and trim(a.fxmcde) = 'TOY'))
and trim(g_l_acct_number) = '4430' ) c


