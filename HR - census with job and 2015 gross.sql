
  
select a.pymast_company_number as Store, a.employee_last_name as "Last Name", 
  a.employee_first_name as "First Name",
  case a.active_code
    when 'P' then 'Part Time'
    when 'A' then 'Full Time'
  end as "Full/Part",  
  j.yrtext as "Job Title", 
  case a.payroll_class
    when 'C' then 'Commission'
    when 'S' then 'Salary'
    when 'H' then 'Hourly'
  end as "Payroll Class",
  a.base_hrly_rate as "Hourly Rate",
  coalesce(c.gross, 0)  as "2015 total gross"
from rydedata.pymast a
left join rydedata.pyprhead ph on ph.yrempn = a.ymempn and a.pymast_company_number = ph.yrco#
left join (
  select distinct yrco#, yrjobd, yrtext
  from rydedata.pyprjobd) j on j.yrjobd = ph.yrjobd 
    and ph.yrco# = j.yrco#
    and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') -- job descriptions
left join (
  select trim(yhdemp) as yhdemp,
    sum(case when year(checkdate) = 2015 then totalgross end) as gross
  from (-- here is basic total gross for each employee with real dates
    select a.ybco#, a.ypbcyy, a.payroll_run_number, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp,
    cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
    from rydedata.pyptbdta a
    inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.payroll_run_number = b.ypbnum) a
  where a.checkdate between '01/01/2015' and '12/31/2015'
  group by yhdemp) c on trim(a.pymast_employee_number) = c.yhdemp    
where a.ymactv <> 'T'
and trim(a.ymname) <> 'TEST'
and a.ymco# in ('RY1','RY2')


--same thing but with dob, gender, hiredate
select a.pymast_company_number as "Store", a.employee_last_name as "Last Name", 
  a.employee_first_name as "First Name",
  case 
    when cast(right(trim(a.birth_date),2) as integer) < 20 then 
      cast (
        case length(trim(a.birth_date))
          when 5 then  '20'||substr(trim(a.birth_date),4,2)||'-'|| '0' || left(trim(a.birth_date),1) || '-' ||substr(trim(a.birth_date),2,2)
          when 6 then  '20'||substr(trim(a.birth_date),5,2)||'-'|| left(trim(a.birth_date),2) || '-' ||substr(trim(a.birth_date),3,2)
        end as date) 
    else  
      cast (
        case length(trim(a.birth_date))
          when 5 then  '19'||substr(trim(a.birth_date),4,2)||'-'|| '0' || left(trim(a.birth_date),1) || '-' ||substr(trim(a.birth_date),2,2)
          when 6 then  '19'||substr(trim(a.birth_date),5,2)||'-'|| left(trim(a.birth_date),2) || '-' ||substr(trim(a.birth_date),3,2)
        end as date) 
  end as "Birth Date",
  sex as "Gender",
  case 
    when cast(right(trim(a.hire_date),2) as integer) < 20 then 
      cast (
        case length(trim(a.hire_date))
          when 5 then  '20'||substr(trim(a.hire_date),4,2)||'-'|| '0' || left(trim(a.hire_date),1) || '-' ||substr(trim(a.hire_date),2,2)
          when 6 then  '20'||substr(trim(a.hire_date),5,2)||'-'|| left(trim(a.hire_date),2) || '-' ||substr(trim(a.hire_date),3,2)
        end as date) 
    else  
      cast (
        case length(trim(a.hire_date))
          when 5 then  '19'||substr(trim(a.hire_date),4,2)||'-'|| '0' || left(trim(a.hire_date),1) || '-' ||substr(trim(a.hire_date),2,2)
          when 6 then  '19'||substr(trim(a.hire_date),5,2)||'-'|| left(trim(a.hire_date),2) || '-' ||substr(trim(a.hire_date),3,2)
        end as date) 
  end as "Hire Date",
  case a.payroll_class
    when 'C' then 'Commission'
    when 'S' then 'Salary'
    when 'H' then 'Hourly'
  end as "Payroll Class",
  a.base_hrly_rate as "Hourly Rate",
  coalesce(c.gross, 0)  as "2015 total gross"
from rydedata.pymast a
left join rydedata.pyprhead ph on ph.yrempn = a.ymempn and a.pymast_company_number = ph.yrco#
left join (
  select distinct yrco#, yrjobd, yrtext
  from rydedata.pyprjobd) j on j.yrjobd = ph.yrjobd 
    and ph.yrco# = j.yrco#
    and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') -- job descriptions
left join (
  select trim(yhdemp) as yhdemp,
    sum(case when year(checkdate) = 2015 then totalgross end) as gross
  from (-- here is basic total gross for each employee with real dates
    select a.ybco#, a.ypbcyy, a.payroll_run_number, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp,
    cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
    from rydedata.pyptbdta a
    inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.payroll_run_number = b.ypbnum) a
  where a.checkdate between '01/01/2015' and '12/31/2015'
  group by yhdemp) c on trim(a.pymast_employee_number) = c.yhdemp    
where a.ymactv <> 'T'
and trim(a.ymname) <> 'TEST'
and a.ymco# in ('RY1','RY2')



