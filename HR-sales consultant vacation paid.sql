Select * 
from RYDEDATA.PYHSCDTA a
where trim(yhcemp) = '128530' 
  and ypbcyy = 114
  and code_id = '74'

-- generate insert statements: into pto_tmp_sc_pto_paid  
Select a.company_number, employee_number,
  b.employee_name,  amount, 
  date(trim(char(yhdcmm))||'/'||trim(char(yhdcdd))||'/'||'20'||trim(char(yhdcyy))) as CheckDate
from RYDEDATA.PYHSCDTA a
left join rydedata.pymast b on trim(a.employee_number) = trim(b.ymempn)
left join rydedata.pyhshdta c on a.payroll_run_number = c.payroll_run_number
  and trim(a.employee_number) = trim(c.employee_)
where a.ypbcyy > 113 
  and a.code_id = '74'
order by company_number, employee_name