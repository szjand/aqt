select pymast_employee_number,
  case when pymast_company_number = 'RY1' then 'GM' else 'Honda Nissan' end as store_location,
  employee_last_name, employee_first_name, employee_middle_name, 'ssn' as ssn, '' as relationship,
  birth_date, sex, address_1, address_2, city_name, state_code_Address_, zip_code, 
  tel_area_code ||'-'||  telephone_number
-- Select *
from RYDEDATA.pymast
where active_code <> 'T'
  and pymast_company_number in ('RY1','RY2')
  and employee_last_name like '%.%'
  

select *
from rydedata.pymast