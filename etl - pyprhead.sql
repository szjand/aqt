


select yrco#, yrempn, yrmgrn, yrjobd, yrjobl, 
  case
    when p.yrhdte = 0 then date('9999-12-31')
    when cast(right(trim(p.yrhdte),2) as integer) < 20 then
      cast (
        case length(trim(p.yrhdte))
          when 5 then  '20'||substr(trim(p.yrhdte),4,2)||'-'|| '0' || left(trim(p.yrhdte),1) || '-' ||substr(trim(p.yrhdte),2,2)
          when 6 then  '20'||substr(trim(p.yrhdte),5,2)||'-'|| left(trim(p.yrhdte),2) || '-' ||substr(trim(p.yrhdte),3,2)
        end as date)
    when cast(right(trim(p.yrhdte),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrhdte))
          when 5 then  '19'||substr(trim(p.yrhdte),4,2)||'-'|| '0' || left(trim(p.yrhdte),1) || '-' ||substr(trim(p.yrhdte),2,2)
          when 6 then  '19'||substr(trim(p.yrhdte),5,2)||'-'|| left(trim(p.yrhdte),2) || '-' ||substr(trim(p.yrhdte),3,2)
        end as date)
  end as yrhdte,
  case
    when p.yrhdto = 0 then date('9999-12-31')
    when cast(right(trim(p.yrhdto),2) as integer) < 20 then
      cast (
        case length(trim(p.yrhdto))
          when 5 then  '20'||substr(trim(p.yrhdto),4,2)||'-'|| '0' || left(trim(p.yrhdto),1) || '-' ||substr(trim(p.yrhdto),2,2)
          when 6 then  '20'||substr(trim(p.yrhdto),5,2)||'-'|| left(trim(p.yrhdto),2) || '-' ||substr(trim(p.yrhdto),3,2)
        end as date)
    when cast(right(trim(p.yrhdto),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrhdto))
          when 5 then  '19'||substr(trim(p.yrhdto),4,2)||'-'|| '0' || left(trim(p.yrhdto),1) || '-' ||substr(trim(p.yrhdto),2,2)
          when 6 then  '19'||substr(trim(p.yrhdto),5,2)||'-'|| left(trim(p.yrhdto),2) || '-' ||substr(trim(p.yrhdto),3,2)
        end as date)
  end as yrhdto,
  case
    when p.yrtdte = 0 then date('9999-12-31')
    when cast(right(trim(p.yrtdte),2) as integer) < 20 then
      cast (
        case length(trim(p.yrtdte))
          when 5 then  '20'||substr(trim(p.yrtdte),4,2)||'-'|| '0' || left(trim(p.yrtdte),1) || '-' ||substr(trim(p.yrtdte),2,2)
          when 6 then  '20'||substr(trim(p.yrtdte),5,2)||'-'|| left(trim(p.yrtdte),2) || '-' ||substr(trim(p.yrtdte),3,2)
        end as date)
    when cast(right(trim(p.yrtdte),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrtdte))
          when 5 then  '19'||substr(trim(p.yrtdte),4,2)||'-'|| '0' || left(trim(p.yrtdte),1) || '-' ||substr(trim(p.yrtdte),2,2)
          when 6 then  '19'||substr(trim(p.yrtdte),5,2)||'-'|| left(trim(p.yrtdte),2) || '-' ||substr(trim(p.yrtdte),3,2)
        end as date)
  end as yrtdte,
  case
    when p.yrrdte = 0 then date('9999-12-31')
    when cast(right(trim(p.yrrdte),2) as integer) < 20 then
      cast (
        case length(trim(p.yrrdte))
          when 5 then  '20'||substr(trim(p.yrrdte),4,2)||'-'|| '0' || left(trim(p.yrrdte),1) || '-' ||substr(trim(p.yrrdte),2,2)
          when 6 then  '20'||substr(trim(p.yrrdte),5,2)||'-'|| left(trim(p.yrrdte),2) || '-' ||substr(trim(p.yrrdte),3,2)
        end as date)
    when cast(right(trim(p.yrrdte),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrrdte))
          when 5 then  '19'||substr(trim(p.yrrdte),4,2)||'-'|| '0' || left(trim(p.yrrdte),1) || '-' ||substr(trim(p.yrrdte),2,2)
          when 6 then  '19'||substr(trim(p.yrrdte),5,2)||'-'|| left(trim(p.yrrdte),2) || '-' ||substr(trim(p.yrrdte),3,2)
        end as date)
  end as yrrdte, 
  case
    when p.yrndte = 0 then date('9999-12-31')
    when cast(right(trim(p.yrndte),2) as integer) < 20 then
      cast (
        case length(trim(p.yrndte))
          when 5 then  '20'||substr(trim(p.yrndte),4,2)||'-'|| '0' || left(trim(p.yrndte),1) || '-' ||substr(trim(p.yrndte),2,2)
          when 6 then  '20'||substr(trim(p.yrndte),5,2)||'-'|| left(trim(p.yrndte),2) || '-' ||substr(trim(p.yrndte),3,2)
        end as date)
    when cast(right(trim(p.yrndte),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrndte))
          when 5 then  '19'||substr(trim(p.yrndte),4,2)||'-'|| '0' || left(trim(p.yrndte),1) || '-' ||substr(trim(p.yrndte),2,2)
          when 6 then  '19'||substr(trim(p.yrndte),5,2)||'-'|| left(trim(p.yrndte),2) || '-' ||substr(trim(p.yrndte),3,2)
        end as date)
  end as yrndte, 
  case
    when p.yrcdte = 0 then date('9999-12-31')
    when cast(right(trim(p.yrcdte),2) as integer) < 20 then
      cast (
        case length(trim(p.yrcdte))
          when 5 then  '20'||substr(trim(p.yrcdte),4,2)||'-'|| '0' || left(trim(p.yrcdte),1) || '-' ||substr(trim(p.yrcdte),2,2)
          when 6 then  '20'||substr(trim(p.yrcdte),5,2)||'-'|| left(trim(p.yrcdte),2) || '-' ||substr(trim(p.yrcdte),3,2)
        end as date)
    when cast(right(trim(p.yrcdte),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrcdte))
          when 5 then  '19'||substr(trim(p.yrcdte),4,2)||'-'|| '0' || left(trim(p.yrcdte),1) || '-' ||substr(trim(p.yrcdte),2,2)
          when 6 then  '19'||substr(trim(p.yrcdte),5,2)||'-'|| left(trim(p.yrcdte),2) || '-' ||substr(trim(p.yrcdte),3,2)
        end as date)
  end as yrcdte,    
  yrbp01, yrbp02, yrbp03, yrbp04, yrbp05, yrbp06, yrbp07, yrbp08,
  yrbp09, yrbp10, yrbp11, yrbp12, yrbp13, yrbp14, yrbp15, yrbp16,
  case
    when p.yrdp01 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp01),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp01))
          when 5 then  '20'||substr(trim(p.yrdp01),4,2)||'-'|| '0' || left(trim(p.yrdp01),1) || '-' ||substr(trim(p.yrdp01),2,2)
          when 6 then  '20'||substr(trim(p.yrdp01),5,2)||'-'|| left(trim(p.yrdp01),2) || '-' ||substr(trim(p.yrdp01),3,2)
        end as date)
    when cast(right(trim(p.yrdp01),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp01))
          when 5 then  '19'||substr(trim(p.yrdp01),4,2)||'-'|| '0' || left(trim(p.yrdp01),1) || '-' ||substr(trim(p.yrdp01),2,2)
          when 6 then  '19'||substr(trim(p.yrdp01),5,2)||'-'|| left(trim(p.yrdp01),2) || '-' ||substr(trim(p.yrdp01),3,2)
        end as date)
  end as yrdp01,  
  case
    when p.yrdp02 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp02),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp02))
          when 5 then  '20'||substr(trim(p.yrdp02),4,2)||'-'|| '0' || left(trim(p.yrdp02),1) || '-' ||substr(trim(p.yrdp02),2,2)
          when 6 then  '20'||substr(trim(p.yrdp02),5,2)||'-'|| left(trim(p.yrdp02),2) || '-' ||substr(trim(p.yrdp02),3,2)
        end as date)
    when cast(right(trim(p.yrdp02),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp02))
          when 5 then  '19'||substr(trim(p.yrdp02),4,2)||'-'|| '0' || left(trim(p.yrdp02),1) || '-' ||substr(trim(p.yrdp02),2,2)
          when 6 then  '19'||substr(trim(p.yrdp02),5,2)||'-'|| left(trim(p.yrdp02),2) || '-' ||substr(trim(p.yrdp02),3,2)
        end as date)
  end as yrdp02,  
  case
    when p.yrdp03 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp03),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp03))
          when 5 then  '20'||substr(trim(p.yrdp03),4,2)||'-'|| '0' || left(trim(p.yrdp03),1) || '-' ||substr(trim(p.yrdp03),2,2)
          when 6 then  '20'||substr(trim(p.yrdp03),5,2)||'-'|| left(trim(p.yrdp03),2) || '-' ||substr(trim(p.yrdp03),3,2)
        end as date)
    when cast(right(trim(p.yrdp03),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp03))
          when 5 then  '19'||substr(trim(p.yrdp03),4,2)||'-'|| '0' || left(trim(p.yrdp03),1) || '-' ||substr(trim(p.yrdp03),2,2)
          when 6 then  '19'||substr(trim(p.yrdp03),5,2)||'-'|| left(trim(p.yrdp03),2) || '-' ||substr(trim(p.yrdp03),3,2)
        end as date)
  end as yrdp03,  
  case
    when p.yrdp04 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp04),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp04))
          when 5 then  '20'||substr(trim(p.yrdp04),4,2)||'-'|| '0' || left(trim(p.yrdp04),1) || '-' ||substr(trim(p.yrdp04),2,2)
          when 6 then  '20'||substr(trim(p.yrdp04),5,2)||'-'|| left(trim(p.yrdp04),2) || '-' ||substr(trim(p.yrdp04),3,2)
        end as date)
    when cast(right(trim(p.yrdp04),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp04))
          when 5 then  '19'||substr(trim(p.yrdp04),4,2)||'-'|| '0' || left(trim(p.yrdp04),1) || '-' ||substr(trim(p.yrdp04),2,2)
          when 6 then  '19'||substr(trim(p.yrdp04),5,2)||'-'|| left(trim(p.yrdp04),2) || '-' ||substr(trim(p.yrdp04),3,2)
        end as date)
  end as yrdp04,  
  case
    when p.yrdp05 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp05),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp05))
          when 5 then  '20'||substr(trim(p.yrdp05),4,2)||'-'|| '0' || left(trim(p.yrdp05),1) || '-' ||substr(trim(p.yrdp05),2,2)
          when 6 then  '20'||substr(trim(p.yrdp05),5,2)||'-'|| left(trim(p.yrdp05),2) || '-' ||substr(trim(p.yrdp05),3,2)
        end as date)
    when cast(right(trim(p.yrdp05),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp05))
          when 5 then  '19'||substr(trim(p.yrdp05),4,2)||'-'|| '0' || left(trim(p.yrdp05),1) || '-' ||substr(trim(p.yrdp05),2,2)
          when 6 then  '19'||substr(trim(p.yrdp05),5,2)||'-'|| left(trim(p.yrdp05),2) || '-' ||substr(trim(p.yrdp05),3,2)
        end as date)
  end as yrdp05,  
  case
    when p.yrdp06 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp06),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp06))
          when 5 then  '20'||substr(trim(p.yrdp06),4,2)||'-'|| '0' || left(trim(p.yrdp06),1) || '-' ||substr(trim(p.yrdp06),2,2)
          when 6 then  '20'||substr(trim(p.yrdp06),5,2)||'-'|| left(trim(p.yrdp06),2) || '-' ||substr(trim(p.yrdp06),3,2)
        end as date)
    when cast(right(trim(p.yrdp06),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp06))
          when 5 then  '19'||substr(trim(p.yrdp06),4,2)||'-'|| '0' || left(trim(p.yrdp06),1) || '-' ||substr(trim(p.yrdp06),2,2)
          when 6 then  '19'||substr(trim(p.yrdp06),5,2)||'-'|| left(trim(p.yrdp06),2) || '-' ||substr(trim(p.yrdp06),3,2)
        end as date)
  end as yrdp06,  
  case
    when p.yrdp07 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp07),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp07))
          when 5 then  '20'||substr(trim(p.yrdp07),4,2)||'-'|| '0' || left(trim(p.yrdp07),1) || '-' ||substr(trim(p.yrdp07),2,2)
          when 6 then  '20'||substr(trim(p.yrdp07),5,2)||'-'|| left(trim(p.yrdp07),2) || '-' ||substr(trim(p.yrdp07),3,2)
        end as date)
    when cast(right(trim(p.yrdp07),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp07))
          when 5 then  '19'||substr(trim(p.yrdp07),4,2)||'-'|| '0' || left(trim(p.yrdp07),1) || '-' ||substr(trim(p.yrdp07),2,2)
          when 6 then  '19'||substr(trim(p.yrdp07),5,2)||'-'|| left(trim(p.yrdp07),2) || '-' ||substr(trim(p.yrdp07),3,2)
        end as date)
  end as yrdp07,  
  case
    when p.yrdp08 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp08),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp08))
          when 5 then  '20'||substr(trim(p.yrdp08),4,2)||'-'|| '0' || left(trim(p.yrdp08),1) || '-' ||substr(trim(p.yrdp08),2,2)
          when 6 then  '20'||substr(trim(p.yrdp08),5,2)||'-'|| left(trim(p.yrdp08),2) || '-' ||substr(trim(p.yrdp08),3,2)
        end as date)
    when cast(right(trim(p.yrdp08),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp08))
          when 5 then  '19'||substr(trim(p.yrdp08),4,2)||'-'|| '0' || left(trim(p.yrdp08),1) || '-' ||substr(trim(p.yrdp08),2,2)
          when 6 then  '19'||substr(trim(p.yrdp08),5,2)||'-'|| left(trim(p.yrdp08),2) || '-' ||substr(trim(p.yrdp08),3,2)
        end as date)
  end as yrdp08,   
  case
    when p.yrdp09 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp09),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp09))
          when 5 then  '20'||substr(trim(p.yrdp09),4,2)||'-'|| '0' || left(trim(p.yrdp09),1) || '-' ||substr(trim(p.yrdp09),2,2)
          when 6 then  '20'||substr(trim(p.yrdp09),5,2)||'-'|| left(trim(p.yrdp09),2) || '-' ||substr(trim(p.yrdp09),3,2)
        end as date)
    when cast(right(trim(p.yrdp09),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp09))
          when 5 then  '19'||substr(trim(p.yrdp09),4,2)||'-'|| '0' || left(trim(p.yrdp09),1) || '-' ||substr(trim(p.yrdp09),2,2)
          when 6 then  '19'||substr(trim(p.yrdp09),5,2)||'-'|| left(trim(p.yrdp09),2) || '-' ||substr(trim(p.yrdp09),3,2)
        end as date)
  end as yrdp09,   
  case
    when p.yrdp10 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp10),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp10))
          when 5 then  '20'||substr(trim(p.yrdp10),4,2)||'-'|| '0' || left(trim(p.yrdp10),1) || '-' ||substr(trim(p.yrdp10),2,2)
          when 6 then  '20'||substr(trim(p.yrdp10),5,2)||'-'|| left(trim(p.yrdp10),2) || '-' ||substr(trim(p.yrdp10),3,2)
        end as date)
    when cast(right(trim(p.yrdp10),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp10))
          when 5 then  '19'||substr(trim(p.yrdp10),4,2)||'-'|| '0' || left(trim(p.yrdp10),1) || '-' ||substr(trim(p.yrdp10),2,2)
          when 6 then  '19'||substr(trim(p.yrdp10),5,2)||'-'|| left(trim(p.yrdp10),2) || '-' ||substr(trim(p.yrdp10),3,2)
        end as date)
  end as yrdp10,     
  case
    when p.yrdp11 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp11),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp11))
          when 5 then  '20'||substr(trim(p.yrdp11),4,2)||'-'|| '0' || left(trim(p.yrdp11),1) || '-' ||substr(trim(p.yrdp11),2,2)
          when 6 then  '20'||substr(trim(p.yrdp11),5,2)||'-'|| left(trim(p.yrdp11),2) || '-' ||substr(trim(p.yrdp11),3,2)
        end as date)
    when cast(right(trim(p.yrdp11),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp11))
          when 5 then  '19'||substr(trim(p.yrdp11),4,2)||'-'|| '0' || left(trim(p.yrdp11),1) || '-' ||substr(trim(p.yrdp11),2,2)
          when 6 then  '19'||substr(trim(p.yrdp11),5,2)||'-'|| left(trim(p.yrdp11),2) || '-' ||substr(trim(p.yrdp11),3,2)
        end as date)
  end as yrdp11, 
  case
    when p.yrdp12 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp12),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp12))
          when 5 then  '20'||substr(trim(p.yrdp12),4,2)||'-'|| '0' || left(trim(p.yrdp12),1) || '-' ||substr(trim(p.yrdp12),2,2)
          when 6 then  '20'||substr(trim(p.yrdp12),5,2)||'-'|| left(trim(p.yrdp12),2) || '-' ||substr(trim(p.yrdp12),3,2)
        end as date)
    when cast(right(trim(p.yrdp12),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp12))
          when 5 then  '19'||substr(trim(p.yrdp12),4,2)||'-'|| '0' || left(trim(p.yrdp12),1) || '-' ||substr(trim(p.yrdp12),2,2)
          when 6 then  '19'||substr(trim(p.yrdp12),5,2)||'-'|| left(trim(p.yrdp12),2) || '-' ||substr(trim(p.yrdp12),3,2)
        end as date)
  end as yrdp12,    
  case
    when p.yrdp13 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp13),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp13))
          when 5 then  '20'||substr(trim(p.yrdp13),4,2)||'-'|| '0' || left(trim(p.yrdp13),1) || '-' ||substr(trim(p.yrdp13),2,2)
          when 6 then  '20'||substr(trim(p.yrdp13),5,2)||'-'|| left(trim(p.yrdp13),2) || '-' ||substr(trim(p.yrdp13),3,2)
        end as date)
    when cast(right(trim(p.yrdp13),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp13))
          when 5 then  '19'||substr(trim(p.yrdp13),4,2)||'-'|| '0' || left(trim(p.yrdp13),1) || '-' ||substr(trim(p.yrdp13),2,2)
          when 6 then  '19'||substr(trim(p.yrdp13),5,2)||'-'|| left(trim(p.yrdp13),2) || '-' ||substr(trim(p.yrdp13),3,2)
        end as date)
  end as yrdp13,  
  case
    when p.yrdp14 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp14),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp14))
          when 5 then  '20'||substr(trim(p.yrdp14),4,2)||'-'|| '0' || left(trim(p.yrdp14),1) || '-' ||substr(trim(p.yrdp14),2,2)
          when 6 then  '20'||substr(trim(p.yrdp14),5,2)||'-'|| left(trim(p.yrdp14),2) || '-' ||substr(trim(p.yrdp14),3,2)
        end as date)
    when cast(right(trim(p.yrdp14),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp14))
          when 5 then  '19'||substr(trim(p.yrdp14),4,2)||'-'|| '0' || left(trim(p.yrdp14),1) || '-' ||substr(trim(p.yrdp14),2,2)
          when 6 then  '19'||substr(trim(p.yrdp14),5,2)||'-'|| left(trim(p.yrdp14),2) || '-' ||substr(trim(p.yrdp14),3,2)
        end as date)
  end as yrdp14,  
  case
    when p.yrdp15 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp15),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp15))
          when 5 then  '20'||substr(trim(p.yrdp15),4,2)||'-'|| '0' || left(trim(p.yrdp15),1) || '-' ||substr(trim(p.yrdp15),2,2)
          when 6 then  '20'||substr(trim(p.yrdp15),5,2)||'-'|| left(trim(p.yrdp15),2) || '-' ||substr(trim(p.yrdp15),3,2)
        end as date)
    when cast(right(trim(p.yrdp15),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp15))
          when 5 then  '19'||substr(trim(p.yrdp15),4,2)||'-'|| '0' || left(trim(p.yrdp15),1) || '-' ||substr(trim(p.yrdp15),2,2)
          when 6 then  '19'||substr(trim(p.yrdp15),5,2)||'-'|| left(trim(p.yrdp15),2) || '-' ||substr(trim(p.yrdp15),3,2)
        end as date)
  end as yrdp15, 
  case
    when p.yrdp16 = 0 then date('9999-12-31')
    when cast(right(trim(p.yrdp16),2) as integer) < 20 then
      cast (
        case length(trim(p.yrdp16))
          when 5 then  '20'||substr(trim(p.yrdp16),4,2)||'-'|| '0' || left(trim(p.yrdp16),1) || '-' ||substr(trim(p.yrdp16),2,2)
          when 6 then  '20'||substr(trim(p.yrdp16),5,2)||'-'|| left(trim(p.yrdp16),2) || '-' ||substr(trim(p.yrdp16),3,2)
        end as date)
    when cast(right(trim(p.yrdp16),2) as integer) >= 20 then
      cast (
        case length(trim(p.yrdp16))
          when 5 then  '19'||substr(trim(p.yrdp16),4,2)||'-'|| '0' || left(trim(p.yrdp16),1) || '-' ||substr(trim(p.yrdp16),2,2)
          when 6 then  '19'||substr(trim(p.yrdp16),5,2)||'-'|| left(trim(p.yrdp16),2) || '-' ||substr(trim(p.yrdp16),3,2)
        end as date)
  end as yrdp16
from rydedata.pyprhead p   


/*



'select yrco#, yrempn, yrmgrn, yrjobd, yrjobl,  ' +
'  case ' +
'    when p.yrhdte = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrhdte),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrhdte)) ' +
'          when 5 then  ''20''||substr(trim(p.yrhdte),4,2)||''-''|| ''0'' || left(trim(p.yrhdte),1) || ''-'' ||substr(trim(p.yrhdte),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrhdte),5,2)||''-''|| left(trim(p.yrhdte),2) || ''-'' ||substr(trim(p.yrhdte),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrhdte),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrhdte)) ' +
'          when 5 then  ''19''||substr(trim(p.yrhdte),4,2)||''-''|| ''0'' || left(trim(p.yrhdte),1) || ''-'' ||substr(trim(p.yrhdte),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrhdte),5,2)||''-''|| left(trim(p.yrhdte),2) || ''-'' ||substr(trim(p.yrhdte),3,2) ' +
'        end as date) ' +
'  end as yrhdte, ' +
'  case ' +
'    when p.yrhdto = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrhdto),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrhdto)) ' +
'          when 5 then  ''20''||substr(trim(p.yrhdto),4,2)||''-''|| ''0'' || left(trim(p.yrhdto),1) || ''-'' ||substr(trim(p.yrhdto),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrhdto),5,2)||''-''|| left(trim(p.yrhdto),2) || ''-'' ||substr(trim(p.yrhdto),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrhdto),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrhdto)) ' +
'          when 5 then  ''19''||substr(trim(p.yrhdto),4,2)||''-''|| ''0'' || left(trim(p.yrhdto),1) || ''-'' ||substr(trim(p.yrhdto),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrhdto),5,2)||''-''|| left(trim(p.yrhdto),2) || ''-'' ||substr(trim(p.yrhdto),3,2) ' +
'        end as date) ' +
'  end as yrhdto, ' +
'  case ' +
'    when p.yrtdte = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrtdte),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrtdte)) ' +
'          when 5 then  ''20''||substr(trim(p.yrtdte),4,2)||''-''|| ''0'' || left(trim(p.yrtdte),1) || ''-'' ||substr(trim(p.yrtdte),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrtdte),5,2)||''-''|| left(trim(p.yrtdte),2) || ''-'' ||substr(trim(p.yrtdte),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrtdte),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrtdte)) ' +
'          when 5 then  ''19''||substr(trim(p.yrtdte),4,2)||''-''|| ''0'' || left(trim(p.yrtdte),1) || ''-'' ||substr(trim(p.yrtdte),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrtdte),5,2)||''-''|| left(trim(p.yrtdte),2) || ''-'' ||substr(trim(p.yrtdte),3,2) ' +
'        end as date) ' +
'  end as yrtdte, ' +
'  case ' +
'    when p.yrrdte = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrrdte),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrrdte)) ' +
'          when 5 then  ''20''||substr(trim(p.yrrdte),4,2)||''-''|| ''0'' || left(trim(p.yrrdte),1) || ''-'' ||substr(trim(p.yrrdte),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrrdte),5,2)||''-''|| left(trim(p.yrrdte),2) || ''-'' ||substr(trim(p.yrrdte),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrrdte),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrrdte)) ' +
'          when 5 then  ''19''||substr(trim(p.yrrdte),4,2)||''-''|| ''0'' || left(trim(p.yrrdte),1) || ''-'' ||substr(trim(p.yrrdte),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrrdte),5,2)||''-''|| left(trim(p.yrrdte),2) || ''-'' ||substr(trim(p.yrrdte),3,2) ' +
'        end as date) ' +
'  end as yrrdte,  ' +
'  case ' +
'    when p.yrndte = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrndte),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrndte)) ' +
'          when 5 then  ''20''||substr(trim(p.yrndte),4,2)||''-''|| ''0'' || left(trim(p.yrndte),1) || ''-'' ||substr(trim(p.yrndte),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrndte),5,2)||''-''|| left(trim(p.yrndte),2) || ''-'' ||substr(trim(p.yrndte),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrndte),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrndte)) ' +
'          when 5 then  ''19''||substr(trim(p.yrndte),4,2)||''-''|| ''0'' || left(trim(p.yrndte),1) || ''-'' ||substr(trim(p.yrndte),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrndte),5,2)||''-''|| left(trim(p.yrndte),2) || ''-'' ||substr(trim(p.yrndte),3,2) ' +
'        end as date) ' +
'  end as yrndte,  ' +
'  case ' +
'    when p.yrcdte = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrcdte),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrcdte)) ' +
'          when 5 then  ''20''||substr(trim(p.yrcdte),4,2)||''-''|| ''0'' || left(trim(p.yrcdte),1) || ''-'' ||substr(trim(p.yrcdte),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrcdte),5,2)||''-''|| left(trim(p.yrcdte),2) || ''-'' ||substr(trim(p.yrcdte),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrcdte),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrcdte)) ' +
'          when 5 then  ''19''||substr(trim(p.yrcdte),4,2)||''-''|| ''0'' || left(trim(p.yrcdte),1) || ''-'' ||substr(trim(p.yrcdte),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrcdte),5,2)||''-''|| left(trim(p.yrcdte),2) || ''-'' ||substr(trim(p.yrcdte),3,2) ' +
'        end as date) ' +
'  end as yrcdte,     ' +
'  yrbp01, yrbp02, yrbp03, yrbp04, yrbp05, yrbp06, yrbp07, yrbp08, ' +
'  yrbp09, yrbp10, yrbp11, yrbp12, yrbp13, yrbp14, yrbp15, yrbp16, ' +
'  case ' +
'    when p.yrdp01 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp01),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp01)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp01),4,2)||''-''|| ''0'' || left(trim(p.yrdp01),1) || ''-'' ||substr(trim(p.yrdp01),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp01),5,2)||''-''|| left(trim(p.yrdp01),2) || ''-'' ||substr(trim(p.yrdp01),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp01),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp01)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp01),4,2)||''-''|| ''0'' || left(trim(p.yrdp01),1) || ''-'' ||substr(trim(p.yrdp01),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp01),5,2)||''-''|| left(trim(p.yrdp01),2) || ''-'' ||substr(trim(p.yrdp01),3,2) ' +
'        end as date) ' +
'  end as yrdp01,   ' +
'  case ' +
'    when p.yrdp02 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp02),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp02)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp02),4,2)||''-''|| ''0'' || left(trim(p.yrdp02),1) || ''-'' ||substr(trim(p.yrdp02),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp02),5,2)||''-''|| left(trim(p.yrdp02),2) || ''-'' ||substr(trim(p.yrdp02),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp02),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp02)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp02),4,2)||''-''|| ''0'' || left(trim(p.yrdp02),1) || ''-'' ||substr(trim(p.yrdp02),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp02),5,2)||''-''|| left(trim(p.yrdp02),2) || ''-'' ||substr(trim(p.yrdp02),3,2) ' +
'        end as date) ' +
'  end as yrdp02,   ' +
'  case ' +
'    when p.yrdp03 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp03),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp03)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp03),4,2)||''-''|| ''0'' || left(trim(p.yrdp03),1) || ''-'' ||substr(trim(p.yrdp03),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp03),5,2)||''-''|| left(trim(p.yrdp03),2) || ''-'' ||substr(trim(p.yrdp03),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp03),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp03)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp03),4,2)||''-''|| ''0'' || left(trim(p.yrdp03),1) || ''-'' ||substr(trim(p.yrdp03),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp03),5,2)||''-''|| left(trim(p.yrdp03),2) || ''-'' ||substr(trim(p.yrdp03),3,2) ' +
'        end as date) ' +
'  end as yrdp03,   ' +
'  case ' +
'    when p.yrdp04 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp04),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp04)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp04),4,2)||''-''|| ''0'' || left(trim(p.yrdp04),1) || ''-'' ||substr(trim(p.yrdp04),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp04),5,2)||''-''|| left(trim(p.yrdp04),2) || ''-'' ||substr(trim(p.yrdp04),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp04),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp04)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp04),4,2)||''-''|| ''0'' || left(trim(p.yrdp04),1) || ''-'' ||substr(trim(p.yrdp04),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp04),5,2)||''-''|| left(trim(p.yrdp04),2) || ''-'' ||substr(trim(p.yrdp04),3,2) ' +
'        end as date) ' +
'  end as yrdp04,   ' +
'  case ' +
'    when p.yrdp05 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp05),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp05)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp05),4,2)||''-''|| ''0'' || left(trim(p.yrdp05),1) || ''-'' ||substr(trim(p.yrdp05),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp05),5,2)||''-''|| left(trim(p.yrdp05),2) || ''-'' ||substr(trim(p.yrdp05),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp05),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp05)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp05),4,2)||''-''|| ''0'' || left(trim(p.yrdp05),1) || ''-'' ||substr(trim(p.yrdp05),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp05),5,2)||''-''|| left(trim(p.yrdp05),2) || ''-'' ||substr(trim(p.yrdp05),3,2) ' +
'        end as date) ' +
'  end as yrdp05,   ' +
'  case ' +
'    when p.yrdp06 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp06),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp06)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp06),4,2)||''-''|| ''0'' || left(trim(p.yrdp06),1) || ''-'' ||substr(trim(p.yrdp06),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp06),5,2)||''-''|| left(trim(p.yrdp06),2) || ''-'' ||substr(trim(p.yrdp06),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp06),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp06)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp06),4,2)||''-''|| ''0'' || left(trim(p.yrdp06),1) || ''-'' ||substr(trim(p.yrdp06),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp06),5,2)||''-''|| left(trim(p.yrdp06),2) || ''-'' ||substr(trim(p.yrdp06),3,2) ' +
'        end as date) ' +
'  end as yrdp06,   ' +
'  case ' +
'    when p.yrdp07 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp07),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp07)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp07),4,2)||''-''|| ''0'' || left(trim(p.yrdp07),1) || ''-'' ||substr(trim(p.yrdp07),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp07),5,2)||''-''|| left(trim(p.yrdp07),2) || ''-'' ||substr(trim(p.yrdp07),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp07),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp07)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp07),4,2)||''-''|| ''0'' || left(trim(p.yrdp07),1) || ''-'' ||substr(trim(p.yrdp07),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp07),5,2)||''-''|| left(trim(p.yrdp07),2) || ''-'' ||substr(trim(p.yrdp07),3,2) ' +
'        end as date) ' +
'  end as yrdp07,   ' +
'  case ' +
'    when p.yrdp08 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp08),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp08)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp08),4,2)||''-''|| ''0'' || left(trim(p.yrdp08),1) || ''-'' ||substr(trim(p.yrdp08),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp08),5,2)||''-''|| left(trim(p.yrdp08),2) || ''-'' ||substr(trim(p.yrdp08),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp08),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp08)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp08),4,2)||''-''|| ''0'' || left(trim(p.yrdp08),1) || ''-'' ||substr(trim(p.yrdp08),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp08),5,2)||''-''|| left(trim(p.yrdp08),2) || ''-'' ||substr(trim(p.yrdp08),3,2) ' +
'        end as date) ' +
'  end as yrdp08,    ' +
'  case ' +
'    when p.yrdp09 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp09),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp09)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp09),4,2)||''-''|| ''0'' || left(trim(p.yrdp09),1) || ''-'' ||substr(trim(p.yrdp09),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp09),5,2)||''-''|| left(trim(p.yrdp09),2) || ''-'' ||substr(trim(p.yrdp09),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp09),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp09)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp09),4,2)||''-''|| ''0'' || left(trim(p.yrdp09),1) || ''-'' ||substr(trim(p.yrdp09),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp09),5,2)||''-''|| left(trim(p.yrdp09),2) || ''-'' ||substr(trim(p.yrdp09),3,2) ' +
'        end as date) ' +
'  end as yrdp09,    ' +
'  case ' +
'    when p.yrdp10 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp10),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp10)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp10),4,2)||''-''|| ''0'' || left(trim(p.yrdp10),1) || ''-'' ||substr(trim(p.yrdp10),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp10),5,2)||''-''|| left(trim(p.yrdp10),2) || ''-'' ||substr(trim(p.yrdp10),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp10),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp10)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp10),4,2)||''-''|| ''0'' || left(trim(p.yrdp10),1) || ''-'' ||substr(trim(p.yrdp10),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp10),5,2)||''-''|| left(trim(p.yrdp10),2) || ''-'' ||substr(trim(p.yrdp10),3,2) ' +
'        end as date) ' +
'  end as yrdp10,      ' +
'  case ' +
'    when p.yrdp11 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp11),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp11)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp11),4,2)||''-''|| ''0'' || left(trim(p.yrdp11),1) || ''-'' ||substr(trim(p.yrdp11),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp11),5,2)||''-''|| left(trim(p.yrdp11),2) || ''-'' ||substr(trim(p.yrdp11),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp11),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp11)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp11),4,2)||''-''|| ''0'' || left(trim(p.yrdp11),1) || ''-'' ||substr(trim(p.yrdp11),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp11),5,2)||''-''|| left(trim(p.yrdp11),2) || ''-'' ||substr(trim(p.yrdp11),3,2) ' +
'        end as date) ' +
'  end as yrdp11,  ' +
'  case ' +
'    when p.yrdp12 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp12),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp12)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp12),4,2)||''-''|| ''0'' || left(trim(p.yrdp12),1) || ''-'' ||substr(trim(p.yrdp12),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp12),5,2)||''-''|| left(trim(p.yrdp12),2) || ''-'' ||substr(trim(p.yrdp12),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp12),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp12)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp12),4,2)||''-''|| ''0'' || left(trim(p.yrdp12),1) || ''-'' ||substr(trim(p.yrdp12),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp12),5,2)||''-''|| left(trim(p.yrdp12),2) || ''-'' ||substr(trim(p.yrdp12),3,2) ' +
'        end as date) ' +
'  end as yrdp12,     ' +
'  case ' +
'    when p.yrdp13 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp13),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp13)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp13),4,2)||''-''|| ''0'' || left(trim(p.yrdp13),1) || ''-'' ||substr(trim(p.yrdp13),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp13),5,2)||''-''|| left(trim(p.yrdp13),2) || ''-'' ||substr(trim(p.yrdp13),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp13),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp13)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp13),4,2)||''-''|| ''0'' || left(trim(p.yrdp13),1) || ''-'' ||substr(trim(p.yrdp13),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp13),5,2)||''-''|| left(trim(p.yrdp13),2) || ''-'' ||substr(trim(p.yrdp13),3,2) ' +
'        end as date) ' +
'  end as yrdp13,   ' +
'  case ' +
'    when p.yrdp14 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp14),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp14)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp14),4,2)||''-''|| ''0'' || left(trim(p.yrdp14),1) || ''-'' ||substr(trim(p.yrdp14),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp14),5,2)||''-''|| left(trim(p.yrdp14),2) || ''-'' ||substr(trim(p.yrdp14),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp14),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp14)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp14),4,2)||''-''|| ''0'' || left(trim(p.yrdp14),1) || ''-'' ||substr(trim(p.yrdp14),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp14),5,2)||''-''|| left(trim(p.yrdp14),2) || ''-'' ||substr(trim(p.yrdp14),3,2) ' +
'        end as date) ' +
'  end as yrdp14,   ' +
'  case ' +
'    when p.yrdp15 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp15),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp15)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp15),4,2)||''-''|| ''0'' || left(trim(p.yrdp15),1) || ''-'' ||substr(trim(p.yrdp15),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp15),5,2)||''-''|| left(trim(p.yrdp15),2) || ''-'' ||substr(trim(p.yrdp15),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp15),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp15)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp15),4,2)||''-''|| ''0'' || left(trim(p.yrdp15),1) || ''-'' ||substr(trim(p.yrdp15),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp15),5,2)||''-''|| left(trim(p.yrdp15),2) || ''-'' ||substr(trim(p.yrdp15),3,2) ' +
'        end as date) ' +
'  end as yrdp15,  ' +
'  case ' +
'    when p.yrdp16 = 0 then date(''9999-12-31'') ' +
'    when cast(right(trim(p.yrdp16),2) as integer) < 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp16)) ' +
'          when 5 then  ''20''||substr(trim(p.yrdp16),4,2)||''-''|| ''0'' || left(trim(p.yrdp16),1) || ''-'' ||substr(trim(p.yrdp16),2,2) ' +
'          when 6 then  ''20''||substr(trim(p.yrdp16),5,2)||''-''|| left(trim(p.yrdp16),2) || ''-'' ||substr(trim(p.yrdp16),3,2) ' +
'        end as date) ' +
'    when cast(right(trim(p.yrdp16),2) as integer) >= 20 then ' +
'      cast ( ' +
'        case length(trim(p.yrdp16)) ' +
'          when 5 then  ''19''||substr(trim(p.yrdp16),4,2)||''-''|| ''0'' || left(trim(p.yrdp16),1) || ''-'' ||substr(trim(p.yrdp16),2,2) ' +
'          when 6 then  ''19''||substr(trim(p.yrdp16),5,2)||''-''|| left(trim(p.yrdp16),2) || ''-'' ||substr(trim(p.yrdp16),3,2) ' +
'        end as date) ' +
'  end as yrdp16 ' +
'from rydedata.pyprhead p'

*/
