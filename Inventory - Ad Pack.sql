-- limit to inventory in april (date_in_invent)
-- 05/01/18 i am going brain dead, what date do i put in the where clause?
-- well, 11/01/2018, for oct month end date_in_invent < 20181101

select trim(a.inpmast_stock_number) as stock_number, a.inpmast_vin as vin, 
  b.num_field_value as ad_pack_amount\
-- select count(*)  
from rydedata.inpmast a 
inner join rydedata.inpoptf b on a.inpmast_vin = b.vin_number
inner join rydedata.inpoptd c on b.company_number = c.company_number
  and b.seq_number = c.seq_number
where a.status = 'I'
  and b.seq_number = 14
  and b.num_field_value <> 0
and  date_in_invent < 20191101 ------------------------------


201802: 474 $116021.03  239.71 per
201803: 407 $ 98973.52  243.18 per
201804: 351 $ 83017.53  236.52 per
208105: 372 $ 87698.80  235.75 per
201806: 395 $ 92350.91  233.80 per
201810: 375 $ 93850.92  250.26 per
201812: 411 $ 92242.08  224.43 per

select sum(b.num_field_value)  as total, count(*), sum(b.num_field_value)/count(*)
from rydedata.inpmast a 
inner join rydedata.inpoptf b on a.inpmast_vin = b.vin_number
inner join rydedata.inpoptd c on b.company_number = c.company_number
  and b.seq_number = c.seq_number
where a.status = 'I'
  and b.seq_number = 14
  and b.num_field_value <> 0
  and  date_in_invent < 20191101




select a.control_number, b.inpmast_vin, a.transaction_amount
from rydedata.glptrns a
join rydedata.inpmast b on trim(a.control_number) = trim(b.inpmast_stock_number)
  and b.status = 'I'
  and b.type_n_u = 'N'
where a.post_status = 'Y'
  and trim(a.account_number) = '133211'

  
  
select trim(a.inpmast_Stock_number) as stock_number, trim(a.inpmast_vin) as vin, -b.transaction_amount as ad_pack_amount
-- select count(*)
from rydedata.inpmast a
join rydedata.glptrns b on trim(a.inpmast_stock_number) = trim(b.control_number)
  and trim(b.account_number) = '133211'
  and b.post_status = 'Y'
where a.status = 'I'
  and a.type_n_u = 'N'  
  and left(trim(a.inpmast_Stock_number), 1) <> 'H'
  and right(trim(a.inpmast_Stock_number), 1) <> 'R'

union
select trim(a.inpmast_stock_number) as stock_number, a.inpmast_vin as vin, 
  b.num_field_value as ad_pack_amount
-- select count(*)  
from rydedata.inpmast a 
inner join rydedata.inpoptf b on a.inpmast_vin = b.vin_number
inner join rydedata.inpoptd c on b.company_number = c.company_number
  and b.seq_number = c.seq_number
where a.status = 'I'
  and b.seq_number = 14
  and b.num_field_value <> 0
  and  date_in_invent < 20200202-----------------------------
  and a.inpmast_stock_number like '%R'

  
-- new car inventory accounts  
Select * 
from RYDEDATA.GLPMAST a
where a.year = 2020
  and a.account_type = '1'
  and a.department = 'NC'
  and a.typical_balance = 'D'
  and trim(a.account_number) <> '126104'
  and trim(a.account_number) not like '3%'
order by account_number  
  
  
-- R units in inventory  

select trim(a.control_number)
from rydedata.glptrns a
join rydedata.glpmast b on trim(a.account_number) = trim(b.account_number)
  and b.year = 2020
  and b.account_type = '1'
  and b.department = 'NC'
  and b.typical_balance = 'D'
  and trim(b.account_number) <> '126104'
  and trim(b.account_number) not like '3%'
where a.control_number like '%R'  
  and a.post_status = 'Y'
group by (a.control_number) 
having sum(a.transaction_amount) > 10000  





  