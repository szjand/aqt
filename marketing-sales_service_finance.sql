-- List B: Sales
select 
  b.last_company_name as LAST_NAME, b.first_name, b.address_1, b.city, b.state_code, 
  b.zip_code, b.phone_number, b.cell_phone, b.email_address,
  c.year, c.make, c.model, 
  a.delivery_date, d.sales_person_name, c.inpmast_vin as VIN
from rydedata.bopmast a
left join rydedata.bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
left join rydedata.inpmast c on a.bopmast_vin = c.inpmast_vin  
left join rydedata.bopslss d on a.record_key = d.deal_key
  and a.bopmast_company_number = d.company_number
  and d.sales_person_type = 'S'
where a.bopmast_company_number = 'RY1'
  and a.bopmast_vin <> ''
  and a.sale_type not in ('F','W')


-- List C: Finance
select 
--  a.record_type, a.effective_apr, a.apr_bmlapr, a.lease_term, a.lease_factor, date_last_lease_payment, odd_last_payment, 
--  a.insurance_term, date_last_payment, effective_apr, lease_term,   
  b.last_company_name as LAST_NAME, b.first_name, b.address_1, b.city, b.state_code, 
  b.zip_code, b.phone_number, b.cell_phone, b.email_address,
--  c.year, c.make, c.model, 
  a.delivery_date, 
  case
    when a.term = 1 then a.lease_term
    else a.term
  end as term, 
  case
    when a.apr = 0 then a.effective_apr
    else a.apr
  end as apr,
  cast(left(digits(a.date_last_payment), 4) || '-' || substr(digits(a.date_last_payment), 5, 2) || '-' || substr(digits(a.date_last_payment), 7, 2) as date)last_payment_date,
  case 
    when a.record_type = 'F' then 'FINANCE'
    when a.record_type = 'L' then 'LEASE'
  end as FIN_LEASE, 
  d.sales_person_name, c.inpmast_vin as VIN
-- select count(*)  
from rydedata.bopmast a
left join rydedata.bopname b on a.buyer_number = b.bopname_record_key
  and a.bopmast_company_number = b.bopname_company_number
left join rydedata.inpmast c on a.bopmast_vin = c.inpmast_vin  
left join rydedata.bopslss d on a.record_key = d.deal_key
  and a.bopmast_company_number = d.company_number
  and d.sales_person_type = 'S'
where a.bopmast_company_number = 'RY1'
  and a.bopmast_vin <> ''
  and a.sale_type not in ('F','W')
  and a.record_type in ('F','L')
  -- and a.delivery_date < '2012-01-01'  --3869
  -- and a.delivery_date between '2012-01-01' and '2015-12-31' -- 8885
  and a.delivery_date between '2016-01-01' and '2017-10-03' -- 4294



select *
from rydedata.bopmast
where bopmast_vin = '3GTU2NEC0HG507953'


select * 
from rydedata.bopmast a
where a.bopmast_company_number = 'RY1'
  and a.bopmast_vin <> ''
  and a.sale_type not in ('F','W')
  and a.record_type in ('F')
  and delivery_date > '09/01/2017'
  and a.vehicle_type = 'N'
  
  


select record_Type, count(*)
from rydedata.bopmast
group by record_type


-- List A service

select 
  b.last_company_name as LAST_NAME, b.first_name, b.address_1, b.city, b.state_code, 
  b.zip_code, b.phone_number, b.cell_phone, b.email_address,
  d.year, d.make, d.model, 
  -- cast(left(digits(e.first_service), 4) || '-' || substr(digits(e.first_service), 5, 2) || '-' || substr(digits(e.first_service), 7, 2) as date) as ro_date,
  cast(left(digits(e.last_service), 4) || '-' || substr(digits(e.last_service), 5, 2) || '-' || substr(digits(e.last_service), 7, 2) as date) as last_service,
  -- e.number_of_ros,
  c.name as service_writer, 
  a.vin
-- select left(trim(ro_number), 2), count(*)
-- select count(*)
from rydedata.sdprhdr a
inner join rydedata.bopname b on a.customer_key = b.bopname_record_key
  and a.company_number = b.bopname_company_number
inner join rydedata.sdpswtr c on a.service_writer_id = c.id
  and a.company_number = c.company_number  
inner join rydedata.inpmast d on trim(a.vin) = trim(d.inpmast_vin)
inner join (
  select a.customer_key, min(a.open_date) as first_service, max(a.open_date) as last_service, count(*) as number_of_ros
  from rydedata.sdprhdr a
  where a.cust_name not like '%VOID%'
    and a.cust_name not like '%INVENT%'
    and a.open_date between 20130901 and 20170901
    and length(trim(a.ro_number)) > 7
    and a.company_number = 'RY1'
  group by a.customer_key) e on a.customer_key = e.customer_key
    and a.open_date = e.last_service
where a.cust_name not like '%VOID%'
  and a.cust_name not like '%INVENT%'
  and a.open_date between 20130901 and 20170901 
  and length(trim(a.ro_number)) > 7
  and a.company_number = 'RY1'
  and b.last_company_name is not null
  and length(trim(a.vin)) = 17
  -- group by left(trim(ro_number), 2) 
  -- limit 500
  -- offset 1000

select a.customer_key, max(a.open_date) as last_service
from rydedata.sdprhdr a
where a.cust_name not like '%VOID%'
  and a.cust_name not like '%INVENT%'
  and a.open_date between 20130901 and 20170901
  and length(trim(a.ro_number)) > 7
  and a.company_number = 'RY1'
group by a.customer_key
  
  
select count(*)
from rydedata.sdprhdr a
inner join rydedata.bopname b on a.customer_key = b.bopname_record_key
  and a.company_number = b.bopname_company_number
where last_company_name like '%BERGST%'

  