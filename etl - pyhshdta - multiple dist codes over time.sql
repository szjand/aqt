-- can this be used for history of dist code?

select yhdco#, trim(yhdemp), trim(ymdist)
from RYDEDATA.PYHSHDTA
group by yhdco#, trim(yhdemp), trim(ymdist)
  having count(*) > 1
order by trim(yhdemp)


select yhdemm, yhdedd, yhdeyy,
  cast (
    case
      when yhdemm between 1 and 9 then '0'||trim(char(yhdemm))
      else trim(char(yhdemm))
    end 
    ||'/'||
    case
      when yhdedd between 1 and 9 then '0'||trim(char(yhdedd))
      else trim(char(yhdedd))
    end 
    ||'/'||
    case
      when yhdeyy between 1 and 9 then '200'||trim(char(yhdeyy))
      else '20'||trim(char(yhdeyy))
    end as date)
from RYDEDATA.PYHSHDTA
where yhdeyy = 12

-- employees with multiple distcodes
select yhdemp
from (
  select distinct yhdco#, trim(yhdemp) as yhdemp, trim(ymdist) as ymdist
  from rydedata.pyhshdta) x
  group by yhdemp
    having count(*) > 1

-- employees with mult distcodes
select distinct yhdco#, trim(yhdemp), trim(ymdist) 
from rydedata.pyhshdta
where trim(yhdemp) in (
  select yhdemp
  from (
    select distinct trim(yhdemp) as yhdemp, trim(ymdist) as ymdist
    from rydedata.pyhshdta) x
    group by yhdemp
      having count(*) > 1)
order by trim(yhdemp)

-- all data, with PayrollEndDate
select 
  cast (
    case
      when yhdemm between 1 and 9 then '0'||trim(char(yhdemm))
      else trim(char(yhdemm))
    end 
    ||'/'||
    case
      when yhdedd between 1 and 9 then '0'||trim(char(yhdedd))
      else trim(char(yhdedd))
    end 
    ||'/'||
    case
      when yhdeyy between 1 and 9 then '200'||trim(char(yhdeyy))
      else '20'||trim(char(yhdeyy))
    end as date) as PayrollEndDate,
  p.*
from RYDEDATA.PYHSHDTA p

-- so, i want min/max payroll end date for each store/emp#/dist where an emp# has mult dist codes

select yhdco#, trim(yhdemp) as employeenumber, trim(ymdist) as DistCode, 
  min(PayrollEndDate) as minDate, max(PayrollEndDate) as maxDate
from (
  select 
    cast (
      case
        when yhdemm between 1 and 9 then '0'||trim(char(yhdemm))
        else trim(char(yhdemm))
      end 
      ||'/'||
      case
        when yhdedd between 1 and 9 then '0'||trim(char(yhdedd))
        else trim(char(yhdedd))
      end 
      ||'/'||
      case
        when yhdeyy between 1 and 9 then '200'||trim(char(yhdeyy))
        else '20'||trim(char(yhdeyy))
      end as date) as PayrollEndDate,
    p.* 
  from RYDEDATA.PYHSHDTA p) x
where trim(yhdemp) in ( -- mult dist codes/empl
    select yhdemp
    from (
      select distinct yhdco#, trim(yhdemp) as yhdemp, trim(ymdist) as ymdist
      from rydedata.pyhshdta) x
      group by yhdemp
        having count(*) > 1)
group by yhdco#, trim(yhdemp), trim(ymdist)
order by employeenumber

-- populate ads.__MultipleDistCodes
select *
from (
  select yhdco#, trim(yhdemp) as employeenumber, trim(ymdist) as DistCode, 
    min(PayrollEndDate) as minDate, max(PayrollEndDate) as maxDate
  from (
    select 
      cast (
        case
          when yhdemm between 1 and 9 then '0'||trim(char(yhdemm))
          else trim(char(yhdemm))
        end 
        ||'/'||
        case
          when yhdedd between 1 and 9 then '0'||trim(char(yhdedd))
          else trim(char(yhdedd))
        end 
        ||'/'||
        case
          when yhdeyy between 1 and 9 then '200'||trim(char(yhdeyy))
          else '20'||trim(char(yhdeyy))
        end as date) as PayrollEndDate,
      p.* 
    from RYDEDATA.PYHSHDTA p
    where yhdeyy*10000 + yhdemm*100 + yhdedd > 90731) x
where trim(yhdemp) in ( -- mult dist codes/empl
    select yhdemp
    from (
      select distinct yhdco#, trim(yhdemp) as yhdemp, trim(ymdist) as ymdist
      from rydedata.pyhshdta
      where yhdeyy*10000 + yhdemm*100 + yhdedd > 90731) x
      group by yhdemp
        having count(*) > 1)
group by yhdco#, trim(yhdemp), trim(ymdist)) x
order by employeenumber, mindate 

-- but the problem is overlapping periods of min/max
select yhdco#, ypbnum, yhdemp, yhdeyy*10000 + yhdemm*100 + yhdedd, ymdist, ymdept, yhrate
from rydedata.pyhshdta
where trim (yhdemp) = '116550'
order by yhdeyy*10000 + yhdemm*100 + yhdedd

-- fact
-- 1 row per store/payrollrun, emp#/year
select yhdco#, ypbnum, yhdemp, yhdeyy
from rydedata.pyhshdta
group by yhdco#, ypbnum, yhdemp, yhdeyy
  having count(*) > 1



select *
select count(*)
from rydedata.pyptbdta


