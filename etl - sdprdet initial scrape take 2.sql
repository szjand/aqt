select left(trim(ptro#), 4), count(*)
from rydedata.sdprdet
where left(trim(ptro#), 2) = '26'
  and ptco# in ('RY1','RY2','RY3')
  and ptdbas <> 'V'
group by left(trim(ptro#), 4)
order by left(trim(ptro#), 4)

select count(*)
from rydedata.sdprdet
where ptco# in ('RY1','RY2','RY3')
  and ptdbas <> 'V'

select left(trim(ptro#), 3), count(*)
from rydedata.sdprdet
where ptco# in ('RY1','RY2','RY3')
  and ptdbas <> 'V'
group by left(trim(ptro#), 3)
order by left(trim(ptro#), 3)

select left(trim(ptro#), 2), count(*)
from rydedata.sdprdet
where ptco# in ('RY1','RY2','RY3')
  and ptdbas <> 'V'
group by left(trim(ptro#), 2)
order by left(trim(ptro#), 2)

select *
from rydedata.sdprdet
where left(trim(ptro#) , 2) = '99'

select count(*)
from rydedata.sdprdet
where length(trim(ptro#)) > 6

select left(trim(ptro#), 4), count(*)
from rydedata.sdprdet
where ptco# in ('RY1','RY2','RY3')
  and ptdbas <> 'V'
  and length(trim(ptro#)) > 6
  and left(trim(ptro#), 4) not in ('1000','1303','1545','4758','4760','4762')
group by left(trim(ptro#), 4)
order by left(trim(ptro#), 4)



select left(trim(ptro#), 4)
from rydedata.sdprdet
where ptco# in ('RY1','RY2','RY3')
  and ptdbas <> 'V'
  and length(trim(ptro#)) > 6
  and left(trim(ptro#), 4) not in ('1000','1303','1545','4758','4760','4762')
group by left(trim(ptro#), 4)


