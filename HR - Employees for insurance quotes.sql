select ymname from (
select ymempn, ymname, j.yrtext, ymdriv, ymdvst,
  case length(trim(p.ymbdte))
    when 5 then '0' || left(trim(p.ymbdte),1) || '/' || substr(trim(p.ymbdte),2,2) || '/' || substr(trim(p.ymbdte),4,2)
    when 6 then left(trim(p.ymbdte),2) || '/' || substr(trim(p.ymbdte),3,2) || '/' || substr(trim(p.ymbdte),5,2)
    else ''
  end as "Birth Date",
  case
    when ymactv = 'A' then 'FULL'
    when ymactv = 'P' then 'PART'
  end
-- select *
from rydedata.pymast p
left join rydedata.pyprhead ph on ph.yrempn = p.ymempn
  and trim(ph.yrjobd) <> '' -- Dale Andrews
  and p.ymco# = ph.yrco#
left join rydedata.pyprjobd j on j.yrjobd = ph.yrjobd 
  and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') 
  and ph.yrco# = j.yrco#
  and j.data <> ''
  and j.seq_ = 0
where ymactv <> 'T'
  and ymco# in ('RY1','RY2') --order by ymname
) x group by ymname having count(*) > 1  


-- in ads.dds, create a table zEmployees, insert this shit into it, then get first, last, and birthdate from dimEmployee
select trim(ymempn) as employeenumber, trim(ymname) as name, trim(j.yrtext) as job, trim(ymdriv) as dl, ymdvst as dlstate,
--  case length(trim(p.ymbdte))
--    when 5 then '0' || left(trim(p.ymbdte),1) || '/' || substr(trim(p.ymbdte),2,2) || '/' || substr(trim(p.ymbdte),4,2)
--    when 6 then left(trim(p.ymbdte),2) || '/' || substr(trim(p.ymbdte),3,2) || '/' || substr(trim(p.ymbdte),5,2)
--    else ''
--  end as "Birth Date",
  case
    when ymactv = 'A' then 'FULL'
    when ymactv = 'P' then 'PART'
  end as fp
-- select *
from rydedata.pymast p
left join rydedata.pyprhead ph on ph.yrempn = p.ymempn
  and trim(ph.yrjobd) <> '' -- Dale Andrews
  and p.ymco# = ph.yrco#
left join rydedata.pyprjobd j on j.yrjobd = ph.yrjobd 
  and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') 
  and ph.yrco# = j.yrco#
  and j.data <> ''
  and j.seq_ = 0
where ymactv <> 'T'
  and ymco# in ('RY1','RY2') --order by ymname
  
  