-- ? useful ? queries
-- delivered vehicles not deleted

Select * from dbo.asset
where name = '14528a'
-- name (stocknumber) can be in table more than once
select * 
from asset
where name in (
  select name
  from asset
  group by name
  having count(*) > 1)
  
-- BIT data type:  1 = true (selected)  0 = false (not selected)
select name, active, deleted
from asset  
--where deleted = 1
where deleted = 0

-- no records that are simoultaneously not active AND not deleted
select *
from asset
where active = 0
and deleted = 0

-- many records simoultaneously active AND deleted
select *
from asset
where active = 1
and deleted = 1

-- no multiples where active and not deleted
select name
from asset
where active = 1
and deleted = 0
group by name 
having count(*) > 1

-- sold vehcile
select *
from asset
where name like '136%'

-- what do asset_types mean?
-- values are Registered and Unregistered
select asset_type, count(*)
from asset
group by asset_type


select *
from asset a
where name like '136%'
and deleted = 0

select * from [user]

select a.active, a.deleted, a.name as stocknumber, a.attribute_value_1 as year, a.attribute_value_2 as make, 
  attribute_value_3 as model, attribute_value_4 as color,  attribute_value_7 as vin, 
  a.asset_status, 
  case s.name
    when 'KEY-CROOKSTON' then 'Crookston'
    when 'KEY-HNDA-SVC' then 'Honda Service'
    when 'key-body-shop' then 'Rydell Body Shop'
    when 'key-mechanical' then 'Rydell Service'
    when 'key-new-car' then 'Rydell New Car'
    when 'key-detail' then 'Rydell Detail'
    when 'key-used-car' then 'Rydell UC'
  end as Cabinet,
  u.first_name + ' ' + u.last_name as 'user', 
  t.*
from asset a
left join cabinet c on a.cabinet_id = c.cabinet_id
left join system s on c.system_id = s.system_id
left join [user] u on a.user_id = u.user_id
left join asset_transactions t on a.asset_id = t.asset_id
where a.deleted = 0
  and a.name in ('14543a', '14599a','14771a','14950xa','14962x','15051aa','15119xx','h3700xa')
  --and a.name like '136%'



select description, count(*) from user_transactions group by description


SELECT   user_transaction_id AS [Transaction ID], 
         first_name + ' ' + "user".last_name  AS [User Name], 
         name AS System, name AS Location, name AS Company, 
         log_level AS Level, description, device, 
         transaction_date AS [Transaction Date], 
         dbo."fn_Convert_UTF_To_Approximate_Local_Time"(transaction_date)  AS [Transaction Local Time] 
FROM     user_transactions , 
         company , 
         system , 
         location 
         LEFT OUTER JOIN "user"  ON user_id=user_id 
WHERE    company_id=company_id 
AND      system_id=system_id 
AND      location_id=location_id


-- 10/27/12
select name, description, asset_status, asset_removal_type, cabinet_id, user_id, checkout_date
from asset
where active = 1
and deleted = 0
order by name


CREATE VIEW "dbo"."v_assets"  
         ([Asset ID], name, description, Status, User, 
         Year_att, Make_att, Model_att, [Ext. Color_att], 
         [Int. Color_att], [Type 2_att], VIN_att, [Asset Type], 
         [Removal Type], Cabinet, System, Location, Reason, 
         [Checkout Date], [Checkout Local Time)]) 
AS 
--Note: your SQL is not valid as some tables are not joined
SELECT   dbo.asset.asset_id, dbo.asset.name, dbo.asset.description, 
         dbo.asset.asset_status, 
         dbo."user".first_name + N' ' + dbo."user".last_name, 
         dbo.asset.attribute_value_1, dbo.asset.attribute_value_2, 
         dbo.asset.attribute_value_3, dbo.asset.attribute_value_4, 
         dbo.asset.attribute_value_5, dbo.asset.attribute_value_6, 
         dbo.asset.attribute_value_7, dbo.asset.asset_type, 
         dbo.asset.asset_removal_type, isnull(c.name, 'None')  , 
         isnull(s.name, 'None')  , isnull(l.name, 'None')  , -- ir.name, 
         dbo.asset.checkout_date, 
         dbo."fn_Convert_UTF_To_Approximate_Local_Time"(dbo.asset.checkout_date)  
--FROM     dbo.issue_reason ir
 --        LEFT OUTER JOIN dbo.asset  ON ir.issue_reason_id=dbo.asset.issue_reason_id
from     dbo.asset
         LEFT OUTER JOIN dbo.cabinet c ON c.cabinet_id=dbo.asset.cabinet_id
         LEFT OUTER JOIN dbo.system s ON s.system_id=c.system_id
         LEFT OUTER JOIN dbo.location l ON l.location_id=s.location_id, 
         dbo.[user] 
WHERE    (dbo.asset.deleted = 0)

select a.name, a.asset_status, RTRIM(LTRIM(d.first_name)) + ' ' + RTRIM(LTRIM(d.last_name)),
  substring(c.name, charindex('-', c.name) + 1, 12)
-- select *
from dbo.asset a
left join dbo.cabinet b on a.cabinet_id = b.cabinet_id
left join dbo.system c on b.system_id = c.system_id
left join dbo.[user] d on a.user_id = d.user_id
where a.deleted = 0

-- 12/16
-- add first 3 characters of first name
-- 10/21/14 cahalan wants keys check out/in time for tracking begin/end of inspections
select a.name, 
  case 
    when asset_status = 'In' then coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet')
    when asset_status = 'Out' then 
      case
        when a.user_id is null then 'Out to ??????'
        else 'Out to ' + left(RTRIM(LTRIM(d.first_name)), 3) + ' ' + RTRIM(LTRIM(d.last_name))
      end
  end, a.asset_status, a.checkout_date, a.updated_date, a.created_date 
-- select *
-- select count(*)
-- select a.name, a.asset_status, c.name, d.first_name, d.last_name
from dbo.asset a
left join dbo.cabinet b on a.cabinet_id = b.cabinet_id
left join dbo.system c on b.system_id = c.system_id
left join dbo.[user] d on a.user_id = d.user_id
where a.deleted = 0
  and a.name like '22740%' order by name
  --and a.name = '15543xxa'
--order by name


select *
from dbo.[user]
where last_name in (
select last_name
from dbo.[user]
group by last_name
having count(*) > 1)
order by last_name

select distinct asset_status
from dbo.asset

select coalesce(cabinet_id, 999999), count(*)
from dbo.asset
where asset_status = 'in'
  and deleted = 0
group by coalesce(cabinet_id, 999999)


select coalesce(user_id, 999999), count(*)
from dbo.asset
where asset_status = 'out'
  and deleted = 0
group by coalesce(user_id, 999999)





select a.name,   
case     
  when asset_status = 'In' then coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet')     
  when asset_status = 'Out' then       
    case          
      when a.user_id is null then 'Out to ??????'         
      else 'Out to ' +  RTRIM(LTRIM(d.last_name))       
    end   
  end as KeyStatus
from dbo.asset a 
left join dbo.cabinet b on a.cabinet_id = b.cabinet_id 
left join dbo.[system] c on b.system_id = c.system_id  
left join dbo.[user] d on a.user_id = d.user_id 
where a.deleted = 0



-- datetime formats
DECLARE @now datetime
SET @now = GETDATE()
select convert(nvarchar(MAX), @now, 0) as output, 0 as style 
union select convert(nvarchar(MAX), @now, 1), 1
union select convert(nvarchar(MAX), @now, 2), 2
union select convert(nvarchar(MAX), @now, 3), 3
union select convert(nvarchar(MAX), @now, 4), 4
union select convert(nvarchar(MAX), @now, 5), 5
union select convert(nvarchar(MAX), @now, 6), 6
union select convert(nvarchar(MAX), @now, 7), 7
union select convert(nvarchar(MAX), @now, 8), 8
union select convert(nvarchar(MAX), @now, 9), 9
union select convert(nvarchar(MAX), @now, 10), 10
union select convert(nvarchar(MAX), @now, 11), 11
union select convert(nvarchar(MAX), @now, 12), 12
union select convert(nvarchar(MAX), @now, 13), 13
union select convert(nvarchar(MAX), @now, 14), 14
--15 to 19 not valid
union select convert(nvarchar(MAX), @now, 20), 20
union select convert(nvarchar(MAX), @now, 21), 21
union select convert(nvarchar(MAX), @now, 22), 22
union select convert(nvarchar(MAX), @now, 23), 23
union select convert(nvarchar(MAX), @now, 24), 24
union select convert(nvarchar(MAX), @now, 25), 25
--26 not valid
union select convert(nvarchar(MAX), @now, 100), 100
union select convert(nvarchar(MAX), @now, 101), 101
union select convert(nvarchar(MAX), @now, 102), 102
union select convert(nvarchar(MAX), @now, 103), 103
union select convert(nvarchar(MAX), @now, 104), 104
union select convert(nvarchar(MAX), @now, 105), 105
union select convert(nvarchar(MAX), @now, 106), 106
union select convert(nvarchar(MAX), @now, 107), 107
union select convert(nvarchar(MAX), @now, 108), 108
union select convert(nvarchar(MAX), @now, 109), 109
union select convert(nvarchar(MAX), @now, 110), 110
union select convert(nvarchar(MAX), @now, 111), 111
union select convert(nvarchar(MAX), @now, 112), 112
union select convert(nvarchar(MAX), @now, 113), 113
union select convert(nvarchar(MAX), @now, 114), 114
union select convert(nvarchar(MAX), @now, 120), 120
union select convert(nvarchar(MAX), @now, 121), 121
--122 to 125 not valid
union select convert(nvarchar(MAX), @now, 126), 126
union select convert(nvarchar(MAX), @now, 127), 127
--128, 129 not valid
union select convert(nvarchar(MAX), @now, 130), 130
union select convert(nvarchar(MAX), @now, 131), 131
--132 not valid
order BY style


select *
from asset
where name = '25923XX'

select name, count(*)
from asset
group by name
order by count(*) desc

select *
from system

select * 
from attribute

select *
from issue_reason

select *
from asset
where name = '15287'


-- from Z:\E\DPSVSeries\Keyper
select RTRIM(LTRIM(a.name)) as name, 
  case 
    when asset_status = 'In' then coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet')
    when asset_status = 'Out' then 
      case  
        when a.user_id is null then 'Out to ??????' 
        else 'Out to ' +  left(RTRIM(LTRIM(d.first_name)),3) + ' ' + RTRIM(LTRIM(d.last_name)) 
      end 
  end as KeyStatus, b.cabinet_id, b.system_id, c.name,
  a.*
from dbo.asset a 
left join dbo.cabinet b on a.cabinet_id = b.cabinet_id 
left join dbo.system c on b.system_id = c.system_id  
left join dbo.[user] d on a.user_id = d.user_id 
where a.deleted = 0
  and a.asset_status = 'In'
order by a.name



select cast(getdate() -1 as date) 

select getdate() - .1 

select *
from dbo.asset
where checkout_date > getdate() - .1



select * from dbo.asset

-- datetime (transaction_date) to date
select transaction_Date, cast(transaction_date as date)
from dbo.asset_transactions
where cast(transaction_date as date) > '2018-07-11'


