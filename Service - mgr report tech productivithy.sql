/*
tables used:
pypclockin
pymast
pdppdet
sdptech
sdprdet
*/
/* 
-- 11/15/11
select rep."Name", max(rep."Tech") as "Tech", sum(rep."Flagged") as "Flagged", coalesce(sum(rep."Total Hours"), 0) as "Worked",
  case when sum(rep."Total Hours") <> 0 then round(sum(rep."Flagged") / sum(rep."Total Hours") * 100, 1) else 0 end as "Prod"  
from
(select base."Year", base."Month", base."Week", base."Date", base."Store", base."EmpNum", base."Name", coalesce(flag."Flagged", 0) as "Flagged", flag."Tech", coalesce(hours."Total Hours", 0) as "Total Hours"  
from 
(select distinct(yiclkind) as "Date", year(yiclkind) as "Year", month(yiclkind) as "Month", week(yiclkind) as "Week", emps.ymco# as "Store", emps.ymempn as "EmpNum", emps.ymname as "Name"
from rydedata.pypclockin, (select ymco#, ymdept, ymdist, ymempn, ymname, ymrate from rydedata.pymast) as emps) as base
left join
(select 
 date(substr(trim(cast(x."Date" as char(12))), 5, 2) || '/' || right(trim(cast(x."Date" as char(12))), 2) || '/' || left(trim(cast(x."Date" as char(12))), 4)) as "Date", x."Tech", p.ymempn as "EmpNum", trim(p.ymname) as "Name", sum(x."Flagged") as "Flagged"
from
 (select xxx.ptdate as "Date", xxx.pttech as "Tech", sum(xxx.ptlhrs) as "Flagged"
  from 
   (select d.ptco#, ptdoc# as ptro#, ptline, ptseq#, d.ptdate, pttech, ptlhrs
    from rydedata.pdppdet d                                                                      
    inner join rydedata.pdpphdr h on h.ptco# = d.ptco# and h.ptpkey = d.ptpkey                 
    where                                                                                        
      ptcode = 'TT'
      and ptspcd <> 'V' -- void?     
      and d.ptdate <> 0
      and pttech in (select sttech                                                              
                     from rydedata.sdptech t                                                     
                       inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn 
                     where t.stco# = 'RY1'
                       and p.ymdist in ('STEC', 'PR/S')
                       and not left(sttech, 1) in ('H', 'M'))
    union                              
    select ptco#, ptro#, ptline, ptseq#, ptdate, pttech, ptlhrs
    from rydedata.sdprdet                                                                        
    where --ptdate = 20111010                                                                    
      ptcode = 'TT'
      and ptdbas <> 'V' -- void? 
      and ptdate <> 0
      and pttech in (select sttech                                                            
                     from rydedata.sdptech t                                                     
                     inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn   
                     where t.stco# = 'RY1'
                       and p.ymdist in ('STEC', 'PR/S')
                       and not left(sttech, 1) in ('H', 'M'))) as xxx
    group by xxx.ptdate, xxx.pttech, xxx.ptro#) as x
  inner join rydedata.sdptech s on s.stco# = 'RY1' and s.sttech = x."Tech"                     
  inner join rydedata.pymast p on p.ymco# = 'RY1' and p.ymempn = s.stpyemp#
group by x."Date", x."Tech", p.ymempn, p.ymname) as flag on flag."EmpNum" = base."EmpNum" and flag."Date" = base."Date"
left join
   (select z."Date", z."EmpNum", trunc(cast(sum(z."Reg Hours") as double), 2) as "Reg Hours", trunc(cast(sum(z."OT Hours") as double), 2) as "OT Hours", 
      trunc(cast(sum(z."Reg Hours") + sum(z."OT Hours") as double), 2) as "Total Hours",  trunc(cast(round(sum(z."Reg Hours") * ymrate, 2) as double), 2) as "Reg Pay", 
      trunc(cast(round(sum(z."OT Hours") * ymrate * 1.5, 2) as double), 2) as "OT Pay", trunc(cast(round(sum(z."Reg Hours") * ymrate + sum(z."OT Hours") * ymrate * 1.5, 2) as double), 2) as "Total Pay"
    from
    -- z
     (select a."Store", a."EmpNum", a."Year", a."Week", a."Date", coalesce(sum(b."Hours"), 0) as "Total Hours Before", a."Hours" as "Hours Today", coalesce(sum(b."Hours"), 0) + a."Hours" as "Total Hours After",
        case when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then a."Hours" when coalesce(sum(b."Hours"), 0) > 40 then 0 else 40 - coalesce(sum(b."Hours"), 0) end as "Reg Hours",
        case when coalesce(sum(b."Hours"), 0) >= 40 then a."Hours" when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then 0 else a."Hours" - (40 - coalesce(sum(b."Hours"), 0)) end as "OT Hours"
      from 
       -- a
       (select yico# as "Store", yiemp# as "EmpNum", year(yiclkind) as "Year", week(yiclkind) as "Week", yiclkind as "Date", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin
        where trim(yicode) = 'O' 
--          and yiclkind >= '10/02/2011' and yiclkind <= '10/08/2011'
        group by yico#, yiemp#, yiclkind) as a
       -- a
      left join 
       -- b
       (select yico# as "Store", yiemp# as "EmpNum", year(yiclkind) as "Year", week(yiclkind) as "Week", yiclkind as "Date", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin
        where trim(yicode) = 'O' 
--         and yiclkind >= '10/02/2011' and yiclkind <= '10/08/2011'
        group by yico#, yiemp#, yiclkind) as b on a."Store" = b."Store" and a."EmpNum" = b."EmpNum" and year(a."Date") = year(b."Date") and week(a."Date") = week(b."Date") and b."Date" < a."Date"
       -- b
      group by a."Year", a."Week", a."Store", a."EmpNum", a."Date", a."Hours"
      order by a."Year", a."Week", a."Store", a."EmpNum", a."Date") as z
      -- z
    left join rydedata.pymast py on py.ymco# = z."Store" and py.ymempn = z."EmpNum" 
--    where z."Date" >= '10/02/2011' and z."Date" <= '10/08/2011' 
    group by z."Date", z."EmpNum", py.ymrate) as hours on base."EmpNum" = hours."EmpNum" and base."Date" = hours."Date"
-- where base."Date" >= :BeginDate and base."Date" <= :EndDate ) as rep
  where base."Date" between cast('2011-11-15' as date) and cast('2011-11-16' as date)) as rep
group by rep."Name" --, rep."Tech"
having max(rep."Tech") is not null
--order by max(rep."Tech")
order by case when sum(rep."Total Hours") <> 0 then round(sum(rep."Flagged") / sum(rep."Total Hours") * 100, 1) else 0 end desc
*/
-- 11/18/11 - modified version

/*
select curdate(), curdate() - dayofweek(curdate())  days 
from sysibm.sysdummy1 

Today = select curdate() from sysibm.sysdummy1
Yesterday = select curdate() - 1 day from sysibm.sysdummy1
First day of current week = select curdate() - (dayofweek(curdate()) - 1) days from sysibm.sysdummy1
First day of last week = select curdate() - (dayofweek(curdate()) - 1 + 7) days from sysibm.sysdummy1 or select curdate() - (dayofweek(curdate()) + 6) days from sysibm.sysdummy1
Last day of last week = select curdate() - dayofweek(curdate()) days from sysibm.sysdummy1
First day of current month = select curdate() - (dayofmonth(curdate()) - 1) days from sysibm.sysdummy1
Last day of current month = select curdate() + 1 month - (dayofmonth(curdate() + 1 month)) days from sysibm.sysdummy1
First day of last month = select curdate() - 1 month - (dayofmonth(curdate() - 1 month) - 1) days from sysibm.sysdummy1
Last day of last month = select curdate() - dayofmonth(curdate() - 1 month) days from sysibm.sysdummy1
*/

select rep."Name", max(rep."Tech") as "Tech", 
  sum(case when rep."Date" = curdate() - 1 day then rep."Flagged" else 0 end) as "Yesterday F", 
  sum(case when rep."Date" = curdate() - 1 day then coalesce(rep."Total Hours", 0) else 0 end) as "Yesterday W",
  case 
    when sum(
      case 
        when rep."Date" = curdate() - 1 day then coalesce(rep."Total Hours", 0) 
        else 0 
      end) <> 0 then round(sum(
        case 
          when rep."Date" = curdate() - 1 day then rep."Flagged" 
          else 0 
        end) / sum(
        case 
          when rep."Date" = curdate() - 1 day then coalesce(rep."Total Hours", 0) 
          else 0 
        end) * 100, 1) else 0 end as "Yesterday P",

  sum(
    case 
      when rep."Date" >= curdate() - (dayofweek(curdate()) - 1) days and rep."Date" <= curdate() - 1 day then rep."Flagged" 
      else 0 
    end) as "This Week F",
  sum(
    case 
      when rep."Date" >= curdate() - (dayofweek(curdate()) - 1) days and rep."Date" <= curdate() - 1 day then coalesce(rep."Total Hours", 0) 
      else 0 
    end) as "This Week W",
  case 
    when sum(
      case 
        when rep."Date" >= curdate() - (dayofweek(curdate()) - 1) days and rep."Date" <= curdate() - 1 day then coalesce(rep."Total Hours", 0) 
        else 0
      end) <> 0 then round(sum(
        case 
          when rep."Date" >= curdate() - (dayofweek(curdate()) - 1) days and rep."Date" <= curdate() - 1 day then rep."Flagged" 
          else 0 
        end) / sum(
          case 
            when rep."Date" >= curdate() - (dayofweek(curdate()) - 1) days and rep."Date" <= curdate() - 1 day then coalesce(rep."Total Hours", 0) 
            else 0
          end) * 100, 1) else 0 end as "This Week P",
  sum(
    case 
      when rep."Date" >= curdate() - (dayofweek(curdate()) - 1 + 7) days and rep."Date" <= curdate() - dayofweek(curdate()) days then rep."Flagged" 
      else 0 
    end) as "Last Week F",
  sum(
    case 
      when rep."Date" >= curdate() - (dayofweek(curdate()) - 1 + 7) days and rep."Date" <= curdate() - dayofweek(curdate()) days then coalesce(rep."Total Hours", 0) 
      else 0 
    end) as "Last Week W",
  case 
    when sum(
      case 
        when rep."Date" >= curdate() - (dayofweek(curdate()) - 1 + 7) days and rep."Date" <= curdate() - dayofweek(curdate()) days then coalesce(rep."Total Hours", 0) 
        else 0 
      end) <> 0 then round(sum(
        case 
          when rep."Date" >= curdate() - (dayofweek(curdate()) - 1 + 7) days and rep."Date" <= curdate() - dayofweek(curdate()) days then rep."Flagged" 
          else 0 
        end) / sum(
          case 
            when rep."Date" >= curdate() - (dayofweek(curdate()) - 1 + 7) days and rep."Date" <= curdate() - dayofweek(curdate()) days then coalesce(rep."Total Hours", 0) 
            else 0 
          end) * 100, 1) 
    else 0 end as "Last Week P",
  sum(
    case 
      when rep."Date" >= curdate() - (dayofmonth(curdate()) - 1) days and rep."Date" <= curdate() - 1 day then rep."Flagged" 
      else 0 
    end) as "MTD F",
  sum(
    case 
      when rep."Date" >= curdate() - (dayofmonth(curdate()) - 1) days and rep."Date" <= curdate() - 1 day then coalesce(rep."Total Hours", 0) 
      else 0 
    end) as "MTD W",
  case 
    when sum(
      case 
        when rep."Date" >= curdate() - (dayofmonth(curdate()) - 1) days and rep."Date" <= curdate() - 1 day then coalesce(rep."Total Hours", 0) 
        else 0 
      end) <> 0 then round(sum(
        case 
          when rep."Date" >= curdate() - (dayofmonth(curdate()) - 1) days and rep."Date" <= curdate() - 1 day then rep."Flagged" 
          else 0 
        end) / sum(
          case 
            when rep."Date" >= curdate() - (dayofmonth(curdate()) - 1) days and rep."Date" <= curdate() - 1 day then coalesce(rep."Total Hours", 0) 
            else 0 
          end) * 100, 1) 
    else 0 end as "MTD P",
    sum(case when rep."Date" >= curdate() - 1 month - (dayofmonth(curdate() - 1 month) - 1) days and rep."Date" <= curdate() - dayofmonth(curdate() - 1 month) days then rep."Flagged" else 0 end) as "Last Month F",
  sum(case when rep."Date" >= curdate() - 1 month - (dayofmonth(curdate() - 1 month) - 1) days and rep."Date" <= curdate() - dayofmonth(curdate() - 1 month) days then coalesce(rep."Total Hours", 0) else 0 end) as "Last Month W",
  case when sum(case when rep."Date" >= curdate() - 1 month - (dayofmonth(curdate() - 1 month) - 1) days and rep."Date" <= curdate() - dayofmonth(curdate() - 1 month) days then coalesce(rep."Total Hours", 0) else 0 end) <> 0 then 
    round(sum(case when rep."Date" >= curdate() - 1 month - (dayofmonth(curdate() - 1 month) - 1) days and rep."Date" <= curdate() - dayofmonth(curdate() - 1 month) days
    then rep."Flagged" else 0 end) / sum(case when rep."Date" >= curdate() - 1 month - (dayofmonth(curdate() - 1 month) - 1) days and rep."Date" <= curdate() - dayofmonth(curdate() - 1 month) days then coalesce(rep."Total Hours", 0) else 0 end) * 100, 1) else 0 end as "Last Month P"
from
(

select base."Year", base."Month", base."Week", base."Date", base."Store", base."EmpNum", base."Name", coalesce(flag."Flagged", 0) as "Flagged", flag."Tech", coalesce(hours."Total Hours", 0) as "Total Hours"  
from 
(

select distinct(yiclkind) as "Date", year(yiclkind) as "Year", month(yiclkind) as "Month", week(yiclkind) as "Week", emps.ymco# as "Store", emps.ymempn as "EmpNum", emps.ymname as "Name"
from rydedata.pypclockin, (select ymco#, ymdept, ymdist, ymempn, ymname, ymrate from rydedata.pymast) as emps) as base
left join
(

select 
 date(substr(trim(cast(x."Date" as char(12))), 5, 2) || '/' || right(trim(cast(x."Date" as char(12))), 2) || '/' || left(trim(cast(x."Date" as char(12))), 4)) as "Date", 
x."Tech", p.ymempn as "EmpNum", trim(p.ymname) as "Name", sum(x."Flagged") as "Flagged"
from
(

  select xxx.ptdate as "Date", xxx.pttech as "Tech", sum(xxx.ptlhrs) as "Flagged"
  from 
   (select d.ptco#, ptdoc# as ptro#, ptline, ptseq#, d.ptdate, pttech, ptlhrs
    from rydedata.pdppdet d                                                                      
    inner join rydedata.pdpphdr h on h.ptco# = d.ptco# and h.ptpkey = d.ptpkey                 
    where                                                                                        
      ptcode = 'TT'
      and ptspcd <> 'V' -- void?     
      and d.ptdate <> 0
      and pttech in (select sttech                                                              
                     from rydedata.sdptech t                                                     
                       inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn 
                     where t.stco# = 'RY1'
                       and p.ymdist in ('STEC', 'PR/S')
                       and not left(sttech, 1) in ('H', 'M'))
    union                              
    select ptco#, ptro#, ptline, ptseq#, ptdate, pttech, ptlhrs
    from rydedata.sdprdet                                                                        
    where --ptdate = 20111010                                                                    
      ptcode = 'TT'
      and ptdbas <> 'V' -- void? 
      and ptdate <> 0
      and pttech in (select sttech                                                            
                     from rydedata.sdptech t                                                     
                     inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn   
                     where t.stco# = 'RY1'
                       and p.ymdist in ('STEC', 'PR/S')
                       and not left(sttech, 1) in ('H', 'M'))) as xxx
    group by xxx.ptdate, xxx.pttech, xxx.ptro#) as x
  inner join rydedata.sdptech s on s.stco# = 'RY1' and s.sttech = x."Tech"                     
  inner join rydedata.pymast p on p.ymco# = 'RY1' and p.ymempn = s.stpyemp#
group by x."Date", x."Tech", p.ymempn, p.ymname) as flag on flag."EmpNum" = base."EmpNum" and flag."Date" = base."Date"
left join (-- hours
    select z."Date", z."EmpNum", trunc(cast(sum(z."Reg Hours") as double), 2) as "Reg Hours", trunc(cast(sum(z."OT Hours") as double), 2) as "OT Hours", 
      trunc(cast(sum(z."Reg Hours") + sum(z."OT Hours") as double), 2) as "Total Hours",  trunc(cast(round(sum(z."Reg Hours") * ymrate, 2) as double), 2) as "Reg Pay", 
      trunc(cast(round(sum(z."OT Hours") * ymrate * 1.5, 2) as double), 2) as "OT Pay", trunc(cast(round(sum(z."Reg Hours") * ymrate + sum(z."OT Hours") * ymrate * 1.5, 2) as double), 2) as "Total Pay"
    from
    -- z
     (select a."Store", a."EmpNum", a."Year", a."Week", a."Date", coalesce(sum(b."Hours"), 0) as "Total Hours Before", a."Hours" as "Hours Today", coalesce(sum(b."Hours"), 0) + a."Hours" as "Total Hours After",
        case when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then a."Hours" when coalesce(sum(b."Hours"), 0) > 40 then 0 else 40 - coalesce(sum(b."Hours"), 0) end as "Reg Hours",
        case when coalesce(sum(b."Hours"), 0) >= 40 then a."Hours" when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then 0 else a."Hours" - (40 - coalesce(sum(b."Hours"), 0)) end as "OT Hours"
      from 
       -- a
       (select yico# as "Store", yiemp# as "EmpNum", year(yiclkind) as "Year", week(yiclkind) as "Week", yiclkind as "Date", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin
        where trim(yicode) = 'O' 
--          and yiclkind >= '10/02/2011' and yiclkind <= '10/08/2011'
          and yiclkind between '03/11/2012' and '03/24/2012'
        group by yico#, yiemp#, yiclkind) as a
       -- a
      left join 
       -- b
       (select yico# as "Store", yiemp# as "EmpNum", year(yiclkind) as "Year", week(yiclkind) as "Week", yiclkind as "Date", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin
        where trim(yicode) = 'O' 
--         and yiclkind >= '10/02/2011' and yiclkind <= '10/08/2011'
          and yiclkind between '03/11/2012' and '03/24/2012'
        group by yico#, yiemp#, yiclkind) as b on a."Store" = b."Store" and a."EmpNum" = b."EmpNum" and year(a."Date") = year(b."Date") and week(a."Date") = week(b."Date") and b."Date" < a."Date"
       -- b
      group by a."Year", a."Week", a."Store", a."EmpNum", a."Date", a."Hours"
      order by a."Year", a."Week", a."Store", a."EmpNum", a."Date") as z
      -- z
    left join rydedata.pymast py on py.ymco# = z."Store" and py.ymempn = z."EmpNum" 
--    where z."Date" >= '10/02/2011' and z."Date" <= '10/08/2011' 
    where z."Date" between '03/11/2012' and '03/24/2012'
    group by z."Date", z."EmpNum", py.ymrate) as hours on base."EmpNum" = hours."EmpNum" and base."Date" = hours."Date"
where base."Date" >= '03/11/2012' and base."Date" <= '03/24/2012' ) as rep
group by rep."Name" --, rep."Tech"
having max(rep."Tech") is not null
--order by max(rep."Tech")
order by case when sum(rep."Total Hours") <> 0 then round(sum(rep."Flagged") / sum(rep."Total Hours") * 100, 1) else 0 end desc



/* Dissecting it */

-- Z: hours
select "EmpNum", sum("Total Pay") from (
select z."Date", z."EmpNum", trunc(cast(sum(z."Reg Hours") as double), 2) as "Reg Hours", 
  trunc(cast(sum(z."OT Hours") as double), 2) as "OT Hours", 
  trunc(cast(sum(z."Reg Hours") + sum(z."OT Hours") as double), 2) as "Total Hours", 
  trunc(cast(round(sum(z."Reg Hours") * ymrate, 2) as double), 2) as "Reg Pay", 
  trunc(cast(round(sum(z."OT Hours") * ymrate * 1.5, 2) as double), 2) as "OT Pay", 
  trunc(cast(round(sum(z."Reg Hours") * ymrate + sum(z."OT Hours") * ymrate * 1.5, 2) as double), 2) as "Total Pay"
from ( -- z
  select a."Store", a."EmpNum", a."Year", a."Week", a."Date", coalesce(sum(b."Hours"), 0) as "Total Hours Before", 
    a."Hours" as "Hours Today", coalesce(sum(b."Hours"), 0) + a."Hours" as "Total Hours After",
    case 
      when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then a."Hours" when coalesce(sum(b."Hours"), 0) > 40 then 0 
      else 40 - coalesce(sum(b."Hours"), 0) 
    end as "Reg Hours",
    case 
      when coalesce(sum(b."Hours"), 0) >= 40 then a."Hours" when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then 0 
      else a."Hours" - (40 - coalesce(sum(b."Hours"), 0)) 
    end as "OT Hours"
  from ( -- a
    select yico# as "Store", yiemp# as "EmpNum", year(yiclkind) as "Year", week(yiclkind) as "Week", yiclkind as "Date", 
      sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
    from rydedata.pypclockin
    where trim(yicode) = 'O' 
      and yiclkind between '03/11/2012' and '03/24/2012'
    group by yico#, yiemp#, yiclkind) as a
    left join (-- b
      select yico# as "Store", yiemp# as "EmpNum", year(yiclkind) as "Year", week(yiclkind) as "Week", yiclkind as "Date", 
      sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
      from rydedata.pypclockin
      where trim(yicode) = 'O' 
        and yiclkind between '03/11/2012' and '03/24/2012'
      group by yico#, yiemp#, yiclkind) as b on a."Store" = b."Store" and a."EmpNum" = b."EmpNum" and year(a."Date") = year(b."Date") and week(a."Date") = week(b."Date") and b."Date" < a."Date"
    group by a."Year", a."Week", a."Store", a."EmpNum", a."Date", a."Hours"
    order by a."Year", a."Week", a."Store", a."EmpNum", a."Date") as z
    left join rydedata.pymast py on py.ymco# = z."Store" and py.ymempn = z."EmpNum" 
    where z."Date" between '03/11/2012' and '03/24/2012'
    group by z."Date", z."EmpNum", py.ymrate) x 
where trim("EmpNum") = '136170'
group by "EmpNum"





-- techs
select sttech, ymname                                                              
                     from rydedata.sdptech t                                                     
                       inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p. ymempn 
                     where t.stco# = 'RY1'
                       and p.ymdist in ('STEC', 'PR/S')
                       and not left(sttech, 1) in ('H', 'M')

select * from rydedata.sdptech where stco# = 'RY1' order by stname

-- flagged hours
select 
  date(substr(trim(cast(x."Date" as char(12))), 5, 2) || '/' || right(trim(cast(x."Date" as char(12))), 2) || '/' || left(trim(cast(x."Date" as char(12))), 4)) as "Date", 
  x."Tech", p.ymempn as "EmpNum", trim(p.ymname) as "Name", sum(x."Flagged") as "Flagged"
from ( 
  select xxx.ptdate as "Date", xxx.pttech as "Tech", sum(xxx.ptlhrs) as "Flagged"
  from (
    select d.ptco#, ptdoc# as ptro#, ptline, ptseq#, d.ptdate, pttech, ptlhrs
    from rydedata.pdppdet d                                                                      
    inner join rydedata.pdpphdr h on h.ptco# = d.ptco# and h.ptpkey = d.ptpkey                 
    where                                                                                        
      ptcode = 'TT'
      and ptspcd <> 'V' -- void?     
      and d.ptdate <> 0
      and pttech in (
        select sttech                                                              
        from rydedata.sdptech t                                                     
        inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn 
        where t.stco# = 'RY1'
        and p.ymdist in ('STEC', 'PR/S')
        and not left(sttech, 1) in ('H', 'M'))
    union                              
    select ptco#, ptro#, ptline, ptseq#, ptdate, pttech, ptlhrs
    from rydedata.sdprdet                                                                        
    where --ptdate = 20111010                                                                    
      ptcode = 'TT'
      and ptdbas <> 'V' -- void? 
      and ptdate <> 0
      and pttech in (
        select sttech                                                            
        from rydedata.sdptech t                                                     
        inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn   
        where t.stco# = 'RY1'
          and p.ymdist in ('STEC', 'PR/S')
          and not left(sttech, 1) in ('H', 'M'))) as xxx
    group by xxx.ptdate, xxx.pttech, xxx.ptro#) as x
  inner join rydedata.sdptech s on s.stco# = 'RY1' and s.sttech = x."Tech"                     
  inner join rydedata.pymast p on p.ymco# = 'RY1' and p.ymempn = s.stpyemp#
group by x."Date", x."Tech", p.ymempn, p.ymname