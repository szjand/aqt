-- previous 12 payperiod gross pay


gross pay : yhdtgp

-- and the actual export
select 
  cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
  trim(yhdemp) as yhdemp, yhdtgp AS TotalGross
from rydedata.pyptbdta a
inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.ypbnum_ = b.ypbnum_
where a.ypbcyy = 113
  and a.ybco# = 'RY1'
  and trim(b.yhdemp) in ('1124436',  '1149500',  '179750',   '152235',   '116185',  '152410',   '164015',   
        '1148065', '1106399',  '168400',   '194675',   '141085',   '1137590',  '1117960',  '136170',   '131370',   
        '1150400',    '1135410',  '1146989',  '113150',   '1107950',  '1134850',  '1133820',  '167580',   
        '161120',   '1133500')
use check date for ben and ben tech historical pay

select trim(yhdemp) as Emp, yhdcyy, sum(yhdtgp) 
from (
  Select yhdemp, yhdeyy, yhdemm, yhdedd, yhdtgp, yhdcyy,
  cast((2000 + yhdcyy) || '-' || yhdcmm || '-' || yhdcdd  as date) realdate
  from RYDEDATA.PYHSHDTA a
  where yhdcyy in (12, 13) -- check year
    --and yhdemm > 5
    and  trim(yhdemp) in ('1124436',  '1149500',  '179750',   '152235',   '116185',  '152410',   '164015',   
      '1148065', '1106399',  '168400',   '194675',   '141085',   '1137590',  '1117960',  '136170',   '131370',   
      '1150400',    '1135410',  '1146989',  '113150',   '1107950',  '1134850',  '1133820',  '167580',   
      '161120',   '1133500')) x
group by  trim(yhdemp) , yhdcyy

select * from rydedata.pyptbdta

-- flag hour adjustments
-- export insert statement into stgFlagTimeAdjustments

select 'XTIM' as source, z.*
from (
  select cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date) as theDate,
    ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, sum(ptlhrs) as hours
  from rydedata.sdpxtim
  where ptdate > 20121231
  group by cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date),
    ptco#, ptro#, ptline, pttech) z
where hours <> 0
union 
select  'ADJ' as source, a.*
from (
  select cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date),
    ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, sum(ptlhrs) as hours
  from rydedata.sdprdet
  --where ptpadj = 'X'
  where ptpadj <> ''
--    and ptdbas = 'V'
--    and ptco# = 'RY1'
    and ptdate > 20121231
    and ptltyp = 'L'
    and ptcode = 'TT'
  group by cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date), 
    ptco#, ptro#, ptline, pttech) a
where hours <> 0
union 
select 'NEG' as source,  
  cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date),
  ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, ptlhrs as hours
from rydedata.sdprdet
where ptdate > 20121231
  and ptlhrs < 0
  and ptdbas <> 'V'


select * from (
select 'XTIM' as source, z.*
from (
  select cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date) as theDate,
    ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, sum(ptlhrs) as hours
  from rydedata.sdpxtim
  where ptdate > 20121231
  group by cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date),
    ptco#, ptro#, ptline, pttech) z
where hours <> 0
union 
select  'ADJ' as source, a.*
from (
  select cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date),
    ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, sum(ptlhrs) as hours
  from rydedata.sdprdet
  --where ptpadj = 'X'
  where ptpadj <> ''
--    and ptdbas = 'V'
--    and ptco# = 'RY1'
    and ptdate > 20121231
    and ptltyp = 'L'
    and ptcode = 'TT'
  group by cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date), 
    ptco#, ptro#, ptline, pttech) a
where hours <> 0
union 
select 'NEG' as source,  
  cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date),
  ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, ptlhrs as hours
from rydedata.sdprdet
where ptdate > 20121231
  and ptlhrs < 0
  and ptdbas <> 'V'
) x where ro = '16139150'



-- time to figure out this fucking payroll shit, dates, etc

Select count(*) from RYDEDATA.PYHSCDTA


SELECT * FROM rydedata.PYHSHDTA  a
left join rydedata.pyhscdta b on a.ypbnum = b.ypbnum
  and trim(a.yhdemp) = trim(b.yhcemp)
WHERE trim(a.yhdemp) = '152235' 
AND a.ypbnum in (705130, 621130)

select ypbnum_, 
from rydedata.pyptbdta
where ypbcyy in (112, 113)
  and ybco# in ('RY1','RY2')

select *
from rydedata.pyptbdta
where ypbnum_ = 131000

select a.*
from (
  select ybco#, ypbcyy, ypbnum_, 
    cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
    cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
    cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
  from rydedata.pyptbdta
  where ypbcyy in (112, 113)
    and ybco# in ('RY1','RY2')) a
order by ybco#, ypbcyy, ypbnum_

select ytdco#, ytddcd, ytddsc
from rydedata.pypcodes
where ytdtyp = '1'
  and ytdco# in ('RY1','RY2')


SELECT yhdco#, ypbcyy, ypbnum AS batch, yhdseq, trim(yhdemp) as yhdemp,
  yhdtgp AS TotalGross,
  yhdtga AS TotalAdjGross,
  yhdhrs AS RegHours,yhdoth AS OTHours,
cast((2000 + yhdeyy) || '-' || yhdemm || '-' || yhdedd  as date) as endDate,
cast((2000 + yhdcyy) || '-' || yhdcmm || '-' || yhdcdd  as date)  as checkDate
FROM rydedata.PYHSHDTA a 
where trim(yhdemp) in ('1124436',  '1149500',  '179750',   '152235',   '116185',  '152410',   '164015',   
      '1148065', '1106399',  '168400',   '194675',   '141085',   '1137590',  '1117960',  '136170',   '131370',   
      '1150400',    '1135410',  '1146989',  '113150',   '1107950',  '1134850',  '1133820',  '167580',   
      '161120',   '1133500')
  and ypbcyy in (112, 113)

select yhcco#, ypbcyy, ypbnum, trim(yhcemp) as yhcemp, yhccde, yhccam, yhccea, yhccds,
  ytddsc
from rydedata.pyhscdta a
inner join rydedata.pypcodes b on a.yhcco# = ytdco# and trim(a.yhccde) = trim(b.ytddcd)
where ytdtyp = '1'
  and ytdco# in ('RY1','RY2')
  and a.ypbcyy in (112,113)


-- girodat
select c.ymname, b.yhdemp, b.yhrate, b.totalgross, b.totaladjgross, b.reghours, b.othours,
  a.ypbnum_, a.ybseq, a.startdate, a.enddate, a.checkdate, 
  d.*
from (
  select ybco#, ypbcyy, ypbnum_, ybseq, 
    cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
    cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
    cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
  from rydedata.pyptbdta
  where ypbcyy in (112, 113)
    and ybco# in ('RY1','RY2')) a
inner join (
  SELECT yhdco#, ypbcyy, ypbnum AS batch, yhdseq, trim(yhdemp) as yhdemp, yhrate,
    yhdtgp AS TotalGross,
    yhdtga AS TotalAdjGross,
    yhdhrs AS RegHours,yhdoth AS OTHours,
  cast((2000 + yhdeyy) || '-' || yhdemm || '-' || yhdedd  as date) endDate,
  cast((2000 + yhdcyy) || '-' || yhdcmm || '-' || yhdcdd  as date) checkDate
  FROM rydedata.PYHSHDTA a 
  where trim(yhdemp) in ('1124436',  '1149500',  '179750',   '152235',   '116185',  '152410',   '164015',   
        '1148065', '1106399',  '168400',   '194675',   '141085',   '1137590',  '1117960',  '136170',   '131370',   
        '1150400',    '1135410',  '1146989',  '113150',   '1107950',  '1134850',  '1133820',  '167580',   
        '161120',   '1133500')) b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.ypbnum = b.batch
left join rydedata.pymast c on b.yhdemp = trim(c.ymempn)
left join (
  select yhcco#, ypbcyy, ypbnum, trim(yhcemp) as yhcemp, yhccde, yhccam, yhccea, yhccds,
    ytddsc
  from rydedata.pyhscdta a
  inner join rydedata.pypcodes b on a.yhcco# = ytdco# and trim(a.yhccde) = trim(b.ytddcd)
  where ytdtyp = '1'
    and trim(yhccde) <> '83'
    and ytdco# in ('RY1','RY2')
    and a.ypbcyy in (112,113)) d on a.ybco# = d.yhcco# and a.ypbcyy = d.ypbcyy and a.ypbnum_ = d.ypbnum and b.yhdemp = d.yhcemp
where b.yhdemp = '152235'
order by a.checkdate desc


-- what are the distinct codes i'm looking at here
select ybco#, yhccde, yhccds, ytddsc from (
select a.ybco#, c.ymname, b.yhdemp, b.yhrate, b.totalgross, b.totaladjgross, b.reghours, b.othours,
  a.ypbnum_, a.ybseq, a.startdate, a.enddate, a.checkdate, 
  d.*
from (
  select ybco#, ypbcyy, ypbnum_, ybseq, 
    cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
    cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
    cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
  from rydedata.pyptbdta
  where ypbcyy in (112, 113)
    and ybco# in ('RY1','RY2')) a
inner join (
  SELECT yhdco#, ypbcyy, ypbnum AS batch, yhdseq, trim(yhdemp) as yhdemp, yhrate,
    yhdtgp AS TotalGross,
    yhdtga AS TotalAdjGross,
    yhdhrs AS RegHours,yhdoth AS OTHours,
  cast((2000 + yhdeyy) || '-' || yhdemm || '-' || yhdedd  as date) endDate,
  cast((2000 + yhdcyy) || '-' || yhdcmm || '-' || yhdcdd  as date) checkDate
  FROM rydedata.PYHSHDTA a 
  where trim(yhdemp) in ('1124436',  '1149500',  '179750',   '152235',   '116185',  '152410',   '164015',   
        '1148065', '1106399',  '168400',   '194675',   '141085',   '1137590',  '1117960',  '136170',   '131370',   
        '1150400',    '1135410',  '1146989',  '113150',   '1107950',  '1134850',  '1133820',  '167580',   
        '161120',   '1133500')) b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.ypbnum = b.batch
left join rydedata.pymast c on b.yhdemp = trim(c.ymempn)
left join (
  select yhcco#, ypbcyy, ypbnum, trim(yhcemp) as yhcemp, yhccde, yhccam, yhccea, yhccds,
    ytddsc
  from rydedata.pyhscdta a
  inner join rydedata.pypcodes b on a.yhcco# = ytdco# and trim(a.yhccde) = trim(b.ytddcd)
  where ytdtyp = '1'
    and trim(yhccde) <> '83'
    and ytdco# in ('RY1','RY2')
    and a.ypbcyy in (112,113)) d on a.ybco# = d.yhcco# and a.ypbcyy = d.ypbcyy and a.ypbnum_ = d.ypbnum and b.yhdemp = d.yhcemp
) x where yhccde is not null group by ybco#, yhccde, yhccds, ytddsc

the yhccde of interest:
66 training
75 rate adjustment
vac vacation
pto pto
hol
79 commissions


-- girodat
-- limit pyhscdta to 66, 75, 79, vac, pto
select c.ymname, b.yhdemp, b.yhrate, b.totalgross, b.totaladjgross, b.reghours, b.othours,
  a.ypbnum_, a.ybseq, a.startdate, a.enddate, a.checkdate, 
  coalesce(sum(case when trim(yhccde) = 'VAC' then yhccea end), 0) as VacHours,
  coalesce(sum(case when trim(yhccde) = 'VAC' then yhccam end), 0) as VacPay,
  coalesce(sum(case when trim(yhccde) = 'PTO' then yhccea end), 0) as PtoHours,
  coalesce(sum(case when trim(yhccde) = 'PTO' then yhccam end), 0) as PtoPay,
  coalesce(sum(case when trim(yhccde) = 'HOL' then yhccea end), 0) as HolHours,
  coalesce(sum(case when trim(yhccde) = 'HOL' then yhccam end), 0) as HolPay,
  coalesce(sum(case when trim(yhccde) = '66' then yhccam end), 0) as TrainingPay,
  coalesce(sum(case when trim(yhccde) = '75' then yhccam end), 0) as RateAdjPay,
  coalesce(sum(case when trim(yhccde) = '79' then yhccam end), 0) as CommPay
from (
  select ybco#, ypbcyy, ypbnum_, ybseq, 
    cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
    cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
    cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
  from rydedata.pyptbdta
  where ypbcyy in (113)
    and ybco# in ('RY1','RY2')) a
inner join (
  SELECT yhdco#, ypbcyy, ypbnum AS batch, yhdseq, trim(yhdemp) as yhdemp, yhrate,
    yhdtgp AS TotalGross,
    yhdtga AS TotalAdjGross,
    yhdhrs AS RegHours,yhdoth AS OTHours,
  cast((2000 + yhdeyy) || '-' || yhdemm || '-' || yhdedd  as date) endDate,
  cast((2000 + yhdcyy) || '-' || yhdcmm || '-' || yhdcdd  as date) checkDate
  FROM rydedata.PYHSHDTA a 
  where trim(yhdemp) in ('1124436',  '1149500',  '179750',   '152235',   '116185',  '152410',   '164015',   
        '1148065', '1106399',  '168400',   '194675',   '141085',   '1137590',  '1117960',  '136170',   '131370',   
        '1150400',    '1135410',  '1146989',  '113150',   '1107950',  '1134850',  '1133820',  '167580',   
        '161120',   '1133500')
    and yhdtgp <> 0) b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.ypbnum = b.batch
left join rydedata.pymast c on b.yhdemp = trim(c.ymempn)
left join (
  select yhcco#, ypbcyy, ypbnum, trim(yhcemp) as yhcemp, yhccde, yhccam, yhccea, yhccds,
    ytddsc
  from rydedata.pyhscdta a
  inner join rydedata.pypcodes b on a.yhcco# = ytdco# and trim(a.yhccde) = trim(b.ytddcd)
--  where ytdtyp <> '2'
--    and trim(yhccde) <> '83'
  where trim(yhccde) in ('66','75','79','VAC','PTO', 'HOL')
    and ytdco# in ('RY1','RY2')
    and a.ypbcyy in (112,113)) d on a.ybco# = d.yhcco# and a.ypbcyy = d.ypbcyy and a.ypbnum_ = d.ypbnum and b.yhdemp = d.yhcemp
where b.yhdemp = '152235'
group by c.ymname, b.yhdemp, b.yhrate, b.totalgross, b.totaladjgross, b.reghours, b.othours,
  a.ypbnum_, a.ybseq, a.startdate, a.enddate, a.checkdate
order by a.checkdate desc



-- ok, so the whole point of this exercise is to derive previous 12 payperiod totalgross/total hours
-- the good news, is that the pyptbdta.ybstart conicides with day.biweeklypayperiodstartdate

-- shit fuck 1
shit fucking pyhscdta code of HOL is not in pypcodes
select * from rydedata.pyhscdta where ypbnum = 913130 and trim(yhcemp) = '152235'

select * from rydedata.pyhshdta where ypbnum_ = 913130 and trim(yhdemp) = '152235'

select * from (
  select yhcco#, ypbcyy, ypbnum, trim(yhcemp) as yhcemp, yhccde, yhccam, yhccea, yhccds,
    ytddsc
  from rydedata.pyhscdta a
  inner join rydedata.pypcodes b on a.yhcco# = ytdco# and trim(a.yhccde) = trim(b.ytddcd)
--  where ytdtyp <> '2'
--    and trim(yhccde) <> '83'
  where trim(yhccde) in ('66','75','79','VAC','PTO', 'HOL')
    and ytdco# in ('RY1','RY2')
    and a.ypbcyy in (112,113)
) x where trim(yhcemp) = '152235'  and ypbnum = 913130

select * from rydedata.pypcodes where ytdco# = 'RY1'

-- shit fuck 2
batch 802130 
start 7/14/13
checkdate 8/2/13
pyhshdta shows 54.5 hours
factClockHours shows 73 hours
biweekly 7/14 - 7/27
ot and pto are ok

select *
from rydedata.pypclkl
where trim(ylemp#) = '152235'
  and ylclkind > '2013-07-01'

select a.*
from rydedata.pypclockin a
where trim(yiemp#) = '152235'
  and yiclkind between '2013-07-14' and '2013-07-27'


-- time clock agrees with factClockHours,
-- do not know how the fuck pyhshdta gets the lower number of hours
select yico# as "Company", yiemp# as "EmpNum", week(yiclkind) as "Week", yiclkind as "Day", 
  sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
  timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
from rydedata.pypclockin
where trim(yicode) = 'O' 
  and yiclkind between '2013-07-14' and '2013-07-27'
  and trim(yiemp#) = '152235'
group by yico#, yiemp#, yiclkind




-- 12/16
the key change is using pyptbdta to get pay period start dates

select * from rydedata.pyhshdta

select a.ybco#, a.ypbcyy, a.ypbnum_, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp,
  cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
  cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
from rydedata.pyptbdta a
inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.ypbnum_ = b.ypbnum_
where a.ypbcyy = 113
  and a.ybco# = 'RY1'
  and trim(b.yhdemp) in ('1124436',  '1149500',  '179750',   '152235',   '116185',  '152410',   '164015',   
        '1148065', '1106399',  '168400',   '194675',   '141085',   '1137590',  '1117960',  '136170',   '131370',   
        '1150400',    '1135410',  '1146989',  '113150',   '1107950',  '1134850',  '1133820',  '167580',   
        '161120',   '1133500')

-- and the actual export
select 
  cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
  trim(yhdemp) as yhdemp, yhdtgp AS TotalGross
from rydedata.pyptbdta a
inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.ypbnum_ = b.ypbnum_
where a.ypbcyy = 113
  and a.ybco# = 'RY1'
  and trim(b.yhdemp) in ('1124436',  '1149500',  '179750',   '152235',   '116185',  '152410',   '164015',   
        '1148065', '1106399',  '168400',   '194675',   '141085',   '1137590',  '1117960',  '136170',   '131370',   
        '1150400',    '1135410',  '1146989',  '113150',   '1107950',  '1134850',  '1133820',  '167580',   
        '161120',   '1133500')
order by startdate desc,  yhdemp desc


SELECT ptro#, ptline
FROM rydedata.SDPRDET
WHERE ptltyp = 'A'
  and length(trim(ptro#)) > 6
GROUP BY ptro#, ptline
HAVING COUNT(*) > 1


select a.ymempn, a.ymname
from rydedata.pymast a
where ymdist = 'SALE'
  and ymco# = 'RY1'
  and ymactv <> 'T'
  and ymname <> 'ADRIAN, JOEL S'
  and ymname not like 'DALEN%'
  and ymname not like 'KACH%'
and ymname not like 'DULAN%'
and ymname not like 'PEARSON, ROB%'
and ymname not like 'ROEM%'
and ymname not like 'KUHN%'
and ymname not like 'HOGEN%'   
order by ymname


so, for these folks, what was the total gross pay from 12/1/12 thru 11/30/13
-- here is basic total gross for each employee with real dates
select a.ybco#, a.ypbcyy, a.payroll_run_number, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp,
  cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
  cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
from rydedata.pyptbdta a
inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.payroll_run_number = b.ypbnum_


select ymname as "name", sum(totalgross) as "total gross"
from (-- here is basic total gross for each employee with real dates
  select a.ybco#, a.ypbcyy, a.payroll_run_number, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp,
    cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
    cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
  cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
  from rydedata.pyptbdta a
  inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.payroll_run_number = b.ypbnum_) a
inner join (
  select a.ymempn, a.ymname
  from rydedata.pymast a
  where ymdist = 'SALE'
    and ymco# = 'RY1'
    and ymactv <> 'T'
    and ymname <> 'ADRIAN, JOEL S'
    and ymname not like 'DALEN%'
    and ymname not like 'KACH%'
  and ymname not like 'DULAN%'
  and ymname not like 'PEARSON, ROB%'
  and ymname not like 'ROEM%'
  and ymname not like 'KUHN%'
  and ymname not like 'HOGEN%') b on trim(a.yhdemp) = trim(b.ymempn)
where a.checkdate between '12/01/2012' and '11/30/2013'
group by ymname

