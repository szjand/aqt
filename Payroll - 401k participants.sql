select --a.code_type, a.code_id, 
  a.description as TYPE_OF_DEDUCTION, b.employee_name,
  b.address_1, b.city_name, b.state_code_address_, b.zip_code
from rydedata.pyhscdta a
inner join rydedata.pymast b on a.employee_number = b.pymast_employee_number
  and b.active_code <> 'T'
where a.payroll_cen_year = 118
  and a.code_id in ('91','95','99','98')
group by a.code_type, a.code_id, a.description, b.employee_name,
  b.address_1, b.city_name, b.state_code_address_, b.zip_code
order by employee_name  