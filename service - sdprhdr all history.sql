Select * from RYDEDATA.SDPRHDR 

Select * from RYDEDATA.SDPRHDR where ro_type <> '' order by open_date desc

Select * from RYDEDATA.SDPRHDR where left(open_date,4) = 1995

Select min(open_date) from RYDEDATA.SDPRHDR 

select left(open_date, 4) from RYDEDATA.SDPRHDR 

Select count(*) from RYDEDATA.SDPRHDR where ptcnam like '*VOI%'

Select company_number, left(open_date, 4), count(*) 
-- select count(*)
from RYDEDATA.SDPRHDR 
where ptcnam not like '*VOI%'
  and company_number in ('RY1','RY2')
  and left(open_date, 4) > 2004
group by company_number, left(open_date, 4)


-- the migrated data looks ok as far as customer relationship resolving 
select *
from RYDEDATA.SDPRHDR a
left join rydedata.bopname b on a.company_number = b.bopname_company_number
  and a.customer_key = b.bopname_record_key
where a.ptcnam not like '*VOI%'
  and a.company_number in ('RY1','RY2')
  and left(a.open_date, 4) > 2004
order by a.open_date  

Select distinct length(open_date) from RYDEDATA.SDPRHDR

select a.ro_number
--  select *
from RYDEDATA.SDPRHDR a
left join rydedata.bopname b on a.company_number = b.bopname_company_number
  and a.customer_key = b.bopname_record_key
where a.ptcnam not like '*VOI%'
  and a.company_number in ('RY1','RY2')
  and left(a.open_date, 4) > 2004
group by a.ro_number
having count(*) > 1 

select a.company_number, trim(a.ro_number), a.customer_key, trim(a.cust_name), 
  cast(left(digits(a.open_date), 4) || '-' || substr(digits(a.open_date), 5, 2) 
    || '-' || substr(digits(a.open_date), 7, 2) as date) as open_date,
  a.vin, b.zip_code
--  select *
from RYDEDATA.SDPRHDR a
left join rydedata.bopname b on a.company_number = b.bopname_company_number
  and a.customer_key = b.bopname_record_key
where a.ptcnam not like '*VOI%'
  and a.company_number in ('RY1','RY2')
  and left(a.open_date, 4) > 2004
order by a.open_date  
