Select 'RY1' as Store, pgsgrp, pgdesc, pgmanf
from pdpsgrp
where pgco# = 'RY1'
union
Select 'RY2' as Store, pgsgrp, pgdesc, pgmanf
from pdpsgrp
where pgco# = 'RY2'
union
Select 'RY3' as Store, pgsgrp, pgdesc, pgmanf
from pdpsgrp
where pgco# = 'RY3'
order by Store, pgsgrp

select pmco#, pmsgrp, count(*)
from pdpmast
group by pmco#, pmsgrp
order by pmco#, pmsgrp
