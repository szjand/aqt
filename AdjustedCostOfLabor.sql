select wtf1.*, wtf2.*
from (
select ymname, sum(xx1."Total Hours") as "ClockHours", sum(xx1."Total Pay") as "Pay"
from (
  select py.ymname, week(pt.yiclkind) as "Week", 
    sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) as "Total Hours",
    case when sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) > 40 
      then 40
      else sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) 
    end as "Reg Hours",
    case when sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) > 40 
      then Round(sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) - 40, 2)
      else 0 
    end as "OT Hours",
    case when sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) > 40 
      then Round(40 * Avg(py.ymrate), 2)
      else sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2) * py.ymrate) 
    end as "Reg Pay",
    case when sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) > 40 
    then Round((sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) - 40) * Min(py.ymrate * 1.5), 2)
      else 0 
    end as "OT Pay",
    Round(case when sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) > 40 
      then Round(40 * Avg(py.ymrate), 2)
    else sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2) * py.ymrate) 
    end + 
    case when sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) > 40 
    then Round((sum(Round(Real(left(digits(pt.yiclkoutt - pt.yiclkint), 2)) + Real(substr(digits(pt.yiclkoutt - pt.yiclkint), 3, 2)) / 60 + Real(right(digits(pt.yiclkoutt - pt.yiclkint), 2)) / 3600, 2)) - 40) * Min(py.ymrate * 1.5), 2)
      else 0 
    end, 2) as "Total Pay"
  from pypclockin pt
  left outer join pymast py on pt.YICO# = py.YMCO# and pt.YIEMP# = py.YMEMPN
  where pt.YICO# = 'RY1' and 
  trim(pt.yicode) = 'O' 
  and pt.YICLKIND between '8/1/2009' and '8/31/2009' 
  and trim(pt.YIEMP#) in (
    '4225','8005','16185','31370','36170','41085','45614','49112','52235','64015',
    '73315','79750','84878','94204','106399','94675','108248','113940','117960',
    '123220','124436','125855','135410','137590','137625','144265','148065','148497','149950')
  group by py.ymname, week(pt.YICLKIND)) as xx1
--order by py.ymname, week(pt.yiclkind)) as xx1
group by ymname) wtf1
left join (
select xx2.ymname, sum(xx2.ptlhrs) as "FlaggedHours", sum(xx2.ptcost) as "Cost" 
from ( 
  select py.ymname, line.ptdate, line.ptlhrs, line.ptcost 
  from sdprdet line 
  left join sdptech tech on tech.sttech = line.pttech 
  left join pymast py on tech.stpyemp# = py.ymempn 
  where line.ptco# = 'RY1' 
  and line.ptdate >= 20090801 
  and line.ptdate <= 20090831 
  and line.pttech in ( 
    select sttech 
    from sdptech 
    where trim(stpyemp#) in(
        '4225','8005','16185','31370','36170','41085','45614','49112',
        '52235','64015','73315','79750','84878','94204','106399','94675','108248','113940',
        '117960','123220','124436','125855','135410','137590','137625','144265','148065','148497','149950')) 
  union all 
  select py.ymname, line.ptdate, line.ptlhrs, line.ptcost 
  from pdppdet line 
  left join sdptech tech on tech.sttech = line.pttech 
  left join pymast py on tech.stpyemp# = py.ymempn 
  where line.ptco# = 'RY1' 
  and line.ptdate >= 20090801 
  and line.ptdate <= 20090831 
  and line.pttech in (
    select sttech 
    from sdptech 
    where trim(stpyemp#) in (
        '4225','8005','16185','31370','36170','41085','45614','49112',
        '52235','64015','73315','79750','84878','94204','106399','94675','108248','113940',
        '117960','123220','124436','125855','135410','137590','137625','144265','148065','148497','149950')) ) xx2 group by xx2.ymname ) wtf2 on wtf2.ymname = wtf1.ymname
        
  