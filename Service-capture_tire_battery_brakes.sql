-- honda 03/04
Select a.ptro#, a.ptdate, a.ptline, b.* 
from RYDEDATA.SDPRDET a
left join rydedata.sdprtxt b on trim(a.ptro#) = trim(b.sxro#) and a.ptline = b.sxline
  and (sxtext like '%PAS%' or sxtext liek '%FAIL%'
where trim(ptlopc) = '24Z'
  and a.ptdate > 20190303
order by a.ptro#,a.ptline

-- gm 02/18

Select distinct a.ptro#, 
  case
    when b.sxtext is null then 'NO'
    else 'YES'
  end has_it
from RYDEDATA.SDPRDET a
left join rydedata.sdprtxt b on trim(a.ptro#) = trim(b.sxro#) and a.ptline = b.sxline
  and (sxtext like '%PAS%' or sxtext like '%FAIL%')
where trim(ptlopc) = '23I'
  and a.ptdate > 20190217
  
  
select count(*), -- 407 of 788
  sum(case when has_it = 'YES' then 1 else 0 end) 
from (
  Select distinct a.ptro#, 
    case
      when b.sxtext is null then 'NO'
      else 'YES'
    end has_it
  from RYDEDATA.SDPRDET a
  left join rydedata.sdprtxt b on trim(a.ptro#) = trim(b.sxro#) and a.ptline = b.sxline
    and (sxtext like '%PAS%' or sxtext like '%FAIL%')
  where trim(ptlopc) = '23I'
    and a.ptdate > 20190217) a


Select distinct a.ptro#, 
  case
    when b.sxtext is null then 'NO'
    else 'YES'
  end has_it
from RYDEDATA.SDPRDET a
left join rydedata.sdprtxt b on trim(a.ptro#) = trim(b.sxro#) and a.ptline = b.sxline
  and (sxtext like '%PAS%' or sxtext like '%FAIL%')
where trim(ptlopc) = '24Z'
  and a.ptdate > 20190303
  
  
select count(*), -- 30 of 215
  sum(case when has_it = 'YES' then 1 else 0 end) 
from (
  Select distinct a.ptro#, 
    case
      when b.sxtext is null then 'NO'
      else 'YES'
    end has_it
  from RYDEDATA.SDPRDET a
  left join rydedata.sdprtxt b on trim(a.ptro#) = trim(b.sxro#) and a.ptline = b.sxline
    and (sxtext like '%PAS%' or sxtext like '%FAIL%')
  where trim(ptlopc) = '24Z'
    and a.ptdate > 20190303) a  
  
  
  
/*
4/30/19
  What would be the chances I could get you to run this reporting again for me? 
  I was looking for the inspections from 3/11/19 through 4/27/19. 
  The only difference is I wanted to add PDQ repair orders as well for both stores. 
  Is it something you can do by chance?
*/
/*

I didn�t realize we had PDQ in the last group of numbers as well. 
Are you able to add 23Q to the GM list because that is what PDQ uses for 
the inspection and the tires, batteries and brake measurements. 
That would explain why our tracking % was way down last time too. Thank you
*/

-- GM
Select distinct a.ptro#, 
  case
    when b.sxtext is null then 'NO'
    else 'YES'
  end has_it
from RYDEDATA.SDPRDET a
left join rydedata.sdprtxt b on trim(a.ptro#) = trim(b.sxro#) and a.ptline = b.sxline
  and (sxtext like '%PAS%' or sxtext like '%FAIL%')
where trim(ptlopc) in( '23I','23Q')
  and a.ptdate between 20190311 and 20190427
  
  
select count(*), -- 796 of 1516, with 23Q 1699 of 5116
  sum(case when has_it = 'YES' then 1 else 0 end) 
from (
  Select distinct a.ptro#, 
    case
      when b.sxtext is null then 'NO'
      else 'YES'
    end has_it
  from RYDEDATA.SDPRDET a
  left join rydedata.sdprtxt b on trim(a.ptro#) = trim(b.sxro#) and a.ptline = b.sxline
    and (sxtext like '%PAS%' or sxtext like '%FAIL%')
  where trim(ptlopc) in( '23I','23Q')
    and a.ptdate between 20190311 and 20190427) a
    
-- Honda Nissan

Select distinct a.ptro#, 
  case
    when b.sxtext is null then 'NO'
    else 'YES'
  end has_it
from RYDEDATA.SDPRDET a
left join rydedata.sdprtxt b on trim(a.ptro#) = trim(b.sxro#) and a.ptline = b.sxline
  and (sxtext like '%PAS%' or sxtext like '%FAIL%')
where trim(ptlopc) = '24Z'
  and a.ptdate between 20190311 and 20190427
  
  
select count(*), -- 202 of 797
  sum(case when has_it = 'YES' then 1 else 0 end) 
from (
  Select distinct a.ptro#, 
    case
      when b.sxtext is null then 'NO'
      else 'YES'
    end has_it
  from RYDEDATA.SDPRDET a
  left join rydedata.sdprtxt b on trim(a.ptro#) = trim(b.sxro#) and a.ptline = b.sxline
    and (sxtext like '%PAS%' or sxtext like '%FAIL%')
  where trim(ptlopc) = '24Z'
    and a.ptdate between 20190311 and 20190427) a      

  