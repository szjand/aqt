-- 2/4/16 yearly sales reflects cost, dan is interested in price
select "Part#", "On Hand", "Yearly $ Sales", "Yearly Units", 
  b.cost, b.list_price, b.trade_price, b.flat_price
from (
  SELECT coalesce(ps.pfaprt, pd.pmpart) as "Part#", sum(pmonhd) as "On Hand",
    int(sum((select coalesce(sum(tr.ptqty * tr.ptcost), 0)
             from rydedata.pdptdet tr
             where tr.ptpart = coalesce(ps.pfaprt, pd.pmpart)
             and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())
             and ptcode in ('CP', 'IS', 'SA', 'WS', 'SC')
            ))) as "Yearly $ Sales",
    sum((select coalesce(sum(tr.ptqty), 0)
         from rydedata.pdptdet tr
         where tr.ptpart = coalesce(ps.pfaprt, pd.pmpart)
           and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())
           and ptcode in ('CP', 'IS', 'SA', 'WS', 'SC')
        )) as "Yearly Units"
  FROM rydedata.pdpmast pd
    left join rydedata.pdpmrpl ps on pd.pmpart = ps.pfoprt and pd.pmco# = ps.pfco#
  WHERE pd.pmco# = 'RY1'
    AND trim(pd.pmnprt) = ''
    AND trim(pmgrpc) in ('01835', '01836')
  GROUP BY coalesce(ps.pfaprt, pd.pmpart) ) a
left join rydedata.pdpmast b on a."Part#" = b.pmpart
  and b.company_number = 'RY1' 
WHERE "On Hand" > 0 or "Yearly Units" > 0      
ORDER BY "Yearly Units" desc


PTCODE - Transaction Code
AP = Add Part
BO = Back Ordered
CM = Comment
CN = Cancelled
CP = RO Customer Pay Sale
CQ = Converted Quantity
CR = RO Correction
CS = RO Cause
DC = Discounts
DP = Delete Part
FR = Factory Return
GC = Stock Group Change
IA = Manual Inventory Adjustment
IS = RO Internal Sale
LA = Lifo Adjust
LS = Lost Sale
MP = Merged Part
OF = Fees
OR = Ordered
PA = Physical Inventory Adjust
PC = Part count from last physical inventory
PO = Purchase order
RC = Special Order Receipt
RC = Received
RO = Reorder
RS = Restocking charge
RT = Counter Return
SA = Counter Sale
SC = RO Service Contract Sale
SH = Shipping
SL = Sublet
SR = RO Return
TT = RO Tech Time
WS = RO Warranty sale
ZA = assign core part
ZR = remove core part

-- 2/15/16 i think dan wants to see list price vs actual sale price ?
-- hmm, look at accounting
select a.ptinv#, a.ptcode, a.ptdate, a.ptcdate, a.ptqty, a.ptcost, a.ptlist, a.ptnet,
  b.*
from rydedata.pdptdet a
left join rydedata.glptrns b on trim(a.ptinv#) = trim(b.gtdoc#)
-- from dds.gmfs_accounts
  and trim(b.gtacct) in ('146700','146707','146800','147700',    
    '147800','148004','148005','148104','148105','148134','148204',    
    '148304','148904','149004','149104','149131','149132','149133',    
    '149134','149135','149136','149137','149138','149141','149142')
where trim(a.ptpart) = '19254718'
  and a.ptdate > 20150000
  and a.ptcode in ('CP', 'IS', 'SA', 'WS', 'SC')
  
  
select *
from rydedata.glptrns
where trim(gtctl#) = '19205412'  



