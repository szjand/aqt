Select * 
from RYDEDATA.GLPCRHD

Select count(*) -- 51295
from RYDEDATA.GLPCRHD

-- gnrec# is unique
select gnrec#
from rydedata.glpcrhd
group by gnrec#
  having count(*) > 1


Select * 
from RYDEDATA.GLPCRHD h
left join rydedata.glpcrdt d on h.gnrec# = d.gtrec#
where gndate = current date 
order by h.gnrec#

GNCKEY
GNDATE
GNRAMT
GNTRN#

select min(GNTRN#), max(GNTRN#)
from rydedata.glpcrhd

-- final query for code
-- no alterations requ'd
select GNCO#, GNREC#, GNTYPE, GNCKEY, GNDATE, 
  GNRAMT, GNTRN#, GNUSER
from rydedata.glpcrhd
/******************** GLPCRDT ***************************/
select *
from rydedata.glpcrdt

select count(*) -- 171422
from rydedata.glpcrdt

-- natural key
select gtrec#, gtrseq#
from rydedata.glpcrdt
group by gtrec#, gtrseq#
  having count(*) > 1

select min(gtdate),max(gtdate)
from rydedata.glpcrdt

 