select company_number as store, employee_name as name, hire_date,
  case when term_date > current_date then null else term_date end as term_date,
  fixed_ded_amt as amount
from ( 
select * 
  from (
  Select a.company_number, b.employee_name, 
    case 
      when b.hire_date = 0 then date('9999-12-31')
      when cast(right(trim(b.hire_date),2) as integer) < 20 then 
        cast (
          case length(trim(b.hire_date))
            when 5 then  '20'||substr(trim(b.hire_date),4,2)||'-'|| '0' || left(trim(b.hire_date),1) || '-' ||substr(trim(b.hire_date),2,2)
            when 6 then  '20'||substr(trim(b.hire_date),5,2)||'-'|| left(trim(b.hire_date),2) || '-' ||substr(trim(b.hire_date),3,2)
          end as date) 
      when cast(right(trim(b.hire_date),2) as integer) >= 20 then  
        cast (
          case length(trim(b.hire_date))
            when 5 then  '19'||substr(trim(b.hire_date),4,2)||'-'|| '0' || left(trim(b.hire_date),1) || '-' ||substr(trim(b.hire_date),2,2)
            when 6 then  '19'||substr(trim(b.hire_date),5,2)||'-'|| left(trim(b.hire_date),2) || '-' ||substr(trim(b.hire_date),3,2)
          end as date) 
    end as hire_date,
    case 
      when b.termination_date = 0 then date('9999-12-31')
      when cast(right(trim(b.termination_date),2) as integer) < 20 then 
        cast (
          case length(trim(b.termination_date))
            when 5 then  '20'||substr(trim(b.termination_date),4,2)||'-'|| '0' || left(trim(b.termination_date),1) || '-' ||substr(trim(b.termination_date),2,2)
            when 6 then  '20'||substr(trim(b.termination_date),5,2)||'-'|| left(trim(b.termination_date),2) || '-' ||substr(trim(b.termination_date),3,2)
          end as date) 
      when cast(right(trim(b.hire_date),2) as integer) >= 20 then  
        cast (
          case length(trim(b.hire_date))
            when 5 then  '19'||substr(trim(b.termination_date),4,2)||'-'|| '0' || left(trim(b.termination_date),1) || '-' ||substr(trim(b.termination_date),2,2)
            when 6 then  '19'||substr(trim(b.termination_date),5,2)||'-'|| left(trim(b.termination_date),2) || '-' ||substr(trim(b.termination_date),3,2)
          end as date)
     end as term_date, 
  a.fixed_Ded_amt
  -- select count(*)
  from RYDEDATA.PYDEDUCT a
  inner join rydedata.pymast b on trim(a.employee_number) = trim(b.pymast_employee_number)
  where trim(a.ded_pay_code) = '107') x
where term_date >= '2017-01-01') y
order by store, name