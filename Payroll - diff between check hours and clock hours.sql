/*
fuck me, the actual period of time for accruing is 1/15/12 thru 1/28/12

*/
Select yhdhrs /* reg hours */, yhdoth /* overtime hours */
from rydedata.PYHSHDTA
where trim(yhdemp) = '11650'
  and yhdeyy = 12
  and yhdemm = 2
  and yhdedd = 1

select *
from rydedata.pypclockin
where trim(yiemp#) = '11650'
  and yiclkind between '01/15/2012' and '01/31/2012'

select yiclkind, yiclkint, yiclkoutt,
  round(TIMESTAMPDIFF(2, cast(cast(yiclkoutt as timestamp) - cast(yiclkint as timestamp) as char(22)))/3600.0, 2)
from rydedata.pypclockin
where trim(yiemp#) = '11650'
  and yiclkind between '01/15/2012' and '01/31/2012'

select --yiclkind, -- 99.92 hours
  sum(round(TIMESTAMPDIFF(2, cast(cast(yiclkoutt as timestamp) - cast(yiclkint as timestamp) as char(22)))/3600.0, 2))
from rydedata.pypclockin
where trim(yiemp#) = '11650'
and yiclkind between '01/15/2012' and '01/28/2012'
group by yiclkind


/************* 2/26/12/ **************************************************/

-- payroll run: 217000
-- start & end date
select ybco#, ybstart, ybend
from rydedata.pyptbdta
where ypbcyy = 112
  and ypbnum = 217000

select ybstat, count(*) from rydedata.pyptbdta group by ybstat
select * from rydedata.pyptbdta where ybstat = 'O'

-- hours from paycheck
select yhdco#, trim(yhdemp) as yhdemp, yhdhrs as RegHrs, yhdoth as OTHours
from rydedata.pyhshdta
where ypbnum = 217000

-- clock data for interval
select yico#, yiemp#, yiclkind, yiclkint, yiclkoutt, 
  round(TIMESTAMPDIFF(2, cast(cast(yiclkoutt as timestamp) - cast(yiclkint as timestamp) as char(22)))/3600.0, 2) as TotalHours
from rydedata.pypclockin
where yiclkind between '2012-01-29' and '2012-02-11'
  and yicode = 'O'

-- clockhours per emp per interval
select yico#, yiemp#, sum(TotalHours) as ClockHours
from ( 
  select yico#, trim(yiemp#) as yiemp#, yiclkind, yiclkint, yiclkoutt, 
    round(TIMESTAMPDIFF(2, cast(cast(yiclkoutt as timestamp) - cast(yiclkint as timestamp) as char(22)))/3600.0, 2) as TotalHours
  from rydedata.pypclockin
  where yiclkind between '2012-01-29' and '2012-02-11'
    and yicode = 'O') c
group by yico#, yiemp#

-- employees bi-weekly / hourly 
select ymco#, trim(ymempn) as ymempn
from rydedata.pymast
where ymclas = 'H'
  and ympper = 'B'

-- payroll run batch 217000 - 01/29/12 thru 02/11/2012
select a.ymempn, a.ymname, b.CheckRegHrs, b.CheckOTHours, 
  (b.CheckRegHrs + b.CheckOTHours) as TotalCheckHrs, c.ClockHours, 
  (b.CheckRegHrs + b.CheckOTHours) - c.ClockHours as Diff
from ( -- a: employees bi-weekly / hourly
  select ymco#, ymname, trim(ymempn) as ymempn
  from rydedata.pymast
  where ymclas = 'H'
    and ympper = 'B') a
inner join ( -- b: hours from paycheck
    select yhdco#, trim(yhdemp) as yhdemp, yhdhrs as CheckRegHrs, yhdoth as CheckOTHours
    from rydedata.pyhshdta
    where ypbnum = 217000) b on a.ymco# = b.yhdco#
  and a.ymempn = b.yhdemp
left join ( -- c: clockhours per emp per interval
    select yico#, yiemp#, sum(TotalHours) as ClockHours
    from ( 
      select yico#, trim(yiemp#) as yiemp#, yiclkind, yiclkint, yiclkoutt, 
        round(TIMESTAMPDIFF(2, cast(cast(yiclkoutt as timestamp) - cast(yiclkint as timestamp) as char(22)))/3600.0, 2) as TotalHours
      from rydedata.pypclockin
      where yiclkind between '2012-01-29' and '2012-02-11'
        and yicode = 'O') c
    group by yico#, yiemp#) c on a.ymco# = c.yico#
  and  a.ymempn = c.yiemp#
where (b.CheckRegHrs + b.CheckOTHours) - c.ClockHours <> 0


-- payroll run batch 217000 - 01/29/12 thru 02/11/2012
select a.ymempn, a.ymname, b.yhdhrs as CheckRegHrs, b.yhdoth as CheckOTHours, 
  (b.yhdhrs + b.yhdoth) as TotalCheckHrs, c.ClockHours, 
  (b.yhdhrs + b.yhdoth) - c.ClockHours as Diff
-- select *
from rydedata.pymast a
inner join rydedata.pyhshdta b on a.ymco# = b.yhdco#
  and trim(a.ymempn) = trim(b.yhdemp)
left join ( -- c: clockhours per emp per interval
    select yico#, yiemp#, sum(TotalHours) as ClockHours
    from ( 
      select yico#, trim(yiemp#) as yiemp#, yiclkind, yiclkint, yiclkoutt, 
        round(TIMESTAMPDIFF(2, cast(cast(yiclkoutt as timestamp) - cast(yiclkint as timestamp) as char(22)))/3600.0, 2) as TotalHours
      from rydedata.pypclockin
      where yiclkind between '2012-01-29' and '2012-02-11'
        and yicode = 'O') x
    group by yico#, yiemp#) c on a.ymco# = c.yico#
  and  trim(a.ymempn) = trim(c.yiemp#)
where a.ymclas = 'H'
  and a.ympper = 'B'
  and b.ypbnum = 217000
  and b.ypbcyy = 112
  and (b.yhdhrs + b.yhdoth) - c.ClockHours <> 0

-- payroll run batch 330000 - 03/11/12 thru 03/241/2012
select a.ymempn, a.ymname, b.yhdhrs as CheckRegHrs, b.yhdoth as CheckOTHours, 
  (b.yhdhrs + b.yhdoth) as TotalCheckHrs, c.ClockHours, 
  (b.yhdhrs + b.yhdoth) - c.ClockHours as Diff
-- select *
from rydedata.pymast a
inner join rydedata.pyhshdta b on a.ymco# = b.yhdco#
  and trim(a.ymempn) = trim(b.yhdemp)
left join ( -- c: clockhours per emp per interval
    select yico#, yiemp#, sum(TotalHours) as ClockHours
    from ( 
      select yico#, trim(yiemp#) as yiemp#, yiclkind, yiclkint, yiclkoutt, 
        round(TIMESTAMPDIFF(2, cast(cast(yiclkoutt as timestamp) - cast(yiclkint as timestamp) as char(22)))/3600.0, 2) as TotalHours
      from rydedata.pypclockin
      where yiclkind between '2012-03-11' and '2012-03-24'
        and yicode = 'O') x
    group by yico#, yiemp#) c on a.ymco# = c.yico#
  and  trim(a.ymempn) = trim(c.yiemp#)
where a.ymclas = 'H'
  and a.ympper = 'B'
  and b.ypbnum = 330000
  and b.ypbcyy = 112
  and (b.yhdhrs + b.yhdoth) - c.ClockHours <> 0



-- payroll run batch 413000 - 03/25/12 thru 04/07/12
-- hourly employees paid bi-weekly

select a.ymco#, a.ymempn, a.ymname, a.ymdept, b.yhdhrs as CheckRegHrs, b.yhdoth as CheckOTHours, 
  (b.yhdhrs + b.yhdoth) as TotalCheckHrs, c.ClockHours, 
  (b.yhdhrs + b.yhdoth) - c.ClockHours as Diff
-- select *
from rydedata.pymast a
inner join rydedata.pyhshdta b on a.ymco# = b.yhdco#
  and trim(a.ymempn) = trim(b.yhdemp)
left join ( -- c: clockhours per emp per interval
    select yico#, yiemp#, sum(TotalHours) as ClockHours
    from ( 
      select yico#, trim(yiemp#) as yiemp#, yiclkind, yiclkint, yiclkoutt, 
        round(TIMESTAMPDIFF(2, cast(cast(yiclkoutt as timestamp) - cast(yiclkint as timestamp) as char(22)))/3600.0, 2) as TotalHours
      from rydedata.pypclockin
      where yiclkind between '2012-03-25' and '2012-04-07'
        and yicode = 'O') x
    group by yico#, yiemp#) c on a.ymco# = c.yico#
  and  trim(a.ymempn) = trim(c.yiemp#)
where a.ymclas = 'H'
  and a.ympper = 'B'
  and b.ypbnum = 413000
  and b.ypbcyy = 112
  and (b.yhdhrs + b.yhdoth) - c.ClockHours <> 0
order by a.ymco#, a.ymname


select a.ymco#, a.ymempn, a.ymname, a.ymdept, b.yhdhrs as CheckRegHrs, b.yhdoth as CheckOTHours, 
  (b.yhdhrs + b.yhdoth) as TotalCheckHrs, c.ClockHours, 
  (b.yhdhrs + b.yhdoth) - c.ClockHours as Diff,
  x.yhccam/yhcrat as TrainingHrs
from rydedata.pymast a
inner join rydedata.pyhshdta b on a.ymco# = b.yhdco#
  and trim(a.ymempn) = trim(b.yhdemp)
left join ( -- c: clockhours per emp per interval
    select yico#, yiemp#, sum(TotalHours) as ClockHours
    from ( 
      select yico#, trim(yiemp#) as yiemp#, yiclkind, yiclkint, yiclkoutt, 
        round(TIMESTAMPDIFF(2, cast(cast(yiclkoutt as timestamp) - cast(yiclkint as timestamp) as char(22)))/3600.0, 2) as TotalHours
      from rydedata.pypclockin
      where yiclkind between '2012-03-25' and '2012-04-07'
        and yicode = 'O') x
    group by yico#, yiemp#) c on a.ymco# = c.yico#
  and  trim(a.ymempn) = trim(c.yiemp#)
left join rydedata.pyhscdta x on trim(a.ymempn) = trim(x.yhcemp)
  and trim(yhccde) = '66' 
  and b.ypbcyy = x.ypbcyy
  and b.ypbnum = x.ypbnum
where a.ymclas = 'H'
  and a.ympper = 'B'
  and b.ypbnum = 413000
  and b.ypbcyy = 112
  and (b.yhdhrs + b.yhdoth) - c.ClockHours <> 0
order by a.ymco#, a.ymdept


select *
from rydedata.pyhscdta
where ypbcyy = 112
  and ypbnum = 413000
  and trim(yhcemp) = '116185'

select a.ymco#, a.ymempn, a.ymname, a.ymdept, b.yhdhrs as CheckRegHrs, b.yhdoth as CheckOTHours, 
  (b.yhdhrs + b.yhdoth) as TotalCheckHrs, c.ClockHours, 
  (b.yhdhrs + b.yhdoth) - c.ClockHours as Diff,
  x.yhccam/yhcrat as TrainingHrs
-- select *
from rydedata.pymast a
inner join rydedata.pyhshdta b on a.ymco# = b.yhdco#
  and trim(a.ymempn) = trim(b.yhdemp)
left join ( -- c: clockhours per emp per interval
    select yico#, yiemp#, sum(TotalHours) as ClockHours
    from ( 
      select yico#, trim(yiemp#) as yiemp#, yiclkind, yiclkint, yiclkoutt, 
        round(TIMESTAMPDIFF(2, cast(cast(yiclkoutt as timestamp) - cast(yiclkint as timestamp) as char(22)))/3600.0, 2) as TotalHours
      from rydedata.pypclockin
      where yiclkind between '2012-03-25' and '2012-04-07'
        and yicode = 'O') x
    group by yico#, yiemp#) c on a.ymco# = c.yico#
  and  trim(a.ymempn) = trim(c.yiemp#)
left join rydedata.pyhscdta x on trim(a.ymempn) = trim(x.yhcemp)
  and trim(yhccde) = '66' 
  and b.ypbcyy = x.ypbcyy
  and b.ypbnum = x.ypbnum
where a.ymclas = 'H'
  and a.ympper = 'B'
  and b.ypbnum = 413000
  and b.ypbcyy = 112
  and (b.yhdhrs + b.yhdoth) - c.ClockHours <> 0
order by a.ymco#, a.ymname



-- 7/25
-- trying to figure out the deal with multiple concurrent time clock records for an employee
-- specifically 114205 on 6/13/2012
-- my guess is that Kim just "fixes it" while doing payroll
select a.ymco#, a.ymempn, a.ymname, a.ymdept, b.yhdhrs as CheckRegHrs, b.yhdoth as CheckOTHours, 
  (b.yhdhrs + b.yhdoth) as TotalCheckHrs, c.ClockHours, 
  (b.yhdhrs + b.yhdoth) - c.ClockHours as Diff,
  x.yhccam/yhcrat as TrainingHrs
-- select *
from rydedata.pymast a
inner join rydedata.pyhshdta b on a.ymco# = b.yhdco#
  and trim(a.ymempn) = trim(b.yhdemp)
left join ( -- c: clockhours per emp per interval
    select yico#, yiemp#, sum(TotalHours) as ClockHours
    from ( 
      select yico#, trim(yiemp#) as yiemp#, yiclkind, yiclkint, yiclkoutt, 
        round(TIMESTAMPDIFF(2, cast(cast(yiclkoutt as timestamp) - cast(yiclkint as timestamp) as char(22)))/3600.0, 2) as TotalHours
      from rydedata.pypclockin
      where yiclkind between '2012-06-03' and '2012-06-16'
        and yicode = 'O') x
    group by yico#, yiemp#) c on a.ymco# = c.yico#
  and  trim(a.ymempn) = trim(c.yiemp#)
left join rydedata.pyhscdta x on trim(a.ymempn) = trim(x.yhcemp)
  and trim(yhccde) = '66' 
  and b.ypbcyy = x.ypbcyy
  and b.ypbnum = x.ypbnum
where a.ymclas = 'H'
  and a.ympper = 'B'
  and b.ypbnum = 622000
  and b.ypbcyy = 112
  and (b.yhdhrs + b.yhdoth) - c.ClockHours <> 0
order by a.ymco#, a.ymname

-- these are the ones with overlapping concurrent logins
select a.*
from rydedata.pypclockin a
inner join (
SELECT yico#, yiemp#, yiclkind, min(yiclkint) AS minIn, MAX(yiclkint) AS maxIN,
  MIN(yiclkoutt) AS minOut, MAX(yiclkoutt) AS maxOut
FROM rydedata.PYPCLOCKIN  
--WHERE yiemp# = '114205'
--  AND yiclkind = '06/13/2012'
GROUP BY yico#, yiemp#, yiclkind
HAVING COUNT(*) > 1
  AND MAX(yiclkint) < MIN(yiclkoutt)) b on trim(a.yico#) = trim(b.yico#)
    and trim(a.yiemp#) = trim(b.yiemp#)
    and a.yiclkind = b.yiclkind
order by yico#, yiemp#, yiclkind
