/*
Terminated employees for Marianne
set the date in the where clause
*/

select distinct pym.ymco# AS "Store", pym.ymname as "Name", pym.ymempn as "Emp #", 
  case 
    when cast(right(trim(pym.ymhdto),2) as integer) < 20 then 
      cast (
        case length(trim(pym.ymhdto))
          when 5 then  '20'||substr(trim(pym.ymhdto),4,2)||'-'|| '0' || left(trim(pym.ymhdto),1) || '-' ||substr(trim(pym.ymhdto),2,2)
          when 6 then  '20'||substr(trim(pym.ymhdto),5,2)||'-'|| left(trim(pym.ymhdto),2) || '-' ||substr(trim(pym.ymhdto),3,2)
        end as date) 
    else  
      cast (
        case length(trim(pym.ymhdto))
          when 5 then  '19'||substr(trim(pym.ymhdto),4,2)||'-'|| '0' || left(trim(pym.ymhdto),1) || '-' ||substr(trim(pym.ymhdto),2,2)
          when 6 then  '19'||substr(trim(pym.ymhdto),5,2)||'-'|| left(trim(pym.ymhdto),2) || '-' ||substr(trim(pym.ymhdto),3,2)
        end as date) 
  end as "Hire Date",
  CASE pym.ymactv
    when 'A' then 'Full Time'
    when 'P' then 'Part Time'
    when 'T' then 'Terminated'
  end as Status,
  cast (
    case length(trim(pym.ymtdte))
      when 5 then  '20'||substr(trim(pym.ymtdte),4,2)||'-'|| '0' || left(trim(pym.ymtdte),1) || '-' ||substr(trim(pym.ymtdte),2,2)
      when 6 then  '20'||substr(trim(pym.ymtdte),5,2)||'-'|| left(trim(pym.ymtdte),2) || '-' ||substr(trim(pym.ymtdte),3,2)
    end as date) as "Termination Date", 
  jobd.yrtext as "Job Title", pym.ymdept as "Dept", pc.yicdesc as "Dept Description"
from pymast pym
left join pyprhead phd on trim(phd.yrempn) = trim(pym.ymempn)  
  and phd.yrco# = pym.ymco# -- job description (code)
left join pyprjobd jobd on trim(jobd.yrjobd) = trim(phd.yrjobd) 
  and jobd.yrco# = pym.ymco# -- job description in plain english
left join pypclkctl pc on pc.yicdept = pym.ymdept
  and pc.yicco# = pym.ymco#
where pym.ymco#  in ('RY1','RY2','RY3')
--and pym.ymactv = 'T'
  and ymname <> '.,'
  and pym.ymactv = 'T'
  and CAST ( -- since 7/1/2011
    CASE LENGTH(TRIM(PYM.YMTDTE))
      WHEN 5 THEN  '20'||SUBSTR(TRIM(PYM.YMTDTE),4,2)||'-'|| '0' || LEFT(TRIM(PYM.YMTDTE),1) || '-' ||SUBSTR(TRIM(PYM.YMTDTE),2,2)
      WHEN 6 THEN  '20'||SUBSTR(TRIM(PYM.YMTDTE),5,2)||'-'|| LEFT(TRIM(PYM.YMTDTE),2) || '-' ||SUBSTR(TRIM(PYM.YMTDTE),3,2)
    END AS DATE) >= cast('2011-07-01' as date)
order by pym.ymco#, pym.ymname    


