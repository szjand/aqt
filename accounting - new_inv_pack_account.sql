
Can you run a report for me that shows all of the new vehicles still in inventory and the amount of the new car pack (sitting in 133210)?

Jeri Schmiess Penas, CPA


select trim(a.inpmast_stock_number) as stock_number, a.inventory_account, a.inpmast_vin as vin, '133210' as account, coalesce(b.amount, 0) as amount
from rydedata.inpmast a
left join (
  select trim(a.gtctl#) as control, trim(a.gtacct) as account, sum(a.gttamt) as amount
  from rydedata.glptrns a
  where trim(a.gtacct) = '133210'
    and gtpost = 'Y'
  group by trim(a.gtctl#), trim(a.gtacct)
  having sum(a.gttamt) <> 0) b on trim(a.inpmast_stock_number) = b.control
where a.status = 'I'
  and a.type_n_u = 'N'    
  and left(trim(inpmast_stock_number), 1) <> 'H'  
  
  
select trim(a.inpmast_stock_number) as stock_number, a.inventory_account, a.inpmast_vin as vin, '233210' as account, coalesce(b.amount, 0) as amount
from rydedata.inpmast a
left join (
  select trim(a.gtctl#) as control, trim(a.gtacct) as account, sum(a.gttamt) as amount
  from rydedata.glptrns a
  where trim(a.gtacct) = '233210'
    and gtpost = 'Y'
  group by trim(a.gtctl#), trim(a.gtacct)
  having sum(a.gttamt) <> 0) b on trim(a.inpmast_stock_number) = b.control
where a.status = 'I'
  and a.type_n_u = 'N'    
  and left(trim(inpmast_stock_number), 1) = 'H'    