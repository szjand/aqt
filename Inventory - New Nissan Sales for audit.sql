Select bmfran, count(*) from RYDEDATA.BOPMAST where bmvtyp = 'N' and bmco# = 'RY2' group by bmfran
Select * from RYDEDATA.BOPMAST where trim(bmstk#) = 'H4635'

select *
from rydedata.bopmast
where bmfran in ('2NT','2NC')

select *
from rydedata.bopmast
where trim(bmstk#) in ('H4209','H3919')


select bmvin, bmdtaprv, bmsnam, bnadr1, bncity, bnstcd, bnzip, bmapr, bminct, bmpslp, bpname
from rydedata.bopmast a
left join rydedata.bopname b on a.bmbuyr = b.bnkey
left join rydedata.bopslsp c on a.bmpslp = c.bpspid
  and bpactive <> 'N'
where bmfran in ('2NT','2NC')
  and bmdtaprv between date('2012-01-04') and date('2013-01-02')
order by bmvin

select * from rydedata.bopslsp where bpspid = 'DNI'

-- this looks good
select bmvin as VIN, bmdtaprv as Date, bmsnam as "Cust Name", bnadr1 as Address, bncity as City, bnstcd as State, 
  bnzip as Zip, bmapr as APR, bminct as Incentive, bpname as Salesperson
from rydedata.bopmast a
left join rydedata.bopname b on a.bmbuyr = b.bnkey and trim(bmsnam) = trim(bnsnam)
left join (
  select bpspid, max(bpname) as bpname
  from rydedata.bopslsp
  group by bpspid) c on a.bmpslp = c.bpspid
where bmfran in ('2NT','2NC')
  and bmdtaprv between date('2012-01-04') and date('2013-01-02')
  and bnadr1 <> ''
order by bmvin


-- this looks good
-- 1/31 lear complained abt "incentive", was using bminct
-- his example is H4635, looks like the column should be BMRBTE
select bmvin as VIN, bmdtaprv as Date, bmsnam as "Cust Name", bnadr1 as Address, bncity as City, bnstcd as State, 
  bnzip as Zip, bmapr as APR, bmrbte as Incentive, bpname as Salesperson
from rydedata.bopmast a
left join rydedata.bopname b on a.bmbuyr = b.bnkey and trim(bmsnam) = trim(bnsnam)
left join (
  select bpspid, max(bpname) as bpname
  from rydedata.bopslsp
  group by bpspid) c on a.bmpslp = c.bpspid
where bmfran in ('2NT','2NC')
  and bmdtaprv between date('2012-01-04') and date('2013-01-02')
  and bnadr1 <> ''
order by bmvin

/*
*12-10-14
Jon,

Nissan needs a report like this for our audit.  I believe you did it last time.  Are you able to help?

Thanks,

Jeri
From: "Creecy, Jeff" <Jeff.Creecy@Nissan-Usa.com>
Date: December 4, 2014 at 9:27:53 AM CST
To: "mlear@gfhonda.com" <mlear@gfhonda.com>
Cc: "brydell@gfhonda.com" <brydell@gfhonda.com>, "Robinson, Derek" <Derek.Robinson@Nissan-Usa.com>
Subject: 3071 Rydell Nissan of Grand Forks - Sales Audit Data
Mr. Mike Lear,
As per our conversation regarding the sales audit, here are the parameters for the sales data report I need:
          
                   PARAMETERS:
                             SALE DATE      GREATER THAN       02JAN14        AND
                             SALE DATE      LESS THAN            01OCT14        
                   COLUMNS:
                             SERIAL (17)
SALE DATE
BUYER NAME
BUYER ADDRESS
BUYER CITY
BUYER STATE
BUYER ZIP
REBATE
APR
SALESPERSON NAME
LIENHOLDER NAME
 
This report is only for the first nine months of the audit scope. Once the current sales quarter is over, I will contact you to get the remaining sales data. The report needs to be in an Excel format and can be forwarded to me at this same e-mail address. Once I have reviewed this initial data, I will contact you to arrange the exact dates I will be on-site for the audit or to discuss the possibility of a mail-in audit. If you have any issues, let me know.
*/

select bmvin as SERIAL, bmdtaprv as "SALE DATE", bmsnam as "BUYER NAME", bnadr1 as "BUYER ADDRESS", 
  bncity as "BUYER CITY", bnstcd as "BUYER STATE", 
  bnzip as "BUYER ZIP", bmapr as APR, bmrbte as "REBATE", bpname as "SALESPERSON NAME",
  d.lendor_name as "LIENHOLDER NAME"
from rydedata.bopmast a
left join rydedata.bopname b on a.bmbuyr = b.bnkey and trim(bmsnam) = trim(bnsnam)
left join (
  select bpspid, max(bpname) as bpname
  from rydedata.bopslsp
  group by bpspid) c on a.bmpslp = c.bpspid
left join rydedata.boplsrc d on a.lending_source = d.key and a.bmco# = d.company_number
where bmfran in ('2NT','2NC')
  and bmdtaprv between date('2014-01-02') and date('2014-09-30')
  and bnadr1 <> ''
order by bmsnam

/*
1/9/15
Jon,

Can you help me? 
Nissan is now asking for us to compile an audit list from 10/1/14 through 1/2/2015, with the following information:

         
                   PARAMETERS:
                             SALE DATE      GREATER THAN       01OCT14        AND
                             SALE DATE      LESS THAN            03JAN15     
                   COLUMNS:
                             SERIAL (17)
SALE DATE
BUYER NAME
BUYER ADDRESS
BUYER CITY
BUYER STATE
BUYER ZIP
REBATE
APR
SALESPERSON NAME
                             LIENHOLDER NAME

Let me know if you need any additional information.

Thanks,

  
-- 
Michael Lear
*/

select bmvin as SERIAL, bmdtaprv as "SALE DATE", bmsnam as "BUYER NAME", bnadr1 as "BUYER ADDRESS", 
  bncity as "BUYER CITY", bnstcd as "BUYER STATE", 
  bnzip as "BUYER ZIP", bmapr as APR, bmrbte as "REBATE", bpname as "SALESPERSON NAME",
  d.lendor_name as "LIENHOLDER NAME"
from rydedata.bopmast a
left join rydedata.bopname b on a.bmbuyr = b.bnkey and trim(bmsnam) = trim(bnsnam)
left join (
  select bpspid, max(bpname) as bpname
  from rydedata.bopslsp
  group by bpspid) c on a.bmpslp = c.bpspid
left join rydedata.boplsrc d on a.lending_source = d.key and a.bmco# = d.company_number
where bmfran in ('2NT','2NC')
  and bmdtaprv between date('2014-10-01') and date('2015-01-02')
  and bnadr1 <> ''
order by bmsnam

