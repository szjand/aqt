      select Location, Make, Model, EstimatedRetail, TrimLevel, Color, Miles, 
        StockNumber, VIN, BodyStyle, Engine, Transmission, InteriorColor, OptionsList, --OptionsExploded, 
        "Year", SalesStatus,  DateAcquired, DateOnLot, DateSold, Appraisedby, Soldby, ManagerCRV, 
        ManagerCWV, Package, PricingStrategy, PricingStrategyABC,
        Source, SoldAmount, Net, Glump, Segment, EstimatedRecon, Recon, 
        -- Inspection, ItemsFixed, 
        AuctionPurchasedAt, InternetAuctionState, InternetAuctionCity, 
        PulledBy, PullLocation,
        DatePulled, Pricedby, CRVby, CWVby, MSBP, TargetMargin,
        AppShown, AppACV, AppOverAllowance, FWACV, AppMCRV, FWOverAllowance, FWMCRV, AppEstRecon, FWEstRecon,
        Warranty, 
        -- InternetComment, 
        "Set", SetNum, Sheet, GoodOdds, InternetAuctionCity, InternetAuctionState, DRPriceNew, 
        DRMCRVNew, DefectReason, DefectReasonDetails,  DRsubmitdate, DRReviewer, DRRoadTest, DRWalkaround,
        OTWRMCRVNew, OTWRPriceNew, OTWReason, OTWReasonDetails, OTWRSubmitDate, OTWRReviewer, OTWRRoadTest, OTWRWalkaround,
        Inspection, ItemsFixed, SalesComment, Sublet, SubletComments, SubletEndDate, SubletLocation, SubletStartDate,
        DateAtRecon, Detail, DetailStop, Mechanical, SafetyStart
      from frmStoreInventory
--where StockNumber = '10101A'
--where "Year" = '2009'

/*
select CertificationNumber from frmStoreInventory where CertificationNumber is not null order by DateAcquired desc
*/

