Please run a listing of all units in account 130402 and 230402 that are no longer in inventory.  Controlled by stock #.

Thanks,

Jeri Schmiess Penas, CPA


-- sold vehicles: inventory account = 0
select c.gtctl#, sum(d.gttamt)
from (
  select a.gtctl#, a.gtdesc, a.gtacct, a.gttamt, b.inventory_account
  from rydedata.glptrns a
  inner join rydedata.inpmast b on trim(a.gtctl#) =  trim(b.inpmast_stock_number)
  where trim(gtacct) in ('130402','230402')) c
left join rydedata.glptrns d on trim(c.gtctl#) = trim(d.gtctl#) and trim(c.inventory_account) = trim(d.gtacct)
group by c.gtctl#
having sum(d.gttamt) = 0

-- no record in inpmast
select a.gtctl#, a.gtdesc, a.gtacct, a.gttamt, b.inventory_account
from rydedata.glptrns a
left join rydedata.inpmast b on trim(a.gtctl#) =  trim(b.inpmast_stock_number)
where trim(a.gtacct) in ('130402','230402') 
  and b.inpmast_stock_number is null

-- this is much faster
-- 09/01/2021, shit i can't even remember, but i think this is the one for month end
select control
from (
  select trim(a.gtctl#) as control
  from rydedata.glptrns a
  where trim(a.gtacct) in ('130402','230402')
    and gtpost = 'Y'
  group by trim(a.gtctl#) 
  having sum(a.gttamt) <> 0) b
join rydedata.inpmast c on b.control = trim(c.inpmast_stock_number)
  and c.status <> 'I'
order by control -- left(c.inventory_account, 1), control

-- include deal status
select control, d.record_status
from (
  select trim(a.gtctl#) as control
  from rydedata.glptrns a
  where trim(a.gtacct) in ('130402','230402')
    and gtpost = 'Y'
  group by trim(a.gtctl#) 
  having sum(a.gttamt) <> 0) b
join rydedata.inpmast c on b.control = trim(c.inpmast_stock_number)
  and c.status <> 'I'
left join rydedata.bopmast d on trim(c.inpmast_stock_number) = trim(d.bopmast_stock_number)
order by left(c.inventory_account, 1), control

   
-- based on inventory account  balance

with
  inventory as (
    select trim(gtctl#) as control
    from rydedata.glptrns aa
    inner join (
      Select trim(account_number) as account 
      from RYDEDATA.GLPMAST a
      where a.year = 2018
        and a.account_type = '1'
        and a.department = 'NC'
        and a.typical_balance = 'D'
        and trim(a.account_number) <> '126104'
        and trim(a.account_number) not like '3%') bb on trim(aa.gtacct) = bb.account
    where aa.gtdate between curdate() - 900 days and curdate()
      and gtpost = 'Y'
    group by trim(gtctl#)
    having sum(gttamt) < 100)
select b.control
from (
  select trim(a.gtctl#) as control
  from rydedata.glptrns a
  where trim(a.gtacct) in ('130402','230402')
    and gtpost = 'Y'
  group by trim(a.gtctl#) 
  having sum(a.gttamt) <> 0) b
join inventory c on b.control = c.control
order by b.control

     
     
     
with
  inventory as (
    select trim(gtctl#) as control
    from rydedata.glptrns aa
    inner join (
      Select trim(account_number) as account 
      from RYDEDATA.GLPMAST a
      where a.year = 2018
        and a.account_type = '1'
        and a.department = 'NC'
        and a.typical_balance = 'D'
        and trim(a.account_number) <> '126104'
        and trim(a.account_number) not like '3%') bb on trim(aa.gtacct) = bb.account
    where aa.gtdate between curdate() - 900 days and curdate()
      and gtpost = 'Y'
    group by trim(gtctl#)
    having sum(gttamt) < 100)
select b.*
from (
  select trim(a.gtctl#) as control, trim(a.gtacct), sum(a.gttamt)
  from rydedata.glptrns a
  where trim(a.gtacct) = '133210'
    and gtpost = 'Y'
  group by trim(a.gtctl#), trim(a.gtacct)
  having sum(a.gttamt) <> 0) b
join inventory c on b.control = c.control
order by b.control     
        


              