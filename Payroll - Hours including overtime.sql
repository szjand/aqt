
select z."EmpNum", ymname as "Name", sum(z."Reg Hours") as "Reg Hours", 
  sum(z."OT Hours") as "OT Hours", sum(z."Reg Hours") + sum(z."OT Hours") as "Total Hours"
from 
  ( -- z
    select a."Company", a."EmpNum", a."Week", a."Day",
      coalesce(sum(b."Hours"), 0) as "Total Hours Before",
      a."Hours" as "Hours Today",
      coalesce(sum(b."Hours"), 0) + a."Hours" as "Total Hours After",
      case 
        when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then a."Hours"
        when coalesce(sum(b."Hours"), 0) > 40 then 0
        else 40 - coalesce(sum(b."Hours"), 0) 
      end as "Reg Hours",
      case 
        when coalesce(sum(b."Hours"), 0) >= 40 then a."Hours"
        when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then 0
        else a."Hours" - (40 - coalesce(sum(b."Hours"), 0)) 
      end as "OT Hours"
    from 
      ( -- a
        select yico# as "Company", yiemp# as "EmpNum", week(yiclkind) as "Week", yiclkind as "Day", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin
        where trim(yicode) = 'O' and yiclkind >= '12/26/2010' and yiclkind <= '12/31/2011'
        group by yico#, yiemp#, yiclkind) as a
    left join 
      ( -- b
        select yico# as "Company", yiemp# as "EmpNum", week(yiclkind) as "Week", yiclkind as "Day", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin
        where trim(yicode) = 'O' and yiclkind >= '12/26/2010' and yiclkind <= '12/31/2011'
        group by yico#, yiemp#, yiclkind) as b on a."Company" = b."Company" 
          and a."EmpNum" = b."EmpNum" and week(a."Day") = week(b."Day") and b."Day" < a."Day"
    group by a."Week", a."Company", a."EmpNum", a."Day", a."Hours"
    order by a."Week", a."Company", a."EmpNum", a."Day") as z
left join rydedata.pymast py on py.ymco# = z."Company" 
  and py.ymempn = z."EmpNum"
/* single employee for February 2011 */
--where trim(z."EmpNum") = '1113940' and z."Day" >= '2/1/2011' and z."Day" <= '2/28/2011'
--group by z."EmpNum", ymname

/* entire distribution code for February 2011 */
where z."Day" >= '2/1/2011' and z."Day" <= '2/28/2011'
and trim(py.ymempn) in (
  select trim(ymempn)
  from pymast
  where ymco# = 'RY1'
  and ymdist = 'STEC')
group by z."EmpNum", ymname
order by ymname
