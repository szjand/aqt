-- 2011 overtime hours for parts dept
select ymname as name, yhdeyy as year, yhdemm as month, yhdedd as day,
  yhdoth as "Overtime Hours"
from rydedata.pyhshdta
where ymdept = '06'
and yhdco# = 'RY1'
and yhdeyy = 11
and trim(ymdist) = 'PART'
and yhdoth <> 0
order by yhdeyy, yhdemm, yhdedd, ymname

-- grouped by pay period
select yhdeyy as year, yhdemm as month, yhdedd as day, sum(yhdoth) as "Total Overtime Hours"
from rydedata.pyhshdta
where ymdept = '06'
and yhdco# = 'RY1'
and yhdeyy = 11
and trim(ymdist) = 'PART'
and yhdoth <> 0
group by yhdeyy, yhdemm, yhdedd
order by yhdeyy, yhdemm, yhdedd

-- grouped by employee
select ymname, sum(yhdoth) as "Total Overtime Hours"
from rydedata.pyhshdta
where ymdept = '06'
and yhdco# = 'RY1'
and yhdeyy = 11
and trim(ymdist) = 'PART'
and yhdoth <> 0
group by ymname




-- 2011 overtime hours for call center
select ymname as name, yhdeyy as year, yhdemm as month, yhdedd as day,
  yhdoth as "Overtime Hours"
-- select count(*)
from rydedata.pyhshdta
where ymdept in ('54','16')
and yhdco# = 'RY1'
 and yhdeyy = 11
and trim(ymdist) = 'SRVM'
and yhdoth <> 0
order by yhdeyy, yhdemm, yhdedd, ymname



select ymdept, ymdist, ymname
from rydedata.pyhshdta
where yhdeyy = 11
and trim(ymname) = 'MCMILLAN,SHAUNA'


select ymname, ymdept, ymdist, yhdemm, sum(yhdoth), sum(yhdota)
from rydedata.pyhshdta
where yhdco# = 'RY1'
  and yhdeyy = 11
  and ymdept in ('54','16')
  and ymdist not in ('NCR','SOFF','PDQO')
group by ymname, ymdept, ymdist, yhdemm
order by ymname, yhdemm

select distinct ymname
from (
select ymname, ymdept, ymdist, yhdemm, sum(yhdoth), sum(yhdota)
from rydedata.pyhshdta
where yhdeyy = 11
  and ymdept in ('54','16')
group by ymname, ymdept, ymdist, yhdemm) x
order by ymname

