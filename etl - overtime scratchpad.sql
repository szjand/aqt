select a."Company", a."EmpNum", a."Week", a."Day",
      coalesce(sum(b."Hours"), 0) as "Total Hours Before",
      a."Hours" as "Hours Today",
      coalesce(sum(b."Hours"), 0) + a."Hours" as "Total Hours After",
      case 
        when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then a."Hours"
        when coalesce(sum(b."Hours"), 0) > 40 then 0
        else 40 - coalesce(sum(b."Hours"), 0) 
      end as "Reg Hours",
      case 
        when coalesce(sum(b."Hours"), 0) >= 40 then a."Hours"
        when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then 0
        else a."Hours" - (40 - coalesce(sum(b."Hours"), 0)) 
      end as "OT Hours"
    from 
      ( -- a
        select yico# as "Company", yiemp# as "EmpNum", week(yiclkind) as "Week", yiclkind as "Day", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin
        where trim(yicode) = 'O' and yiclkind >= '11/13/2011' and yiclkind <= '11/19/2011'
        group by yico#, yiemp#, yiclkind) as a
    left join 
      ( -- b
        select yico# as "Company", yiemp# as "EmpNum", week(yiclkind) as "Week", yiclkind as "Day", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
       from rydedata.pypclockin
        where trim(yicode) = 'O' and yiclkind >= '11/13/2011' and yiclkind <= '11/19/2011'
        group by yico#, yiemp#, yiclkind) as b on a."Company" = b."Company" 
          and a."EmpNum" = b."EmpNum" and week(a."Day") = week(b."Day") and b."Day" < a."Day"
where trim(a."EmpNum") = '16250'
    group by a."Week", a."Company", a."EmpNum", a."Day", a."Hours"
    order by a."Week", a."Company", trim(a."EmpNum"), a."Day"  



