select *
from rydedata.bopslss a
inner join rydedata.bopmast b on a.deal_key = b.record_key
where sales_person_type = 'S'
  and trim(b.bopmast_stock_number) = '30028'

  and status_c_capped = 'U'
  and sale_date > 20151200


select status_c_capped, count(*)
from rydedata.bopslss
group by status_c_capped

select record_type, count(*)
from rydedata.bopslss
group by record_type

select record_key, bopmast_Stock_number, record_status, sale_type, bopmast_Stock_number,date_capped,
  primary_salespers, b.sales_person_name, secondary_slspers2, c.sales_person_name,
  month(date_capped), d.*
from rydedata.bopmast a
left join rydedata.bopslsp b on a.primary_salespers = b.sales_person_id
  and b.company_number = 'RY1'
left join rydedata.bopslsp c on a.secondary_slspers2 = c.sales_person_id
  and c.company_number = 'RY1'
left join rydedata.bopslss d on a.record_key = d.deal_key
  and d.sales_person_type = 'S'
  and a.bopmast_company_number = d.company_number
where date_capped between '2015-12-01' and '2016-11-30'
and month(date_capped) = 11 and d.sales_person_id = 'LOV'
order by bopmast_stock_number

-- just the december 2015 deals
select month(a.date_capped) as the_month, d.sales_person_id, d.sales_person_name, sum(d.unit_count) as unit_count
from rydedata.bopmast a
left join rydedata.bopslss d on a.record_key = d.deal_key
  and d.sales_person_type = 'S'
  and a.bopmast_company_number = d.company_number
where a.date_capped between '2015-12-01' and '2015-12-31'
group by month(a.date_capped), d.sales_person_id, d.sales_person_name
order by month(a.date_capped), d.sales_person_name


-- strother, the entire year, don't think vision has his honda deals
select d.sales_person_id, d.sales_person_name, sum(d.unit_count) as unit_count
from rydedata.bopmast a
left join rydedata.bopslss d on a.record_key = d.deal_key
  and d.sales_person_type = 'S'
  and a.bopmast_company_number = d.company_number
where a.date_capped between '2015-12-01' and '2016-11-30'
  and d.sales_person_id = 'KEI'
group by d.sales_person_id, d.sales_person_name
