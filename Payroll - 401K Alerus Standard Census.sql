-- Alerus 401K
-- Standard Census Format For Excel
/*  -- this was for 2010
1/28/11 from Kim:
Ron nokelby gross pay should be 61500.20  401k amount is 3689.96
Kim miller gross pay should be 39132.45  401k amount is 3913.25
Steve flaat gross pay should be 76510.80  401k amount is 3060.43
*/
select p.ymco#, p.ymname as Name, 
/*
  cast(
    case length(trim(p.ymbdte))
      when 5 then  '19'||substr(trim(p.ymbdte),4,2)||'-'|| '0' || left(trim(p.ymbdte),1) || '-' ||substr(trim(p.ymbdte),2,2)
      when 6 then  '19'||substr(trim(p.ymbdte),5,2)||'-'|| left(trim(p.ymbdte),2) || '-' ||substr(trim(p.ymbdte),3,2)
    end as date) as "Birth Date",
  case 
    when cast(right(trim(ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as "Hire Date",
  case 
    when cast(right(trim(ymtdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymtdte))
          when 5 then  '20'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
          when 6 then  '20'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymtdte))
          when 5 then  '19'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
          when 6 then  '19'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
        end as date) 
    end as "Term Date",
  case ymactv
    when 'A' then '1'
    when 'P' then '2'
  else null
  end as "Employee Type",
  cast(g.Hours as Integer) as "YTD Hours",
*/
  g.Gross as "Gross Compensation",
--  d.Roth as "Roth Deferral",
  (select sum(yhccam)
    from rydedata.pyhscdta
    where ypbcyy = '110'
    and trim(yhcemp) = trim(p.ymempn)
    and trim(yhccde) = '99') as "Roth Deferral",
  g.total as "Employer Match",
--  d.Profit as "Profit Sharing",
  (select sum(yhccam)
    from rydedata.pyhscdta
    where ypbcyy = '110'
    and trim(yhcemp) = trim(p.ymempn)
    and trim(yhccde) = '91') as "Profit Sharing",
  p.ymstre as "Street Address",
  p.ymcity as City,
  p.ymstat as State,
  p.ymzip as Zip
from rydedata.pymast p
inner join ( -- employer contribution
  select yhdemp, 
    sum(yhcrtm) as total, -- 17 Emplr Curr Ret
    sum(yhdhrs) as Hours,  -- 7 Reg Hours
    sum(yhacm) as Gross -- 8 Curr Adj Comp
  from rydedata.pyhshdta -- Payroll Header transaction file
  where ypbcyy = '110' -- payroll cen + year
  group by yhdemp) g on p.ymempn = g.yhdemp
order by p.ymco#, p.ymname
*/

/* 2/9/12
for 2011, there is an issue with midstream change in deduction code for 401k/roth/retirement/profit sharing ... whatever

getting real fucking close, pull employer contribution from the GL
problem now is with the only Roth: Dharma, adding employee contr + employer contr for employee contr
got it

2/24/12 oops, missing honda 401 contr
1/16/13, going with what is here: need to adjust ypbcyy in 3 places
2/21/13 include employeenumber to facilitate adding ssn
1/28/14 to facilitate adding the ssn, add a ssn column, and export the whole deal into advantage, dds.tmp401K, then 
        join on tmpssn to get the ssn in the report for kim 
*/ 



select p.ymco#, p.ymname as Name, trim(p.ymempn) as ymempn, space(11) as ssn,
  cast(
    case length(trim(p.ymbdte))
      when 5 then  '19'||substr(trim(p.ymbdte),4,2)||'-'|| '0' || left(trim(p.ymbdte),1) || '-' ||substr(trim(p.ymbdte),2,2)
      when 6 then  '19'||substr(trim(p.ymbdte),5,2)||'-'|| left(trim(p.ymbdte),2) || '-' ||substr(trim(p.ymbdte),3,2)
    end as date) as "Birth Date",
  case 
    when cast(right(trim(ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as "Hire Date",
  case 
    when cast(right(trim(ymtdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymtdte))
          when 5 then  '20'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
          when 6 then  '20'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymtdte))
          when 5 then  '19'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
          when 6 then  '19'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
        end as date) 
    end as "Term Date",
  coalesce(  
    case ymactv
      when 'A' then '1'
      when 'P' then '2'
    else null
    end, ' ') as "Employee Type",
  cast(g.Hours as Integer) as "YTD Hours",
  g.Gross as "Gross Compensation",
  coalesce((
    select sum(yhccam)
    from rydedata.pyhscdta
    where ypbcyy = '113'  -- **************************************************************
    and trim(yhcemp) = trim(p.ymempn)
-- the fuckers cheated, using deduction code 99 for employee AND employer amounts
-- oops, yhctyp
    and trim(yhccde) = '99'
    and yhctyp = '2'), 0) as "Roth Deferral",
  coalesce(ec.EmprCont, 0) as "Employer Match",
  coalesce((
    select sum(yhccam)
    from rydedata.pyhscdta
    where ypbcyy = '113'  -- **************************************************************
    and trim(yhcemp) = trim(p.ymempn)
    and trim(yhccde) = '91'), 0) as "Profit Sharing",
  p.ymstre as "Street Address",
  p.ymcity as City,
  p.ymstat as State,
  p.ymzip as Zip
from rydedata.pymast p
inner join ( 
  select yhdemp, 
    sum(yhdhrs) as Hours,  -- 7 Reg Hours
    sum(yhacm) as Gross -- 8 Curr Adj Comp
  from rydedata.pyhshdta 
  where ypbcyy = '113' -- payroll cen + year **************************************************************
  group by yhdemp) g on p.ymempn = g.yhdemp
left join ( -- employer contribution via GL
  SELECT trim(gtctl#) as gtctl#, abs(SUM(gttamt)) as EmprCont 
  FROM rydedata.glptrns
  WHERE gtacct in ('133003', '133004','233001', '233004','333003', '333004')
    AND gtdate BETWEEN '01/01/2011' AND '12/31/2011'
  GROUP BY trim(gtctl#)) ec on trim(p.ymempn) = ec.gtctl# 
order by p.ymco#, p.ymname

/* honda 401k issue */
-- 233001 is where the 401k empr cont is.
select *
from rydedata.glptrns
where gtacct like '23300%'
  AND gtdate BETWEEN '01/01/2011' AND '12/31/2011' 

select *
from rydedata.pydeduct
where ydco# = 'RY2'
and yddcde = '91'

select *
from rydedata.glptrns
where trim(gtdoc#) = '4593'
/* honda 401k issue */



/* 
trying to figure out where the fuck employer contribution was
until i decided to use the GL
select distinct trim(yhdemp) as yhdemp, b.ymname
from rydedata.pyhshdta a
left join rydedata.pymast b on a.yhdemp = trim(b.ymempn)
where yhcsuc <> 0

select *
from rydedata.pydeduct
where trim(ydempn) in ('16298','11650')

select distinct yddcde, yddamt, yddper
from rydedata.pydeduct
where yddper <> ''

-- ok, here is dharma acharya check from 12/17/2011 that shows the 2 different employer contr amounts
-- Retire Emplr (ytd) = 15.83
-- Emplr contrib (ytd) = 364.21
select *
from rydedata.pyhshdta
where ypbnum = 1223000
and trim(yhdemp) = '11650'
-- those amounts are not in pyhshdta
select *
from rydedata.pyhscdta
where ypbnum = 1223000
and trim(yhcemp) = '11650'

-- group and sum pyhscdta for 2011
--, ok, this should be the employee contributions
select ypbcyy, yhcemp, yhccde, sum(yhccam)
from rydedata.pyhscdta
where ypbcyy = '111'
and trim(yhcemp) = '11650'
group by ypbcyy, yhcemp, yhccde
-- where are the fucking employer contributions
-- seems like the should be in pyhshdta
select *
from rydedata.pyhshdta
where ypbnum = 1223000
and trim(yhdemp) = '11650'

  select yhdemp, 
    sum(yhcrtm) as total, -- 17 Emplr Curr Ret
    sum(yhdhrs) as Hours,  -- 7 Reg Hours
    sum(yhacm) as Gross -- 8 Curr Adj Comp
  from rydedata.pyhshdta -- Payroll Header transaction file
  where ypbcyy = '111' -- payroll cen + year

  group by yhdemp


SELECT trim(gtctl#), abs(SUM(gttamt))  
FROM rydedata.glptrns
WHERE gtacct in ('133003', '133004')
  AND gtdate BETWEEN '01/01/2011' AND '12/31/2011'
GROUP BY trim(gtctl#), trim(gtacct)  
*/
/*
 2/10/12 pyactgr to which accounts should 401k/roth be accrued
*/
select ytadic, ytaera, ytacon
from rydedata.pyactgr
where ytaco# = 'RY1'
group by ytadic, ytaera, ytacon

select *
from rydedata.pyactgr
where ytagco = 'RY1'
  and trim(ytadic) = 'MGR'

select *
from rydedata.pyactgr
where trim(ytaera) <> trim (ytacon)

select ydco#, ydempn, yddcde
from rydedata.pydeduct
group by ydco#, ydempn, yddcde
  having count(*) > 1

select d1.*
from rydedata.pydeduct d1
inner join (
    select ydco#, ydempn, yddcde
    from rydedata.pydeduct
    group by ydco#, ydempn, yddcde
    having count(*) > 1) d2 on d1.ydco# = d2.ydco#
  and trim(d1.ydempn) = trim(d2.ydempn)
  and trim(d1.yddcde) = trim(d2.yddcde)
order by ydco#, ydempn, yddcde

select *
from rydedata.pypcodes
where trim(ytddcd) in ('62','275')

select ytdco#, ytdtyp, ytddcd
from rydedata.pypcodes
group by  ytdco#, ytdtyp, ytddcd
order by ytdco#, ytddcd
  having count(*) > 1

select *
from rydedata.pypcodes
where trim(ytddcd) in ('99','91')
order by ytdco#, ytddcd


select *
from rydedata.pypcodes
where ytdact like '1330%'


  SELECT trim(gtacct), count(*)
  --select *
  FROM rydedata.glptrns
  WHERE gtacct in ('133001', '133003', '133004', '133007')
    AND gtdate BETWEEN '01/01/2012' AND '12/31/2012'
  GROUP BY trim(gtacct)
  
/*
1/28/14 to facilitate adding the ssn, add a ssn column, and export the whole deal into advantage, dds.tmp401K, then 
        join on tmpSsnPriorYearWithTerms to get the ssn in the report for kim 
2/7/14 kim called, retirement employer match for john olderbak shows 1379.50 should be 1767.23
       *a* : join to glptrns for generating employer match, i had not adjusted the date
1/12/15 changed the dates and worked ok
1/5/16  added code 99A for Roth (could be 99 or 99A)
1/6/17  pymast.ymstre -> ymadd1
        add: status : active parttime or termed, after employee type
             both original and latest hire date, so do orig hire date, last hire date
1/29/18 no changes
*/ 

select p.ymco#, p.ymname as Name, trim(p.ymempn) as ymempn, space(11) as ssn,
  cast(
    case length(trim(p.ymbdte))
      when 5 then  '19'||substr(trim(p.ymbdte),4,2)||'-'|| '0' || left(trim(p.ymbdte),1) || '-' ||substr(trim(p.ymbdte),2,2)
      when 6 then  '19'||substr(trim(p.ymbdte),5,2)||'-'|| left(trim(p.ymbdte),2) || '-' ||substr(trim(p.ymbdte),3,2)
    end as date) as "Birth Date",
  case 
    when cast(right(trim(ymhdto),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymhdto))
          when 5 then  '20'||substr(trim(p.ymhdto),4,2)||'-'|| '0' || left(trim(p.ymhdto),1) || '-' ||substr(trim(p.ymhdto),2,2)
          when 6 then  '20'||substr(trim(p.ymhdto),5,2)||'-'|| left(trim(p.ymhdto),2) || '-' ||substr(trim(p.ymhdto),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdto))
          when 5 then  '19'||substr(trim(p.ymhdto),4,2)||'-'|| '0' || left(trim(p.ymhdto),1) || '-' ||substr(trim(p.ymhdto),2,2)
          when 6 then  '19'||substr(trim(p.ymhdto),5,2)||'-'|| left(trim(p.ymhdto),2) || '-' ||substr(trim(p.ymhdto),3,2)
        end as date) 
    end as "Original Hire Date",    
  case 
    when cast(right(trim(ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as "Latest Hire Date",
  case 
    when cast(right(trim(ymtdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymtdte))
          when 5 then  '20'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
          when 6 then  '20'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymtdte))
          when 5 then  '19'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
          when 6 then  '19'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
        end as date) 
    end as "Term Date",
  coalesce(  
    case ymactv
      when 'A' then '1'
      when 'P' then '2'
    else null
    end, ' ') as "Employee Type",
    case ymactv
      when 'A' then 'Active'
      when 'P' then 'Part-Time'
      when 'T' then 'Termed'
    end as Status,
  cast(g.Hours as Integer) as "YTD Hours",
  g.Gross as "Gross Compensation",
  coalesce((
    select sum(yhccam)
    from rydedata.pyhscdta
    where ypbcyy = '117'  -- **************************************************************
    and trim(yhcemp) = trim(p.ymempn)
-- the fuckers cheated, using deduction code 99 for employee AND employer amounts
-- oops, yhctyp
-- 1/5/16
--    and trim(yhccde) = '99'
    and trim(yhccde) in ('99', '99A')
    and yhctyp = '2'), 0) as "Roth Deferral",
  coalesce(ec.EmprCont, 0) as "Employer Match",
  coalesce((
    select sum(yhccam)
    from rydedata.pyhscdta
    where ypbcyy = '117'  -- **************************************************************
    and trim(yhcemp) = trim(p.ymempn)
    and trim(yhccde) = '91'), 0) as "Profit Sharing",
  p.ymadd1 as "Street Address",
  p.ymcity as City,
  p.ymstat as State,
  p.ymzip as Zip
from rydedata.pymast p
inner join ( 
  select yhdemp, 
    sum(yhdhrs) as Hours,  -- 7 Reg Hours
    sum(yhacm) as Gross -- 8 Curr Adj Comp
  from rydedata.pyhshdta 
  where ypbcyy = '117' -- payroll cen + year **************************************************************
  group by yhdemp) g on p.ymempn = g.yhdemp
left join ( -- employer contribution via GL
  SELECT trim(gtctl#) as gtctl#, abs(SUM(gttamt)) as EmprCont 
  FROM rydedata.glptrns
  WHERE gtacct in ('133003', '133004','233001', '233004','333003', '333004')
-- *a*  
    AND gtdate BETWEEN '01/01/2017' AND '12/31/2017' -- **************************************************************
  GROUP BY trim(gtctl#)) ec on trim(p.ymempn) = ec.gtctl# 
order by p.ymco#, p.ymname
  
/*
1/30/20
kim is asking for this for 2019
i can find the report that i sent her for 2018
but i am not sure this is the sql that generated it
the above query is for 2017, don't know where the sql for 2018 is
the data in dds.tmp401K matches the 2018 spreadsheet
modify the above query (below) to generate 2019 sata
*/

select p.ymco#, p.ymname as Name, trim(p.ymempn) as ymempn, space(11) as ssn,
  cast(
    case length(trim(p.ymbdte))
      when 5 then  '19'||substr(trim(p.ymbdte),4,2)||'-'|| '0' || left(trim(p.ymbdte),1) || '-' ||substr(trim(p.ymbdte),2,2)
      when 6 then  '19'||substr(trim(p.ymbdte),5,2)||'-'|| left(trim(p.ymbdte),2) || '-' ||substr(trim(p.ymbdte),3,2)
    end as date) as "Birth Date",
  case 
    when cast(right(trim(ymhdto),2) as integer) < 30 then 
      cast (
        case length(trim(p.ymhdto))
          when 5 then  '20'||substr(trim(p.ymhdto),4,2)||'-'|| '0' || left(trim(p.ymhdto),1) || '-' ||substr(trim(p.ymhdto),2,2)
          when 6 then  '20'||substr(trim(p.ymhdto),5,2)||'-'|| left(trim(p.ymhdto),2) || '-' ||substr(trim(p.ymhdto),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdto))
          when 5 then  '19'||substr(trim(p.ymhdto),4,2)||'-'|| '0' || left(trim(p.ymhdto),1) || '-' ||substr(trim(p.ymhdto),2,2)
          when 6 then  '19'||substr(trim(p.ymhdto),5,2)||'-'|| left(trim(p.ymhdto),2) || '-' ||substr(trim(p.ymhdto),3,2)
        end as date) 
    end as "Original Hire Date",    
  case 
    when cast(right(trim(ymhdte),2) as integer) < 30 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as "Latest Hire Date",
  case 
    when cast(right(trim(ymtdte),2) as integer) < 30 then 
      cast (
        case length(trim(p.ymtdte))
          when 5 then  '20'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
          when 6 then  '20'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymtdte))
          when 5 then  '19'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
          when 6 then  '19'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
        end as date) 
    end as "Term Date",
  coalesce(  
    case ymactv
      when 'A' then '1'
      when 'P' then '2'
    else null
    end, ' ') as "Employee Type",
    case ymactv
      when 'A' then 'Active'
      when 'P' then 'Part-Time'
      when 'T' then 'Termed'
    end as Status,
  cast(g.Hours as Integer) as "YTD Hours",
  g.Gross as "Gross Compensation",
  coalesce((
    select sum(yhccam)
    from rydedata.pyhscdta
    where ypbcyy = '119'  -- **************************************************************
    and trim(yhcemp) = trim(p.ymempn)
-- the fuckers cheated, using deduction code 99 for employee AND employer amounts
-- oops, yhctyp
-- 1/5/16
--    and trim(yhccde) = '99'
    and trim(yhccde) in ('99', '99A')
    and yhctyp = '2'), 0) as "Roth Deferral",
  coalesce(ec.EmprCont, 0) as "Employer Match",
  coalesce((
    select sum(yhccam)
    from rydedata.pyhscdta
    where ypbcyy = '119'  -- **************************************************************
    and trim(yhcemp) = trim(p.ymempn)
    and trim(yhccde) = '91'), 0) as "Profit Sharing",
  p.ymadd1 as "Street Address",
  p.ymcity as City,
  p.ymstat as State,
  p.ymzip as Zip
from rydedata.pymast p
inner join ( 
  select yhdemp, 
    sum(yhdhrs) as Hours,  -- 7 Reg Hours
    sum(yhacm) as Gross -- 8 Curr Adj Comp
  from rydedata.pyhshdta 
  where ypbcyy = '119' -- payroll cen + year **************************************************************
  group by yhdemp) g on p.ymempn = g.yhdemp
left join ( -- employer contribution via GL
  SELECT trim(gtctl#) as gtctl#, abs(SUM(gttamt)) as EmprCont 
  FROM rydedata.glptrns
  WHERE gtacct in ('133003', '133004','233001', '233004','333003', '333004')
-- *a*  
    AND gtdate BETWEEN '01/01/2019' AND '12/31/2019' -- **************************************************************
  GROUP BY trim(gtctl#)) ec on trim(p.ymempn) = ec.gtctl# 
order by p.ymco#, p.ymname


************************** MOVED TO POSTGRESQL  11/10/2020  ******************************************************

select p.ymco#, p.ymname as Name, trim(p.ymempn) as ymempn, space(11) as ssn,
  cast(
    case length(trim(p.ymbdte))
      when 5 then  '19'||substr(trim(p.ymbdte),4,2)||'-'|| '0' || left(trim(p.ymbdte),1) || '-' ||substr(trim(p.ymbdte),2,2)
      when 6 then  '19'||substr(trim(p.ymbdte),5,2)||'-'|| left(trim(p.ymbdte),2) || '-' ||substr(trim(p.ymbdte),3,2)
    end as date) as "Birth Date",
  case 
    when cast(right(trim(ymhdto),2) as integer) < 30 then 
      cast (
        case length(trim(p.ymhdto))
          when 5 then  '20'||substr(trim(p.ymhdto),4,2)||'-'|| '0' || left(trim(p.ymhdto),1) || '-' ||substr(trim(p.ymhdto),2,2)
          when 6 then  '20'||substr(trim(p.ymhdto),5,2)||'-'|| left(trim(p.ymhdto),2) || '-' ||substr(trim(p.ymhdto),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdto))
          when 5 then  '19'||substr(trim(p.ymhdto),4,2)||'-'|| '0' || left(trim(p.ymhdto),1) || '-' ||substr(trim(p.ymhdto),2,2)
          when 6 then  '19'||substr(trim(p.ymhdto),5,2)||'-'|| left(trim(p.ymhdto),2) || '-' ||substr(trim(p.ymhdto),3,2)
        end as date) 
    end as "Original Hire Date",    
  case 
    when cast(right(trim(ymhdte),2) as integer) < 30 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as "Latest Hire Date",
  case 
    when cast(right(trim(ymtdte),2) as integer) < 30 then 
      cast (
        case length(trim(p.ymtdte))
          when 5 then  '20'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
          when 6 then  '20'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymtdte))
          when 5 then  '19'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
          when 6 then  '19'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
        end as date) 
    end as "Term Date",
  coalesce(  
    case ymactv
      when 'A' then '1'
      when 'P' then '2'
    else null
    end, ' ') as "Employee Type",
    case ymactv
      when 'A' then 'Active'
      when 'P' then 'Part-Time'
      when 'T' then 'Termed'
    end as Status,
  cast(g.Hours as Integer) as "YTD Hours",
  g.Gross as "Gross Compensation",
  coalesce((
    select sum(yhccam)
    from rydedata.pyhscdta
    where ypbcyy = '120'  -- **************************************************************
    and trim(yhcemp) = trim(p.ymempn)
-- the fuckers cheated, using deduction code 99 for employee AND employer amounts
-- oops, yhctyp
-- 1/5/16
--    and trim(yhccde) = '99'
    and trim(yhccde) in ('99','99A','99B','99C')
    and yhctyp = '2'), 0) as "Roth Deferral",
  coalesce(ec.EmprCont, 0) as "Employer Match",
  coalesce((
    select sum(yhccam)
    from rydedata.pyhscdta
    where ypbcyy = '120'  -- **************************************************************
    and trim(yhcemp) = trim(p.ymempn)
    and trim(yhccde) in('91','91B','91C')
    and yhctyp = '2'), 0) as "Profit Sharing",
  p.ymadd1 as "Street Address",
  p.ymcity as City,
  p.ymstat as State,
  p.ymzip as Zip
from rydedata.pymast p
inner join ( 
  select yhdemp, 
    sum(yhdhrs) as Hours,  -- 7 Reg Hours
    sum(yhacm) as Gross -- 8 Curr Adj Comp
  from rydedata.pyhshdta 
  where ypbcyy = '120' -- payroll cen + year **************************************************************
  group by yhdemp) g on p.ymempn = g.yhdemp
left join ( -- employer contribution via GL
  SELECT trim(gtctl#) as gtctl#, abs(SUM(gttamt)) as EmprCont 
  FROM rydedata.glptrns
  WHERE gtacct in ('133003', '133004','233001', '233004','333003', '333004')
-- *a*  
    AND gtdate BETWEEN '01/01/2020' AND '12/31/2020' -- **************************************************************
  GROUP BY trim(gtctl#)) ec on trim(p.ymempn) = ec.gtctl# 
where trim(p.pymast_employee_number) in( '123100','196341')
order by p.ymco#, p.ymname


