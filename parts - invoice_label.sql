with
  total as (
    select sum(ptqty) 
    from rydedata.pdptdet
    where trim(ptinv#) = '15421386')
select a.invoice_number, a.sort_name, 
  b.ptpart,  
  trim(char(row_number() over (partition by b.ptinv# order by ptseq#)))
  || ' of ' ||
  trim(char((select * from total)))
from rydedata.pdpthdr a
inner join rydedata.pdptdet b on a.company_number = b.ptco#
  and trim(a.invoice_number) = trim(b.ptinv#)
where trim(a.invoice_number) = '15421386'
  and b.ptqty > 0
order by b.ptseq#


when there is more than one of a part 15421297

select *
from rydedata.pdptdet
where ptdate > 20181000
  and ptqty > 1
  and left(trim(ptinv#),4) = '1542'
  and ptseq# > 600
order by ptinv#  


when there is more than one of a part 15421297

with
  total as (
    select sum(ptqty) 
    from rydedata.pdptdet
    where trim(ptinv#) = '15421297')
select a.invoice_number, a.sort_name, 
  ptseq#, b.ptpart, ptqty, 
  trim(char(row_number() over (partition by b.ptinv# order by ptseq#)))
  || ' of ' ||
  trim(char((select * from total)))
from rydedata.pdpthdr a
inner join rydedata.pdptdet b on a.company_number = b.ptco#
  and trim(a.invoice_number) = trim(b.ptinv#)
where trim(a.invoice_number) = '15421297'
  and b.ptqty > 0
order by b.ptseq#


select max(ptqty) from rydedata.pdptdet where left(trim(ptinv#),3) = '154'


with 
  n(n) as (
    select 1 from sysibm.sysdummy1
    union all
    select n+1 from n where n < 1000)
select n 
from n
order by n


-- this does it    
with
  total as (
    select sum(ptqty) 
    from rydedata.pdptdet
    where trim(ptinv#) = '15421297'),
  n(n) as (
    select 1 from sysibm.sysdummy1
    union all
    select n+1 from n where n < 1000)    
select aa.invoice_number, aa.sort_name, aa.purchase_order_, aa.ptpart, 
  row_number() over (partition by aa.invoice_number order by aa.ptseq#) as item_number, 
  (select * from total) as total_items
from (    
  select a.invoice_number, a.sort_name, a.purchase_order_,
    ptseq#, b.ptpart, ptqty
  from rydedata.pdpthdr a
  inner join rydedata.pdptdet b on a.company_number = b.ptco#
    and trim(a.invoice_number) = trim(b.ptinv#)
  where trim(a.invoice_number) = '15421297') aa
cross join n bb
where bb.n <= aa.ptqty
order by aa.ptseq#



with
  total as (
    select sum(ptqty) 
    from rydedata.pdptdet
    where trim(ptinv#) = '15421297'),
  items(n) as (
    select 1 from sysibm.sysdummy1
    union all
    select n+1 from items where n < 1000)    
select aa.invoice_number, aa.sort_name, aa.purchase_order_, aa.ptpart, 
  row_number() over (partition by aa.invoice_number order by aa.ptseq#) as item_number, 
  (select * from total) as total_items
from (    
  select a.invoice_number, a.sort_name, a.purchase_order_,
    ptseq#, b.ptpart, ptqty
  from rydedata.pdpthdr a
  inner join rydedata.pdptdet b on a.company_number = b.ptco#
    and trim(a.invoice_number) = trim(b.ptinv#)
  where trim(a.invoice_number) = '15421297') aa
cross join items bb
where bb.n <= aa.ptqty
order by aa.ptseq#

  
-- 10/17/18 need the ship to info

with
  total as (
    select sum(ptqty) 
    from rydedata.pdptdet
    where trim(ptinv#) = '15421386')
select a.invoice_number, a.sort_name, 
  b.*
from rydedata.pdpthdr a
inner join rydedata.pdptdet b on a.company_number = b.ptco#
  and trim(a.invoice_number) = trim(b.ptinv#)
where trim(a.invoice_number) = '15421386'
  -- and b.ptqty > 0
order by b.ptseq#


select a.invoice_number, a.sort_name, 
  b.*
from rydedata.pdpthdr a
inner join rydedata.pdptdet b on a.company_number = b.ptco#
  and trim(a.invoice_number) = trim(b.ptinv#)
where trim(a.invoice_number) like '1542%'
  and ptline = 999
  and ptseq# <> 0
limit 500

with

  
-- and this takes care of it  
with
  total as (
    select sum(ptqty) 
    from rydedata.pdptdet
    where trim(ptinv#) = '15421386'),
  n(n) as (
    select 1 from sysibm.sysdummy1
    union all
    select n+1 from n where n < 1000),
  freight_out as (
    select ptpart
    from rydedata.pdptdet
    where trim(ptinv#) = '15421386'
      and ptline = 999
      and ptseq# = 0)       
select aa.invoice_number, aa.sort_name, aa.purchase_order_, aa.ptpart, 
  row_number() over (partition by aa.invoice_number order by aa.ptseq#) as item_number, 
  (select * from total) as total_items,
  (select * from freight_out)
from (    
  select a.invoice_number, a.sort_name, a.purchase_order_,
    ptseq#, b.ptpart, ptqty
  from rydedata.pdpthdr a
  inner join rydedata.pdptdet b on a.company_number = b.ptco#
    and trim(a.invoice_number) = trim(b.ptinv#)
  where trim(a.invoice_number) = '15421386') aa
cross join n bb
where bb.n <= aa.ptqty
order by aa.ptseq#  


-- 11/14/18
-- parts is complaining, label is printing from invoice (pdptdet) in sequence order
-- they want in the picking ticket order, don't know where to find that data.
-- picking ticket order is not reflected in pdptdet

example: invoice: 15425485
pick ticket:
  po: SCHULTZ
  sold to: NORTHWEST AUTO BODY
  
  
select *
from rydedata.pdpthdr a
inner join rydedata.pdptdet b on a.company_number = b.ptco#
  and trim(a.invoice_number) = trim(b.ptinv#)
where trim(a.invoice_number) = '15425485'

-- can't find anything in glppohd (purchase orders)
select *
from rydedata.glppohd
where trim(po_number) = '15425485'

select *
from rydedata.glppohd
where trim(vendor_name) like '%NORTWHEST%'

select *
from rydedata.glppohd
where po_date = '2018-11-14'


select * from rydedata.glppohd where po_amount = 2832.30  
  
select * from rydedata.glppohd  where trim(vendor_number) = '1104797'

-- 11/15/18
from help:
Note: Parts print on the picking ticket based on the bin location assigned. 
Bins are sorted in order by the following values: numeric, alpha-numeric, and alphabetic in ascending order. 



select *
from rydedata.pdpthdr a
inner join rydedata.pdptdet b on a.company_number = b.ptco#
  and trim(a.invoice_number) = trim(b.ptinv#)
where trim(a.invoice_number) = '15425485'

-- holy shit, the goofy sort order above, looks like the default db2 sort order
select a.ptinv#, a.ptseq#, a.ptpart, b.bin_location
from rydedata.pdptdet a
left join rydedata.pdpmast b on trim(a.ptpart) =trim(b.part_number)
where trim(a.ptinv#) = '15425485'
order by b.bin_location


with
  total as (
    select sum(ptqty) 
    from rydedata.pdptdet
    where trim(ptinv#) = '15425485'),
  n(n) as (
    select 1 from sysibm.sysdummy1
    union all
    select n+1 from n where n < 1000),
  freight_out as (
    select ptpart
    from rydedata.pdptdet
    where trim(ptinv#) = '15425485'
      and ptline = 999
      and ptseq# = 0)       
select aa.invoice_number, aa.sort_name, aa.purchase_order_, aa.ptpart, 
  row_number() over (partition by aa.invoice_number order by aa.ptseq#) as item_number, 
  (select * from total) as total_items,
  (select * from freight_out)
from (    
  select a.invoice_number, a.sort_name, a.purchase_order_,
    ptseq#, b.ptpart, ptqty
  from rydedata.pdpthdr a
  inner join rydedata.pdptdet b on a.company_number = b.ptco#
    and trim(a.invoice_number) = trim(b.ptinv#)
  where trim(a.invoice_number) = '15425485') aa
cross join n bb
where bb.n <= aa.ptqty
order by aa.ptseq#  

-- add pdpmast for bin_location
with
  total as (
    select sum(ptqty) 
    from rydedata.pdptdet
    where trim(ptinv#) = '15425485'),
  n(n) as (
    select 1 from sysibm.sysdummy1
    union all
    select n+1 from n where n < 1000),
  freight_out as (
    select ptpart
    from rydedata.pdptdet
    where trim(ptinv#) = '15425485'
      and ptline = 999
      and ptseq# = 0)       
select aa.invoice_number, aa.sort_name, aa.purchase_order_, aa.ptpart, 
  row_number() over (partition by aa.invoice_number order by aa.ptseq#) as item_number, 
  (select * from total) as total_items,
  (select * from freight_out) -- , aa.bin_location
from (    
  select a.invoice_number, a.sort_name, a.purchase_order_,
    ptseq#, b.ptpart, ptqty, c.bin_location
  from rydedata.pdpthdr a
  inner join rydedata.pdptdet b on a.company_number = b.ptco#
    and trim(a.invoice_number) = trim(b.ptinv#)
  left join rydedata.pdpmast c on trim(b.ptpart) = trim(c.part_number)
    and a.company_number = c.company_number
  where trim(a.invoice_number) = '15425485') aa
cross join n bb
where bb.n <= aa.ptqty
order by aa.bin_location, aa.ptpart




select * from rydedata.pdptdet  where trim(ptinv#) = '15429002'

select * from rydedata.pdpthdr 
where trans_date = 20181207
 and trim( sort_name) = 'PIERCE AUTO BODY'


select * from rydedata.pdpthdr 
where trim( sort_name) = 'PIERCE AUTO BODY'
  and trans_date  > 20180000
