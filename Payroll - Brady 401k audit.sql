From: Dew, Lynn [mailto:lynn.dew@bradymartz.com] 
Sent: Tuesday, July 07, 2015 2:52 PM
To: Taylor Monson
Subject: FW: Follow up

Hi Taylor,

These were the items I had requested from Kim.  Any information would be helpful!  Thanks!


-  I remember when you and I were looking for Michael Yem�s contributions to the 401k plan, we had to go back a ways to find a payroll where he had been contributing, but I can�t recall which pay period we ended up looking at.  Could you please send me Michael�s portion of the payroll register for any period in which he contributed to the plan as well as the statement showing the Employer contributions to his account?

-  Robert Rydell maxes out his contribution each year.  Does he make a one-time payment?  If so, could you send me documentation of the pay period in which he made his contribution?

-  Lastly, Tony Arrington had a hardship distribution in May of 2014.  Could you send me a pay stub of his from October to verify that no deferrals were made for the 6 months thereafter?



Select b.ymname, a.payroll_run_number, a.emply_curr_ret, a.emplr_curr_ret
-- select *
from RYDEDATA.PYHSHDTA a
inner join rydedata.pymast b on a.employee_ = b.ymempn
--where a.payroll_cen_year = 114
where lower(b.ymname) like 'yem%'
  and a.emply_curr_ret <> 0
  
  
Select b.ymname, b.ymempn, a.amount, a.entered_amount, a.description, 
  c.check_date, c.payroll_end_date, d.reference_check_
-- select *
from RYDEDATA.pyhscdta a
inner join rydedata.pymast b on a.employee_number = b.ymempn
left join rydedata.pyptbdta c on a.payroll_run_number = c.payroll_run_number
  and c.company_number = 'RY1'
left join rydedata.pyhshdta d on c.payroll_run_number = d.payroll_run_number
  and c.company_number = d.company_number  
  and c.payroll_cen_year = d.payroll_cen_year
  and b.ymempn = d.employee_
where a.payroll_cen_year = 114
  AND lower(b.ymname) like 'yem%'
  and a.code_id in ('91','99')
  
  
select * from rydedata.pymast where lower(ymname) like 'rydell%'  
Bob: 2120492

Select b.ymname, b.ymempn, a.amount, a.entered_amount, a.description, 
  c.check_date, c.payroll_end_date, d.reference_check_
-- select *
from RYDEDATA.pyhscdta a
inner join rydedata.pymast b on a.employee_number = b.ymempn
left join rydedata.pyptbdta c on a.payroll_run_number = c.payroll_run_number
  and c.company_number = 'RY2'
left join rydedata.pyhshdta d on c.payroll_run_number = d.payroll_run_number
  and c.company_number = d.company_number  
  and c.payroll_cen_year = d.payroll_cen_year
  and b.ymempn = d.employee_
where a.payroll_cen_year = 114
  AND b.ymempn = '2120492'
  and a.code_id in ('91','99')