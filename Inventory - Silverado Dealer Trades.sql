select gtdate as date_in, trim(gtctl#) as stock_in, 
from rydedata.glptrns
where trim(gtacct) = '123700'
  and gttamt > 10000
  and gtdate > '2016-01-01'
  
  
select *
from rydedata.glptrns
where trim(gtacct) = '123700'
  and gttamt < -10000
  and gtdate > '2016-01-01'  
  
-- though sketchy relying on the description, this is probably good enuf  
select a.gtdate, trim(a.gtctl#) as stock_number, a.gtdesc as "Sold To", b.body_style
-- select *
from rydedata.glptrns a
inner join rydedata.inpmast b on trim(a.gtctl#) = trim(b.imstk#)
  and b.model = 'SILVERADO 1500'
  and b.body_style like '%CREW%'
where trim(a.gtacct) = '123700'
  and trim(a.gtjrnl) = 'VSN'  
  and a.gtdate > '2016-01-01'  
  and (left(trim(a.gtdesc), 3) = 'D/T' or left(trim(a.gtdesc), 2) = 'DT')
  
  
  
select * 
from rydedata.glptrns a
inner join rydedata.inpmast b on trim(a.gtctl#) = trim(b.imstk#)
  and b.model = 'SILVERADO 1500'
  and b.body_style like '%CREW%'
where a.gtdate > '2016-01-01'
  and trim(a.gtacct) = '123700'
  and trim(a.gtjrnl) = 'VSN'
  and not exists (
    select 1
    from rydedata.glptrns 
    where trim(gtctl#) = trim(a.gtctl#) 
    and trim(gtacct) = '1429301')  