/*  BOPMAST */
FIELD   Field Def      VALUE      
BMSTAT  Record Status             In Process Deal (Unaccepted)
BMSTAT  Record Status   A         Accepted Deal
BMSTAT  Record Status   U         Capped Deal
BMTYPE  Record Type     C         Cash, Wholesale, Fleet, or Dealer Xfer
BMTYPE  Record Type     F         Financed Retail Deal
BMTYPE  Record Type     L         Financed Lease Deal
BMTYPE  Record Type     O         Cash Deal w/ Owner Financing
BMVTYP  Vehicle Type    N         New  
BMVTYP  Vehicle Type    U         Used  
BMWHSL  Sale Type       F         Fleet Deal
BMWHSL  Sale Type       L         Lease Deal
BMWHSL  Sale Type       R         Retail Deal
BMWHSL  Sale Type       W         Wholesale Deal
BMWHSL  Sale Type       X         Dealer Xfer


Select a.company_number, a.sale_date, a.deal_key, a.status_c_capped, a.sales_person_id, 
  a.sales_person_name, a.count_unit, a.unit_count, 
  b.bopmast_company_number, b.record_Status, trim(b.bopmast_stock_number) as stock_number, 
  b.bopmast_vin, b.bopmast_search_name, b.primary_salespers, b.secondary_slspers2,
  b.unwind_flag, b.date_saved, b.date_approved, b.date_capped
from RYDEDATA.BOPSLSS a
inner join rydedata.bopmast b on a.deal_key = b.record_key
where a.sale_Date >= 20160000
  and a.sales_person_type = 'S'
order by trim(b.bopmast_stock_number)


-- from tool, uc unwinds 2016
-- only 26961XX shows up (resold 2/5/16)
Select a.company_number, a.sale_date, a.deal_key, a.status_c_capped, a.sales_person_id, 
  a.sales_person_name, a.count_unit, a.unit_count, 
  b.bopmast_company_number, b.record_Status, trim(b.bopmast_stock_number) as stock_number, 
  b.bopmast_vin, b.bopmast_search_name, b.primary_salespers, b.secondary_slspers2,
  b.unwind_flag, b.date_saved, b.date_approved, b.date_capped
from RYDEDATA.BOPSLSS a
inner join rydedata.bopmast b on a.deal_key = b.record_key
where a.sale_Date >= 20160000
  and a.sales_person_type = 'S'
  and trim(b.bopmast_Stock_number) in ('27099A','26679A','24949C','26961XX','27454P')
  
select a.*, b.inpmast_stock_number
from rydedata.inpcmnt a
left join rydedata.inpmast b on a.vin = b.imvin
where upper(comment) like '%WOUND%'
  and transaction_date > 20160000
order by trim(inpmast_stock_number)


-- split deals
Select a.company_number, a.sale_date, a.deal_key, a.status_c_capped, a.sales_person_id, 
  a.sales_person_name, a.count_unit, a.unit_count, 
  b.bopmast_company_number, b.record_Status, trim(b.bopmast_stock_number) as stock_number, 
  b.bopmast_vin, b.bopmast_search_name, b.primary_salespers, b.secondary_slspers2,
  b.unwind_flag, b.date_saved, b.date_approved, b.date_capped
from RYDEDATA.BOPSLSS a
inner join rydedata.bopmast b on a.deal_key = b.record_key
where a.sale_Date >= 20160000
  and a.sales_person_type = 'S'
  and b.secondary_slspers2 <> ''
order by trim(b.bopmast_stock_number)

-- 2/11/16 make bopmast the base

-- bopslsp does not consistently include the employee number, need to use dimSalesConsultant

select year(date_capped)*100 + month(date_capped), bopmast_company_number, 
  record_key, bopmast_Stock_number, bopmast_vin, 
  bopmast_search_name, date_capped, primary_salespers, secondary_slspers2,
  b.year as model_year, b.make, b.model 
-- select *
from rydedata.bopmast a
left join rydedata.inpmast b on a.bopmast_vin = b.imvin
where date_capped > '12/31/2015'
  and a.sale_type <> 'W'


  