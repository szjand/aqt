SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
--INTO #jon
FROM (
    SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, 
      cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) as ptdate,
      d.pttech, 
      coalesce(d.ptlhrs, 0) AS ptlhrs
    FROM rydedata.PDPPDET d
    INNER JOIN rydedata.PDPPHDR h ON d.ptpkey = h.ptpkey
    WHERE d.ptcode = 'TT'
      AND cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) BETWEEN '03/11/2012' AND '03/24/2012'
  UNION 
    SELECT s.ptco#, s.ptro#, s.ptline, 
      cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) as ptdate,
      s.pttech, 
      sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
    FROM rydedata.SDPRDET s
    WHERE s.ptdate IS NOT NULL 
      and s.ptdate <> 0
      AND s.ptcode = 'TT'
      AND cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) BETWEEN  '03/11/2012' AND '03/24/2012'
      AND s.ptlhrs < 300
    GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech) x
--  UNION -- negative xtim adj
--  SELECT ptco#, ptro#, ptline, ptdate, pttech, round(SUM(ptlhrs),2) AS ptlhrs
--  FROM (
--    SELECT DISTINCT ptco#, ptpkey, ptro#, ptline, ptdate, pttech, coalesce(ptlhrs, 0 ) AS ptlhrs
--    FROM rydedata.SDPXTIM) xx 
--  WHERE xx.ptlhrs < 0
--    AND xx.ptdate BETWEEN  '04/09/2012' AND '04/12/2012'
--    AND 
--      EXISTS (
--        SELECT 1
--        FROM rydedata.SDPRDET
--        WHERE ptco# = xx.ptco#
--          AND ptro# = xx.ptro#
--          AND ptline = xx.ptline
--          AND pttech = xx.pttech
--          AND abs(ptlhrs) = abs(xx.ptlhrs)) 
--      OR EXISTS (  
--      SELECT 1
--        FROM rydedata.PDPPDET d
--        WHERE d.ptco# = xx.ptco#
--          AND d.ptpkey = xx.ptpkey
--          AND d.ptline = xx.ptline
--          AND d.pttech = xx.pttech
--          AND abs(d.ptlhrs) = abs(xx.ptlhrs))     
--  GROUP BY ptco#, ptdate, ptro#, ptline, pttech) y  
--WHERE ptlhrs <> 0    
GROUP BY ptco#, ptdate, pttech, ptro#, ptline





SELECT pttech, round(SUM(flaghours), 2) 
FROM #jon  
WHERE ptco# = 'ry1'
GROUP BY pttech

select pttech, round(sum(flaghours), 2)
from (
SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
FROM (
    SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, 
      cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) as ptdate,
      d.pttech, 
      coalesce(d.ptlhrs, 0) AS ptlhrs
    FROM rydedata.PDPPDET d
    INNER JOIN rydedata.PDPPHDR h ON d.ptpkey = h.ptpkey
    WHERE d.ptcode = 'TT'
      AND cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) BETWEEN '03/11/2012' AND '03/24/2012'
  UNION 
    SELECT s.ptco#, s.ptro#, s.ptline, 
      cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) as ptdate,
      s.pttech, 
      sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
    FROM rydedata.SDPRDET s
    WHERE s.ptdate IS NOT NULL 
      and s.ptdate <> 0
      AND s.ptcode = 'TT'
      AND cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) BETWEEN  '03/11/2012' AND '03/24/2012'
      AND s.ptlhrs < 300
    GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech) x
GROUP BY ptco#, ptdate, pttech, ptro#, ptline) y
where ptco# = 'RY1'
group by pttech
order by pttech



select pttech, round(sum(flaghours), 2)
from (
SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
FROM (
    SELECT s.ptco#, s.ptro#, s.ptline, 
      cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) as ptdate,
      s.pttech, 
      sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
    FROM rydedata.SDPRDET s
    WHERE s.ptdate IS NOT NULL 
      and s.ptdate <> 0
      AND s.ptcode = 'TT'
      AND cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) BETWEEN  '03/11/2012' AND '03/24/2012'
      AND s.ptlhrs < 300
    GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech) x
GROUP BY ptco#, ptdate, pttech, ptro#, ptline) y
where ptco# = 'RY1'
group by pttech
order by pttech

select pttech, round(sum(flaghours), 2)
from (
SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
FROM (
    SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, 
      cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) as ptdate,
      d.pttech, 
      coalesce(d.ptlhrs, 0) AS ptlhrs
    FROM rydedata.PDPPDET d
    INNER JOIN rydedata.PDPPHDR h ON d.ptpkey = h.ptpkey
    WHERE d.ptcode = 'TT'
      AND cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) BETWEEN '03/11/2012' AND '03/24/2012') x
GROUP BY ptco#, ptdate, pttech, ptro#, ptline) y
where ptco# = 'RY1'
group by pttech
order by pttech




select * from (
    SELECT 'PDPDET' as source, d.ptco#, h.ptdoc# AS ptro#, d.ptline, 
      cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) as ptdate,
      d.pttech, 
      coalesce(d.ptlhrs, 0) AS ptlhrs
    FROM rydedata.PDPPDET d
    INNER JOIN rydedata.PDPPHDR h ON d.ptpkey = h.ptpkey
    WHERE d.ptcode = 'TT'
      AND cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) BETWEEN '03/11/2012' AND '03/24/2012'
  UNION 
    SELECT 'SDPRDET' as source, s.ptco#, s.ptro#, s.ptline, 
      cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) as ptdate,
      s.pttech, 
      sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
    FROM rydedata.SDPRDET s
    WHERE s.ptdate IS NOT NULL 
      and s.ptdate <> 0
      AND s.ptcode = 'TT'
      AND cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) BETWEEN  '03/11/2012' AND '03/24/2012'
      AND s.ptlhrs < 300
    GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech) x
where pttech = '537'
order by ptro#


SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
FROM (
    SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, 
      cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) as ptdate,
      d.pttech, 
      coalesce(d.ptlhrs, 0) AS ptlhrs
    FROM rydedata.PDPPDET d
    INNER JOIN rydedata.PDPPHDR h ON d.ptpkey = h.ptpkey
    WHERE d.ptcode = 'TT'
      AND cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) BETWEEN '03/11/2012' AND '03/24/2012'
  UNION 
    SELECT s.ptco#, s.ptro#, s.ptline, 
      cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) as ptdate,
      s.pttech, 
      sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
    FROM rydedata.SDPRDET s
    WHERE s.ptdate IS NOT NULL 
      and s.ptdate <> 0
      AND s.ptcode = 'TT'
      AND cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) BETWEEN  '03/11/2012' AND '03/24/2012'
      AND s.ptlhrs < 300
    GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech) x
where trim(ptro#) = '16083781'
GROUP BY ptco#, ptdate, pttech, ptro#, ptline

-- sdprdet
select pttech, sum(FlagHours)
from (
  SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
  FROM (
    SELECT s.ptco#, s.ptro#, s.ptline, 
      cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) as ptdate,
      s.pttech, 
      sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
    FROM rydedata.SDPRDET s

  WHERE pttech IN ('502','506','511','519','522','526','528','532','536','537',
    '540','542','545','557','566','572','573','574','575','577','578','583',
    '585','595','596','598','608','610','611','623','624','625','631')    
      AND s.ptdate IS NOT NULL 
      and s.ptdate <> 0
      AND s.ptcode = 'TT'
      AND cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) BETWEEN  '03/11/2012' AND '03/24/2012'
      AND s.ptlhrs < 300
      and s.ptdbas <> 'V'
      and s.ptco# = 'RY1'
    GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech) x
  GROUP BY ptco#, ptdate, pttech, ptro#, ptline) y
group by  pttech 
order by pttech

-- sdprdet including join to glptrns
select pttech, sum(FlagHours)
from (
  SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
  FROM (
    SELECT s.ptco#, s.ptro#, s.ptline, 
      cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) as ptdate,
      s.pttech, 
      sum(coalesce(s.ptlhrs, 0)) AS ptlhrs
    FROM rydedata.SDPRDET s

  WHERE pttech IN ('502','506','511','519','522','526','528','532','536','537',
    '540','542','545','557','566','572','573','574','575','577','578','583',
    '585','595','596','598','608','610','611','623','624','625','631')    
      AND s.ptdate IS NOT NULL 
      and s.ptdate <> 0
      AND s.ptcode = 'TT'
      AND cast(left(digits(s.ptdate), 4) || '-' || substr(digits(s.ptdate), 5, 2) || '-' || substr(digits(s.ptdate), 7, 2) as date) BETWEEN  '03/11/2012' AND '03/24/2012'
      AND s.ptlhrs < 300
      and s.ptdbas <> 'V'
      and s.ptco# = 'RY1'
      and exists ( -- eliminate voids
        select 1 
        from rydedata.glptrns
        where trim(gtdoc#) = trim(s.ptro#)
        and gtpost <> 'V') 
    GROUP BY s.ptco#, s.ptro#, s.ptline, s.ptdate, s.pttech) x
  GROUP BY ptco#, ptdate, pttech, ptro#, ptline) y
group by  pttech 
order by pttech

-- pdppdet
SELECT ptco#, ptdate, pttech, ptro#, ptline, round(sum(ptlhrs), 2) AS FlagHours
FROM (
    SELECT d.ptco#, h.ptdoc# AS ptro#, d.ptline, 
      cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) as ptdate,
      d.pttech,
     sum(coalesce(d.ptlhrs, 0)) AS ptlhrs
    FROM rydedata.PDPPDET d
    INNER JOIN rydedata.PDPPHDR h ON d.ptpkey = h.ptpkey
    WHERE d.ptcode = 'TT'
      and d.ptco# = 'RY1'
      AND cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) BETWEEN '03/11/2012' AND '03/24/2012'
    group by d.ptco#, d.ptdate, d.pttech, h.ptdoc#, d.ptline) y
GROUP BY ptco#, ptdate, pttech, ptro#, ptline

-- rel between xtim and ptdbas

select count(distinct ptro#)
from rydedata.sdprdet d
where ptcode = 'TT'
  and ptdbas = 'V'
  and exists (
    select 1 
    from rydedata.sdpxtim
    where trim(ptro#) = trim(d.ptro#))


select count (distinct ptro#)  -- 
from rydedata.sdprdet d
where ptcode = 'TT'
  and ptdbas = 'V'


select *
from rydedata.sdprdet
where ptdbas = 'V'
and trim(ptro#) in (
'16082189',
'16083518',
'16083770',
'16084302',
'16083172',
'16083781',
'16084081',
'16082335')

select *
from rydedata.glptrns
where trim(gtdoc#) in (
'16082189',
'16083518',
'16083770',
'16084302',
'16083172',
'16083781',
'16084081',
'16082335')
order by gtdoc#, gtseq#, gtacct


select *
from rydedata.glptrns
where trim(gtdoc#) = '16083443'
  and trim(gtacct) = '124700'
order by gtdoc#, gtseq#, gtacct


select ptro#, sum(ptlhrs)
from rydedata.sdprdet d
where trim(d.pttech) = '502'
  and ptcode = 'TT'
  and ptdate <> 0
  AND cast(left(digits(d.ptdate), 4) || '-' || substr(digits(d.ptdate), 5, 2) || '-' || substr(digits(d.ptdate), 7, 2) as date) BETWEEN '03/11/2012' AND '03/24/2012'
group by ptro#
order by ptro#

select *
from rydedata.sdprdet
where trim(ptro#) = '16083443'
  and trim(pttech) = '502'


select ptco#, ptro#, ptswid, ptrtch, ptvin, ptodom, ptmilo, ptptot as "Parts Total" , ptltot as "Labor Total", ptstot as "Sublet Total"
-- select *
from rydedata.sdprhdr 
where ptdate > 20120400
and left(trim(ptro#), 2) = '16'

select ptco#, ptro#
from rydedata.sdprhdr
group by ptco#, ptro#
having count(*) > 1


select ptco#, ptro#, ptswid, ptrtch, ptvin, ptodom, ptmilo, ptptot as "Parts Total" , ptltot as "Labor Total", ptstot as "Sublet Total"
-- select *
from rydedata.sdprdet
where ptdate > 20120400
  and ptcode = 'TT'
and left(trim(ptro#), 2) = '16'


select ptro#, count(*)
from (
select distinct ptco#, ptro#, pttech
from rydedata.sdprdet
where ptdate > 20120400
  and ptcode = 'TT'
and left(trim(ptro#), 2) = '16') x
group by ptro#
having count(*) > 1

select distinct ptro#, pttech
from rydedata.sdprdet
where trim(ptro#) in ('16085795','16085386','16085581')

