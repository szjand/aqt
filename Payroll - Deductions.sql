-- deductions
-- 12/7/12
-- pydeduct: Employee deduction control file
-- pypcodes: Payroll pay codes and deduction codes
select a.ydempn, ydtype, yddcde, yddamt, b.* 
from rydedata.pydeduct a
left join rydedata.pypcodes b on trim(a.yddcde) = trim(b.ytddcd)
  and a.ydco# = b.ytdco#
where ydempn in ('1130426','1110750')
order by ydempn, yddcde


/*
Report #1 All  three companies and the medical, dental, vision deductions and the deduction amounts
Report #2 All three companies with Health savings and flexible spending account deduction amounts

Health Savings: greg: 18 HSA PreTax
Flex: maryanne: 8 Daycare
select * from rydedata.pymast where ymname like 'PETRO%' or ymname like 'SORUM%'
greg: 1130426
ma: 1110750
*/

select distinct ytdco#, ytdtyp, ytddcd, ytddsc from rydedata.pypcodes 
where trim(ytddsc) like '%DEN%'
order by ytddsc, ytdco#

select a.ymco#, a.ymempn, a.ymname, ymactv, b.yddcde, c.ytddsc, b.yddamt, b.yddper
from rydedata.pymast a
left join rydedata.pydeduct b on a.ymco# = b.ydco#
  and trim(a.ymempn) = trim(b.ydempn)
left join rydedata.pypcodes c on b.ydco# = c.ytdco#
  and trim(b.yddcde) = trim(c.ytddcd)
where ymactv <> 'T'
  and yddcde is not null 
  and trim(yddcde) = '119'



-- from MOO
107  Health
111  Dental
295  Vision
119    Med/Dent Exp

--Report #1 All  three companies and the medical, dental, vision deductions and the deduction amounts

select a.ymco#, a.ymempn, a.ymname,
  sum(case when trim(yddcde) = '107' then coalesce(yddamt,0) else 0 end) as Health,
  sum(case when trim(yddcde) = '111' then coalesce(yddamt,0) else 0 end) as Dental,
  sum(case when trim(yddcde) = '295' then coalesce(yddamt,0) else 0 end) as Vision,
  sum(case when trim(yddcde) = '119' then coalesce(yddamt,0) else 0 end) as "Med/Dent"
from rydedata.pymast a
left join rydedata.pydeduct b on a.ymco# = b.ydco#
  and trim(a.ymempn) = trim(b.ydempn)
left join rydedata.pypcodes c on b.ydco# = c.ytdco#
  and trim(b.yddcde) = trim(c.ytddcd)
  and yddcde in ('107','111','295','119')
where ymactv <> 'T'
group by a.ymco#, a.ymempn, a.ymname

--Report #2 All three companies with Health savings and flexible spending account deduction amounts
115  8 DAYCARE EXP  
96   18 HSA PRETAX

select a.ymco#, a.ymempn, a.ymname,
  sum(case when trim(yddcde) = '115' then coalesce(yddamt,0) else 0 end) as Flex,
  sum(case when trim(yddcde) = '96' then coalesce(yddamt,0) else 0 end) as Savings
from rydedata.pymast a
left join rydedata.pydeduct b on a.ymco# = b.ydco#
  and trim(a.ymempn) = trim(b.ydempn)
left join rydedata.pypcodes c on b.ydco# = c.ytdco#
  and trim(b.yddcde) = trim(c.ytddcd)
  and yddcde in ('115','96')
where ymactv <> 'T'
group by a.ymco#, a.ymempn, a.ymname


