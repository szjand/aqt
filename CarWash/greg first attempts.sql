'DRIVER=Firebird/InterBase(r) driver;Database=192.168.200.25:D:\SiteWatch\DB\SiteWatch.FDB;Port=3050;Dialect=3; Charset=NONE;Role=;Connection lifetime=15;Pooling=true; MinPoolSize=0;MaxPoolSize=50;Packet Size=8192;ServerType=0;';

-- ARM Sales for Today
select cast(JoinDate as Date) as "Sale Date", FirstName || ' ' || LastName as "Name", d.name as "Plan", Total as "Amount" --, email
from salearmplan a
left join sale b on a.saleid = b.objid
left join customer c on a.customer = c.objid
left join plantype d on a.plantype = d.objid
where cast(JoinDate as Date) = 'today'

-- ARM Sales Total Amount for Today
select count(*) as "Plans", sum("Amount") as "Total"
from (select cast(JoinDate as Date) as "Sale Date", c.name, d.name as "Plan", Total as "Amount"
      from salearmplan a
      left join sale b on a.saleid = b.objid
      left join customer c on a.customer = c.objid
      left join plantype d on a.plantype = d.objid
      where cast(JoinDate as Date) = 'today')

-- Plan Washes Today
select LogDate as "Sale Date", FirstName || ' ' || LastName as "Name", c.Name as "Service"
from sale a
left join saleitems b on a.objid = b.saleid 
left join item c on b.Item = c.objid
left join customer d on a.ArmCustomer = d.objid
where logdate = 'today'
  and armcustomer <> 0
  and Total = 0
  and Val = 100
order by LastName, FirstName

-- Total Washes Today
select count(*) as "Washes"
from sale a
where logdate = 'today'
  and a.code <> 'XPTCASH'
  and not exists 
    (select null
     from sale b
     inner join salearmplan c on b.objid = c.saleid and cast(JoinDate as Date) = 'today'
     where b.objid = a.objid)

-- Total Washes Today -- This Hour
select count(*) as "Washes"
from sale a
where logdate = 'today' and extract(hour from created) = extract(hour from current_timestamp)
  and a.code <> 'XPTCASH'
  and not exists 
    (select null
     from sale b
     inner join salearmplan c on b.objid = c.saleid and cast(JoinDate as Date) = 'today'
     where b.objid = a.objid)

-- Total Washes Today -- Last Hour
select count(*) as "Washes"
from sale a
where (logdate = 'today') and extract(hour from created) = extract(hour from current_timestamp) - 1
  and a.code <> 'XPTCASH'
  and not exists 
    (select null
     from sale b
     inner join salearmplan c on b.objid = c.saleid and cast(JoinDate as Date) = 'today'
     where b.objid = a.objid)

-- Total Washes Today by Type
select Name, count(*)
from sale a
left join saleitems b on a.objid = b.saleid 
left join item c on b.Item = c.objid 
where logdate = 'today'
  and Name not in ('-Basic Light', '-Deluxe Light', '-Express Wash', 'Gleam Body Seal', '-Repel', '-Salt Shield', '-Gleam Body Seal', '-Spot Free Rinse', '-Trifoam', 
                   '-Ultimate Light', 'ARM Redeemer Pkg', 'CASH', 'CHANGE', 'COMP FAST PASS-Extra', 'Cash/Change', 'House Acct Charge', 'Internal House Acct',
                   'Intl Acct. Basic $2', 'Owner Disc Pkg', 'Owner Wash-Base', 'Pkg Placeholder', 'Std. Cust. Promo Pkg', 'Unl. BASIC EXP EPkg', 'Unl. BASIC EXP Pkg', 
                   'Unl. Ultim EXP EPkg', 'Unl. Ultim EXP Pkg', 'Visa / MC', 'XPT Cust. Promo Pkg.', '_Basic Wash', '_Deluxe Wash', '_Package Wash', '_Ultimate Express',
                   'Discover', 'ARM Ultimate', 'Charity Tender', 'American Express', 'COMP Ultimate EXT', 'Chartiy Gift Card', 'COMP Ultimate EXT', 'Charity Gift Card',
                   ' COMP FAST PASS PK', 'Today''s UltimE Dsc', 'Detail Adjust_E-fs', 'Detail Adjust_base', 'Unl. Ultim EXP SldP', 'Unl. Ultim EXP Sold', 'COMP FAST PASS-Base',
                   '-Detail Replacement', '`Wash Free Form Note', 'PDQ WASH Pkg', 'PDQ WASH Rdm')
  and Name <> 'Package Wash'
  and a.code <> 'XPTCASH'
  and not exists 
    (select null
     from sale b
     inner join salearmplan c on b.objid = c.saleid and cast(JoinDate as Date) = 'today'
     where b.objid = a.objid)
group by Name
order by case 
  when Name = 'Basic Wash' then 1
  when Name = 'Deluxe Wash' then 2
  when Name = 'Ultimate Express' then 3
  when Name = 'Express Wash & Wax' then 4
  when Name = 'PDQ WASH' then 5
  when Name = 'Fleet RdmP' then 6
  else 99 end;

-- Club Members
select cast(JoinDate as Date) as "Sale Date", FirstName || ' ' || LastName as "Name", d.name as "Plan", Total as "Amount", case when Yr is null then 'Unknown' else Yr || ' ' || e.Name || ' ' || f.Name end as "Vehicle" --, email
from salearmplan a
left join sale b on a.saleid = b.objid
left join customer c on a.customer = c.objid
left join plantype d on a.plantype = d.objid
left join carmake e on c.make = e.id
left join carmodel f on c.model = f.id
where a.status = 1
order by LastName, FirstName

-- Club Members by Plan Type
select d.name as "Plan", count(*)
from salearmplan a
left join sale b on a.saleid = b.objid
left join customer c on a.customer = c.objid
left join plantype d on a.plantype = d.objid
where a.status = 1
group by d.name

-- Number of Club Members
select count(*) as "Club Members"
from salearmplan a
where a.status = 1


-- Total Washes This Month
select count(*)
from sale a
where extract(year from logdate) = extract(year from current_timestamp)
  and extract(month from logdate) = extract(month from current_timestamp)
  and a.code <> 'XPTCASH'
  and not exists 
    (select null
     from sale b
     inner join salearmplan c on b.objid = c.saleid and extract(year from JoinDate) = extract(year from dateadd(-1 month to current_timestamp)) and extract(month from JoinDate) = extract(month from dateadd(-1 month to current_timestamp))
     where b.objid = a.objid)

-- Total Washes Last Month
select count(*)
from sale a
where extract(year from logdate) = extract(year from dateadd(-1 month to current_timestamp))
  and extract(month from logdate) = extract(month from dateadd(-1 month to current_timestamp))
  and a.code <> 'XPTCASH'
  and not exists 
    (select null
     from sale b
     inner join salearmplan c on b.objid = c.saleid and extract(year from JoinDate) = extract(year from dateadd(-1 month to current_timestamp)) and extract(month from JoinDate) = extract(month from dateadd(-1 month to current_timestamp))
     where b.objid = a.objid)


select current_timestamp from RDB$DATABASE

select cast(dateadd(-1 day to current_timestamp) as Date) from RDB$DATABASE

select extract(year from dateadd(-1 month to current_timestamp)) from RDB$DATABASE
select extract(month from dateadd(-1 month to current_timestamp)) from RDB$DATABASE

select * from rdb$indices order by rdb$index_name
