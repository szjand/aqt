Select ymname as "Emp-Name", ymhdte as "Hire-Date", ymrate as "HR-Rate", ymbdte as "Birthdate", ymsex as "Sex", ymmari as "Mar-Stat", ymstre as "Address", ymcity as "City", ymstat as "State", ymzip as "Zip",
  (select sum(yhdtgp)
  from pyhshdta
  where yhdemp = py.ymempn) as "YTD Gross"
from pymast as py
where ymco# = 'RY1' and
  ymactv = 'A'
order by ymname
  
Select ymname as "Emp-Name", ymhdte as "Hire-Date", ymrate as "HR-Rate", ymbdte as "Birthdate", ymsex as "Sex", ymmari as "Mar-Stat", ymstre as "Address", ymcity as "City", ymstat as "State", ymzip as "Zip",
  (select sum(yhdtgp)
  from pyhshdta
  where yhdemp = py.ymempn) as "YTD Gross"
from pymast as py
where ymco# = 'RY2' and
  ymactv = 'A'
order by ymname

Select ymname as "Emp-Name", ymhdte as "Hire-Date", ymrate as "HR-Rate", ymbdte as "Birthdate", ymsex as "Sex", ymmari as "Mar-Stat", ymstre as "Address", ymcity as "City", ymstat as "State", ymzip as "Zip",
  (select sum(yhdtgp)
  from pyhshdta
  where yhdemp = py.ymempn) as "YTD Gross"
from pymast as py
where ymco# = 'RY3' and
  ymactv = 'A'
order by ymname

/*
-----------------------------------------------------------------------------------------
-- EEOC RY1 only, all active full and part time employees: Name, Dept, JobTitle, Gender
pymast.ymactv:  T: terminated
                A: active
                P: part time
pyprjobd: full text description of job codes  (PDQ/T - PDQ Tech) 
need link to pyprjobd, where do i get the individuals job code : pyprhead
need dept description, have dept code in pypmast, where do i get the description of dept code 03 -- never mind          
               
*/
select * from pymast where trim(Ymempn) = '11650'

select distinct pym.ymname as "Name", pym.ymdept as "Department", jobd.yrtext as "Job Title",  pym.ymsex as "Gender"
from pymast pym
left join pyprhead phd on trim(phd.yrempn) = trim(pym.ymempn) and phd.yrco# = 'RY1' -- job description (code)
left join pyprjobd jobd on trim(jobd.yrjobd) = trim(phd.yrjobd) and jobd.yrco# = 'RY1' -- job description in plain english
where pym.ymco# = 'RY1'
and pym.ymactv <> 'T'
order by pym.ymname