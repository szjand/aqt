select 
  sum(case when response.AverageTime < 30 then 1 else 0 end) as '<30',
  sum(case when response.AverageTime < 30 and response.AppointmentDate is not null then 1 else 0 end) as '<30 Appt',
  sum(case when response.AverageTime < 30 and response.AppointmentShow is not null then 1 else 0 end) as '<30 Appt Show',
  sum(case when response.AverageTime < 30 and response.DeliveredDate is not null then 1 else 0 end) as '<30 Delivered',
  sum(case when response.AverageTime >= 30 and response.AverageTime < 60 then 1 else 0 end) as '30-60',
  sum(case when response.AverageTime >= 30 and response.AverageTime < 60 and response.AppointmentDate is not null then 1 else 0 end) as '30-60 Appt',
  sum(case when response.AverageTime >= 30 and response.AverageTime < 60 and response.AppointmentShow is not null then 1 else 0 end) as '30-60 Appt Show',
  sum(case when response.AverageTime >= 30 and response.AverageTime < 60 and response.DeliveredDate is not null then 1 else 0 end) as '30-60 Delivered',
  sum(case when response.AverageTime > 60 then 1 else 0 end) as '>60',
  sum(case when response.AverageTime > 60 and response.AppointmentDate is not null then 1 else 0 end) as '>60 Appt',
  sum(case when response.AverageTime > 60 and response.AppointmentShow is not null then 1 else 0 end) as '>60 Appt Show',
  sum(case when response.AverageTime > 60 and response.DeliveredDate is not null then 1 else 0 end) as '>60 Delivered'
-- select *  
from (
  select pkDealID, 
    c.Email,
    responseBH.AverageTime,
    responseBH.CustomerResponses,
    dfDelivered.DateCreated as DeliveredDate,
    a.DateCreated as AppointmentDate,
    case when a.IsAttendedByCustomer = 1 then a.DateCompleted else null end as AppointmentShow
  from deal d (nolock)
    left join customer c (nolock) on c.pkCustomerID=d.fkCustomerID
    outer apply (select top 1 * from DealFlag df (nolock) where df.fkDealID=d.pkDealID and df.DealFlagType in (5,8,9) order by df.DateCreated asc) dfDone -- 5,8,9 is a deal that is dead,sold,delivered respectively
    outer apply (select top 1 * from Appointment a (nolock) where a.fkDealID = d.pkDealID order by a.DateCreated asc) a 
    outer apply (select top 1 * from DealFlag df (nolock) where df.fkDealID=d.pkDealID and df.DealFlagType in (9) order by df.DateCreated asc) dfDelivered
    outer apply (select case 
      when a.datecreated < dfDone.DateCreated then a.datecreated
      else dfDone.DateCreated end as stoppingDate) stoppingDate -- stop tracking the response time when the deal is done or an appiontment is created
    outer apply (
      select avg(datediff(minute, cr.datecreated, reply.datecreated)) as AverageTime, 
        sum(1) as CustomerResponses
      from crumb cr (nolock)
        outer apply ( -- find the first contact attempt (phone,video,text,email) from a store user AFTER the customer sent in a message during biz hours
          select top 1 DateCreated
          from DealLog dl (nolock)
          where dl.fkDealID=cr.fkDealID 
            and dl.DealLogType=251 -- contact attempt
            and dl.DateCreated > cr.DateCreated -- after the email from the customer
            order by dl.pkDealLogID asc) reply
      where cr.CrumbType in (15,33) -- customer email, incoming text from customer
        and dbo.fnIsDuringILMDistributionHours(cr.fkstoreid, cr.datecreated) = 1 -- business hours only
        and cr.fkDealID=d.pkDealID -- message is tied to deal
        and cr.DateCreated < stoppingDate.stoppingDate -- customer message was before the stopping date of the deal
    ) responseBH
  where d.fkStoreID=39 -- 39 = chevy, 40 = honda/nissan
    and d.DateCreated > '9-1-2014' -- show only for deals after sept 1
    and d.SourceType=6 -- ilm only 
    and responseBH.AverageTime is not null -- only show customers that have sent messages that we're able to show a BH reponse time against
) response 