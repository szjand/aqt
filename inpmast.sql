
-- inpmast with dup stocknumbers
select inp.*
from inpmast inp
inner join (
  select imstk#
  from inpmast
  group by imstk# 
  having count(imstk#) > 1) stk on stk.imstk# = inp.imstk#
where inp.imstk# <> ''  
order by inp.imstk#
  
  