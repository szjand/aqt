-- duplicate keys in bopname
select bn.bnco#, bn.bnkey, bn.bntype, bn.bnsnam
from bopname bn
where bn.bnkey in (
  select bnkey
  from bopname
  group by bnkey
  having count(*) > 1)
order by bnkey

-- duplicate name in bopname
select bn.bnco#, bn.bnkey, bn.bntype, bn.bnsnam
from bopname bn
where bn.bnsnam in (
  select bnsnam
  from bopname
  group by bnsnam
  having count(*) > 1)
order by bnsnam