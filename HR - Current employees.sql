/*
Good afternoon Jon  Could you make a report for Maryann  she would like by store
Name,  hire date, position, department, payroll class which would be  either ( H,  C,  or   S)
Just current employees
Thanks 
Kim 

*/

select distinct pym.ymco# AS "Store", pym.ymname as "Name", 
  case 
    when cast(right(trim(pym.ymhdto),2) as integer) < 20 then 
      cast (
        case length(trim(pym.ymhdto))
          when 5 then  '20'||substr(trim(pym.ymhdto),4,2)||'-'|| '0' || left(trim(pym.ymhdto),1) || '-' ||substr(trim(pym.ymhdto),2,2)
          when 6 then  '20'||substr(trim(pym.ymhdto),5,2)||'-'|| left(trim(pym.ymhdto),2) || '-' ||substr(trim(pym.ymhdto),3,2)
        end as date) 
    else  
      cast (
        case length(trim(pym.ymhdto))
          when 5 then  '19'||substr(trim(pym.ymhdto),4,2)||'-'|| '0' || left(trim(pym.ymhdto),1) || '-' ||substr(trim(pym.ymhdto),2,2)
          when 6 then  '19'||substr(trim(pym.ymhdto),5,2)||'-'|| left(trim(pym.ymhdto),2) || '-' ||substr(trim(pym.ymhdto),3,2)
        end as date) 
  end as "Orig Hire Date",
  jobd.yrtext as "Job Title", pym.ymdept as "Dept", pc.yicdesc as "Dept Description",
  pym.ymclas as "Payroll Class" 
from rydedata.pymast pym
left join rydedata.pyprhead phd on trim(phd.yrempn) = trim(pym.ymempn)  
  and phd.yrco# = pym.ymco# -- job description (code)
left join rydedata.pyprjobd jobd on trim(jobd.yrjobd) = trim(phd.yrjobd) 
  and jobd.yrco# = pym.ymco# -- job description in plain english
left join rydedata.pypclkctl pc on pc.yicdept = pym.ymdept
  and pc.yicco# = pym.ymco#
where pym.ymco#  in ('RY1','RY2','RY3')
and pym.ymactv <> 'T'
order by pym.ymco#, pym.ymname



/*
Good morning Jon can I get a payroll report with the employee name ,  dept, and hire date.  Just for the Rydell store.
Thanks kim
*/
select pym.ymco#, pym.ymname as "Name", pym.ymdept as "Dept", pc.yicdesc as "Dept Description",
  case 
    when cast(right(trim(pym.ymhdto),2) as integer) < 20 then 
      cast (
        case length(trim(pym.ymhdto))
          when 5 then  '20'||substr(trim(pym.ymhdto),4,2)||'-'|| '0' || left(trim(pym.ymhdto),1) || '-' ||substr(trim(pym.ymhdto),2,2)
          when 6 then  '20'||substr(trim(pym.ymhdto),5,2)||'-'|| left(trim(pym.ymhdto),2) || '-' ||substr(trim(pym.ymhdto),3,2)
        end as date) 
    else  
      cast (
        case length(trim(pym.ymhdto))
          when 5 then  '19'||substr(trim(pym.ymhdto),4,2)||'-'|| '0' || left(trim(pym.ymhdto),1) || '-' ||substr(trim(pym.ymhdto),2,2)
          when 6 then  '19'||substr(trim(pym.ymhdto),5,2)||'-'|| left(trim(pym.ymhdto),2) || '-' ||substr(trim(pym.ymhdto),3,2)
        end as date) 
  end as "Orig Hire Date"
  --jobd.yrtext as "Job Title", pym.ymdept as "Dept", pc.yicdesc as "Dept Description",
  --pym.ymclas as "Payroll Class" 
from rydedata.pymast pym
left join rydedata.pyprhead phd on trim(phd.yrempn) = trim(pym.ymempn)  
  and phd.yrco# = pym.ymco# -- job description (code)
left join rydedata.pyprjobd jobd on trim(jobd.yrjobd) = trim(phd.yrjobd) 
  and jobd.yrco# = pym.ymco# -- job description in plain english
left join rydedata.pypclkctl pc on pc.yicdept = pym.ymdept
  and pc.yicco# = pym.ymco#
where pym.ymco#  in ('RY2', 'RY3')
and pym.ymactv <> 'T'
order by pym.ymco#, pym.ymname



/*
current ry1 employees and depts
*/
select distinct pym.ymname as "Name", pym.ymdept as "Dept", pc.yicdesc as "Dept Description", 
  case ymactv
    when 'A' then 'Full'
    when 'P' then 'Part'
  end as "Full/Part"
from rydedata.pymast pym
left join rydedata.pyprhead phd on trim(phd.yrempn) = trim(pym.ymempn)  
  and phd.yrco# = pym.ymco# -- job description (code)
left join rydedata.pyprjobd jobd on trim(jobd.yrjobd) = trim(phd.yrjobd) 
  and jobd.yrco# = pym.ymco# -- job description in plain english
left join rydedata.pypclkctl pc on pc.yicdept = pym.ymdept
  and pc.yicco# = pym.ymco#
where pym.ymco# = 'RY1'
  and pym.ymactv <> 'T'
order by pym.ymname


/*
Helloooooooo…..
Can you pull me a report when you get a chance.  For this location only, I need employee name, department and manager.  Only these three fields are needed.  Thank you for your help ?
*/

select *
from rydedata.pymast

Select * from RYDEDATA.pyprhead
where trim(yrempn) = '11605'


select distinct pym.ymname as "Name", pym.ymdept as "Dept",
  coalesce((select ymname from rydedata.pymast where trim(ymempn) = trim(h.yrmgrn)), 'Fairy God Mother') as mgr
from rydedata.pymast pym
left join rydedata.pyprhead h on trim(pym.ymempn) = trim(h.yrempn)
where pym.ymco# = 'RY1'
  and pym.ymactv <> 'T'
order by mgr

/*
current store and employees
*/
select pym.ymco# as Store, pym.ymname as "Name"
from rydedata.pymast pym
where pym.ymco#  in ('RY1','RY2','RY3')
and pym.ymactv <> 'T'

/*
I am having trouble finding a report for the auditors.  This is the info they need for the Rydell store.

Employees for 2011 and 2012, position, hire & term dates, home addresses (alphabetical order); no date of birth or social security numbers.  

*/

select * from (
select distinct pym.ymname as "Name", jobd.yrtext as "Position",
  case 
    when cast(right(trim(pym.ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(pym.ymhdte))
          when 5 then  '20'||substr(trim(pym.ymhdte),4,2)||'-'|| '0' || left(trim(pym.ymhdte),1) || '-' ||substr(trim(pym.ymhdte),2,2)
          when 6 then  '20'||substr(trim(pym.ymhdte),5,2)||'-'|| left(trim(pym.ymhdte),2) || '-' ||substr(trim(pym.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(pym.ymhdte))
          when 5 then  '19'||substr(trim(pym.ymhdte),4,2)||'-'|| '0' || left(trim(pym.ymhdte),1) || '-' ||substr(trim(pym.ymhdte),2,2)
          when 6 then  '19'||substr(trim(pym.ymhdte),5,2)||'-'|| left(trim(pym.ymhdte),2) || '-' ||substr(trim(pym.ymhdte),3,2)
        end as date) 
  end as "Hire Date",
  case 
    when cast(right(trim(pym.ymtdte),2) as integer) < 20 then 
      cast (
        case length(trim(pym.ymtdte))
          when 5 then  '20'||substr(trim(pym.ymtdte),4,2)||'-'|| '0' || left(trim(pym.ymtdte),1) || '-' ||substr(trim(pym.ymtdte),2,2)
          when 6 then  '20'||substr(trim(pym.ymtdte),5,2)||'-'|| left(trim(pym.ymtdte),2) || '-' ||substr(trim(pym.ymtdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(pym.ymtdte))
          when 5 then  '19'||substr(trim(pym.ymtdte),4,2)||'-'|| '0' || left(trim(pym.ymtdte),1) || '-' ||substr(trim(pym.ymtdte),2,2)
          when 6 then  '19'||substr(trim(pym.ymtdte),5,2)||'-'|| left(trim(pym.ymtdte),2) || '-' ||substr(trim(pym.ymtdte),3,2)
        end as date) 
  end as "Term Date",
  ymstre as Address, ymcity as city, ymstat as state, ymzip as zip
from rydedata.pymast pym
left join rydedata.pyprhead phd on trim(phd.yrempn) = trim(pym.ymempn)  
  and phd.yrco# = pym.ymco# -- job description (code)
left join rydedata.pyprjobd jobd on trim(jobd.yrjobd) = trim(phd.yrjobd) 
  and jobd.yrco# = pym.ymco# -- job description in plain english
left join rydedata.pypclkctl pc on pc.yicdept = pym.ymdept
  and pc.yicco# = pym.ymco#
where pym.ymco#  in ('RY1','RY2','RY3')
and pym.ymco# = 'RY1'
and length(trim(pym.ymname)) > 6) x
-- where "Hire Date" >= '2011-01-01'
where coalesce("Term Date", '9999-12-31') >= '2011-01-01'
order by "Name"

/* 5/28/14
Can you do a report for me with  current employees, name,  department, activity code (full or part time), Job title and gender 

Kim Miller
*/

select distinct pym.ymco# AS "Store", pym.ymname as "Name", 
  pym.ymdept as "Dept", pc.yicdesc as "Dept Description",
  case active_code
    when 'A' then 'Full'
    when 'P' then 'Part'
  end as "Activity Code",
  jobd.yrtext as "Job Title", sex as Gender
from rydedata.pymast pym
left join rydedata.pyprhead phd on trim(phd.yrempn) = trim(pym.ymempn)  
  and phd.yrco# = pym.ymco# -- job description (code)
left join rydedata.pyprjobd jobd on trim(jobd.yrjobd) = trim(phd.yrjobd) 
  and jobd.yrco# = pym.ymco# -- job description in plain english
left join rydedata.pypclkctl pc on pc.yicdept = pym.ymdept
  and pc.yicco# = pym.ymco#
where pym.ymco#  in ('RY1','RY2')
and pym.ymactv <> 'T'
order by pym.ymco#, pym.ymname

/*
for Ben / compli / pto policy

*/

select distinct pym.ymco# AS "Store", trim(pym.ymempn) as employeeNumber, pym.ymname as "Name", 
  pym.ymdept as "DeptCode", pc.yicdesc as "DeptDescription",
  coalesce((select ymname from rydedata.pymast where ymempn = phd.yrmgrn), 'NoneListed') as "Mgr",
  case active_code
    when 'A' then 'Full'
    when 'P' then 'Part'
  end as "Status",
  --pym.ymhdto as "Orig Hire Date",

  case 
    when cast(right(trim(pym.ymhdto),2) as integer) < 20 then 
      cast (
        case length(trim(pym.ymhdto))
          when 5 then  '20'||substr(trim(pym.ymhdto),4,2)||'-'|| '0' || left(trim(pym.ymhdto),1) || '-' ||substr(trim(pym.ymhdto),2,2)
          when 6 then  '20'||substr(trim(pym.ymhdto),5,2)||'-'|| left(trim(pym.ymhdto),2) || '-' ||substr(trim(pym.ymhdto),3,2)
        end as date) 
    else  
      cast (
        case length(trim(pym.ymhdto))
          when 5 then  '19'||substr(trim(pym.ymhdto),4,2)||'-'|| '0' || left(trim(pym.ymhdto),1) || '-' ||substr(trim(pym.ymhdto),2,2)
          when 6 then  '19'||substr(trim(pym.ymhdto),5,2)||'-'|| left(trim(pym.ymhdto),2) || '-' ||substr(trim(pym.ymhdto),3,2)
        end as date) 
  end as "Orig Hire Date",    
  case 
    when cast(right(trim(pym.ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(pym.ymhdte))
          when 5 then  '20'||substr(trim(pym.ymhdte),4,2)||'-'|| '0' || left(trim(pym.ymhdte),1) || '-' ||substr(trim(pym.ymhdte),2,2)
          when 6 then  '20'||substr(trim(pym.ymhdte),5,2)||'-'|| left(trim(pym.ymhdte),2) || '-' ||substr(trim(pym.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(pym.ymhdte))
          when 5 then  '19'||substr(trim(pym.ymhdte),4,2)||'-'|| '0' || left(trim(pym.ymhdte),1) || '-' ||substr(trim(pym.ymhdte),2,2)
          when 6 then  '19'||substr(trim(pym.ymhdte),5,2)||'-'|| left(trim(pym.ymhdte),2) || '-' ||substr(trim(pym.ymhdte),3,2)
        end as date) 
  end as "Last Hire Date", 
  jobd.yrtext as "Job Title"
from rydedata.pymast pym
left join rydedata.pyprhead phd on trim(phd.yrempn) = trim(pym.ymempn)  
  and phd.yrco# = pym.ymco# -- job description (code)
left join rydedata.pyprjobd jobd on trim(jobd.yrjobd) = trim(phd.yrjobd) 
  and jobd.yrco# = pym.ymco# -- job description in plain english
left join rydedata.pypclkctl pc on pc.yicdept = pym.ymdept
  and pc.yicco# = pym.ymco#
where pym.ymco#  in ('RY1','RY2')
and pym.ymactv <> 'T' order by ymname

