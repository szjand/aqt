Select rrn(a), a.* from RYDEDATA.GLPTRNS a where trim(gtctl#) = '16248989' order by gttrn#, gtseq#


Select rrn(a), a.* from RYDEDATA.GLPTRNS a where gttrn# = 3460711 order by gttrn#, gtseq#

Select rrn(a), a.* from RYDEDATA.GLPTRNS a where trim(gtdoc#) = '19251773' order by gttrn#, gtseq#

Select rrn(a), gtpost, gttrn#, gtseq#, gtdate, gtctl#, gtdoc#, gtdesc, gttamt


Select rrn(a), gtpost, gttrn#, gtseq#, gtdate, gtctl#, gtdoc#, gtdesc, gttamt
from RYDEDATA.GLPTRNS a 
where gtpost = '' 
  and gtdate > '12/31/2015'
order by gtdate desc   

Select rrn(a), gtpost, gttrn#, gtseq#, gtdate, gtctl#, gtdoc#, gtdesc, gttamt
from RYDEDATA.GLPTRNS a 
where gtpost <> '' 
  and rrn(a) in (
    Select rrn(a)
    from RYDEDATA.GLPTRNS a 
    where gtpost = '' 
      and gtdate > '12/31/2015')

select rrn(a), a.* from rydedata.glptrns a where rrn(a) = 16087124

gtpost = ''

Select rrn(a), gtpost, gttrn#, gtseq#, gtdate, gtctl#, gtdoc#, gtdesc, gttamt, b.*
from RYDEDATA.GLPTRNS a 
left join rydedata.glpdtim b on a.gttrn# = b.gqtrn#
where gtpost = '' 
  and gtdate between '09/01/2016' and '11/05/2016'


select * rydedata.glptrns where gttrn# = 346206

select gttrn#, gtdoc#, count(*)
from RYDEDATA.GLPTRNS a 
where gtpost = '' 
  and gtdate between '09/01/2016' and '11/05/2016'
group by gttrn#, gtdoc#
order by count(*) 


Select trim(gtdoc#), trim(gtctl#) 
from RYDEDATA.GLPTRNS a 
where gtpost = ''
  and gtdate between '01/01/2016' and '11/05/2016'
group by gtdoc#, gtctl#

Select rrn(a), gtpost, gttrn#, gtseq#, gtdate, gtctl#, gtdoc#, gtdesc, gttamt
from RYDEDATA.GLPTRNS a 
where gttrn# = 3499200  
union
Select rrn(a), gtpost, gttrn#, gtseq#, gtdate, gtctl#, gtdoc#, gtdesc, gttamt
from RYDEDATA.GLPTRNS a 
where trim(gtctl#) = '2745010'


Select rrn(a), gtpost, gttrn#, gtseq#, gtdate, gtctl#, gtdoc#, gtdesc, gttamt, gtjrnl, gtacct
from RYDEDATA.GLPTRNS a 
where gttrn# = 3473755 
union
Select rrn(a), gtpost, gttrn#, gtseq#, gtdate, gtctl#, gtdoc#, gtdesc, gttamt, gtjrnl, gtacct
from RYDEDATA.GLPTRNS a 
where trim(gtctl#) = '19252930'
order by gttrn#, gtseq#


Select rrn(a), gtpost, gttrn#, gtseq#, gtdate, gtctl#, gtdoc#, gtdesc, gttamt
from RYDEDATA.GLPTRNS a
where rrn(a) = 16171200 and 16151202




Select rrn(a), gtpost, gttrn#, gtseq#, gtdate, gtctl#, gtdoc#, gtdesc, gttamt, gtjrnl, gtacct
from RYDEDATA.GLPTRNS a 
where trim(gtdoc#) = '2735961'


select gtdate, year(gtdate)
from rydedata.glptrns

Select trim(gtdoc#), trim(gtctl#) 
from RYDEDATA.GLPTRNS a 
where gtpost = ''
  and gtdate between '01/01/2016' and '11/05/2016'
group by gtdoc#, gtctl#

select year, count(*)
from (
  Select trim(gtdoc#), trim(gtctl#), year(gtdate) as year
  from RYDEDATA.GLPTRNS a 
  where gtpost not in ('Y','V')
  group by gtdoc#, gtctl#, year(gtdate)) a
group by year




-- by year
select year(gtdate) as the_year, count(*)
from rydedata.glptrns
where gtpost not in ('Y','V')
  and year(gtdate) > 2017
group by year(gtdate)

-- by month
select year(gtdate) as the_year, month(gtdate) as the_month, count(*)
from rydedata.glptrns a
where gtpost not in ('Y','V')
  and year(gtdate) in (2016,2017)
group by year(gtdate), month(gtdate)
order by month(gtdate)





select sum(gttamt)
from rydedata.glptrns
where trim(gtdoc#) = '19258677'

select *
from (
select gtacct, sum(gttamt)
from rydedata.glptrns 
where trim(Gtdoc#) = '19236900'
group by gtacct) a
inner join rydedata.glpmast b on trim(a.gtacct) = trim(b.account_number)
  and b.year = 2016



  
Select gtdoc#, sum(gttamt), count(*), max(gtdate)
from RYDEDATA.GLPTRNS a 
where gtpost not in ('Y','V')
  and year(gtdate) = 2014
group by gtdoc#
order by max(gtdate) desc 

select rrn(a), a.*
from rydedata.glptrns a
where trim(gtctl#) = '2747087'

-- from fs 201611 (fact_fs_troubleshooting)
select *
from rydedata.glptrns 
where trim(gtdoc#) = '16252372'
order by gttrn#, gtseq#



select *
from (
select gtacct, gtpost, sum(gttamt)
from rydedata.glptrns 
where trim(Gtdoc#) = '19236900'
group by gtacct, gtpost) a
inner join rydedata.glpmast b on trim(a.gtacct) = trim(b.account_number)
  and b.year = 2016
order by account_type
  
  
select *
from rydedata.glptrns 
where trim(gtdoc#) = '19236900'
order by gttrn#, gtseq#  

-- transactions that have multiple post statuses
select gttrn#
from (
select gttrn#, gtpost
from rydedata.glptrns
where year(gtdate) = 2016 --and month(gtdate) = 10
group by gttrn#, gtpost) a
group by gttrn#
having count(*) > 1


select gttrn#, gtpost
from rydedata.glptrns
where gttrn# = 3492805
group by gttrn#, gtpost

select * from rydedata.glptrns where trim(Gtdoc#) = '16255360'


3492805 seq 1 & 2, void seq 1 & 2


select gttrn#, gtpost
from rydedata.glptrns
where gttrn# = 3327671
group by gttrn#, gtpost

select * from rydedata.glptrns where gttrn# = 3327671

select * from rydedata.glptrns where trim(gtdoc#) = '16236702'


select * from rydedata.glptrns where gttrn# = 3434462

select * from rydedata.glptrns a where rrn(a) = 15925735

select * from rydedata.glptrns where trim(gtdoc#) = '19258677'

select * from rydedata.sdprhdr where trim(ptro#) = '16244904'


select *
from rydedata.glptrns
where trim(gtdoc#) = 'REF070716' and gttamt = 1215.46


select * from rydedata.sdprhdr where trim(ptro#) = '19258677'

select a.gttrn#, a.gtseq#, gtpost, a.gtdate, a.gtdoc#, b.final_close_date
from rydedata.glptrns a
inner join rydedata.sdprhdr b on trim(a.gtdoc#) = trim(b.ro_number)
  and final_close_date > 0
where year(a.gtdate) = 2016
  and a.gtpost not in ('Y', 'V')
  and left(trim(gtdoc#), 1) = '1'
order by final_close_date desc   
  
-- closed ros with unposted lines  
select trim(a.gtdoc#) as ro, b.final_close_date, count(*) as unposted_lines, sum(gttamt)
from rydedata.glptrns a
inner join rydedata.sdprhdr b on trim(a.gtdoc#) = trim(b.ro_number)
  and final_close_date > 0
where year(a.gtdate) = 2016
  and a.gtpost not in ('Y', 'V')
  and left(trim(gtdoc#), 1) = '1'
group by a.gtdoc#, b.final_close_date  
order by final_close_date desc   
    
select * from rydedata.glptrns where gttrn# = 3371202 order by gtseq#
select * from rydedata.glptrns where trim(gtdoc#) = 'REF070716' order by gttrn#, gtseq#


-- which ones are not ros
select trim(a.gtdoc#) as ro
from rydedata.glptrns a
left join rydedata.sdprhdr b on trim(a.gtdoc#) = trim(b.ro_number)
  and final_close_date > 0
where year(a.gtdate) = 2016
  and a.gtpost not in ('Y', 'V')
  and left(trim(gtdoc#), 1) = '1'
  and b.ro_number is null
group by a.gtdoc#


select * from rydedata.glptrns where gttrn# = 3184699 order by gtseq#

select * from rydedata.glptrns where gttrn# = 3509993


-- for nightly script

select count(*) 
from (
  select a.gtdoc#
  from rydedata.glptrns a
  inner join rydedata.sdprhdr b on trim(a.gtdoc#) = trim(b.ro_number)
    and final_close_date > 0
--  where year(a.gtdate) = 2016
  where final_close_date > 20161100
    and a.gtpost not in ('Y', 'V')
    and left(trim(gtdoc#), 1) = '1'
  group by a.gtdoc#) x
    
    
                select count(*)
                from (
                  select a.gtdoc#
                  from rydedata.glptrns a
                  inner join rydedata.sdprhdr b on trim(a.gtdoc#) = trim(b.ro_number)
                    and final_close_date > 20170000
                  where year(a.gtdate) > 2015
                    and a.gtpost not in ('Y', 'V')
                    and left(trim(gtdoc#), 1) = '1'
                  group by a.gtdoc#) x    

select *
from rydedata.glptrns
where trim(gtdoc#) in (
                  select trim(a.gtdoc#)
                  from rydedata.glptrns a
                  inner join rydedata.sdprhdr b on trim(a.gtdoc#) = trim(b.ro_number)
                    and final_close_date > 20170000
                  where year(a.gtdate) > 2015
                    and a.gtpost not in ('Y', 'V')
                    and left(trim(gtdoc#), 1) = '1'
                  group by a.gtdoc#)
                  
