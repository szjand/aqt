/*
select * from rydedata.bopmast

select count(*)
from rydedata.bopmast

select bmstat, count(*)
from rydedata.bopmast
group by bmstat

select * 
from rydedata.bopmast
where bmstat = 'U'

select bmvin, bmstk#
from rydedata.bopmast
where bmstat = 'U'
group by bmvin, bmstk#
  having count(*) > 1

select *
from rydedata.bopmast
where bmstk# in (
  select bmstk#
  from rydedata.bopmast
  where bmstat = 'U'
  group by bmvin, bmstk#
    having count(*) > 1)
order by bmvin

-- unique
select bmco#, bmkey
from rydedata.bopmast
group by bmkey, bmco#
  having count(*) > 1

-- unique stk/vin
select b1.*
from rydedata.bopmast b1 
inner join (
    select bmstk#, bmvin, max(bmkey) as bmkey
    from rydedata.bopmast
    where bmstat = 'U'
    group by bmstk#, bmvin) b2 on b1.bmstk# = b2.bmstk#
  and b1.bmvin = b2.bmvin
  and b1.bmkey = b2.bmkey

-- unique stk/vin (proof)
select bmstk#, bmvin
from (
select b1.*
from rydedata.bopmast b1 
inner join (
    select bmstk#, bmvin, max(bmkey) as bmkey
    from rydedata.bopmast
    where bmstat = 'U'
    group by bmstk#, bmvin) b2 on b1.bmstk# = b2.bmstk#
  and b1.bmvin = b2.bmvin
  and b1.bmkey = b2.bmkey) x
group by bmstk#, bmvin
  having count(*) > 1

-- so now, the question is which fields to include
-- this seems like a reasonable subset
select bmco#, b1.bmkey, bmstat, bmtype, bmvtyp, bmfran, 
  bmwhsl, b1.bmstk#, b1.bmvin, bmbuyr, bmcbyr, bmsnam, 
  bmpric, bmtrad, bmtacv, bmdtor, bmsact, bmunwd, bmdoc#,
  bmdtsv, bmdtaprv, bmdtcap, b9wkip, bmdelvdt, bmfigrs, 
  bmhgrs
-- select *
from rydedata.bopmast b1 
inner join (
    select bmstk#, bmvin, max(bmkey) as bmkey
    from rydedata.bopmast
    where bmstat = 'U'
    group by bmstk#, bmvin) b2 on b1.bmstk# = b2.bmstk#
  and b1.bmvin = b2.bmvin
  and b1.bmkey = b2.bmkey
where bmdtor > 20111210


BMKEY
BMBUYR
BMCBYR
BMPRIC
BMTRAD
BMTACV
BMDTOR
BMDTSV
BMDTAPRV
BMDTCAP
B9WKIP
BMDELVDT
BMFIGRS
BMHGRS

select min(b1.BMHGRS), max(b1.BMHGRS)
from rydedata.bopmast b1 
inner join (
    select bmstk#, bmvin, max(bmkey) as bmkey
    from rydedata.bopmast
    where bmstat = 'U'
    group by bmstk#, bmvin) b2 on b1.bmstk# = b2.bmstk#
  and b1.bmvin = b2.bmvin
  and b1.bmkey = b2.bmkey

select *
from rydedata.bopmast
where bmdtcap = cast(null as date)

select bmco#, bmkey, bmstat, bmtype, bmvtyp, bmfran, 
  bmwhsl, bmstk#, bmvin, bmbuyr, bmcbyr, bmsnam, 
  bmpric, bmtrad, bmtacv, bmdtor, bmsact, bmunwd, bmdoc#,
  bmdtsv, bmdtaprv, bmdtcap, b9wkip, bmdelvdt, bmfigrs, 
  bmhgrs
from rydedata.bopmast
where bmdtcap = (
    select min(bmdtcap)
    from rydedata.bopmast)
  and bmstat = 'U'
-- ok, so what dates do i need to encode
-- and how am i going to deal with null bmdtcap -- do nothing, both date types are date, let's see how it works
-- base query
select bmco#, b1.bmkey, bmstat, bmtype, bmvtyp, bmfran, 
  bmwhsl, b1.bmstk#, b1.bmvin, bmbuyr, bmcbyr, bmsnam, 
  bmpric, bmtrad, bmtacv, bmdtor, bmsact, bmunwd, bmdoc#,
  bmdtsv, bmdtaprv, bmdtcap, b9wkip, bmdelvdt, bmfigrs, 
  bmhgrs
from rydedata.bopmast b1 
inner join (
    select bmstk#, bmvin, max(bmkey) as bmkey
    from rydedata.bopmast
    where bmstat = 'U'
    group by bmstk#, bmvin) b2 on b1.bmstk# = b2.bmstk#
  and b1.bmvin = b2.bmvin
  and b1.bmkey = b2.bmkey

-- final query for code
select bmco#, b1.bmkey, bmstat, bmtype, bmvtyp, bmfran, 
  bmwhsl, b1.bmstk#, b1.bmvin, bmbuyr, bmcbyr, bmsnam, 
  bmpric, bmtrad, bmtacv, 
  cast(left(digits(bmdtor), 4) || '-' || substr(digits(bmdtor), 5, 2) || '-' || substr(digits(bmdtor), 7, 2) as date) as bmdtor,
  bmsact, bmunwd, bmdoc#,
  cast(left(digits(bmdtsv), 4) || '-' || substr(digits(bmdtsv), 5, 2) || '-' || substr(digits(bmdtsv), 7, 2) as date) as bmdtsv,
  bmdtaprv, 
  case 
    when bmdtcap < date('1899-01-01') then date('9999-12-31')
    else bmdtcap
  end as bmdtcap,
  b9wkip, bmdelvdt, bmfigrs, 
  bmhgrs
from rydedata.bopmast b1 
inner join (
    select bmstk#, bmvin, max(bmkey) as bmkey
    from rydedata.bopmast
    where bmstat = 'U'
    group by bmstk#, bmvin) b2 on b1.bmstk# = b2.bmstk#
  and b1.bmvin = b2.bmvin
  and b1.bmkey = b2.bmkey



-- yep, the fucker fails when bmdtcap is null, delphi can't read the field (AdoQuery.FieldByName freaks out)

select 
  case 
    when bmdtcap < date('1899-01-01') then date('9999-12-31')
    else bmdtcap
  end
from rydedata.bopmast
where trim(bmstk#) = '12827'

/*      
OpenQuery(AdoQuery, 'select ' +
          'bmco#, b1.bmkey, bmstat, bmtype, bmvtyp, bmfran, ' +
          'bmwhsl, b1.bmstk#, b1.bmvin, bmbuyr, bmcbyr, bmsnam, ' +
          'bmpric, bmtrad, bmtacv, ' +
          'cast(left(digits(bmdtor), 4) || ''-'' || substr(digits(bmdtor), 5, 2) || ''-'' || substr(digits(bmdtor), 7, 2) as date) as bmdtor, ' +
          'bmsact, bmunwd, bmdoc#, ' +
          'cast(left(digits(bmdtsv), 4) || ''-'' || substr(digits(bmdtsv), 5, 2) || ''-'' || substr(digits(bmdtsv), 7, 2) as date) as bmdtsv, ' +
          'bmdtaprv, ' +
          'case ' +
          '  when bmdtcap < date(''1899-01-01'') then date(''9999-12-31'') ' +
          '  else bmdtcap ' +
          'end as bmdtcap, ' +
          'b9wkip, bmdelvdt, bmfigrs, bmhgrs ' +
          'from rydedata.bopmast b1 ' +
          'inner join (' +
          '    select bmstk#, bmvin, max(bmkey) as bmkey ' +
          '    from rydedata.bopmast ' +
          '    where bmstat = ''U'' ' +
          '    group by bmstk#, bmvin) b2 on b1.bmstk# = b2.bmstk# ' +
          '  and b1.bmvin = b2.bmvin ' +
          '  and b1.bmkey = b2.bmkey');
*/


-- 2/12/12 all the attributes

Select distinct bmtermi from RYDEDATA.BOPMAST

select min(bmdtor), max(bmdtor)
from rydedata.bopmast

select 
  BMCO#, BMKEY, BMSTAT, BMTYPE, BMVTYP, BMFRAN, 
  BMWHSL, BMSTK#, BMVIN, BMBUYR, BMCBYR, BMSNAM, 
  BMPRIC, BMDOWN, BMTRAD, BMTACV, BMTDPO, BMAPR, 
  BMTERM, BMITRM, BMDYFP, BMPFRQ, BMPYMT, BMAMTF, 
  case 
    when BMDTOR = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMDTOR), 4) || '-' || substr(digits(BMDTOR), 5, 2) || '-' || substr(digits(BMDTOR), 7, 2) as date) 
  end as BMDTOR,
  case 
    when BMDTFP = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMDTFP), 4) || '-' || substr(digits(BMDTFP), 5, 2) || '-' || substr(digits(BMDTFP), 7, 2) as date) 
  end as BMDTFP,
  case 
    when BMDTLP = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMDTLP), 4) || '-' || substr(digits(BMDTLP), 5, 2) || '-' || substr(digits(BMDTLP), 7, 2) as date) 
  end as BMDTLP,
  case 
    when BMDTSE = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMDTSE), 4) || '-' || substr(digits(BMDTSE), 5, 2) || '-' || substr(digits(BMDTSE), 7, 2) as date) 
  end as BMDTSE,
  BMTAXG, BMLSRC, BMSSRC, BMISRC, BMGSRC, BMSRVC, 
  BMSVCA, BMSDED, BMSMIL, BMSMOS, BMSCST, 
  case 
    when BMSCSD = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMSCSD), 4) || '-' || substr(digits(BMSCSD), 5, 2) || '-' || substr(digits(BMSCSD), 7, 2) as date) 
  end as BMSCSD,
  case 
    when BMSCED = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMSCED), 4) || '-' || substr(digits(BMSCED), 5, 2) || '-' || substr(digits(BMSCED), 7, 2) as date) 
  end as BMSCED,
  BMSVAC, BMCLPM, BMLVPM, BMAHPM, BMGAPP, 
  BMGCST, BMCLCD, BMAHCD, BMGPCD, BMTAX1, 
  BMTAX2, BMTAX3, BMTAX4, BMTAX5, BMBRAT, 
  BMRHBP, BMTRD#, BMTSK#, BMSVC#, BMCLC#, 
  BMAHC#, BMGAP#, BMFVND, BMPDIC, BMPDAL, BMPDP#, 
  case 
    when BMPDED = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMPDED), 4) || '-' || substr(digits(BMPDED), 5, 2) || '-' || substr(digits(BMPDED), 7, 2) as date) 
  end as BMPDED, 
  BMPDAN, BMPDAA, BMPDAC, BMPDAS, BMPDAZ, 
  BMPDPN, BMPDCL, BMPDCP, BMPDFT, BMPDCD, 
  BMPDPD, BMPDFD, BMPDVU, BMPDPM, BMATOT, 
  BMFTOT, BMTPAY, BMICHG, BMVCST, BMICST, 
  BMADDC, BMHLDB, BMPACK, BMCGRS, BMINCT, 
  BMRBTE, BMODOM, BMPAMT, 
  case 
    when BMPBDT = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMPBDT), 4) || '-' || substr(digits(BMPBDT), 5, 2) || '-' || substr(digits(BMPBDT), 7, 2) as date) 
  end as BMPBDT, 
  BMPCKF, BMNPPY, BMLRES, BMARES, BMGRES, 
  BMPRES, BMMRES, BMSRES, BMFRES, BMTRES, 
  BMPSLP, BMSLP2, BMDDWN, 
  case 
    when BMDDDD = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMDDDD), 4) || '-' || substr(digits(BMDDDD), 5, 2) || '-' || substr(digits(BMDDDD), 7, 2) as date) 
  end as BMDDDD, 
  BMLOF, BMLOFO, BMEAPR, BMMSRP, BMLPRC, 
  BMCAPC, BMCAPR, BMLAPR, BMLFAC, BMLTRM, 
  BMRESP, BMRESA, BMNETR, BMTDEP, BMMIPY, 
  BMMPYA, BMEMRT, BMEMR2, BMEMCG, BMACQF,
  BMLPAY, BMLPYM, BMLSTX, BMLCHG, BMCRTX,
  BMCRT1, BMCRT2, BMCRT3, BMCRT4, BMCRT6, 
  BMSECD, BMDPOS, BMCSHR, BMLFBR, BMITOT, 
  BMCTAX, BMSDOR, BMAFOR, BMFETO, BMCSTX, 
  BMAPYL, BMRAT1, BMRAT2, BMRAT3, BMRAT4, 
  BMRAT6, BMEXT1, BMEXT2, BMEXT3, BMEXT4, 
  BMTOR1, BMTOR2, BMTOR3, BMTOR4, BMTOR6, 
  BMFROR, BMCOVR, BMHOVR, BMTOVR, BMRBDP, 
  BMCSEL, BMCIOP, BMSACT, BMUNWD, BMDOC#, 
  BMBAGE, BMCBAG, BMLCOV, BMHCOV, 
  case 
    when BMITDT = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMITDT), 4) || '-' || substr(digits(BMITDT), 5, 2) || '-' || substr(digits(BMITDT), 7, 2) as date) 
  end as BMITDT, 
  BMLTXG, BMCBLP, 
  case 
    when BMDTSV = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMDTSV), 4) || '-' || substr(digits(BMDTSV), 5, 2) || '-' || substr(digits(BMDTSV), 7, 2) as date) 
  end as BMDTSV, 
  case 
    when BMDTTP = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMDTTP), 4) || '-' || substr(digits(BMDTTP), 5, 2) || '-' || substr(digits(BMDTTP), 7, 2) as date) 
  end as BMDTTP, 
  BMCCRT,BMRDWN, BMTDWN, BMTCRD, 
  BMGCAP, BMDINCT,BMPRNT, BMNVDR, 
  case
     when BMDTAPRV < date('1899-12-31') then date('9999-12-31')
     else BMDTAPRV
  end as BMDTAPRV,
  case
     when BMDTCAP < date('1899-12-31') then date('9999-12-31')
     else BMDTCAP
  end as BMDTCAP,
  BMEMCB, BMEMPB, BMEMRB, BMMPYB, 
  BMMYAB, BMNTRB, BMRSAB, BMRSPB, B9AMTAX, 
  B9EPAY, B9FPAY, B9FPCODE, B9HTERM, B9LPTAX, 
  B9LTRAD, B9ONETTR, B9ONTEQ, B9PDP#, 
  B9PEAMT, B9PETAX, B9SCTAX, B9THCOV, 
  B9TXBASE, B9WKIP, 
  case 
    when BMLDTLP = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(BMLDTLP), 4) || '-' || substr(digits(BMLDTLP), 5, 2) || '-' || substr(digits(BMLDTLP), 7, 2) as date) 
  end as BMLDTLP, 
  BMRAT5, BMRAT7, BMOLPM, BMOCDT, BMLLOPT, 
  BMRFTX1, BMRFTX2, BMPYXINS, BMPBKR, BMBRRI, 
  BMCRRI, BMBGEN, BMCGEN, BMCICD, BMCIPM, 
  BMUICD, BMUIPM, BMUFTX1, BMUFTX2, BMRESIN, 
  BMDISEL, BMREMIC, BMPRMPCT, BMRAT8, BMDELVDT, 
  BMIMILE, BMIMRT, BMIMCG, BMLPYREM, BMFIGRS,
  BMHGRS, BMMTHGAPP, BMIMFLAG, BMIMTHLD, BMGAPTAX
from rydedata.bopmast b1 
inner join (
    select bmstk# as stk#, bmvin as vin, max(bmkey) as thekey
    from rydedata.bopmast
    where bmstat = 'U'
    group by bmstk#, bmvin) b2 on b1.bmstk# = b2.stk#
  and b1.bmvin = b2.vin
  and b1.bmkey = b2.thekey


-- 9/19/12 have been bringing down only capped deals
-- knock that shit off
-- get rid of the inner join, then see what the data looks like



