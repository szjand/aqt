/* 5/24
pursuant to chat with Jeremy, if an ro (line? - probably, duh, no line in header) exists in SHDR and PHDR and PHDR.PTWARO <> 2
are dup lines about to happen
*/
select ptdoc#, ptdate, ptwaro
from rydedata.pdpphdr ph
where exists (
    select 1
    from rydedata.sdprhdr 
    where ptco# = ph.ptco#
      and trim(ptro#) = trim(ph.ptdoc#))
  and ph.ptdtyp = 'RO'
  and ptwaro <> '2'


select *
from rydedata.sdprhdr
where trim(ptro#) = '2662347'

-- jeremy's code
select p.ptpkey, p.ptco#, p.ptdoc#, h.ptco#, h.ptro#
from rydedata.pdpphdr p join rydedata.sdprhdr h on p.ptco# = h.ptco# 
and trim(p.ptdoc#) = trim(h.ptro#)                             
where p.ptdtyp = 'RO' and p.ptwaro <> '2'          
and h.ptcdat > 0                                   





/* 5/24 */




-- 16003406 looks like a void RO, don't know how i can tell that from sdprhdr or sdprdet

select *
from rydedata.glptrns
where trim(gtdoc#) = '16003406'

select *
from rydedata.sdprhdr
where trim(ptro#) = '16003406'

select *
from rydedata.sdprdet
where trim(ptro#) = '16003406'

select ptco#, ptro#, ptline, ptltyp, ptseq#--, ptcode--, ptdate--, ptlhrs, ptlamt
from rydedata.sdprdet d
where ptdbas <> 'V' -- not sure what exacty this column indicates
  and exists ( -- eliminate voids
    select 1 
    from rydedata.glptrns
    where trim(gtdoc#) = trim(d.ptro#)
    and gtpost <> 'V') 
 --and trim(ptro#) = '16003406'
  and ptdate > 20120211
group by ptco#, ptro#, ptline, ptltyp, ptseq#--, ptcode--, ptdate--, ptlhrs, ptlamt
  having count(*) > 1
--order by ptdate


select ptdbas, count(*) --3744749 = null, 20510 = v
from rydedata.sdprdet
group by ptdbas


select ptco#, ptro#, ptline, ptltyp, ptseq#, ptdate
from rydedata.sdprdet d
where ptdbas <> 'V' -- not sure what exactly this column indicates
  and exists ( -- eliminate voids
    select 1 
    from rydedata.glptrns
    where trim(gtdoc#) = trim(d.ptro#)
    and gtpost <> 'V') 
  and ptdate >= 20120520 -- current year
group by ptco#, ptro#, ptline, ptltyp, ptseq#, ptdate
  having count(*) > 1
order by ptdate desc

select *
from rydedata.pdppdet

select d.ptpkey, d.ptco#, h.ptdoc#, d.ptline, d.ptltyp, d.ptseq#, d.ptdate
from rydedata.pdpphdr h
inner join rydedata.pdppdet d on h.ptpkey = d.ptpkey
where h.ptdtyp = 'RO'
group by d.ptpkey, d.ptco#, h.ptdoc#, d.ptline, d.ptltyp, d.ptseq#, d.ptdate
having count(*) > 1


select *
from rydedata.pdppdet
where ptpkey = 119228
  and ptline = 1
  and ptseq# = 41

select *
from rydedata.sdprdet
where ptro# = '16088864'
  and ptline = 2



select ptcpst
from rydedata.sdprhdr
where trim(ptro#) in (
select trim(ptro#)--, ptline, ptltyp, ptseq#--, ptcode--, ptdate--, ptlhrs, ptlamt
from rydedata.sdprdet d
where ptdbas <> 'V' -- not sure what exacty this column indicates
  and exists ( -- eliminate voids
    select 1 
    from rydedata.glptrns
    where trim(gtdoc#) = trim(d.ptro#)
    and gtpost <> 'V') 
 --and trim(ptro#) = '16003406'
  and ptdate > 20120408
group by ptco#, ptro#, ptline, ptltyp, ptseq#--, ptcode--, ptdate--, ptlhrs, ptlamt
  having count(*) > 1)


select ptco#, ptro#, ptline, ptltyp, ptseq# 
from rydedata.sdprdet d 
where ptdbas <> 'V' -- not sure what exactly this column indicates 
                    --PTDBAS will have either an X or a V  X =  Exclude from Tech Time Report, V = Voided Tech Time
and exists ( -- eliminate voids 
select 1 
from rydedata.glptrns 
where trim(gtdoc#) = trim(d.ptro#) 
and gtpost <> 'V') 
and ptdate > 20120000 -- current year 
group by ptco#, ptro#, ptline, ptltyp, ptseq# 
having count(*) > 1

select ptco#, count(*)
from (
select ptco#, ptro#, count(*)
from (
select ptco#, ptro#, ptline, ptltyp, ptseq# 
from rydedata.sdprdet d 
where ptdbas <> 'V' -- not sure what exactly this column indicates 
                    --PTDBAS will have either an X or a V  X =  Exclude from Tech Time Report, V = Voided Tech Time
and exists ( -- eliminate voids 
select 1 
from rydedata.glptrns 
where trim(gtdoc#) = trim(d.ptro#) 
and gtpost <> 'V') 
and ptdate > 20120000 -- current year 
group by ptco#, ptro#, ptline, ptltyp, ptseq# 
having count(*) > 1) x
group by ptco#, ptro#) y
group by ptco#



select sum(yhdota)
from rydedata.pyhshdta
where yhdco# = 'RY1'
  and yhdcyy = 12


select h.ptro#, h.ptdate as hDate, h.ptcdat, h.ptfcdt, d.ptdate as dDate
from rydedata.sdprhdr h
left join rydedata.sdprdet d on trim(h.ptro#) = trim(d.ptro#) 
where h.ptco# = 'RY1'
  and h.ptdate > 20120000
  and d.ptro# is not null 
order by trim(h.ptro#)

select *
from (
  select h.ptro#, h.ptdate as hDate, h.ptcdat, h.ptfcdt, d.ptdate as dDate
  from rydedata.sdprhdr h
  left join rydedata.sdprdet d on trim(h.ptro#) = trim(d.ptro#) 
  where h.ptco# = 'RY1'
    and h.ptdate > 20120000
    and d.ptro# is not null) x
where hdate <> ddate
 

select *
from rydedata.sdppadj
where sjco# = 'RY1'
order by sjcode

select *
from rydedata.sdprtxt
where trim(sxro#) = '16087291'

select *
from rydedata.sdpprice
where trim(spclac) <> trim(sptlac)

select *
from rydedata.sdpprice
where (
  splr1a <> splr1b or 
  splr1b <> splr1c or
  splr1a <> splr1c)

select trim(''''||trim(acct)||''''||',')
--select *26618
from(
select distinct sptlac as acct
from rydedata.sdpprice
union 
select distinct spclac as acct
from rydedata.sdpprice)x
order by trim(acct)


select *
from rydedata.pdpphdr
where trim(ptdoc#) = '16088422'


select ptrsts, count(*)
from rydedata.pdpphdr
where ptdtyp = 'RO'
group by ptrsts
order by ptrsts

select *
from rydedata.pdpphdr
where ptrsts = 'R'

select *
from rydedata.pdpphdr
where ptrsts = '6'


select *
from rydedata.sdprdet
where trim(ptro#) = '16087613'
  and ptline = 3

select *
-- select count(*)
from rydedata.sdprtxt
where trim(sxro#) = '16086753'


select year(yiclkind), month(yiclkind), count(*)
from rydedata.pypclockin
where trim(yiemp#) = '24410'
group by year(yiclkind), month(yiclkind)
order by  year(yiclkind), month(yiclkind)



select month(gtdate), count(*) 
from rydedata.glptrns
where year(gtdate) = 2012
  and month(gtdate) between 1 and 6
group by month(gtdate) 

select ptdate, count(*) 
from rydedata.pdppdet
where ptdate between 20120501 and 20120601
group by ptdate
order by ptdate desc


-- 7/5/13

there are ros for which there are multiple rows for the same ro/line/type/seq
looks like the last one happened in 10/2012, guessing the are artifacts from all
the problems with ro lines doubling
thinking it is a test i do against daily download of sdprtxt, generate an email if it comes up

select sxco#,sxro#,sxline,sxltyp, sxseq# from (
Select sxco#,sxro#,sxline,sxltyp,sxseq#,sxtext
from RYDEDATA.SDPRTXT
group by  sxco#,sxro#,sxline,sxltyp,sxseq#,sxtext
) x group by sxco#,sxro#,sxline,sxltyp, sxseq# having count(*) > 1


select* from rydedata.sdprtxt where trim(sxro#) = '16099158' and sxline = 1 order by sxseq#

select a.*
from rydedata.sdprtxt a
inner join (
  select sxco#,sxro#,sxline,sxltyp, sxseq# 
  from (
    Select sxco#,sxro#,sxline,sxltyp,sxseq#,sxtext
    from RYDEDATA.SDPRTXT
    group by  sxco#,sxro#,sxline,sxltyp,sxseq#,sxtext) x group by sxco#,sxro#,sxline,sxltyp, sxseq# having count(*) > 1) b on
a.sxco# = b.sxco# and a.sxro# = b.sxro# and a.sxline = b.sxline and a.sxltyp = b.sxltyp and a.sxseq# = b.sxseq#
order by a.sxro#, a.sxline, a.sxseq#

