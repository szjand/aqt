Select * from RYDEDATA.glpcust where search_name like 'G%'

g & k: vendor number 16999
ameripride: vendor numberL 2530, 3AMER001, 1530

select * from rydedata.glptrns where gtdate between '2014-10-01' and '2014-12-31' and trim(gtvnd#) in ('2530','3AMER001','1530')

select distinct gtacct from rydedata.glptrns where gtdate between '2014-10-01' and '2014-12-31' and trim(gtvnd#) in ('2530','3AMER001','1530')

select * from rydedata.glptrns where gtdate between '2014-10-01' and '2014-12-31' and gtacct = '16904' and gtdesc not like 'Payroll%'

select * from rydedata.glptrns where gtdate between '2014-10-01' and '2014-12-31' and trim(gtctl#) in ('2530','3AMER001','1530') and gtacct not in ('130000','120300','12703','132404')

select * from rydedata.glptrns a
left join rydedata.glpmast b on a.gtacct = b.account_number
  and b.year = 2014
where gtdate between '2014-10-01' and '2014-12-31' and trim(gtctl#) in ('2530','3AMER001','1530') and gtacct not in ('130000','120300','12703','132404')

select month(a.gtdate) as month, b.department, sum(a.gttamt)
from rydedata.glptrns a
left join rydedata.glpmast b on a.gtacct = b.account_number
  and b.year = 2014
where gtdate between '2014-10-01' and '2014-12-31' 
  and trim(gtctl#) in ('2530','3AMER001','1530') 
  and gtacct not in ('130000','120300','12703','132404')
group by month(a.gtdate), b.department  
order by month(a.gtdate), b.department

select b.department, sum(a.gttamt) as "Ameripride 2014"
from rydedata.glptrns a
left join rydedata.glpmast b on a.gtacct = b.account_number
  and b.year = 2014
where gtdate between '2014-10-01' and '2014-12-31' 
  and trim(gtctl#) in ('2530','3AMER001','1530') 
  and gtacct not in ('130000','120300','12703','132404')
group by b.department  


select gtdtyp, gttype, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#, gtrdtyp, gtvnd#, gtdesc, gttamt, department, account_Desc
from rydedata.glptrns a
left join rydedata.glpmast b on a.gtacct = b.account_number
  and b.year = 2013
where gtdate between '2013-10-01' and '2013-12-31' 
  and trim(gtctl#) = '16999' and gtdesc like '%UNI%'
  and gtacct not in ('130000','120300','12703','132404')
  
select b.department, sum(a.gttamt) as "G & K 2013"
from rydedata.glptrns a
left join rydedata.glpmast b on a.gtacct = b.account_number
  and b.year = 2013
where gtdate between '2013-10-01' and '2013-12-31' 
  and trim(gtctl#) = '16999' and gtdesc like '%UNI%'  
  and gtacct not in ('130000','120300','12703','132404')
group by b.department    


select a.department, coalesce("Ameripride 2014", 0) as "Ameripride 2014", coalesce("G & K 2013", 0) as "G & K 2013"
from (
  select b.department, sum(a.gttamt) as "Ameripride 2014"
  from rydedata.glptrns a
  left join rydedata.glpmast b on a.gtacct = b.account_number
    and b.year = 2014
  where gtdate between '2014-10-01' and '2014-12-31' 
    and trim(gtctl#) in ('2530','3AMER001','1530') 
    and gtacct not in ('130000','120300','12703','132404')
  group by b.department) a
left join (
  select b.department, sum(a.gttamt) as "G & K 2013"
  from rydedata.glptrns a
  left join rydedata.glpmast b on a.gtacct = b.account_number
    and b.year = 2013
  where gtdate between '2013-10-01' and '2013-12-31' 
    and trim(gtctl#) = '16999' and gtdesc like '%UNI%'  
    and gtacct not in ('130000','120300','12703','132404')
  group by b.department) b on a.department = b.department  
  
/*
2/3/15
what i am struggling with is how to determine what is actually the spend on uniforms, how to narrow
down the glptrns entries
uncomfortable with gtacct not in, or gtdesc like

*/  
  

