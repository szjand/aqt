Select * 
from RYDEDATA.GLPMAST a
where a.year = 2020
  and a.account_type = '1'
  and a.department = 'NC'
  and a.typical_balance = 'D'
  and trim(a.account_number) <> '126104'
  and trim(a.account_number) not like '3%'
order by account_number