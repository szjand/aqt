-- this is very close, but for multiples of some employees stephen berg, chris garceau, jeff girodat
-- the problem was in physhdta: 
-- GIRODAT, JEFF S.
-- GIRODAT,JEFF S.

-- so, this is good, the only issue was that Tony Telken and Shannon Bridgeford were somehow not included
-- 1/18/11: Kim says tony and shannon contribute to Roth not 401K yhccde 99 not 91
select e.ymco# as "Company", e.ymname as Name, e.yhcemp as "Emp No", e.total as "Employee Contr", r.total as "Employer Contr" 
from ( -- employee contribution
  Select pym.ymco#, pym.ymname, yhcemp, sum(yhccam) as total
  from rydedata.PYhscdta pyh -- Payroll Code transaction file
  inner join rydedata.pymast pym on pyh.yhcemp = pym.ymempn
  where trim(yhccde) = '91' -- code id (yhccds(description): 401K R)
    and ypbcyy = 110 -- payroll cen + year
  group by  pym.ymco#,pym.ymname, yhcemp) e
left join ( -- employer contribution
  select yhdemp, sum(yhcrtm) as total -- 17 Emplr Curr Ret
  from rydedata.pyhshdta -- Payroll Header transaction file
  where ypbcyy = '110' -- payroll cen + year
    and yhcrtm <> 0
  group by yhdemp) r on e.yhcemp = r.yhdemp
order by e.ymco#, e.ymname


  Select pym.ymco#, pym.ymname, yhcemp, sum(yhccam) as total, yhccde
  from rydedata.PYhscdta pyh -- Payroll Code transaction file
  left join rydedata.pymast pym on pyh.yhcemp = pym.ymempn
  where trim(yhccde) in ('91','99') -- code id (yhccds(description): 401K R)
  and ypbcyy = 110 -- payroll cen + year
  and ymname like '%SCHWA%'
  group by  pym.ymco#,pym.ymname, yhcemp, yhccde
order by pym.ymname

select * from pyhscdta where yhcco# = 'RY2' order by yhcemp
select * from pymast where ymname like 'TELK%' -- 2135910

select *
from pyhscdta p
where not exists (
  select 1
  from pymast
  where ymempn = p.yhcemp)


select * from pymast where ymname like '%BRID%'

-- dupped yhccde due to multiple spellings of yhccds
select distinct yhccde, yhccds
from rydedata.pyhscdta
order by yhccde


select yhccds, count(*) from pyhscdta where yhccds in ('10 DISAB PREM','10 DISAB EXP','10 USA DISAB') group by yhccds

-- current employees (12/13/11), hired in 2009 or 2010, whether they are making 401K contributions

select ymco#, ymname, 
  case 
    when right(digits(ymhdte),2) < 20 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as "Hire Date",
  case 
    when right(digits(ymbdte),2) < 20 then 
      cast (
        case length(trim(p.ymbdte))
          when 5 then  '20'||substr(trim(p.ymbdte),4,2)||'-'|| '0' || left(trim(p.ymbdte),1) || '-' ||substr(trim(p.ymbdte),2,2)
          when 6 then  '20'||substr(trim(p.ymbdte),5,2)||'-'|| left(trim(p.ymbdte),2) || '-' ||substr(trim(p.ymbdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymbdte))
          when 5 then  '19'||substr(trim(p.ymbdte),4,2)||'-'|| '0' || left(trim(p.ymbdte),1) || '-' ||substr(trim(p.ymbdte),2,2)
          when 6 then  '19'||substr(trim(p.ymbdte),5,2)||'-'|| left(trim(p.ymbdte),2) || '-' ||substr(trim(p.ymbdte),3,2)
        end as date) 
    end as "Birth Date",
  case
    when (
      select sum(yhccam)
      from rydedata.pyhscdta
      where ypbcyy = '111'
        and yhccde = '91'
        and trim(yhcemp) = trim(p.ymempn) 
      group by yhcemp, yhccde, yhccds) is null then 'NO'
    else 'YES'
  end as "Cont to 401K"
from rydedata.pymast p
where ymactv = 'A'
  and trim(ymname) <> 'TEST'
  and length(trim(ymname)) > 3
  and right(digits(ymhdte), 2) in (9,10)
order by ymco#, ymname

/*
1-24-12
Hi Jon I need a report for the 401k match for 2011, we should have done one last year also but this is what I need             

Name,  ss#, date of birth, date of hire, term date,  (activity code (part or full time))  hours worked, gross wages, employee contribution,  employer match 

Thanks
Kim
*/
-- so, this is good, the only issue was that Tony Telken and Shannon Bridgeford were somehow not included
-- 1/18/11: Kim says tony and shannon contribute to Roth not 401K yhccde 99 not 91
select e.ymco# as "Company", e.ymname as Name, e.yhcemp as "Emp No", e.total as "Employee Contr", r.total as "Employer Contr" 
from ( -- employee contribution
  Select pym.ymco#, pym.ymname, yhcemp, sum(yhccam) as total
  from rydedata.PYhscdta pyh -- Payroll Code transaction file
  inner join rydedata.pymast pym on pyh.yhcemp = pym.ymempn
  where trim(yhccde) = '91' -- code id (yhccds(description): 401K R)
    and ypbcyy = 111 -- payroll cen + year
  group by  pym.ymco#,pym.ymname, yhcemp) e
left join ( -- employer contribution
  select yhdemp, sum(yhcrtm) as total -- 17 Emplr Curr Ret
  from rydedata.pyhshdta -- Payroll Header transaction file
  where ypbcyy = '111' -- payroll cen + year
    and yhcrtm <> 0
  group by yhdemp) r on e.yhcemp = r.yhdemp
order by e.ymco#, e.ymname

/*
1-24-12
Thanks for the list  we need to include the roth 401k and match 
*/
select e.ymco# as "Company", e.ymname as Name, e.yhcemp as "Emp No", e.total as "Employee Contr", r.total as "Employer Contr" 
-- select count(*) -- adding yhccde 99 gives 3 more records
from ( -- employee contribution
  Select pym.ymco#, pym.ymname, yhcemp, sum(yhccam) as total
  from rydedata.PYhscdta pyh -- Payroll Code transaction file
  inner join rydedata.pymast pym on pyh.yhcemp = pym.ymempn
  where trim(yhccde) in ('91', '99') -- code id (yhccds(description): 401K R)
    and ypbcyy = 111 -- payroll cen + year
  group by  pym.ymco#,pym.ymname, yhcemp) e
left join ( -- employer contribution
  select yhdemp, sum(yhcrtm) as total -- 17 Emplr Curr Ret
  from rydedata.pyhshdta -- Payroll Header transaction file
  where ypbcyy = '111' -- payroll cen + year
    and yhcrtm <> 0
  group by yhdemp) r on e.yhcemp = r.yhdemp
order by e.ymco#, e.ymname
/*
1-24-12
Thanks jon part way through the year dealer track changed the roth and the roth match so the roth match shows  
on retire emplr line and on  emplr contrib line.  If you look at  dharma acharya on Rydell and tony Telken on Honda 
Any chance I could get a line added to the report for the total gross wages?  
*/

select ypbnum, yhccde, yhccds, sum(yhccam)
from rydedata.pyhscdta
where trim(yhcemp) = '11650'
  and ypbcyy = 111
group by ypbnum, yhccde, yhccds

select *
from rydedata.pyhshdta
where trim(yhdemp) = '11650'
  and ypbcyy = 111
order by yhdedd, yhdemm




SELECT *
FROM rydedata.pyhscdta
WHERE ypbnum = 1223000
AND trim(yhcemp) = '11650'




/*
11/8/14
Good Morning Jon can you make a report with employee name, company and what percent they are doing for the 401k  they fields would be       
91 for the 401k and 99 and 99a for the roth
*/
select b.ymco#, b.ymname, trim(a.employee_number), a.ded_pay_code, a.fixed_ded_amt 
from rydedata.pydeduct a
inner join rydedata.pymast b on trim(a.employee_number) = trim(b.ymempn)
  and b.ymactv <> 'T'
where a.ded_pay_code in ('99','91')
order by ymco#, ymname

