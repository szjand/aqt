-- greg wants wes's AR records from 2022 thru current

Select * 
from RYDEDATA.GLPARSH a
where lower(a.customer_name) like '%rydell%'
  and lower(a.customer_name) like '%wes%'


select a.customer_name, a.customer_number, b.* 
from rydedata.glparsh a
join rydedata.glparsd b on a.transaction_key = b.trans_number
  and b.trans_date > '12/31/2021'
  and trim(account_number) <> '122000'
where trim(customer_name) = 'RYDELL, WESLEY DAVID'
order by b.trans_date desc

select *
from rydedata.glparsd
where control_number = '1120800'

select count(*) --737
select b.*
from rydedata.glparsh a
join rydedata.glparsd b on a.transaction_key = b.trans_number
  and b.trans_date > '12/31/2021'
  and trim(account_number) = '122000'
  and trim(control_number) = '1120800'
where trim(customer_name) = 'RYDELL, WESLEY DAVID'

-- from jeri
--Will you please pull this report but limit it to account 122000 and control 1120800?

select a.customer_name, b.* 
from rydedata.glparsh a
join rydedata.glparsd b on a.transaction_key = b.trans_number
  and b.trans_date > '12/31/2021'
  and trim(account_number) = '122000'
  and trim(control_number) = '1120800'
--where trim(a.customer_name) = 'RYDELL, WESLEY DAVID'
order by b.trans_date desc

-- this makes much more sense, 737 rows, but a bunch of near dups, exc trans_number
select *
from rydedata.glparsd b 
where b.trans_date > '12/31/2021'
  and trim(account_number) = '122000'
  and trim(control_number) = '1120800'
--where trim(a.customer_name) = 'RYDELL, WESLEY DAVID'
order by trans_number_gttrn#, tran_seq_number
order by b.trans_date desc

-- try to de-dup it, eliminate the trans_number
-- liked this result, sent to jeri & greg
select distinct control_number,trans_number_gttrn#,tran_seq_number, trans_date, document_number,
  doc_type, journal, trim(reference_number) as reference_number, description, 
  transaction_amt, originating_co_, account_number
from rydedata.glparsd b 
where b.trans_date > '12/31/2021'
  and trim(account_number) = '122000'
  and trim(control_number) = '1120800'
--where trim(a.customer_name) = 'RYDELL, WESLEY DAVID'
order by trans_number_gttrn#, tran_seq_number
--order by b.trans_date desc
