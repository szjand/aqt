select m.ymempn, m.ymname, m.ymactv, 
  h.yrbp01, h.yrbp02, h.yrbp03, h.yrbp04,
  h.yrdp01, yrdp02, yrdp03, yrdp04, yrdp05, yrdp06, yrdp07,
  m.ymrdte as "Last Raise", m.ymsaly as "Base Salary",
  m.ymrate as "Base Hrly Rate",
  m.ymclas as "Payroll Class", m.ympper as "Pay Period",
  m.ymdist as "Distrib Code",
  m.ymlsup as "Last Update", m.ymlsck as "Last Check"
  
from pymast m
left join pyprhead h on m.ymempn = h.yrempn
where m.ymdept = '04'
and m.ymactv <> 'T'
and m.ymco# = 'RY1'
order by m.ymname


-- pyprhead (Employee Salary History) has a series of base salary and raise date fields
-- but it appears that all the raise date fields are 0
select * from pyprhead where trim(yrempn)  = '111210'

-- pyprnote (Employe Salary History)is no help either

select * from pyprnote where trim(yrempn) = '111210'
/*
select * from pyprhead
where yrempn in (
  select ymempn
  from pymast
  where ymdept = '04'
  and ymactv <> 'T'
  and ymco# = 'RY1')
*/