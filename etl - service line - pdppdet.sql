-- the issues is multiple A lines with multiple dates

select ptco#, ptro#, ptline
from (
  SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptfran, pd. ptline, pd.ptltyp, pd.ptcode,
    pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptdate, 0 as ptlamt, pd.ptlopc,
    pd.ptcrlo
  FROM rydedata.PDPPHDR ph
  LEFT JOIN rydedata.PDPPDET pd ON ph.ptco# = pd.ptco#
    AND ph.ptpkey = pd.ptpkey
  WHERE trim(ph.ptdtyp) = 'RO' 
    AND trim(ph.ptdoc#) <> ''
    AND ph.ptco# IN ('RY1','RY2','RY3')
    AND trim(pd.ptltyp) = 'A'
  GROUP BY ph.ptco#, ph.ptdoc#, ph.ptfran, pd. ptline, pd.ptltyp, pd.ptcode,
    pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptdate, pd.ptlopc, pd.ptcrlo)x
group by ptco#, ptro#, ptline
having count(*) > 1
order by ptco#, ptro#, ptline



SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptfran, pd. ptline, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptdate, 0 as ptlamt, pd.ptlopc,
  pd.ptcrlo
FROM rydedata.PDPPHDR ph
LEFT JOIN rydedata.PDPPDET pd ON ph.ptco# = pd.ptco#
  AND ph.ptpkey = pd.ptpkey
where trim(ph.ptdoc#) in (
select trim(ptro#)
from (
  SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptfran, pd. ptline, pd.ptltyp, pd.ptcode,
    pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptdate, 0 as ptlamt, pd.ptlopc,
    pd.ptcrlo
  FROM rydedata.PDPPHDR ph
  LEFT JOIN rydedata.PDPPDET pd ON ph.ptco# = pd.ptco#
    AND ph.ptpkey = pd.ptpkey
  WHERE trim(ph.ptdtyp) = 'RO' 
    AND trim(ph.ptdoc#) <> ''
    AND ph.ptco# IN ('RY1','RY2','RY3')
   AND trim(pd.ptltyp) = 'A'
  GROUP BY ph.ptco#, ph.ptdoc#, ph.ptfran, pd. ptline, pd.ptltyp, pd.ptcode,
    pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptdate, pd.ptlopc, pd.ptcrlo)x
  group by ptco#, ptro#, ptline
  having count(*) > 1)
and pd.ptltyp = 'A'
