select swco#, swname, swswid, swpwrd, 
  swsc01 as "Technician Time Entry",  
  SWSC02 as "Change RO Service Writer", 
  swsc03 as "Flag Warranty Lines",
  swsc04 as "Sublet Entry",
  swsc05 as "Print Pre-Invoices",
  swsc06 as "Unlock RO to Change",
  swsc07 as "Close All Repair Orders", 
  swsc08 as "Parts Entry",
  swsc09 as "Accept Credit Limit Exceeded", 
  swsc10 as "Close Customer Pay Only",
  swsc11 as "Discounts",
  swsc12 as "Review Special Orders",
--  swsc13,
  swsc14 as "Override Deductible",
  swsc15 as "Change Actual Retail Amount",
  swsc16 as "Override Labor Rate",
  swsc17 as "Open Additional RO",
  swsc18 as "Delete Labor Operation",
  swsc19 as "Delete Shop Supplies",
  swsc20 as "Override Dispatching",
  swsc21 as "Change Internal G/L Account",
  swsc22 as "Allow Labor Tax Override",
  swsc23 as "Add New Labor Op Codes",
  swsc24 as "Change Appt Service Writer",
  swsc25 as "Allow Policy Adjust Pay Meth",
  swsc26 as "Allow Release Hold"
  
from sdpswtr
--where (swname like 'TONY%' or swname like 'Ken E%')
   --where swname like 'j%'
--where swswid in ('717','793','403','726','402','111','806','644','704','720','401','728','410')  
--where swco# = 'RY1' 
order by swname
	

-- who can apply discounts
select swname as Name, swswid as "Writer ID",  
case swsc11 
  when '2' then 'All'
  when '1' then 'All excluding variable'
end as "Can Apply Discount"
from sdpswtr
where swco# = 'RY1'
  and swsc11 <> '0'
  and swactive = 'Y'
order by swswid;

