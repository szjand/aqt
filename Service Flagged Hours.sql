--***** Begin *****
-- Flagged hours on closed RO's
select ymname as "Tech", sd.pttech, sum(ptlhrs) as "Hours", count(distinct ptro#) as "ROs"
from sdprdet sd
  left join sdptech st on sd.pttech = st.sttech
  left join pymast py on st.stpyemp# = py.ymempn
where sd.pttech in ('502', '503', '506', '511', '518', '519', '525', '526', '528', '532', '536', '537', 
                    '539', '545', '546', '552', '557', '561', '566', '572', '577', '583', '585', '594',
                    '595', '597', '599', '607', '631')
  and sd.ptco# = 'RY1'
  and ptdate = 20091019
  and ptcode = 'TT'
  and ptline not in (select ptline from pdppdet pd inner join pdpphdr pdh on pd.ptpkey = pdh.ptpkey and pdh.ptdoc# = sd.ptro#)
group by ymname, sd.pttech

union all

-- Need to sum the RO counts from the detail query above, otherwise count is low if techs working on same RO
select 'Total', ' ', sum(xx.ptlhrs), sum(xx.ros) from
(select ymname, sd.pttech, sum(ptlhrs) as ptlhrs, count(distinct ptro#) as ros
from sdprdet sd
  left join sdptech st on sd.pttech = st.sttech
  left join pymast py on st.stpyemp# = py.ymempn
where sd.pttech in ('502', '503', '506', '511', '518', '519', '525', '526', '528', '532', '536', '537', 
                    '539', '545', '546', '552', '557', '561', '566', '572', '577', '583', '585', '594',
                    '595', '597', '599', '607', '631')
  and sd.ptco# = 'RY1'
  and ptdate = 20091019
  and ptcode = 'TT'
  and ptline not in (select ptline from pdppdet pd inner join pdpphdr pdh on pd.ptpkey = pdh.ptpkey and pdh.ptdoc# = sd.ptro#)
group by ymname, sd.pttech) as xx
--***** End *****


--***** Begin *****
-- Flagged hours on open RO's
select ymname as "Tech", sd.pttech, sum(ptlhrs) as "Hours", count(distinct pdh.ptdoc#) as "ROs"
from pdppdet sd
  left join pdpphdr pdh on sd.ptpkey = pdh.ptpkey
  left join sdptech st on sd.pttech = st.sttech
  left join pymast py on st.stpyemp# = py.ymempn
where sd.pttech in ('502', '503', '506', '511', '518', '519', '525', '526', '528', '532', '536', '537', 
                    '539', '545', '546', '552', '557', '561', '566', '572', '577', '583', '585', '594',
                    '595', '597', '599', '607', '631')
  and sd.ptco# = 'RY1'
  and sd.ptdate = 20091012
  and sd.ptcode = 'TT'
--and ptline not in (select ptline from pdppdet pd inner join pdpphdr pdh on pd.ptpkey = pdh.ptpkey and pdh.ptdoc# = sd.ptro#)
group by ymname, sd.pttech

union all

-- Need to sum the RO counts from the detail query above, otherwise count is low if techs working on same RO
select 'Total', ' ', sum(xx.ptlhrs), sum(xx.ros) from
(select ymname, sd.pttech, sum(ptlhrs) as ptlhrs, count(distinct pdh.ptdoc#) as ros
from pdppdet sd
  left join pdpphdr pdh on sd.ptpkey = pdh.ptpkey
  left join sdptech st on sd.pttech = st.sttech
  left join pymast py on st.stpyemp# = py.ymempn
where sd.pttech in ('502', '503', '506', '511', '518', '519', '525', '526', '528', '532', '536', '537', 
                    '539', '545', '546', '552', '557', '561', '566', '572', '577', '583', '585', '594',
                    '595', '597', '599', '607', '631')
  and sd.ptco# = 'RY1'
  and sd.ptdate = 20091019
  and sd.ptcode = 'TT'
--and ptline not in (select ptline from pdppdet pd inner join pdpphdr pdh on pd.ptpkey = pdh.ptpkey and pdh.ptdoc# = sd.ptro#)
group by ymname, sd.pttech) as xx
--***** End *****



