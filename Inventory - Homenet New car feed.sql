/*
11-14:  added rydedata. qualifier for all tables - welcome to the world of dreams
12/15:  added 123707: MV1
*/
SELECT 'Rydell GM Auto Center' as DEALERSHIPNAME, 
trim(imvin) as VIN, trim(IMSTK#) as STOCK, 'NEW' as "NEW-USED", IMYear as YEAR, IMMake as MAKE, 
  IMMODL as MODEL, immcode as MODELNUMBER, imbody as BODY, '' as TRANSMISSION, '' as TRIM, 
  '' as DOORS, imodom as MILES, '' as CYLINDERS, '' as DISPLACEMENT, '' AS DRIVETRAIN, 
  imtrim as trimlevel, imcolr as EXTCOLOR, color_code,
  imtrim as INTCOLOR, 
  --'' as INVOICE, 
  coalesce((
    select ionval 
    from rydedata.inpoptf f 
    inner join rydedata.inpoptd d on f.ioco# = d.idco# 
      and f.iovin = i.imvin 
      and f.ioseq# = d.idseq# 
      and iddesc = 'Flooring/Payoff'), 0) as "INVOICE", -- opt field Internet Price  
  IMPRIC as MSRP,
  '' as BOOKVALUE, 
  coalesce((  -- ad invoice per t arrington request
    select ionval 
    from rydedata.inpoptf f 
    inner join rydedata.inpoptd d on f.ioco# = d.idco# 
      and f.iovin = i.imvin 
      and f.ioseq# = d.idseq# 
      and iddesc = 'Internet Price'), 0) as "SELLINGPRICE", -- opt field Internet Price
  '' as "DATE-IN-STOCK", '' as CERTIFIED, '' as DESCRIPTION, '' as OPTIONS
-- select count(*)
FROM rydedata.INPMAST i
inner join (
  select gtctl#, sum(gttamt)as net
  from rydedata.glptrns
  where trim(gtacct) in (-- Inventory accounts only
    '123100', -- Chev
    '123101', -- Cad
    '123105', -- Buick
    '123700', -- Chev Lt Trk
    '123701', -- Cad Trk
    '123705', -- Buick Trk
    '123706', -- GMC Trk
    '123707', -- MV-1
    '223000', -- Honda Trk
    '223100', -- Honda
    '223105', -- Nissan
    '223700', -- Nissan Trk
    '323102', -- Buick
    '323702', -- Buick Trk
    '323703') -- GMC Trk) 
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by gtctl#
  having sum(coalesce(gttamt,0)) > 1) ia on trim(i.imstk#) = trim(ia.gtctl#)
WHERE IMSTAT = 'I'
and IMType = 'N'
order by inpmast_stock_number
