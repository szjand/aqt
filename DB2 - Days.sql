-- 10/14/2020 i like this even better
converting decimal to date 
TO_DATE works to convert the decimal to a date, but does not appear to work in the math

select open_date, close_date, final_close_date,
    TO_DATE(cast(open_date as varchar(8)), 'YYYYMMDD'),
	DATE( TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD')),
	DATE( TIMESTAMP_FORMAT(cast(close_date as varchar(8)), 'YYYYMMDD')),
	DATE( TIMESTAMP_FORMAT(cast(final_close_date as varchar(8)), 'YYYYMMDD')),
	TO_DATE(cast(close_date as varchar(8)), 'YYYYMMDD') - TO_DATE(cast(open_date as varchar(8)), 'YYYYMMDD')
from rydedata.sdprhdr
where open_date <> close_date
  and open_date > 2020101
  and close_date <> 0
  and final_close_date <> 0

-- ro day from open_date to close_date
-- this does not work across years
select open_date, close_Date,
  DATE( TIMESTAMP_FORMAT(cast(close_date as varchar(8)), 'YYYYMMDD'))  - DATE( TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD')) as days_to_close, count(*) as the_count, min(ro_number), max(ro_number)
from rydedata.sdprhdr
where open_date > 20190101
  and open_date <> close_Date
group by open_date, close_date, DATE( TIMESTAMP_FORMAT(cast(close_date as varchar(8)), 'YYYYMMDD'))  - DATE( TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD'))
order by  DATE( TIMESTAMP_FORMAT(cast(close_date as varchar(8)), 'YYYYMMDD'))  - DATE( TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD')) desc

-- ro day from open_date to close_date
-- this does not work across years
-- preface the conversion with DAYS() takes care of it
select open_date, close_Date,
  days(DATE( TIMESTAMP_FORMAT(cast(close_date as varchar(8)), 'YYYYMMDD')))  - days(DATE( TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD'))) as days_to_close, 
	count(*) as the_count, min(ro_number), max(ro_number)
from rydedata.sdprhdr
where open_date > 20190101
  and open_date <> close_Date
group by open_date, close_date, 
	days(DATE( TIMESTAMP_FORMAT(cast(close_date as varchar(8)), 'YYYYMMDD')))  - days(DATE( TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD')))
order by  days(DATE( TIMESTAMP_FORMAT(cast(close_date as varchar(8)), 'YYYYMMDD')))  - days(DATE( TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD'))) desc


select 
  days(DATE( TIMESTAMP_FORMAT(cast(close_date as varchar(8)), 'YYYYMMDD')))  - days(DATE( TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD'))) as days_to_close, 
	count(*) as the_count, min(ro_number), max(ro_number)
from rydedata.sdprhdr
where open_date > 20190101
  and open_date <> close_Date
group by days(DATE( TIMESTAMP_FORMAT(cast(close_date as varchar(8)), 'YYYYMMDD')))  - days(DATE( TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD')))
order by  days(DATE( TIMESTAMP_FORMAT(cast(close_date as varchar(8)), 'YYYYMMDD')))  - days(DATE( TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD'))) desc


select 20201001 - 20200101 FROM SYSIBM.SYSDUMMY1 -- this won't work

-- days since Jan 1, 0001 + 1
-- The result is 1 more than the number of days from January 1, 0001 to D, where D is the date that would occur if the DATE function were applied to the argument.
select days(curdate()) from sysibm.sysdummy1


-- for arkona.ext_sdprhdr_close_dates
select ro_number, open_date, close_date, final_close_date
select count(*)  -- 92503, open = close = 65885
from rydedata.sdprhdr
where days(DATE( TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD'))) > days(curdate()) -365
  and open_date = close_date
order by open_date

-- this was totally fucked up
Select imstk#, imdinv, date(now()), cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date),
date(now()) - cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date) as Days 
from RYDEDATA.INPMAST
where trim(imstk#) = '10178'


-- same thing
Select imstk#, imdinv, curdate(), cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date),
curdate() - cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date) as Days 
from RYDEDATA.INPMAST
where trim(imstk#) = '10178'

-- this was interesting but timestampdiff assumes 30 days per month
Select imstk#, imdinv, curdate(), cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date),
TimeStampDiff(16, cast(Current_TimeStamp - cast(cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date) as timestamp) as char(22)))
from RYDEDATA.INPMAST
where trim(imstk#) = '10178'


-- this looks to be best for now
Select imstk#, imdinv, curdate(),
days(curdate()) - days(cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date))
from RYDEDATA.INPMAST
where imstat = 'I'

Select imstk#, imdinv, curdate(),
days(curdate()) - days(cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date)),
curdate() - 365 day
from RYDEDATA.INPMAST
where imstat = 'I'

select imdinv,
  cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date) as imdate,
  curdate() as curdate,
  curdate() - 1 month, 
  month(cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date)), 
-- previous month, day 1 a year ago
  curdate() - 1 year - 1 month - (day(curdate() - 1 year - 1 month) - 1) day,
  (cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date)) - 1 year - 1 month - (day((cast(left(trim(imdinv),4)||'-'||substr(trim(imdinv),5,2)||'-'||right(trim(imdinv),2) as date)) - 1 year - 1 month) - 1) day
-- last day of month of previous month
from rydedata.inpmast
where imstat = 'I'

