/*
Maryann 1/3/12
Can you provide me with a report for two employees.  
I need to know their total hours worked for 2011 for both employees.  
I cannot have vacation or sick time in the report or I need it separate from the total hours.  
Kyle worked both at Honda and here so if you could include both companies that would be great.  


John Sandstrom- Sales
Kyle Bontrager- PDQ 
*/

select *
from rydedata.pymast
where ymname like '%SAND%' or ymname like '%BONTR%'

John: 1122151
Kyle: 116525

select *
from rydedata.pypclockin
where trim(yiemp#) in ('1122151')
  and yicode ='O'
  and year(yiclkind) = 2011

select yiemp#, 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
from rydedata.pypclockin
where trim(yiemp#) in ('1122151','116525', '216510')
  and yicode ='O'
  and year(yiclkind) = 2011
group by yiemp#

select *
from rydedata.pyhshdta
where trim(yhdemp) in ('116525', '216510','1122151')


select yhdemp, sum(yhdhrs)
from rydedata.pyhshdta
where trim(yhdemp) in ('116525', '216510','1122151')
  and yhdcyy = 11
group by yhdemp

select distinct YHXHOL
from rydedata.pyhshdta

select *
from rydedata.pyhshdta
where yhxhol = 'Y'


select *
from rydedata.sdprdet
where trim(ptro#) = '16075674'

select *
from rydedata.sdprdet
where trim(ptro#) = '16075674'

select ptco#, ptro#, ptline, ptseq#, ptltyp, ptcode, ptdate, ptlhrs, ptlamt, ptcost
from rydedata.sdprdet
where ptdate > 20111100
  --and trim(ptro#) = '16075674'
  and ptlhrs <> 0
group by ptco#, ptro#, ptline, ptseq#, ptltyp, ptcode, ptdate, ptlhrs, ptlamt, ptcost
  having count(*) > 1

