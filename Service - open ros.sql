Select a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, 
  b.ptline, b.*
from RYDEDATA.PDPPHDR a
left join rydedata.pdppdet b on a.pending_key = b.ptpkey
  and b.ptcode = 'TT'
where trim(a.document_number) = '16317176'

Select * 
from RYDEDATA.sdprhdr a 
 left join rydedata.sdprdet b on trim(a.ro_number) = trim(b.ptro#)
where trim(ro_number) = '16298324'

select *
from rydedata.glptrns
where trim(gtctl#) = '18058696'

-- no line status on any, don't get it, that's because i wasn't looking at the correct ptcode
select *
from RYDEDATA.PDPPHDR a
left join rydedata.pdppdet b on a.pending_key = b.ptpkey
  and b.ptcode = 'TT'
where document_type = 'RO'
  and document_number <> ''
  and company_number in ('RY1','RY2')
  and customer_key <> 0
order by document_number

-- so the question becomes, if a line is closed does it disappear from pdp
select *
from (
  select a.document_number, a.ro_status, listagg(distinct b.ptline, ',') within group (order by b.ptline)
  from rydedata.pdpphdr a
  left join rydedata.pdppdet b on a.pending_key = b.ptpkey
  --  and b.ptcode = 'TT'
  where document_type = 'RO'
    and document_number <> ''
    and company_number in ('RY1','RY2')
    and customer_key <> 0
  group by  a.document_number, a.ro_status) aa
left join (
  Select a.ro_number,  listagg(distinct b.ptline, ',') within group (order by b.ptline)
  from RYDEDATA.sdprhdr a 
   left join rydedata.sdprdet b on trim(a.ro_number) = trim(b.ptro#)
  where final_close_date = 0
  group by a.ro_number) bb on trim(aa.document_number) = trim(bb.ro_number)


-- honda void/not void
select listagg(distinct aa.document_number, ',')
from (
  select a.document_number, a.ro_status, a.open_tran_date, listagg(distinct b.ptline, ',') within group (order by b.ptline)
  from rydedata.pdpphdr a
  left join rydedata.pdppdet b on a.pending_key = b.ptpkey
  --  and b.ptcode = 'TT'
  where document_type = 'RO'
    and document_number <> ''
    and company_number in ('RY2')
    and customer_key <> 0
  group by  a.document_number, a.ro_status, a.open_tran_date) aa
inner join (
  select gtctl#, gtpost
  from rydedata.glptrns
  where gtpost <> 'Y'
     and left(trim(gtctl#), 1) = '2') bb on trim(aa.document_number) = trim(bb.gtctl#)

select * 
from rydedata.glptrns
where trim(gtctl#) = '16314714'


select ptltyp, ptcode from rydedata.pdppdet group by ptltyp, ptcode order by ptcode

-- ok, go ahead and mock it up from here, leaning toward extracting from db2 to postgresal

-- pdpphdr, pdppdet, pdptdet, service writer, sdphist, customer, opcode

-- no line in pdppdet
select * from rydedata.sdprhdr where trim(ro_number) = '2773034'

--   2776960
select a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, a.ro_status, 
  a.coupon_discount, a.total_coupon_disc,
  b.ptline, b.ptltyp, b.ptcode, b.ptdate, b.pttech, 
  -- labor
  case when b.ptcode = 'TT' then b.ptnet - b.ptcost end as labor_gross,
  case when b.ptcode = 'TT' then b.ptlhrs end as flag_hours,
  -- parts
  case when b.ptltyp = 'P' and ptcode in ('CP','SC', 'WS', 'IS') then b.ptqty * b.ptnet end as parts_gross,
  case when b.ptltyp = 'A' then ptsvctyp end as service_type,
  case when b.ptltyp = 'A' then ptlsts end as line_status,
  case when b.ptltyp = 'A' then ptlpym end as payment_type
-- select ptline,ptltyp,ptcode,b.ptdate,ptqty,ptnet,ptlsts,ptsvctyp,ptlpym, pttech, ptlopc,ptcrlo,ptlhrs
-- select b.*
from RYDEDATA.PDPPHDR a
left join rydedata.pdppdet b on a.pending_key = b.ptpkey
--  and b.ptcode = 'TT'
where document_type = 'RO'
  and document_number <> ''
  and company_number in ('RY1','RY2')
  and customer_key <> 0
  and trim(a.document_number) = '2777028'  order by ptline, ptseq#
order by document_number, ptline

-- lets get it down to one row per line

select a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, a.ro_status, 
  a.coupon_discount, a.total_coupon_disc,
  b.ptline, 
  -- labor
  sum(case when b.ptcode = 'TT' then b.ptnet - b.ptcost end) as labor_gross,
  sum(case when b.ptcode = 'TT' then b.ptlhrs end) as flag_hours,
  max(case when b.ptcode = 'TT' then  b.pttech end) as tech,
  -- parts
  sum(case when b.ptltyp = 'P' and ptcode in ('CP','SC', 'WS', 'IS') then b.ptqty * (b.ptnet - b.ptcost) end) as parts_gross,
  max(case when b.ptltyp = 'A' then ptsvctyp end) as service_type,
  max(case when b.ptltyp = 'A' then ptlpym end) as payment_type
-- select ptline,ptltyp,ptcode,b.ptdate,ptqty,ptnet,ptlsts,ptsvctyp,ptlpym, pttech, ptlopc,ptcrlo,ptlhrs
from RYDEDATA.PDPPHDR a
left join rydedata.pdppdet b on a.pending_key = b.ptpkey
--  and b.ptcode = 'TT'
where a.document_type = 'RO'
  and a.document_number <> ''
  and a.company_number in ('RY1','RY2')
  and a.customer_key <> 0
  and b.ptline < 900
  and trim(a.document_number) = '2777028' -- order by ptcode, ptltyp
group by a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, a.ro_status, 
  a.coupon_discount, a.total_coupon_disc,
  b.ptline
order by a.document_number, b.ptline

select * from rydedata.pdptdet where trim(ptinv#) = '2777028'

select * from rydedata.glptrns where trim(gtctl#) = '2777028'


-- figured out all my issues, matched ui for gross on 2777028
-- next step for the query
-- leave out the coupon, there aren't that many, i am too lazy to allocate it
select document_number, open_tran_date, cust_name, service_writer_id, ro_Status, service_Type, payment_type,
  sum(labor_gross) as labor_gross, sum(parts_gross) as parts_gross, sum(flag_hours) as flag_hours,
  sum(labor_gross + parts_gross) as total_gross
from (
select a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, a.ro_status, 
  a.coupon_discount, 
  b.ptline, 
  -- labor
  coalesce(sum(case when b.ptcode = 'TT' then b.ptnet - b.ptcost end), 0) as labor_gross,
  coalesce(sum(case when b.ptcode = 'TT' then b.ptlhrs end), 0) as flag_hours,
  max(case when b.ptcode = 'TT' then  b.pttech end) as tech,
  -- parts
  coalesce(sum(case when b.ptltyp = 'P' and ptcode in ('CP','SC', 'WS', 'IS') then b.ptqty * (b.ptnet - b.ptcost) end), 0) as parts_gross,
  max(case when b.ptltyp = 'A' then ptsvctyp end) as service_type,
  max(case when b.ptltyp = 'A' then ptlpym end) as payment_type
-- select ptline,ptltyp,ptcode,b.ptdate,ptqty,ptnet,ptlsts,ptsvctyp,ptlpym, pttech, ptlopc,ptcrlo,ptlhrs
from RYDEDATA.PDPPHDR a
left join rydedata.pdppdet b on a.pending_key = b.ptpkey
--  and b.ptcode = 'TT'
where a.document_type = 'RO'
  and a.document_number <> ''
  and a.company_number in ('RY1','RY2')
  and a.customer_key <> 0
  and b.ptline < 900
--  and trim(a.document_number) = '2777028' -- order by ptcode, ptltyp
group by a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, a.ro_status, 
  a.coupon_discount, b.ptline) x
where labor_gross + parts_Gross <> 0
group by document_number, open_tran_date, cust_name, service_writer_id, ro_Status, service_Type, payment_type
order by document_number



select a.company_number, a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, a.ro_status, 
  max(case when b.ptltyp = 'A' then ptsvctyp end) as service_type,
  max(case when b.ptltyp = 'A' then ptlpym end) as payment_type,
  coalesce(sum(case when b.ptcode = 'TT' then b.ptlhrs end), 0) as flag_hours,
  coalesce(sum(case when b.ptcode = 'TT' then b.ptnet - b.ptcost end), 0) as labor_gross,
  coalesce(sum(case when b.ptltyp = 'P' and ptcode in ('CP','SC', 'WS', 'IS') then b.ptqty * (b.ptnet - b.ptcost) end), 0) as parts_gross
from RYDEDATA.PDPPHDR a
left join rydedata.pdppdet b on a.pending_key = b.ptpkey
where a.document_type = 'RO'
  and a.document_number <> ''
  and a.company_number in ('RY1','RY2')
  and a.customer_key <> 0
  and b.ptline < 900
and a.ro_status = '4'
group by a.company_number, a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, a.ro_status



-- look at cashier status with serv history: no close_cust status with cashier/delay
-- see if there is a close date from sdprhdr - matches sdphist exactly
-- giving up for now
select curdate(), aa.company_number as store, aa.document_number as ro, aa.cust_name as customer,
  dd.name as writer, 
  case aa.ro_status
  when 'L' then 'G/L Error'
  when '2' then 'In Process'
  when '3' then 'Approved by Parts'
  when '4' then 'Cashier'
  when '5' then 'Cashier, Delayed Close'
  when '6' then 'Pre-Invoice'
  when '7' then 'Odom Required'
  when '8' then 'Waiting for Parts'
  when '9' then 'Parts Approval Reqd'
  else 'Unknown'
  end as ro_status, aa.service_type, aa.payment_type, aa.flag_hours, 
  aa.labor_gross, aa.parts_gross,
  days(curdate()) - days(cast(left(trim(open_tran_date),4)||'-'||substr(trim(open_tran_date),5,2)||'-'||right(trim(open_tran_date),2) as date)) as days_open,
  coalesce(case when aa.ro_status = '4' then days(curdate()) - days(date(bb.updated_time)) end, 0) as days_in_cashier
  -- cc.description
-- select ro_status, count(*)
from (
  select a.company_number, a.document_number, a.open_tran_date, a.cust_name, 
      a.service_writer_id,
      a.ro_status, 
      max(case when b.ptltyp = 'A' then ptlpym end) as payment_type,
    max(case when b.ptltyp = 'A' then ptsvctyp end) as service_type,  
    coalesce(sum(case when b.ptcode = 'TT' then b.ptlhrs end), 0) as flag_hours,
    coalesce(sum(case when b.ptcode = 'TT' then b.ptnet - b.ptcost end), 0) as labor_gross,
    coalesce(sum(case when b.ptltyp = 'P' and ptcode in ('CP','SC', 'WS', 'IS') then b.ptqty * (b.ptnet - b.ptcost) end), 0) as parts_gross
  from RYDEDATA.PDPPHDR a
  left join rydedata.pdppdet b on a.pending_key = b.ptpkey
  where a.document_type = 'RO'
    and a.document_number <> ''
    and a.company_number in ('RY1','RY2')
    and a.customer_key <> 0
    and b.ptline < 900
  group by a.company_number, a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, a.ro_status) aa
left join rydedata.sdphist bb on trim(aa.document_number) = trim(bb.repair_order_number)
  and trim(bb.transaction_code) = 'CLOSE_CUST'
left join rydedata.sdphist bbb on trim(aa.document_number) = trim(bbb.repair_order_number)
  and trim(bb.transaction_code) <> 'CLOSE_CUST'
left join rydedata.sdpsvct cc on aa.company_number = cc.company_number
  and aa.service_type = cc.service_type
left join rydedata.sdpswtr dd on aa.company_number = dd.company_number
  and aa.service_writer_id = dd.id
-- group by ro_status

A Appointment � Service Department
B Appointment � Body Shop
C Appointment � WP
D Appointment � First Visit
E Appointment � Booked from the WEB
L G/L Error
S Cashier � Waiting for Special Order Part
1 Open
2 In Process
3 Approved by Parts Department
4 Cashier
5 Cashier, Delayed Close
6 Pre-Invoice
7 Odom Required
8 Waiting for Parts
9 Parts Approval Required


-- 7/15/18
-- don't need sdphist generated estimate of days in cashier status any more
-- at this level, basic scrape, just set it to 0, will be updated in subsequent update function
                select curdate(), aa.company_number as store, trim(aa.document_number) as ro, 
                  trim(aa.cust_name) as customer,
                  trim(dd.id) as writer, 
                  trim(
                    case aa.ro_status
                      when 'L' then 'G/L Error'
                      when '2' then 'In Process'
                      when '3' then 'Approved by Parts'
                      when '4' then 'Cashier'
                      when '5' then 'Cashier, Delayed Close'
                      when '6' then 'Pre-Invoice'
                      when '7' then 'Odom Required'
                      when '8' then 'Waiting for Parts'
                      when '9' then 'Parts Approval Reqd'
                      else 'Unknown'
                     end) as ro_status, aa.service_type, aa.payment_type, aa.flag_hours, 
                  aa.labor_gross, aa.parts_gross,
                  days(curdate()) - days(cast(left(trim(open_tran_date),4)||'-'||substr(trim(open_tran_date),5,2)
                    ||'-'||right(trim(open_tran_date),2) as date)) as days_open,
          0 as days_in_cashier
                from (
                    select a.company_number, a.document_number, a.open_tran_date, a.cust_name, 
                      a.service_writer_id,
                      a.ro_status, 
                      max(case when b.ptltyp = 'A' then ptlpym end) as payment_type,
                      max(case when b.ptltyp = 'A' then ptsvctyp end) as service_type,  
                      coalesce(sum(case when b.ptcode = 'TT' then b.ptlhrs end), 0) as flag_hours,
                      coalesce(sum(case when b.ptcode = 'TT' then b.ptnet - b.ptcost end), 0) as labor_gross,
                      coalesce(sum(case when b.ptltyp = 'P' and ptcode in ('CP','SC', 'WS', 'IS') then 
                        b.ptqty * (b.ptnet - b.ptcost) end), 0) as parts_gross
                    from RYDEDATA.PDPPHDR a
                    left join rydedata.pdppdet b on a.pending_key = b.ptpkey
                    where a.document_type = 'RO'
                      and a.document_number <> ''
                      and a.company_number in ('RY1','RY2')
                      and a.customer_key <> 0
                      and b.ptline < 900
                    group by a.company_number, a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, 
                        a.ro_status) aa
                left join rydedata.sdpsvct cc on aa.company_number = cc.company_number
                  and aa.service_type = cc.service_type
                left join rydedata.sdpswtr dd on aa.company_number = dd.company_number
                  and aa.service_writer_id = dd.id


-----------------------------------------------------------------------------------------------------------------
--< 18058377
-----------------------------------------------------------------------------------------------------------------
select document_number, open_tran_date, cust_name, service_writer_id, ro_Status, service_Type, payment_type,
  sum(labor_gross) as labor_gross, sum(parts_gross) as parts_gross, sum(flag_hours) as flag_hours,
  sum(labor_gross + parts_gross) as total_gross
from ( -- added ptline to group and get the MR & BS as separate lines
  select a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, a.ro_status, 
    a.coupon_discount, 
    b.ptline, 
    -- labor
    coalesce(sum(case when b.ptcode = 'TT' then b.ptnet - b.ptcost end), 0) as labor_gross,
    coalesce(sum(case when b.ptcode = 'TT' then b.ptlhrs end), 0) as flag_hours,
    max(case when b.ptcode = 'TT' then  b.pttech end) as tech,
    -- parts
    coalesce(sum(case when b.ptltyp = 'P' and ptcode in ('CP','SC', 'WS', 'IS') then b.ptqty * (b.ptnet - b.ptcost) end), 0) as parts_gross,
    max(case when b.ptltyp = 'A' then ptsvctyp end) as service_type,
    max(case when b.ptltyp = 'A' then ptlpym end) as payment_type
  -- select ptline,ptltyp,ptcode,b.ptdate,ptqty,ptnet,ptlsts,ptsvctyp,ptlpym, pttech, ptlopc,ptcrlo,ptlhrs
  from RYDEDATA.PDPPHDR a
  left join rydedata.pdppdet b on a.pending_key = b.ptpkey
  --  and b.ptcode = 'TT'
  where a.document_type = 'RO'
    and a.document_number <> ''
    and a.company_number in ('RY1','RY2')
    and a.customer_key <> 0
    and b.ptline < 900
    and trim(a.document_number) = '18058377' -- order by ptcode, ptltyp
  group by a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, a.ro_status, 
    a.coupon_discount, b.ptline) x
where labor_gross + parts_Gross <> 0
group by document_number, open_tran_date, cust_name, service_writer_id, ro_Status, service_Type, payment_type
order by document_number


somewhere between above and below, it gets condensed into one line, excluding BS service type
shit 
i fucked up, i am guessing it is in the grouping
more specifically in max-ing service type and payment type
actually, the problem was excluding the line from the grouping

                select curdate(), aa.company_number as store, trim(aa.document_number) as ro, 
                  trim(aa.cust_name) as customer,
                  trim(dd.id) as writer, 
                  trim(
                    case aa.ro_status
                      when 'L' then 'G/L Error'
                      when '2' then 'In Process'
                      when '3' then 'Approved by Parts'
                      when '4' then 'Cashier'
                      when '5' then 'Cashier, Delayed Close'
                      when '6' then 'Pre-Invoice'
                      when '7' then 'Odom Required'
                      when '8' then 'Waiting for Parts'
                      when '9' then 'Parts Approval Reqd'
                      else 'Unknown'
                     end) as ro_status, aa.service_type, aa.payment_type, aa.flag_hours, 
                  aa.labor_gross, aa.parts_gross,
                  days(curdate()) - days(cast(left(trim(open_tran_date),4)||'-'||substr(trim(open_tran_date),5,2)
                    ||'-'||right(trim(open_tran_date),2) as date)) as days_open,
          0 as days_in_cashier
                from (
                    select a.company_number, a.document_number, b.ptline, a.open_tran_date, a.cust_name, 
                      a.service_writer_id,
                      a.ro_status, 
                      maxcase when b.ptltyp = 'A' then ptlpym end) as payment_type,
                      max(case when b.ptltyp = 'A' then ptsvctyp end) as service_type,  
                      coalesce(sum(case when b.ptcode = 'TT' then b.ptlhrs end), 0) as flag_hours,
                      coalesce(sum(case when b.ptcode = 'TT' then b.ptnet - b.ptcost end), 0) as labor_gross,
                      coalesce(sum(case when b.ptltyp = 'P' and ptcode in ('CP','SC', 'WS', 'IS') then 
                        b.ptqty * (b.ptnet - b.ptcost) end), 0) as parts_gross
                    from RYDEDATA.PDPPHDR a
                    left join rydedata.pdppdet b on a.pending_key = b.ptpkey
                    where a.document_type = 'RO'
                      and a.document_number <> ''
                      and a.company_number in ('RY1','RY2')
                      and a.customer_key <> 0
                      and b.ptline < 900
                    group by a.company_number, a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, 
                        a.ro_status, b.ptline) aa
                left join rydedata.sdpsvct cc on aa.company_number = cc.company_number
                  and aa.service_type = cc.service_type
                left join rydedata.sdpswtr dd on aa.company_number = dd.company_number
                  and aa.service_writer_id = dd.id
where trim(aa.document_number) = '18058377'

                    select a.company_number, a.document_number, a.open_tran_date, a.cust_name, 
                      a.service_writer_id,
                      a.ro_status, 
                      case when b.ptltyp = 'A' then ptlpym end as payment_type,
                      case when b.ptltyp = 'A' then ptsvctyp end as service_type,  
                      coalesce(case when b.ptcode = 'TT' then b.ptlhrs end, 0) as flag_hours,
                      coalesce(case when b.ptcode = 'TT' then b.ptnet - b.ptcost end, 0) as labor_gross,
                      coalesce(case when b.ptltyp = 'P' and ptcode in ('CP','SC', 'WS', 'IS') then 
                        b.ptqty * (b.ptnet - b.ptcost) end, 0) as parts_gross
                    from RYDEDATA.PDPPHDR a
                    left join rydedata.pdppdet b on a.pending_key = b.ptpkey
                    where a.document_type = 'RO'
                      and a.document_number <> ''
                      and a.company_number in ('RY1','RY2')
                      and a.customer_key <> 0
                      and b.ptline < 900
and trim(a.document_number) = '18058377'

select a.company_number, a.document_number, a.open_tran_date, a.cust_name, 
  a.service_writer_id, a.ro_status, 
    b.*
from RYDEDATA.PDPPHDR a
left join rydedata.pdppdet b on a.pending_key = b.ptpkey
where a.document_type = 'RO'
  and a.document_number <> ''
  and a.company_number in ('RY1','RY2')
  and a.customer_key <> 0
  and b.ptline < 900
  and trim(a.document_number) = '18058377'


-- this is the header info
select a.company_number, trim(a.document_number) as ro, a.ptpkey, trim(a.cust_name) as customer, 
  a.service_writer_id,
                    case a.ro_status
                      when 'L' then 'G/L Error'
                      when '2' then 'In Process'
                      when '3' then 'Approved by Parts'
                      when '4' then 'Cashier'
                      when '5' then 'Cashier, Delayed Close'
                      when '6' then 'Pre-Invoice'
                      when '7' then 'Odom Required'
                      when '8' then 'Waiting for Parts'
                      when '9' then 'Parts Approval Reqd'
                      else a.ro_status
                     end as ro_status,
  cast(left(trim(open_tran_date),4)||'-'||substr(trim(open_tran_date),5,2) ||'-'||
    right(trim(open_tran_date),2) as date) as open_date,
  days(curdate()) - days(cast(left(trim(open_tran_date),4)||'-'||substr(trim(open_tran_date),5,2)
                    ||'-'||right(trim(open_tran_date),2) as date)) as days_open,
  0 as days_in_cashier
from RYDEDATA.PDPPHDR a
where a.document_type = 'RO'
  and a.document_number <> ''
  and a.company_number in ('RY1','RY2')
  and a.customer_key <> 0
  and trim(a.document_number) = '18058377'


with ros as (
  select a.ptpkey
  from RYDEDATA.PDPPHDR a
  where a.document_type = 'RO'
    and a.document_number <> ''
    and a.company_number in ('RY1','RY2')
    and a.customer_key <> 0)
select *
from rydedata.pdppdet a 
inner join ros b on a.ptpkey = b.ptpkey
where a.ptline < 900
  and ptcode  = 'A'


i totally fucked it up and need to redo it

ptltyp A service type, payment type
ptltyp P
---------------------------------------------------
--------------------------------------------------------------
--< 18058377
-----------------------------------------------------------------------------------------------------------------

as i reconfigure, the end result is open ros, leaning toward composite parts: header and detail, at least to get started
need to salvage existing data if possible, the header and detail may be the best way to do that

-- this is the header info
select a.company_number, trim(a.document_number) as ro, a.ptpkey, trim(a.cust_name) as customer, 
  a.service_writer_id,
    case a.ro_status
      when 'L' then 'G/L Error'
      when 'S' then 'Cashier - Waiting for Special Order Part'
    when '1' then 'Open'
      when '2' then 'In Process'
      when '3' then 'Approved by Parts'
      when '4' then 'Cashier'
      when '5' then 'Cashier, Delayed Close'
      when '6' then 'Pre-Invoice'
      when '7' then 'Odom Required'
      when '8' then 'Waiting for Parts'
      when '9' then 'Parts Approval Reqd'
      else a.ro_status
    end as ro_status,
  cast(left(trim(open_tran_date),4)||'-'||substr(trim(open_tran_date),5,2) ||'-'||
    right(trim(open_tran_date),2) as date) as open_date,
  days(curdate()) - days(cast(left(trim(open_tran_date),4)||'-'||substr(trim(open_tran_date),5,2)
                    ||'-'||right(trim(open_tran_date),2) as date)) as days_open,
  0 as days_in_cashier
from RYDEDATA.PDPPHDR a
where a.document_type = 'RO'
  and a.document_number <> ''
  and a.company_number in ('RY1','RY2')
  and a.customer_key <> 0



select *
from rydedata.pdppdet
where ptline < 900
  and (
  ptltyp = 'A'
    or ptcode = 'TT'
    or (ptltyp = 'P' and ptcode in ('CP','SC','WS','IS')))

select *
from rydedata.pdppdet
where ptline < 900
  and ptltyp = 'A'
  and ptpkey = 1431297

select ptpkey, ptline, ptsvctyp, ptlpym
from rydedata.pdppdet
where ptline < 900
  and ptltyp = 'A'
  and ptsvctyp <> ''
  and ptlpym <> ''
group by ptpkey, ptline, ptsvctyp, ptlpym

-- service type & payment type
with 
  headers as (
    select ptpkey
    from RYDEDATA.PDPPHDR a
    where a.document_type = 'RO'
      and a.document_number <> ''
      and a.company_number in ('RY1','RY2')
      and a.customer_key <> 0)
select ro, ptpkey,service_type, payment_type, sum(flag_hours) as flag_hours, 
  sum(labor_sales) as labor_sales, sum(labor_cost) as labor_cost, 
  sum(labor_gross) as labor_gross, sum(parts_gross) as parts_gross
from ( -- and take the line out of the grouping
  select trim(dd.document_number) as ro, aa.*, coalesce(bb.flag_hours,0) as flag_hours, coalesce(bb.labor_sales, 0) as labor_sales,
    coalesce(bb.labor_cost, 0) as labor_cost, coalesce(bb.labor_gross, 0) as labor_gross,
    coalesce(cc.parts_gross, 0) as parts_gross
  from ( -- service type, payment type
    select a.ptpkey, a.ptline, a.ptsvctyp as service_type, a.ptlpym as payment_type
    from rydedata.pdppdet a
    inner join headers b on a.ptpkey = b.ptpkey
    where a.ptline < 900
      and a.ptltyp = 'A'
      and a.ptsvctyp <> ''
      and a.ptlpym <> ''
    group by a.ptpkey, a.ptline, a.ptsvctyp, a.ptlpym) aa
  left join ( -- labor
    select a.ptpkey, a.ptline, sum(coalesce(a.ptlhrs, 0)) as flag_hours, sum(coalesce(a.ptnet, 0)) as labor_sales, 
        sum(coalesce(a.ptcost, 0)) as labor_cost, sum(coalesce(a.ptnet, 0) - coalesce(a.ptcost, 0)) as labor_gross
    from rydedata.pdppdet a
    inner join headers b on a.ptpkey = b.ptpkey
    where a.ptline < 900
      and a.ptcode = 'TT'
    group by a.ptpkey, a.ptline) bb on aa.ptpkey = bb.ptpkey and aa.ptline = bb.ptline
  left join (
    select ptpkey, ptline, sum(parts_gross) as parts_gross
    from ( -- parts
      select a.ptpkey, a.ptline, a.ptqty * (a.ptnet - a.ptcost) as parts_gross
      from rydedata.pdppdet a
      inner join headers b on a.ptpkey = b.ptpkey
      where a.ptline < 900
        and a.ptltyp = 'P'
        and a.ptcode in ('CP','SC','WS','IS')) c
    group by ptpkey, ptline) cc on aa.ptpkey = cc.ptpkey and aa.ptline = cc.ptline
  left join rydedata.pdpphdr dd on aa.ptpkey = dd.ptpkey) ee
where trim(ee.ro) = '2777028'
group by ro, ptpkey, service_type, payment_type

order by ptpkey, ptline


select * from RYDEDATA.PDPPHDR where ptpkey = 1524912


with 
  headers as (
    select ptpkey
    from RYDEDATA.PDPPHDR a
    where a.document_type = 'RO'
      and a.document_number <> ''
      and a.company_number in ('RY1','RY2')
      and a.customer_key <> 0)
select a.ptpkey, a.ptline, sum(coalesce(a.ptlhrs, 0)) as flag_hours, sum(coalesce(a.ptnet, 0)) as labor_sales, 
  sum(coalesce(a.ptcost, 0)) as labor_cost, sum(coalesce(a.ptnet, 0) - coalesce(a.ptcost, 0)) as labor_gross
from rydedata.pdppdet a
inner join headers b on a.ptpkey = b.ptpkey
where a.ptline < 900
  and a.ptcode = 'TT'
group by a.ptpkey, a.ptline


select * from rydedata.pdpphdr where ptpkey = 1506893



    coalesce(sum(case when b.ptcode = 'TT' then b.ptnet - b.ptcost end), 0) as labor_gross,
    coalesce(sum(case when b.ptcode = 'TT' then b.ptlhrs end), 0) as flag_hours,
    max(case when b.ptcode = 'TT' then  b.pttech end) as tech,
    -- parts
    coalesce(sum(case when b.ptltyp = 'P' and ptcode in ('CP','SC', 'WS', 'IS') then b.ptqty * (b.ptnet - b.ptcost) end), 0) as parts_gross,
    max(case when b.ptltyp = 'A' then ptsvctyp end) as service_type,
    max(case when b.ptltyp = 'A' then ptlpym end) as payment_type