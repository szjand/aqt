select * from pdpmast where trim(pmpart) = '19210285'

select count(*)
from pdpmast
where pmdtpo /*date phased out*/ = 20090727


select pmdtpo , count(pmdtpo)
from pdpmast
where pmsgrp = '201'
group by pmdtpo /*date phased out*/
order by pmdtpo desc

-- non stocked with Recent Demand
select pmpart as "Part #", pmdesc as "Description", pmrdmd as "Recent Demand", pmpdmd as "Prior Demand", pmsadm as "Sales Demand", pmonhd as "On Hand", pmdtpo as "Date Phased Out", pmdtls as "Date Last Sold"
from pdpmast
where pmstat = 'N' /* status non-stocked */
and trim(pmco#) = 'RY1'
AND pmsgrp = 201 /* source = 201: AC Delco*/
and pmrdmd > 0
order by pmrdmd desc
