SELECT   coalesce(ps.pfaprt, pd.pmpart) as "Part #", pmdesc as Description,
         (select coalesce(sum(ptqty), 0) 
          from rydedata.pdptdet 
          where (ptpart = pd.pmpart or ptpart in ((select pfoprt from rydedata.pdpmrpl where pfaprt = pd.pmpart))) 
            and ptcode in (
              'CP', /*RO Customer Pay Sale*/
              'IS', /*RO Internal Sale*/ 
              'SA', /*Counter Sale*/
              'WS', /*RO Warranty Sale*/
              'SC', /*RO Service Contract Sale*/ 
              'SR', /*RO Return*/ 
              'CR', /*RO Correction*/ 
              'RT', /*Counter Return*/ 
              'FR') /*Factory Return*/ 
            and ptco# = pd.pmco#
            and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())) as "Yearly Sales", 
         pm.pmstkl as "RIM Level",
         pmonhd as "On Hand",
     pd.qty_reserved as "Other Reserved",
         pmordr as "Reserved",
         pmspor as "On Order",
         pmrsrv as "Spec Order",
         pmbcko as "Back Ordered",
         pm.pmstkl + pmrsrv - pmonhd - pmordr - pmspor - pmbcko as "Needed"
-- select *         
FROM     rydedata.pdpmast pd 
         left join rydedata.pdpmrpl ps on pd.pmpart = ps.pfoprt and pd.pmco# = ps.pfco#
         inner join rydedata.pdppmex pm on pd.pmpart = pm.pmpart and pd.pmco# = pm.pmco# and pm.pmstcd = '02'
WHERE    pd.pmco# = 'RY1'
AND      pd.pmstat = 'A' -- status = Active 
AND      pmsgrp = 201 -- source 201 = ACDelco 
AND      trim(pd.pmnprt) = '' -- 3/15/16 this line is throwing a data mapping error
AND      trim(pm.pmstcd) = '02' -- stock code 02 = RIM
AND      pm.pmstkl + pmrsrv - pmonhd - pmordr - pmspor - pmbcko > 0
limit 100
ORDER BY coalesce(ps.pfaprt, pd.pmpart)
