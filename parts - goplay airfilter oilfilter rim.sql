
      $sql = 'SELECT coalesce(ps.pfaprt, pd.pmpart) as "Part#", sum(pmonhd) as "On Hand",                        '.
             '  int(sum((select coalesce(sum(tr.ptqty * tr.ptnet), 0)                                            '.
             '           from rydedata.pdptdet tr                                                                '.
             '           where tr.ptpart = coalesce(ps.pfaprt, pd.pmpart)                                        '.
             '           and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())    '.
             "           and ptcode in ('CP', 'IS', 'SA', 'WS', 'SC')                                            ".
             '          ))) as "Yearly $ Sales",                                                                 '.
             '  sum((select coalesce(sum(tr.ptqty), 0)                                                           '.
             '       from rydedata.pdptdet tr                                                                    '.
             '       where tr.ptpart = coalesce(ps.pfaprt, pd.pmpart)                                            '.
             '         and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())      '.
             "         and ptcode in ('CP', 'IS', 'SA', 'WS', 'SC')                                              ".
             '      )) as "Yearly Units"                                                                         '.
             'FROM rydedata.pdpmast pd                                                                           '.
             '  left join rydedata.pdpmrpl ps on pd.pmpart = ps.pfoprt and pd.pmco# = ps.pfco#                   '.
             "WHERE pd.pmco# = 'RY1'                                                                             ".
             "  AND trim(pd.pmnprt) = ''                                                                         ".
             "  AND trim(pmgrpc) in ('01835', '01836')                                                           ".
             'GROUP BY coalesce(ps.pfaprt, pd.pmpart)                                                            '.
             'ORDER BY "Yearly $ Sales" desc                                                                     ';
      break;

    case 'airfiltersales':
      $sql = 'SELECT coalesce(ps.pfaprt, pd.pmpart) as "Part#", sum(pmonhd) as "On Hand",                        '.
             '  int(sum((select coalesce(sum(tr.ptqty * tr.ptnet), 0)                                            '.
             '           from rydedata.pdptdet tr                                                                '.
             '           where tr.ptpart = coalesce(ps.pfaprt, pd.pmpart)                                        '.
             '           and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())    '.
             "           and ptcode in ('CP', 'IS', 'SA', 'WS', 'SC')                                            ".
             '          ))) as "Yearly $ Sales",                                                                 '.
             '  sum((select coalesce(sum(tr.ptqty), 0)                                                           '.
             '       from rydedata.pdptdet tr                                                                    '.
             '       where tr.ptpart = coalesce(ps.pfaprt, pd.pmpart)                                            '.
             '         and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())      '.
             "         and ptcode in ('CP', 'IS', 'SA', 'WS', 'SC')                                              ".
             '      )) as "Yearly Units"                                                                         '.
             'FROM rydedata.pdpmast pd                                                                           '.
             '  left join rydedata.pdpmrpl ps on pd.pmpart = ps.pfoprt and pd.pmco# = ps.pfco#                   '.
             "WHERE pd.pmco# = 'RY1'                                                                             ".
             "  AND trim(pd.pmnprt) = ''                                                                         ".
             "  AND trim(pmgrpc) = '03410'                                                                       ".
             'GROUP BY coalesce(ps.pfaprt, pd.pmpart)                                                            '.
             'ORDER BY "Yearly $ Sales" desc                                                                     ';
      break;

    case 'rim':
      $sql = 'SELECT coalesce(ps.pfaprt, pd.pmpart) as "Part", pmdesc as "Description",              '.                                      
              '  (select coalesce(sum(ptqty), 0)                                                     '.
              '     from rydedata.pdptdet                                                            '.
              '     where ptpart = coalesce(ps.pfaprt, pd.pmpart)                                    '.
              "       and ptcode = 'SA')                                                             ".
              '          as "Yearly Sales",                                                          '.
              '  pm.pmstkl as "RIM Level",pmonhd as "On Hand",                                       '.
              '  pmordr as "Reserved" ,pmspor as "On Order",                                         '.
              '  pmrsrv as "Spec Order",pmbcko as "Back Ordered",                                    '.
              '  pm.pmstkl + pmrsrv - pmonhd - pmordr - pmspor - pmbcko as "Needed"                  '.
              'FROM rydedata.pdpmast pd                                                              '.
              'left join rydedata.pdpmrpl ps on pd.pmpart = ps.pfoprt and pd.pmco# = ps.pfco#        '.
              'inner join rydedata.pdppmex pm on pd.pmpart = pm.pmpart                               '.
              "  and pd.pmco# = pm.pmco# and pm.pmstcd = '02'                                        ".
              "WHERE pd.pmco# = 'RY1'                                                                ".
              "  AND pd.pmstat = 'A'                                                                 ".
              "  AND pmsgrp = 201                                                                    ".
              "  AND trim(pd.pmnprt) = ''                                                            ".
              "  AND trim(pm.pmstcd) = '02'                                                          ".
              "  AND pm.pmstkl + pmrsrv - pmonhd - pmordr - pmspor - pmbcko > 0                      ".
              'ORDER BY coalesce(ps.pfaprt, pd.pmpart)                                               ';
      break;      

