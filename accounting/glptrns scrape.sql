4/13/14
need to get serious about accounting warehouse

it appears that when a gl transaction is voided, the corresponding glpdtim row is deleted
sort of, sometimes
well, not really
-- void transactions with and without apply (glpdtim) rows
select *
from (
select year(gtdate) as year, count(*) as withApply
from rydedata.glptrns a
where gtpost = 'V'
  and gtdate > '2011-01-31'
  and exists (
    select 1
    from rydedata.glpdtim
    where gqtrn# = a.gttrn#)
group by year(gtdate)) a
left join (
select year(gtdate) as year, count(*) as noApply
from rydedata.glptrns a
where gtpost = 'V'
  and gtdate > '2011-01-31'
  and not exists (
    select 1
    from rydedata.glpdtim
    where gqtrn# = a.gttrn#)
group by year(gtdate)) b on a.year = b.year

-- are there any apply rows without a matching trans row
select date_of_apply, count(*)
from rydedata.glpdtim a
where not exists (
  select 1
  from rydedata.glptrns
  where gttrn# = a.gqtrn#)
group by date_of_apply  
order by date_of_apply desc
-- curious about, but not sure of the meaning of these
select *
from rydedata.glpdtim a
where not exists (
  select 1
  from rydedata.glptrns
  where gttrn# = a.gqtrn#)
order by date_of_apply desc
  
-- trans rows with no apply  
-- !! all are void
select gtpost, count(*)
-- select *
from rydedata.glptrns a
where gtpost = 'V'
  and gtdate > '2011-01-31'
  and not exists (
    select 1
    from rydedata.glpdtim
    where gqtrn# = a.gttrn#)
group by gtpost
  
  
select *
from rydedata.glptrns
where where gtpost = 'V'
  and gtdate > '2011-01-31'

select count(*)
select *
from rydedata.glptrns
where month(gtdate) = 3
and year(gtdate) = 2014
and gtpost = 'Y'

select gttrn#, gtseq#
from rydedata.glptrns
where month(gtdate) = 3
and year(gtdate) = 2014
and gtpost = 'Y'


select *
from rydedata.glptrns a
where gtdate > '2012-01-01'
  and gtpost = 'V'
  and not exists (
    select 1
    from rydedata.glpdtim
    where gqtrn# = a.gttrn#)
    
-- march transactions applied outside of march    
select a.gttrn#, a.gtdate, b.*
from rydedata.glptrns a
inner join rydedata.glpdtim b on a.gttrn# = b.gqtrn#
where gtdate between '2014-03-01' and '2014-03-31'  
  and b.gqdate not between 20140301 and 20140331
  
select min(gqdate), max(gqdate)
from rydedata.glptrns a
inner join rydedata.glpdtim b on a.gttrn# = b.gqtrn#
where gtdate between '2014-03-01' and '2014-03-31'  
  and b.gqdate not between 20140301 and 20140331  

-- wtf, applied before march ???  
select a.gttrn#, a.gtdate, b.*
--select *
from rydedata.glptrns a
inner join rydedata.glpdtim b on a.gttrn# = b.gqtrn#
where gtdate between '2014-03-01' and '2014-03-31'  
  and b.gqdate < 20140301      
  