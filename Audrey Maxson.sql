Select * from RYDEDATA.BOPNAME where bnsnam like 'MAXSON%'

select * from bopname where BNKEY in (217890, 209463)

select *
from bopvref
where bvkey in (217890, 209463)
--where bvkey = 252250
and bvedat = 0

select bvkey, count(*) as HowMany
from bopvref
group by bvkey
order by count(bvkey) desc

select bnco#, bn.bnkey, bn.bnsnam as Name, bv.HowMany
--select *
from bopname bn
inner join (
  select bvkey, count(*) as HowMany
  from bopvref
  group by bvkey) bv on bv.bvkey = bn.bnkey
order by HowMany desc

select *
from inpmast 
where imvin in(
  select bvvin
  from bopvref
	  where bvkey = 323005)
	  
select *
from bopvref
where bvvin in (
	select bvvin
	from bopvref
	where bvkey = 323005
	and bvedat = 0)
order by bvvin	  
	  
-- KL1TD66637B794790
-- BROWN,ANNETTE	  

select * from bopname where bnsnam = 'BROWN, ANNETTE'

select *
from bopvref
where bvkey in (
  select bnkey
  from bopname
  where bnsnam = 'BROWN, ANNETTE')
  
select bvvin, count(bvvin)
from bopvref
group by bvvin
having count(bvvin) > 1

select *
from bopvref
where bvvin in (
	select bvvin
	from bopvref
	where bvedat = 0
	group by bvvin
	having count(bvvin) > 1)
order by bvvin

select * from bopvref where bvvin = 'KL1TD66637B794790'

select bn.bnkey, bn.bnco#, bn.bnsnam as Name, bv.HowMany
from bopname bn
inner join (
  select bvkey, count(*) as HowMany
  from bopvref
  where bvedat = 0
  group by bvkey
  having count(*) > 4) bv on bv.bvkey = bn.bnkey
order by HowMany desc

select * from bopname where bnsnam = 'JEFF HANSEN'
-- Jeff Hansen's cars
select * from bopvref where trim(bvkey) = '252250' order by bvsdat desc

select * from bopname  order by bnsnam

select bvsdat, count(bvsdat)
from bopvref
where trim(bvkey) = '252250'
group by bvsdat
order by count(bvsdat) desc

select * from bopvref where trim(bvkey) = '289743' and bvedat = '0'


select bn.bnkey, bn.bnco#, bn.bnsnam as Name, bv.HowMany
from bopname bn
inner join (
  select bvkey, count(*) as HowMany
  from bopvref
  where bvedat = 0
  group by bvkey
  having count(*) > 4) bv on bv.bvkey = bn.bnkey
order by HowMany desc

-- Types in CustVehXref
select bvtype, count(*) 
from bopvref /*Customer/Vehicle Reference*/
group by bvtype

-- Names currently assoc with 10 or more vehicles
select bn.bnkey, bn.bnco#, bn.bnsnam as Name, bv.HowMany
from bopname bn
inner join (
  select bvkey, count(*) as HowMany
  from bopvref
  where bvedat = 0 /*End Date*/
  group by bvkey
  having count(*) > 9) bv on bv.bvkey = bn.bnkey
order by HowMany desc

-- export as csv
select bn.bnco#, bn.bnkey, bn.bntype, bn.bnsnam, bv.* -- bn.bnctype, bn.bnccid, bn.bnsxfnam, bn.bnsxlnam
from bopname bn
left join bopvref bv on bv.bvkey = bn.bnkey
where bn.bnkey in (
  select bn1.bnkey
  from bopname bn1
  inner join (
    select bvkey, count(*) as HowMany
    from bopvref
    where bvedat = 0
    group by bvkey
    having count(*) > 9) bv on bv.bvkey = bn1.bnkey)
order by bn.bnkey   

select *
from bopvref
where trim(bvvin) = '1GTEK19B25E250487'


select bntype, count(*)
from bopname
group by bntype


select n.bnco#, n.bnkey, n.bnsnam, v.*
from bopname n
left join bopvref v on v.bvkey = n.bnkey
where n.bntype = ' '
order by n.bnsnam

select imvin, count(*)
from inpmast
group by imvin
having count(*) > 1







	

--so let's grab another random vin:  1G2ZG58N274113231

select * from bopvref where bvvin = '1G2ZG58N274113231'
select * from bopname where bnkey = '277599'  'CAVANAUGH, JUSTIN' :  202661, 277599

sold to Justin Cavanaugh 2/13/09 98076X

select *
from inpmast
where imvin = '1G2ZG58N274113231'

select bnsnam, count(bnsnam)
from bopname
group by bnsnam
having count(bnsnam) > 1
order by count(bnsnam) desc

select bn.bnsnam, bn.bnkey, bn.bnadr1, bv.*
from bopname bn
left join  bopvref bv on bv.bvkey = bn.bnkey
where trim(bnsnam) = 'OLSON, STEVE'
order by bnadr1

select *
from bopname
where trim(bnsnam) = 'OLSON, STEVE'
order by bnphon
