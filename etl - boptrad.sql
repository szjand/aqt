Select * from RYDEDATA.bopmast where trim(bmstk#) = '22035P'

select * from rydedata.Boptrad where key = 22758

select count(*) from rydedata.boptrad

select company_number, count(*) from rydedata.boptrad group by company_number

select trim(stock_)
from rydedata.boptrad
group by trim(stock_)
having count(*) > 1

-- multiple stocknumbers are, i believe, bad juju and can safely be excluded
select *
from rydedata.boptrad
where trim(stock_) in (
select trim(stock_)
from rydedata.boptrad
group by trim(stock_)
having count(*) > 1)
order by stock_

-- multiple keys represent multiple trades
select *
from rydedata.boptrad
where key in (
select key
from rydedata.boptrad
group by key
having count(*) > 1)
order by key desc

-- greg suggested that bopmast records that do not exist in boptrad would represent purchases
-- i do not know if a purchase would be in bopmast
-- does not look like it
select * from rydedata.bopmast where trim(bmstk#) = '22793XX'

select * from rydedata.inpmastjs where trim(imstk#) = '22793XX'

-- bare bones minimal required
-- export as insert statement into extArkonaBOPTRAD
select company_number, key,trim(vin), trim(stock_) as stocknumber
from rydedata.boptrad a
where trim(stock_) not in (
  select trim(stock_)
  from rydedata.boptrad
  group by trim(stock_)
  having count(*) > 1)
 and exists ( -- elimiates a few with no matching key in bopmast
   select 1
   from rydedata.bopmast
   where bmkey = a.btkey)
  
select key, stocknumber
from (
  select company_number, key, vin, trim(stock_) as stocknumber
  from rydedata.boptrad a
  where trim(stock_) not in (
    select trim(stock_)
    from rydedata.boptrad
    group by trim(stock_)
    having count(*) > 1)) a 
group by key, stocknumber having count(*) > 1


SELECT *
FROM rydedata.BOPTRAD
WHERE key = 23438


select key, vin
from rydedata.boptrad
group by key, vin
having count(*) > 1


select a.*
from rydedata.boptrad a
inner join 

select bopmast_company_number, bmkey
--select *
from rydedata.bopmast
group by bopmast_company_number, bmkey having count(*) > 1

-- 8/19 dups are not the slam dunk i assumed above

-- acv/trad_allowance: boptrad vs bopmast
-- fuck, bopmast does not break out values for individual trades, but adds them up
-- so the following shows multiple rows when there is more than one trade
select a.company_number, a.key, a.vin, a.stock_, a.trade_allowance, a.acv, 
  b.trade_allowance, b.trade_ACV, b.bmdtor, b.bopmast_vin
from rydedata.boptrad a
left join rydedata.bopmast b on a.company_number = b.bmco# AND a.key = b.bmkey -- and trim(a.vin) = trim(b.bopmast_vin) 
where a.trade_allowance <> b.trade_allowance or a.acv <> trade_acv
order by b.bmdtor desc 

-- so, this shows that there are differences, not a zillion, but some (18 in 2014)
-- which implies to me that the "true" acv/trade allow are what are calculated as 
-- part of the deal (which can be different than what is in boptrad, but since
-- the deal does not break out those values for the individual trades
select *
from (
  select a.key, sum(a.trade_allowance) as aTradeAllow, sum(a.acv) as aAcv, 
    b.trade_allowance, b.trade_ACV, b.bmdtor, b.bopmast_vin, b.bmstk#
  from rydedata.boptrad a
  left join rydedata.bopmast b on a.company_number = b.bmco# AND a.key = b.bmkey -- and trim(a.vin) = trim(b.bopmast_vin) 
  group by a.key, b.trade_allowance, b.trade_ACV, b.bmdtor, b.bopmast_vin, b.bmstk#) c
where aTradeAllow <> trade_allowance or aAcv <> trade_acv
order by bmdtor desc 

-- and acv is often <> trade_allowance
select a.key, a.vin, a.stock_, a.trade_allowance, a.acv,
  (select bmdtor from rydedata.bopmast where bmco# = a.company_number AND bmkey = a.key) as bmdtor
from rydedata.boptrad a
where a.trade_allowance <> acv
order by (select bmdtor from rydedata.bopmast where bmco# = a.company_number AND bmkey = a.key) desc 

example of discrepancy:
21723PR
              allow      acv
A            5500       5500
AA           1000       1000
             6500       6500
deal         7000       6500             

-- per converstion with jeri, over/under allow is not relevant at the individual trade level, but is relevant to the
-- vehicle being sold

so, do i record and track allowance at the trade level (acquisition) at all ?

-- 1/28/15 wtf the fuck did i mean: -- 8/19 dups are not the slam dunk i assumed above
-- well, for one thing, if a trade stock number is entered incorrectly, when (and if) it
-- eventually gets corrected, the boptrad record is not updated
select a.company_number, a.key, a.vin, a.stock_,
 b.record_status, b.bopmast_stock_number, b.bopmast_vin, b.date_approved, 
 b.date_capped, b.bopmast_search_name
from rydedata.boptrad a
left join rydedata.bopmast b on a.company_number = b.bmco# AND a.key = b.bmkey 
where a.company_number in ('RY1','RY2')
  and trim (stock_) in (
    select trim(stock_)
    from rydedata.boptrad
    group by trim(stock_)
    having count(*) > 1)
order by a.stock_, a.key   


6/5/15 - pk_boptrad:  company_number,key,vin

select company_number,key,vin
from rydedata.boptrad
group by company_number,key,vin
having count(*) > 1


 

   
   
    