SELECT trim(IMSTK#) as StockNumber,  imiact as"Inv Acct"
from Inpmast i
left join (
  select gtctl#, sum(gttamt)as net
  from rydedata.glptrns
  where trim(gtacct) in (-- Inventory accounts only
    '123100', -- Chev
    '123101', -- Cad
    '123105', -- Buick
    '123700', -- Chev Lt Trk
    '123701', -- Cad Trk
    '123705', -- Buick Trk
    '123706', -- GMC Trk
    '223000', -- Honda Trk
    '223100', -- Honda
    '223105', -- Nissan
    '223700', -- Nissan Trk
    '323102', -- Buick
    '323702', -- Buick Trk
    '323703') -- GMC Trk) 
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by gtctl#
    having sum(coalesce(gttamt,0)) > 1) ia on trim(i.imstk#) = trim(ia.gtctl#)
where imstat = 'I'
and imtype = 'N'
and ia.gtctl# is null 
order by imiact, imstk#