-- 3/3/10 added imdinv, imdlv & imcost - no historical values, but current (not all)
-- 4/4/10 added imstat (to enable checking inventory)
Select trim(imvin) as VIN, trim(imstk#) as StockNumber, CHAR(imyear) as Year, immake as Make, 
  immcode as ModelCode, immodl as Model, imbody as Body, imcolr as Color, 
  case imdinv
    when 0 then null
    else DATE(INSERT(INSERT(DIGITS(imdinv),5,0,'-'),8,0,'-')) 
   end as imdinv, 
  case imddlv
    when 0 then null
    else DATE(INSERT(INSERT(DIGITS(imddlv),5,0,'-'),8,0,'-')) 
   end as imddlv, imcost, imstat
from rydedata.INPMAST i
where imtype = 'U'
and imstk# <> ''
and imcost <> 0
order by imdinv desc

/*
select count(*)
from RYDEDATA.INPMAST i
where imtype = 'U'
and imstk# <> ''
and imdinv <> 0


select imdinv, count(*)
from inpmast
group by imdinv
order by imdinv

select 
  case imdinv
    when 0 then null
    else DATE(INSERT(INSERT(DIGITS(imdinv),5,0,'-'),8,0,'-')) -- this works
--    else DATE(TRANSLATE('ABCD-EF-GH',DIGITS(imdinv),'ABCDEFGH')) -- doesn't work
  end as imdinv
from inpmast
group by imdinv
order by imdinv  

select digits(imdinv) from inpmast where imdinv is not null


*/
-- from the code
     'select imstk#, imvin, imyear, immake, immodl, imbody, imcolr, imtrim, imodom, impric, f.ionval ' +
          'from rydedata.inpmast i ' +
          'inner join ( ' +
          '  select gtacct, gtctl#, sum(gttamt) as net ' +
          '  from rydedata.glptrns  ' +
          '  where  trim(gtpost) <> ''V'' ' +
          '  group by gtacct, gtctl# ' +
          '      having sum(coalesce(gttamt, 0)) > 1) ia  on trim(i.imstk#) = trim(ia.gtctl#) ' +
          'inner join ( ' +
          '  select distinct gmacct ' +
          '  from rydedata.glpmast ' +
          '  where ( ' +
          '    gmdesc like ''INV N/%'' ' +
          '    or ' +
          '    gmdesc like ''INV-NEW%'' ' +
          '    or ' +
          '    gmdesc like ''INV NEW%'')) a on trim(ia.gtacct) = trim(a.gmacct) ' +
          'left join rydedata.inpoptf f on f.iovin = i.imvin and f.ioseq# = ''9'' ' +
          'where imstat = ''I'' ' +
          '  and imtype = ''N'

     select imstk#, imvin, imyear, immake, immodl, imbody, imcolr, imtrim, imodom, impric, f.ionval
     from rydedata.inpmast i 
     inner join (
       select gtacct, gtctl#, sum(gttamt) as net 
       from rydedata.glptrns  
       where  trim(gtpost) <> 'V'
       group by gtacct, gtctl# 
           having sum(coalesce(gttamt, 0)) > 1) ia  on trim(i.imstk#) = trim(ia.gtctl#) 
     inner join ( 
       select distinct gmacct 
       from rydedata.glpmast 
       where ( 
         gmdesc like 'INV N/%'
         or
         gmdesc like 'INV-NEW%'
         or
         gmdesc like 'INV NEW%')) a on trim(ia.gtacct) = trim(a.gmacct) 
     left join rydedata.inpoptf f on f.iovin = i.imvin and f.ioseq# = '9'
     where imstat = 'I'
       and imtype = 'N'

select date('01/24/2012') from sysibm.sysdummy1