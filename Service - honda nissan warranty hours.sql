select c.make, sum(b.ptlhrs) as flag_hours
-- select *
from rydedata.sdprhdr a
inner join rydedata.sdprdet b on trim(a.ro_number) = trim(b.ptro#)
  and ptlpym = 'W'
inner join rydedata.inpmast c on a.vin = c.inpmast_vin
  and c.make in ('HONDA','NISSAN')
where final_close_date between 20180101 and 20181231  
-- where trim(ro_number) = '2791131'
group by c.make