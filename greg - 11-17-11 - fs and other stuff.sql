--------------------------------------------------
-- Create procedure RYDEDATA.emps  *** Run as 1 statement ***
--------------------------------------------------
Create Procedure RYDEDATA.emps () 
                Language SQL  
                Dynamic Result Sets 1 
                Reads SQL Data    
begin
  DECLARE C1 CURSOR FOR SELECT ymname, ymdist FROM rydedata.pymast;
  OPEN C1;
  RETURN;
end

call rydedata.emps()

drop rydedata.emps


select *
from RYDEDATA.FFPXREFDTA
where fxcyy = 2011
  and fxconsol = '' -- Main GM Statement
  and left(trim(fxfact), 3) >= '400'

  and cast(left(trim(fxfact), 3) as integer) >= 400
  and integer(left(trim(fxfact), 3)) >= 400 and integer(left(trim(fxfact), 3)) <= 445 


select fxgact
from rydedata.ffpxrefdta
where fxcyy = 2011
  and fxconsol = ''
  and left(trim(fxfact), 3) >= '400'

select distinct fxfact, cast(substr(trim(fxfact), 2, 2) as integer)--fxgact
from rydedata.ffpxrefdta
where fxcyy = 2011
  and fxconsol = ''
  and ((trim(fxfact) like '4__A') and (cast(substr(trim(fxfact), 2, 2) as integer) <= 57))


-- Chevrolet Sales Page 5A



select gtctl#, sum(gttamt)
from rydedata.glptrns gl
where gl.gtdate >= '10/1/2011' 
  and gl.gtdate <= '10/31/2011' 
  and gl.gtpost <> 'V'
  and trim(gl.gtacct) in ('144600', '144601', '145000', '145001')
group by gtctl#





-- Used Car Deals
-- 446A = Certified Cars Sales    646A = Certified Cars Cost    647A = Recon
-- 446B = Other Used Cars Sales   646B = Other Used Cars Cost   647B = Recon
-- 450A = Certified Truck Sales   650A = Certified Trucks Cost  651A = Recon
-- 450B = Other Used Truck Sales  650B = Other Used Trucks Cost 651B = Recon






select x."Stock#", x."Sales" as "Sales", x."COS", x."Recon", x."COS" + x."Recon" as "Cost", x."Sales" + x."COS" + x."Recon" as "Gross" 
from
(select gtctl# as "Stock#", sum(case when trim(fxfact) in ('446A', '446B', '450A', '450B') then gttamt else 0 end) as "Sales", sum(case when trim(fxfact) in ('646A', '646B', '650A', '650B') then gttamt else 0 end) as "COS", 
  sum(case when trim(fxfact) in ('647A', '647B', '651A', '651B') then gttamt else 0 end) as "Recon"
from rydedata.glptrns gl 
  inner join rydedata.ffpxrefdta on gtacct = fxgact and fxcyy = '2011' and fxconsol = '' and trim(fxfact) in ('446A', '646A', '647A', '446B', '646B', '647B', '450A', '650A', '651A', '450B', '650B', '651B')
where gl.gtdate >= '10/1/2011' 
  and gl.gtdate <= '10/31/2011' 
  and gl.gtpost <> 'V'
group by gtctl#) as x 
order by x."Stock#"


select x."Account", abs(x."Sales") as "Sales", x."COS", x."Recon", x."COS" + x."Recon" as "Cost", abs(x."Sales") - x."COS" - x."Recon" as "Gross" 
from

(select gtacct as "Account", sum(case when trim(fxfact) in ('446A', '446B', '450A', '450B') then gttamt else 0 end) as "Sales", sum(case when trim(fxfact) in ('646A', '646B', '650A', '650B') then gttamt else 0 end) as "COS", 
  sum(case when trim(fxfact) in ('647A', '647B', '651A', '651B') then gttamt else 0 end) as "Recon"
from rydedata.glptrns gl 
  inner join rydedata.ffpxrefdta on gtacct = fxgact and fxcyy = '2011' and fxconsol = '' and trim(fxfact) in ('446A', '646A', '647A', '446B', '646B', '647B', '450A', '650A', '651A', '450B', '650B', '651B')
where gl.gtdate >= '10/1/2011' 
  and gl.gtdate <= '10/31/2011' 
  and gl.gtpost <> 'V'
--  and gl.gtjrnl in ('GJE', 'STD', 'WTD')
  group by gtacct) as x
order by abs(x."Sales")




-- New Vehicle Gross
select year(gtdate) as "Year", month(gtdate) as "Month", sum(gttamt) as "Sales"
from rydedata.glptrns
where gtpost <> 'V'
  and trim(gtacct) in (select fxgact
                       from rydedata.ffpxrefdta
                       where fxcyy = 2011
                         and fxconsol = ''
  and ((trim(fxfact) like '4__A') and (cast(substr(trim(fxfact), 2, 2) as integer) <= 41)))
and gtdate >= '9/1/2011' and gtdate <= '9/30/2011'
group by year(gtdate), month(gtdate)

-- Find all accounts that are routed into line 023 on the GM financial statement
-- Appears A = New Car, B = Used Car, C = Lease, D = Service, E = Body Shop and F = Parts
select fxfact as "FS Account", fxgact as "GL Account", gmdesc as "Description"
from rydedata.ffpxrefdta f
  inner join rydedata.glpmast g on gmco# = 'RY1' and g.gmacct = f.fxgact
where fxcyy = 2011
  and fxconsol = ''
  --and trim(fxgact) = '12324'
  and left(trim(FXFACT), 3) = '023'

-- Sum of transactions for a selected financial statement account
select year(gtdate) as "Year", month(gtdate) as "Month", sum(gttamt) as "Amount"
from rydedata.glptrns
where gtpost <> 'V'
  and trim(gtacct) in (select fxgact
                       from rydedata.ffpxrefdta
                       where fxcyy = 2011
                         and fxconsol = ''
                         and fxfact like '024D%')
  and gtdate >= '8/1/2011' and gtdate <= '8/31/2011'
group by year(gtdate), month(gtdate)




















-- The corresponding Tech Productivity Report in DealerTrack is accurate to tenths of hours
-- Tech hours worked in this report is accurate to the hundreths of hours and matches the DealerTrack Time Keeping Report
select z."EmpNum", sttech as "Tech", ymdist as "Dept", ymname as "Name", 
  sum(z."Reg Hours") as "Reg Hours", sum(z."OT Hours") as "OT Hours", sum(z."Reg Hours") + sum(z."OT Hours") as "Total Hours",
  round(sum(z."Reg Hours") * ymrate, 2) as "Reg Pay", round(sum(z."OT Hours") * ymrate * 1.5, 2) as "OT Pay", round(sum(z."Reg Hours") * ymrate + sum(z."OT Hours") * ymrate * 1.5, 2) as "Total Pay"
from 
  ( -- z
    select a."Company", a."EmpNum", a."Week", a."Day",
      coalesce(sum(b."Hours"), 0) as "Total Hours Before",
      a."Hours" as "Hours Today",
      coalesce(sum(b."Hours"), 0) + a."Hours" as "Total Hours After",
      case 
        when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then a."Hours"
        when coalesce(sum(b."Hours"), 0) > 40 then 0
        else 40 - coalesce(sum(b."Hours"), 0) 
      end as "Reg Hours",
      case 
        when coalesce(sum(b."Hours"), 0) >= 40 then a."Hours"
        when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then 0
        else a."Hours" - (40 - coalesce(sum(b."Hours"), 0)) 
      end as "OT Hours"
    from 
      ( -- a
        select yico# as "Company", yiemp# as "EmpNum", week(yiclkind) as "Week", yiclkind as "Day", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin
        where trim(yicode) = 'O' and yiclkind >= '12/26/2010' and yiclkind <= '12/31/2011'
        group by yico#, yiemp#, yiclkind) as a
    left join 
      ( -- b
        select yico# as "Company", yiemp# as "EmpNum", week(yiclkind) as "Week", yiclkind as "Day", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
       from rydedata.pypclockin
        where trim(yicode) = 'O' and yiclkind >= '12/26/2010' and yiclkind <= '12/31/2011'
        group by yico#, yiemp#, yiclkind) as b on a."Company" = b."Company" 
          and a."EmpNum" = b."EmpNum" and week(a."Day") = week(b."Day") and b."Day" < a."Day"
    group by a."Week", a."Company", a."EmpNum", a."Day", a."Hours"
    order by a."Week", a."Company", a."EmpNum", a."Day") as z
left join rydedata.pymast py on py.ymco# = z."Company" 
  and py.ymempn = z."EmpNum"
left join rydedata.sdptech sd on sd.stpyemp# = z."EmpNum" and trim(sd.sttech) > '501' and sd.stactv <> 'N'
where z."Day" >= '9/1/2011' and z."Day" <= '9/13/2011'
and trim(py.ymempn) in (
  select trim(ymempn)
  from rydedata.pymast
  where ymco# = 'RY1'
--  and ymname like 'SHERECK%'
  --and ymdist in ('WTEC')
    and ymdist = 'BTEC' 
  --and trim(ymdept) in ('03', '04')
)
group by z."EmpNum", ymname, ymdist, ymrate, sttech
order by sttech


select * from sdptech



-- List of active technicians comparing hourly pay rates to their costing rate
-- select * from sdptech
-- select * from pymast
select ymdept as "Dept", sttech as "Tech #", stname as "Tech Name", stpyemp# as "Emp #", ymname as "Name", ymrate as "Hourly Rate", ymrate * 1.2 as "Costing Formula", stlrat as "Current Costing", stlrat - (ymrate * 1.2) as "Difference"
from rydedata.pymast p
  inner join sdptech t on stco# = 'RY2' and stactv <> 'N' and t.stpyemp# = p.ymempn
where ymco# = 'RY2'
  and ymactv <> 'T' 
order by ymdept, ymname


-- Find all accounts that are routed into line 023 on the GM financial statement
-- Appears A = New Car, B = Used Car, C = Lease, D = Service, E = Body Shop and F = Parts
select fxfact as "FS Account", fxgact as "GL Account", gmdesc as "Description"
from rydedata.ffpxrefdta f
  inner join rydedata.glpmast g on gmco# = 'RY1' and g.gmacct = f.fxgact
where fxcyy = 2011
  and fxconsol = ''
  --and trim(fxgact) = '12324'
  and left(trim(FXFACT), 3) = '023'

-- Sum of transactions for a selected financial statement account
select year(gtdate) as "Year", month(gtdate) as "Month", sum(gttamt) as "Amount"
from rydedata.glptrns
where gtpost <> 'V'
  and trim(gtacct) in (select fxgact
                       from rydedata.ffpxrefdta
                       where fxcyy = 2011
                         and fxconsol = ''
                         and fxfact like '024D%')
  and gtdate >= '8/1/2011' and gtdate <= '8/31/2011'
group by year(gtdate), month(gtdate)


-- Find the current months Body Shop Bonus
select *
from rydedata.glptrns
where gtdate >= '7/1/2011' and gtdate <= '7/31/2011'
  and gtacct = '167500'
  and left(trim(gtdesc), 3) <> 'Pay'
  and gtjrnl = 'STD'




-- Find the account that employee gross pay goes into for each distribution code in payroll
-- select * from pyactgr
select ytadic as "Dist Code", ytaseq as "Sequence", ytadsc as "Description", ytagpp as "Percent", ytagpa as "GL Account", gmdesc as "Description"
from rydedata.pyactgr p
  inner join rydedata.glpmast g on gmco# = 'RY1' and g.gmacct = p.ytagpa
where ytaco# = 'RY1'
order by ytadic, ytaseq


-- Based on payroll distribution codes and financial statement routings, list all employees that hit a financial statement line
-- select * from pyactgr
select distinct ymname as "Name", '023D' as "Account"
from rydedata.pymast p
where ymdist in (select ytadic  
                 from rydedata.pyactgr 
                 where ytaco# = 'RY1'
                   and ytagpa in (select fxgact
                                  from rydedata.ffpxrefdta 
                                  where fxcyy = 2011
                                    and fxconsol = ''
                                    and fxfact like '023D%'))
order by ymname

-- Based on interfaced GL entries from payroll, list all employees that hit a financial statement line
select gtdate, ymname, ymempn, ymdept, ymdist, ytagpp, case when ymclas = 'H' then 'Hourly' when ymclas = 'C' then 'Commission' when ymclas = 'S' then 'Salary' end as "Type", sum(gttamt)
from rydedata.glptrns g
  left join rydedata.pymast p on trim(p.ymempn) = trim(g.gtctl#)
  left join rydedata.pyactgr d on ytaco# = 'RY1' and d.ytadic = p.ymdist and trim(d.ytagpa) = trim(g.gtacct)
where gtpost <> 'V'
--  and left(trim(gtdesc), 3) = 'Pay'
--  and ymdist in ('PDQW', 'SRVM')
  and gtdate >= '8/1/2011' and gtdate <= '8/31/2011'
  and trim(gtacct) in (select trim(fxgact)
                       from RYDEDATA.FFPXREFDTA
                       where fxcyy = 2011
                        and fxconsol = ''
                         and left(trim(FXFACT), 4) = '023D')
group by ymname, ymempn, ymdept, ymdist, ytagpp, ymclas, gtdate
order by ymdept, ymdist, ymname

-- Based on interfaced GL entries from payroll, list all employees that hit a financial statement line
select gtdate, ymname, ymempn, ymdept, ymdist, ytagpp, case when ymclas = 'H' then 'Hourly' when ymclas = 'C' then 'Commission' when ymclas = 'S' then 'Salary' end as "Type", sum(gttamt)
from rydedata.glptrns g
  left join rydedata.pymast p on trim(p.ymempn) = trim(g.gtctl#)
  left join rydedata.pyactgr d on ytaco# = 'RY1' and d.ytadic = p.ymdist and trim(d.ytagpa) = trim(g.gtacct)
where gtpost <> 'V'
--  and left(trim(gtdesc), 3) = 'Pay'
--  and ymdist in ('PDQW', 'SRVM')
  and gtdate >= '8/1/2011' and gtdate <= '8/31/2011'
  and trim(gtacct) in (select trim(fxgact)
                       from RYDEDATA.FFPXREFDTA
                       where fxcyy = 2011
                         and fxconsol = ''
                         and left(trim(FXFACT), 4) = '023D')
group by ymname, ymempn, ymdept, ymdist, ytagpp, ymclas, gtdate
order by ymdept, ymdist, ymname

-- Based on interfaced GL entries from payroll, list all employees that hit a financial statement line
select gtdate, ymname, ymempn, ymdept, ymdist, ytagpp, case when ymclas = 'H' then 'Hourly' when ymclas = 'C' then 'Commission' when ymclas = 'S' then 'Salary' end as "Type", sum(gttamt)
from rydedata.glptrns g
  left join rydedata.pymast p on trim(p.ymempn) = trim(g.gtctl#)
  left join rydedata.pyactgr d on ytaco# = 'RY1' and d.ytadic = p.ymdist and trim(d.ytagpa) = trim(g.gtacct)
where gtpost <> 'V'
  and left(trim(gtdesc), 3) = 'Pay'
  and ymdist in ('CWAS')
  and gtdate >= '7/1/2011' and gtdate <= '8/31/2011'
  and trim(gtacct) in (select trim(fxgact)
                       from RYDEDATA.FFPXREFDTA
                       where fxcyy = 2011
                         and fxconsol = ''
                         and left(trim(FXFACT), 4) = '023D')
group by ymname, ymempn, ymdept, ymdist, ytagpp, ymclas, gtdate
order by gtdate, ymname

-- Based on interfaced GL entries from payroll, list all employees that hit a financial statement line
select ymname, gtdate, sum(gttamt) as "Pay"
from rydedata.glptrns g
  left join rydedata.pymast p on trim(p.ymempn) = trim(g.gtctl#)
  left join rydedata.pyactgr d on ytaco# = 'RY1' and d.ytadic = p.ymdist and trim(d.ytagpa) = trim(g.gtacct)
where gtpost <> 'V'
  and left(trim(gtdesc), 3) = 'Pay'
  and gtdate >= '7/1/2011' and gtdate <= '7/31/2011'
  and trim(gtacct) in (select trim(fxgact)
                       from RYDEDATA.FFPXREFDTA
                       where fxcyy = 2011
                         and fxconsol = ''
                         and left(trim(FXFACT), 4) = '023D')
group by ymname, gtdate
order by gtdate, ymname

-- Based on interfaced GL entries from payroll, list all employees that hit a financial statement line
select ymdist, sum(case when month(gtdate) = 7 then gttamt else 0 end) as "July Pay", sum(case when month(gtdate) = 8 then gttamt else 0 end) as "Aug Pay"
from rydedata.glptrns g
  left join rydedata.pymast p on trim(p.ymempn) = trim(g.gtctl#)
  left join rydedata.pyactgr d on ytaco# = 'RY1' and d.ytadic = p.ymdist and trim(d.ytagpa) = trim(g.gtacct)
where gtpost <> 'V'
  and left(trim(gtdesc), 3) = 'Pay'
  and gtdate >= '7/1/2011' and gtdate <= '8/31/2011'
  and trim(gtacct) in (select trim(fxgact)
                       from RYDEDATA.FFPXREFDTA
                       where fxcyy = 2011
                         and fxconsol = ''
                         and left(trim(FXFACT), 4) = '023D')
group by ymdist
order by ymdist

-- Based on interfaced GL entries from payroll, sum a particular payroll
select sum(gttamt) as "Pay"
from rydedata.glptrns g
--  left join rydedata.pymast p on trim(p.ymempn) = trim(g.gtctl#)
--  left join rydedata.pyactgr d on ytaco# = 'RY1' and d.ytadic = p.ymdist and trim(d.ytagpa) = trim(g.gtacct)
where gtpost <> 'V'
  and left(trim(gtdesc), 3) = 'Pay'
  and gtdate = '8/5/2011'
  and trim(gtacct) in (select trim(fxgact)
                       from RYDEDATA.FFPXREFDTA
                       where fxcyy = 2011
                         and fxconsol = ''
                         and left(trim(FXFACT), 4) = '023D')


-- Payroll that hit Honda's Tech Pay
select sum(gttamt) as "Pay"
from rydedata.glptrns g
--  left join rydedata.pymast p on trim(p.ymempn) = trim(g.gtctl#)
--  left join rydedata.pyactgr d on ytaco# = 'RY1' and d.ytadic = p.ymdist and trim(d.ytagpa) = trim(g.gtacct)
where gtpost <> 'V'
  and left(trim(gtdesc), 3) = 'Pay'
  and year(gtdate) * 100 + month(gtdate) = 201108
  and trim(gtacct) in ('224700', '224701', '224704', '224723')

-- Based on interfaced GL entries from payroll, sum non payroll entries
select sum(gttamt)
from rydedata.glptrns g
where gtpost <> 'V'
  and left(trim(gtdesc), 3) <> 'Pay'
  and month(gtdate) = 7
  and trim(gtacct) in (select trim(fxgact)
                       from RYDEDATA.FFPXREFDTA
                       where fxcyy = 2011
                         and fxconsol = ''
                         and left(trim(FXFACT), 4) = '023D')

/* This is the motherlode file shows each pay check line and what account it hits
select *
from rydedata.pyactocd
*/

select *
from pypcodes
where ytddsc like '%COMM%'

select *
from rydedata.pyhshdta
where ypbcyy = 111 
  and ypbnum = 805001 
  and trim(yhdemp) = '148245'

-- This is the paycheck line item detail -- Calculates actual body shop bonus
select ymname as "Name", ymdist as "Dist", sum(case when trim(yhccde) = '79' then yhccea else 0 end) as "Bonus", sum(case when trim(yhccde) = '70' then yhccea else 0 end) as "Xtra Bonus"
from rydedata.pyhscdta p
  left join rydedata.pyhshdta h on h.yhdco# = 'RY1' and h.ypbcyy = p.ypbcyy and h.ypbnum = p.ypbnum and yhdemp = yhcemp
where yhcco# = 'RY1'
  and yhdcyy = 11 
  and yhdcmm = 7
  --and yhdcdd = 5
  and trim(p.yhccde) in ('70', '79') -- Tech Xtra Bonus / Commission
  and ymdist = 'BTEC'
group by ymname, ymdist
order by ymname


select *
from RYDEDATA.sd

select *
from glptrns
where 
  left(trim(gtdesc), 3) = 'Pay'
  and trim(gtctl#) in ('148245')
  and gtdate >= '8/1/2011' and gtdate <= '8/31/2011'


*/

select * from pyactgr
select * from glpmast
select * from pyactocd
select * from pypcodes

/* Jeri questions
   Which account does she make accruals for?
   Does she make the entire accrual in the Gross Pay account?
   Why are Cartiva, TRC, Saturn Development, Hobby Shop employees routed all over the place?
*/


-- Get list of company names based on active company numbers from the payroll distribution file
select distinct ytaco#, schnam
from pyactgr p
  inner join rydedata.sepcomp c on c.scco# = p.ytaco#

-- Get list of GL Accounts that have payroll distribution codes associated with them
-- select * from pymast
select distinct ytagpa as "Account", gmdesc as "Description"
from pyactgr p
  inner join rydedata.glpmast g on gmco# = 'RY1' and g.gmacct = p.ytagpa
where ytaco# = 'RY1'
order by ytagpa

-- Get list of Other Pay Codes with their distribution percentages and targets
select ytadic as "Dist Code", ytaopc as "Pay Code", ytddsc as "Description", ytaopp as "Percent", ytaopa as "GL Acct#", gmdesc as "Description"
from rydedata.pyactocd p
  inner join rydedata.pypcodes c on c.ytdco# = 'RY1' and c.ytddcd = p.ytaopc
  inner join rydedata.glpmast g on gmco# = 'RY1' and g.gmacct = p.ytaopa
where ytaco# = 'RY1'

-- Calculate the employee individual pay amounts for a line on the financial statement based on payroll distribution codes
select z."EmpNum", ymdist as "Dist", ymname as "Name", case when ymclas = 'H' then 'Hourly' when ymclas = 'C' then 'Commission' when ymclas = 'S' then 'Salary' end as "Type",
  sum(z."Reg Hours") as "Reg Hours", sum(z."OT Hours") as "OT Hours", sum(z."Reg Hours") + sum(z."OT Hours") as "Total Hours",
  case when ymclas = 'H' then round(sum(z."Reg Hours") * ymrate, 2) when ymclas = 'S' then sum(Emp."Daily Salary") when ymclas = 'C' then 99999 end as "Reg Pay", 
  case when ymclas = 'H' then round(sum(z."OT Hours") * ymrate * 1.5, 2) when ymclas = 'S' then 0 when ymclas = 'C' then 99999 end as "OT Pay", 
  case when ymclas = 'H' then round(sum(z."Reg Hours") * ymrate + sum(z."OT Hours") * ymrate * 1.5, 2) when ymclas = 'S' then sum(Emp."Daily Salary") when ymclas = 'C' then 99999 end as "Total Pay"
from 
select * from  (select distinct yiclkind as "TheDate", case when dayofweek(yiclkind) in (2,3,4,5,6) then 'Weekday' else 'Weekend' end as "DayType"
   from rydedata.pypclockin
   where yiclkind >= '12/26/2010' and yiclkind <= '12/31/2011') as DayTable
  left join 
  ( -- z
    select a."Company", a."EmpNum", a."Week", a."Day",
      coalesce(sum(b."Hours"), 0) as "Total Hours Before",
      a."Hours" as "Hours Today",
      coalesce(sum(b."Hours"), 0) + a."Hours" as "Total Hours After",
      case 
        when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then a."Hours"
        when coalesce(sum(b."Hours"), 0) > 40 then 0
        else 40 - coalesce(sum(b."Hours"), 0) 
      end as "Reg Hours",
      case 
        when coalesce(sum(b."Hours"), 0) >= 40 then a."Hours"
        when coalesce(sum(b."Hours"), 0) + a."Hours" < 40 then 0
        else a."Hours" - (40 - coalesce(sum(b."Hours"), 0)) 
      end as "OT Hours", 0 as "DailySalary"
   from 
      ( -- a
        select yico# as "Company", yiemp# as "EmpNum", week(yiclkind) as "Week", yiclkind as "Day", 
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin c
          left join rydedata.pymast p on p.ymempn = c.yiemp#
        where trim(yicode) = 'O' and ymclas = 'H' and yiclkind >= '12/26/2010' and yiclkind <= '12/31/2011'
        group by yico#, yiemp#, yiclkind) as a
    left join 
      ( -- b
        select yico# as "Company", yiemp# as "EmpNum", week(yiclkind) as "Week", yiclkind as "Day",
          sum(round(timestampdiff(2, cast(timestamp(yiclkoutd, yiclkoutt) - 
            timestamp(yiclkind, yiclkint) as char(22))) / 3600.0, 2)) as "Hours"
        from rydedata.pypclockin c
          left join rydedata.pymast p on p.ymempn = c.yiemp#
        where trim(yicode) = 'O' and ymclas = 'H' and yiclkind >= '12/26/2010' and yiclkind <= '12/31/2011'
        group by yico#, yiemp#, yiclkind) as b on a."Company" = b."Company" 
          and a."EmpNum" = b."EmpNum" and week(a."Day") = week(b."Day") and b."Day" < a."Day"
    group by a."Week", a."Company", a."EmpNum", a."Day", a."Hours"
    order by a."Week", a."Company", a."EmpNum", a."Day") as z on DayTable."TheDate" = z."Day"

union all

(select xy."TheDate", xy."DayType", xx."Company", xx."EmpNum", week(xy."TheDate"), xy."TheDate", 0, 0, 0, 0, 0, case when xy."DayType" = 'Weekday' then xx."Daily Salary" else 0 end as "Daily Salary" from  (select distinct yiclkind as "TheDate", case when dayofweek(yiclkind) in (2,3,4,5,6) then 'Weekday' else 'Weekend' end as "DayType"
   from rydedata.pypclockin
   where yiclkind >= '12/26/2010' and yiclkind <= '12/31/2011') as xy,
      (select ymco# as "Company", ymempn as "EmpNum", ymsaly / 10 as "Daily Salary" from pymast where ymempn in (select ymempn from pymast where ymname like 'LONGOR%')) as xx)


left join rydedata.pymast py on py.ymco# = z."Company" 
  and py.ymempn = z."EmpNum"
/* Calculated pay between two dates for a line on the financial statement */
where z."Day" >= '8/1/2011' and z."Day" <= '8/31/2011'
  and z."EmpNum" in (select ymempn 
                     from pymast
                     where ymdist in (select ytadic  
                                      from rydedata.pyactgr 
                                      where ytaco# = 'RY1'
                                        and ytagpa in (select fxgact
                                                       from rydedata.ffpxrefdta 
                                                       where fxcyy = 2011
                                                         and fxconsol = ''
                                                         and fxfact like '021D%')))

group by z."EmpNum", ymname, ymdist, ymrate, ymclas
order by ymname





  select * from (select distinct yiclkind as "TheDate", case when dayofweek(yiclkind) in (2,3,4,5,6) then 'Weekday' else 'Weekend' end as "DayType"
   from rydedata.pypclockin
   where yiclkind >= '12/26/2010' and yiclkind <= '12/31/2010') as DayTable, (select ymempn, ymsaly / 10 from pymast where ymempn in (select ymempn from pymast where ymname like 'LONGOR%')) as Emp







