select count(*) -- 1635
from rydedata.pdpphdr

select count(distinct trim(ptdoc#)) -- 875
from rydedata.pdpphdr

select *
from rydedata.pdpphdr


select distinct ptco#
from rydedata.pdpphdr

select ptpkey
from rydedata.pdpphdr
group by ptpkey
  having count(*) > 1

select ptdoc#, count(*) 
from rydedata.pdpphdr
-- where ptdoc# = ''
group by ptdoc#
--  having count(*) > 1
order by count(*) desc


select *
from rydedata.pdpphdr
where ptdoc# = ''

-- so, for now, ptpkey is unique

select distinct pttmin from rydedata.pdpphdr

select MIN(PTPKEY), MAX(PTPKEY) FROM rydedata.pdpphdr            
select MIN(PTDATE), MAX(PTDATE) FROM rydedata.pdpphdr            
select MIN(PTCKEY), MAX(PTCKEY) FROM rydedata.pdpphdr            
select MIN(PTPHON), MAX(PTPHON) FROM rydedata.pdpphdr            
select MIN(PTSKEY), MAX(PTSKEY) FROM rydedata.pdpphdr            
select MIN(PTPLVL), MAX(PTPLVL) FROM rydedata.pdpphdr            
select MIN(PTPTOT), MAX(PTPTOT) FROM rydedata.pdpphdr            
select MIN(PTSHPT), MAX(PTSHPT) FROM rydedata.pdpphdr            
select MIN(PTSPOD), MAX(PTSPOD) FROM rydedata.pdpphdr            
select MIN(PTSPDC), MAX(PTSPDC) FROM rydedata.pdpphdr            
select MIN(PTSPDP), MAX(PTSPDP) FROM rydedata.pdpphdr            
select MIN(PTSTAX), MAX(PTSTAX) FROM rydedata.pdpphdr            
select MIN(PTSTAX2), MAX(PTSTAX2) FROM rydedata.pdpphdr          
select MIN(PTSTAX3), MAX(PTSTAX3) FROM rydedata.pdpphdr          
select MIN(PTSTAX4), MAX(PTSTAX4) FROM rydedata.pdpphdr          
select MIN(PTREC#), MAX(PTREC#) FROM rydedata.pdpphdr            
select MIN(PTTEST), MAX(PTTEST) FROM rydedata.pdpphdr            
select MIN(PTSVCO), MAX(PTSVCO) FROM rydedata.pdpphdr            
select MIN(PTSVCO2), MAX(PTSVCO2) FROM rydedata.pdpphdr          
select MIN(PTDEDA), MAX(PTDEDA) FROM rydedata.pdpphdr            
select MIN(PTDEDA2), MAX(PTDEDA2) FROM rydedata.pdpphdr          
select MIN(PTWDED), MAX(PTWDED) FROM rydedata.pdpphdr            
select MIN(PTODOM), MAX(PTODOM) FROM rydedata.pdpphdr            
select MIN(PTMILO), MAX(PTMILO) FROM rydedata.pdpphdr            
select MIN(PTTMIN), MAX(PTTMIN) FROM rydedata.pdpphdr            
select MIN(PTPDTM), MAX(PTPDTM) FROM rydedata.pdpphdr            
select MIN(PTRTIM), MAX(PTRTIM) FROM rydedata.pdpphdr            
select MIN(PTLTOT), MAX(PTLTOT) FROM rydedata.pdpphdr            
select MIN(PTSTOT), MAX(PTSTOT) FROM rydedata.pdpphdr            
select MIN(PTDEDP), MAX(PTDEDP) FROM rydedata.pdpphdr            
select MIN(PTCPHZ), MAX(PTCPHZ) FROM rydedata.pdpphdr            
select MIN(PTCPST), MAX(PTCPST) FROM rydedata.pdpphdr            
select MIN(PTCPST2), MAX(PTCPST2) FROM rydedata.pdpphdr          
select MIN(PTCPST3), MAX(PTCPST3) FROM rydedata.pdpphdr          
select MIN(PTCPST4), MAX(PTCPST4) FROM rydedata.pdpphdr          
select MIN(PTWAST), MAX(PTWAST) FROM rydedata.pdpphdr            
select MIN(PTWAST2), MAX(PTWAST2) FROM rydedata.pdpphdr          
select MIN(PTWAST3), MAX(PTWAST3) FROM rydedata.pdpphdr          
select MIN(PTWAST4), MAX(PTWAST4) FROM rydedata.pdpphdr          
select MIN(PTINST), MAX(PTINST) FROM rydedata.pdpphdr            
select MIN(PTINST2), MAX(PTINST2) FROM rydedata.pdpphdr          
select MIN(PTINST3), MAX(PTINST3) FROM rydedata.pdpphdr          
select MIN(PTINST4), MAX(PTINST4) FROM rydedata.pdpphdr          
select MIN(PTSCST), MAX(PTSCST) FROM rydedata.pdpphdr            
select MIN(PTSCST2), MAX(PTSCST2) FROM rydedata.pdpphdr          
select MIN(PTSCST3), MAX(PTSCST3) FROM rydedata.pdpphdr          
select MIN(PTSCST4), MAX(PTSCST4) FROM rydedata.pdpphdr          
select MIN(PTCPSS), MAX(PTCPSS) FROM rydedata.pdpphdr            
select MIN(PTWASS), MAX(PTWASS) FROM rydedata.pdpphdr            
select MIN(PTINSS), MAX(PTINSS) FROM rydedata.pdpphdr            
select MIN(PTSCSS), MAX(PTSCSS) FROM rydedata.pdpphdr            
select MIN(PTHCPN), MAX(PTHCPN) FROM rydedata.pdpphdr            
select MIN(PTHDSC), MAX(PTHDSC) FROM rydedata.pdpphdr            
select MIN(PTTCDC), MAX(PTTCDC) FROM rydedata.pdpphdr            
select MIN(PTPBMF), MAX(PTPBMF) FROM rydedata.pdpphdr            
select MIN(PTPBDL), MAX(PTPBDL) FROM rydedata.pdpphdr            
select MIN(PTCPTD), MAX(PTCPTD) FROM rydedata.pdpphdr            
select MIN(PTDTAR), MAX(PTDTAR) FROM rydedata.pdpphdr            
select MIN(PTARCK), MAX(PTARCK) FROM rydedata.pdpphdr            
select MIN(PTWRO#), MAX(PTWRO#) FROM rydedata.pdpphdr            
select MIN(PTAPDT), MAX(PTAPDT) FROM rydedata.pdpphdr            
select MIN(PTDTLC), MAX(PTDTLC) FROM rydedata.pdpphdr            
select MIN(PTDTPI), MAX(PTDTPI) FROM rydedata.pdpphdr            
select MIN(PTRNT#), MAX(PTRNT#) FROM rydedata.pdpphdr            
select MIN(PTCREATE), MAX(PTCREATE) FROM rydedata.pdpphdr        
select MIN(PTCTHOLD), MAX(PTCTHOLD) FROM rydedata.pdpphdr        
select MIN(PTDLVMTH), MAX(PTDLVMTH) FROM rydedata.pdpphdr        
select MIN(PTSHIPSEQ), MAX(PTSHIPSEQ) FROM rydedata.pdpphdr      
select MIN(PTTAXGO), MAX(PTTAXGO) FROM rydedata.pdpphdr          


-- query for code
select
  PTCO#, PTPKEY, PTCPID, PTSTAT, PTDTYP, PTTTYP, 
  cast(left(digits(PTDATE), 4) || '-' || substr(digits(PTDATE), 5, 2) || '-' || substr(digits(PTDATE), 7, 2) as date) as PTDATE,
  PTDOC#, 
  PTCUS#, PTCKEY, PTCNAM, PTPHON, PTSKEY, PTPMTH, PTSTYP, PTPLVL,
  PTTAXE, PTPTOT, PTSHPT, PTSPOD, PTSPDC, PTSPDP, PTSPDO, PTSPOH,
  PTSTAX, PTSTAX2, PTSTAX3, PTSTAX4, PTCPNM, PTSRTK, PTACTP, PTCCAR
  PTPO#, PTREC#, PTODOC, PTINTA, PTRTYP, PTRSTS, PTWARO, PTPAPRV,
  PTSAPRV, PTSWID, PTRTCH, PTTEST, PTSVCO, PTSVCO2, PTDEDA, PTDEDA2
  PTWDED, PTVIN, PTSTK#, PTTRCK, PTFRAN, PTODOM, PTMILO, PTRTIM,  
  PTTAG#, PTCHK#, PTHCMT, PTSSOR, PTHMOR, PTLTOT, PTSTOT, PTDEDP,
  PTCPHZ, PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, 
  PTWAST3, PTWAST4, PTINST, PTINST2, PTINST3, PTINST4, PTSCST,
  PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, PTINSS, PTSCSS, 
  PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTCPTD, PTOSTS, 
  case 
    when PTDTAR = 0 then cast('9999-12-31' as date)
    else cast(left(digits(PTDTAR), 4) || '-' || substr(digits(PTDTAR), 5, 2) || '-' || substr(digits(PTDTAR), 7, 2) as date) 
  end as PTDTAR, 
  PTARCK, PTWRO#, PTWSID, PTAUTH, PTAPDT, PTDTLC, PTDTPI, PTIPTY, 
  PTPRTY, PTRNT#, PTTIIN, 
  case 
    when cast(PTCREATE as date) = '0001-01-01' then cast('9999-12-31 00:00:00' as timestamp)
    else PTCREATE 
  end as PTCREATE, 
  case 
    when cast(PTCTHOLD as date) = '0001-01-01' then cast('9999-12-31 00:00:00' as timestamp)
    else PTCTHOLD 
  end as PTCTHOLD, 
  PTAUTH#, PTAUTHID,  
  PTDLVMTH, PTSHIPSEQ, PTDSPTEAM, PTTAXGO
from rydedata.pdpphdr



