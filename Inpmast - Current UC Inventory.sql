Select trim(imvin) as VIN, trim(imstk#) as StockNumber, CHAR(imyear) as Year, immake as Make, 
  immcode as ModelCode, immodl as Model, imbody as Body, imcolr as Color, 
  case imdinv
    when 0 then null
    else DATE(INSERT(INSERT(DIGITS(imdinv),5,0,'-'),8,0,'-')) 
   end as imdinv, 
  case imddlv
    when 0 then null
    else DATE(INSERT(INSERT(DIGITS(imddlv),5,0,'-'),8,0,'-')) 
   end as imddlv, imcost, imstat
from INPMAST i
where imtype = 'U'
and imstk# <> ''
and imstat = 'I'
order by imdinv desc