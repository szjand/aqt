-- where we will point to for information about any fact

select gtdtyp, gtjrnl, gtdoc#, gtctl#, 
  case 
    when gtjrnl in ('SVI', 'SCA', 'SWA') then ptcnam 
    when gtjrnl in ('VSU', 'VSN') then bmsnam 
    when gtjrnl in ('PVI', 'PVU') then imvin 
    when gtjrnl in ('PCA', 'PCC') then ptsnam
    when gtjrnl in ('PAY') then ymname
    when gtjrnl in ('CRC') then gtdesc
    when gtjrnl in ('POT') then coalesce(guname, gtdesc)
    when gtjrnl in ('CDH') then gtdesc
    when gtjrnl in ('CDP') then gtdesc
    when gtjrnl in ('CRD') then gtdesc
    when gtjrnl in ('BTD') then gtdesc
    when gtjrnl in ('BTR') then gtdesc
    when gtjrnl in ('WTD') then gtdesc
    when gtjrnl in ('DRI') then gtdesc
    when gtjrnl in ('SJE') then gtdesc
    when gtjrnl in ('GJE') then gtdesc
    else 'WTF'
  end as Desc,
  case
    when gtjrnl in ('CDP') then gcsnam
    else ''
  end as "Additional Info"
from rydedata.glptrns
-- per greg each of these joins is a dimension
left join rydedata.sdprhdr on ptro# = gtdoc# and gtjrnl in ('SVI', 'SCA', 'SWA') and gtdtyp <> 'J' -- ro
left join rydedata.bopmast on bmstk# = gtdoc# and gtjrnl in ('VSU', 'VSN') and gtdtyp <> 'J' -- deal
left join rydedata.inpmast on imdoc# = gtdoc# and gtjrnl in ('PVI', 'PVU') and gtdtyp <> 'J' -- vehicle
left join rydedata.pdpthdr on ptinv# = gtdoc# and gtjrnl in ('PCA', 'PCC') and gtdtyp <> 'J' -- parts invoice
left join rydedata.pyhshdta on yhdref = gtdoc# and gtjrnl in ('PAY') and gtdtyp <> 'J' -- payroll/paycheck
left join rydedata.glppohd on gupo# = gtdoc# and gtjrnl in ('POT') and gtdtyp <> 'J' -- purchase order
left join rydedata.glpcust on gcvnd# = gtctl# and gtjrnl in ('CDP') and gtdtyp <> 'J' -- vendor
where gtoco# = 'RY1'
  and gtdate >= '2/1/2012'
