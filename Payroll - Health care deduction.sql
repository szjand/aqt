/*
don't forget: 
  where ypbcyy = '111' -- payroll cen + year

1/21/13 kim wants the name of anyone employed during 2012 and how much health care (107) they paid
*/
select ymco# as Store, ymname as Name,  b.BirthDate, 
  year(now()) - year(b.birthdate) -
    case 
      when
        month(b.BirthDate)*100 + day(b.BirthDate) > month(CurDate())*100 + day(CurDate()) then 1
      else 0
    end as Age,
  ymsex as Gender, 
  ymsaly as Salary,
  coalesce(d.yddamt, 0) as "HEALTH CARE", g.gross
from rydedata.pymast p
left join ( -- generate birth date
  select ymempn, 
    case 
      when cast(right(trim(ymbdte),2) as integer) < 20 then 
        cast (
          case length(trim(ymbdte))
            when 5 then  '20'||substr(trim(ymbdte),4,2)||'-'|| '0' || left(trim(ymbdte),1) || '-' ||substr(trim(ymbdte),2,2)
            when 6 then  '20'||substr(trim(ymbdte),5,2)||'-'|| left(trim(ymbdte),2) || '-' ||substr(trim(ymbdte),3,2)
          end as date) 
      else  
        cast (
          case length(trim(ymbdte))
            when 5 then  '19'||substr(trim(ymbdte),4,2)||'-'|| '0' || left(trim(ymbdte),1) || '-' ||substr(trim(ymbdte),2,2)
            when 6 then  '19'||substr(trim(ymbdte),5,2)||'-'|| left(trim(ymbdte),2) || '-' ||substr(trim(ymbdte),3,2)
          end as date) 
      end as BirthDate
  from rydedata.pymast
  where ymactv <> 'T'
  and length(trim(ymname)) > 4) b on p.ymempn = b.ymempn
left join rydedata.pydeduct d on p.ymempn = d.ydempn and yddcde = '107'

inner join ( -- gross wages to date from Payroll - Mutual of Omaha 2010 totals
  select yhdemp, 
    sum(yhacm) as Gross -- 8 Curr Adj Comp
  from rydedata.pyhshdta -- Payroll Header transaction file
  where ypbcyy = '112' -- payroll cen + year
  group by yhdemp) g on p.ymempn = g.yhdemp

where p.ymactv <> 'T'
and length(trim(ymname)) > 4 -- eliminate junk data
order by ymco#, ymname



-- 1/21/13 kim wants the name of anyone employed during 2012 and how much health care (107) they paid for the year

select ymco# as Store, ymname as Name, -- b.hiredate, b.termdate,
  sum(coalesce(c.yhccam, 0)) as "HEALTH CARE"
-- select *
from rydedata.pymast p
left join ( -- generate hire date, termdate
  select ymempn, 
    case 
      when cast(right(trim(ymhdte),2) as integer) < 20 then 
        cast (
          case length(trim(ymhdte))
            when 5 then  '20'||substr(trim(ymhdte),4,2)||'-'|| '0' || left(trim(ymhdte),1) || '-' ||substr(trim(ymhdte),2,2)
            when 6 then  '20'||substr(trim(ymhdte),5,2)||'-'|| left(trim(ymhdte),2) || '-' ||substr(trim(ymhdte),3,2)
          end as date) 
      else  
        cast (
          case length(trim(ymhdte))
            when 5 then  '19'||substr(trim(ymhdte),4,2)||'-'|| '0' || left(trim(ymhdte),1) || '-' ||substr(trim(ymhdte),2,2)
            when 6 then  '19'||substr(trim(ymhdte),5,2)||'-'|| left(trim(ymhdte),2) || '-' ||substr(trim(ymhdte),3,2)
          end as date) 
      end as HireDate,
    coalesce(
    case 
      when cast(right(trim(ymtdte),2) as integer) < 20 then 
        cast (
          case length(trim(ymtdte))
            when 5 then  '20'||substr(trim(ymtdte),4,2)||'-'|| '0' || left(trim(ymtdte),1) || '-' ||substr(trim(ymtdte),2,2)
            when 6 then  '20'||substr(trim(ymtdte),5,2)||'-'|| left(trim(ymtdte),2) || '-' ||substr(trim(ymtdte),3,2)
          end as date) 
      else  
        cast (
          case length(trim(ymbdte))
            when 5 then  '19'||substr(trim(ymtdte),4,2)||'-'|| '0' || left(trim(ymtdte),1) || '-' ||substr(trim(ymtdte),2,2)
            when 6 then  '19'||substr(trim(ymtdte),5,2)||'-'|| left(trim(ymtdte),2) || '-' ||substr(trim(ymtdte),3,2)
          end as date) 
      end, cast('9999-12-31' as date)) as TermDate
  from rydedata.pymast 
  --where ymactv <> 'T'
  where length(trim(ymname)) > 4) b on trim(p.ymempn) = trim(b.ymempn)
left join rydedata.pyhscdta c on trim(p.ymempn) = trim(c.yhcemp)
  and c.ypbcyy = 112
  and c.yhccde = '107'
WHERE length(trim(ymname)) > 4 -- eliminate junk data
  and hiredate < '12/31/2012'
  and termdate > '12/31/2011'
group by ymco#, ymname 
order by ymco#, ymname

