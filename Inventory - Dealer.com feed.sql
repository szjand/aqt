/*
the purpose is to simulate the arkona extract/scheduled job
*/

/* from greg - optional fields
select imvin as VIN, trim(imstk#) as StockNum, imyear as Year, immake as Make, immodl as Model, 
  imcost as Net, impric as Price,
  coalesce((select sum(gttamt) from glptrns where gtctl# = inp.imstk# and gtjrnl in ('SVI', 'POT') and gtdtyp <> 'J'), 0) as Recon
from inpmast inp where imstat = 'I'
and imtype = 'U' 
and imcost > 0 
order by imstk#;


select imvin as VIN, trim(imstk#) as StockNum, imyear as Year, immake as Make, immodl as Model, imcost as Net, 
  coalesce((select ionval from inpoptf f inner join inpoptd d on f.ioco# = d.idco# and f.iovin = inp.imvin and f.ioseq# = d.idseq# and iddesc = 'Internet Price'), 0) as InternetPrice,
  coalesce((select ionval from inpoptf f inner join inpoptd d on f.ioco# = d.idco# and f.iovin = inp.imvin and f.ioseq# = d.idseq# and iddesc = 'Best Price'), '0') as BestPrice,
  coalesce((select sum(gttamt) from glptrns where gtctl# = inp.imstk# and gtjrnl in ('SVI', 'POT') and gtdtyp <> 'J'), 0) as Recon 
from inpmast inp 
where imstat = 'I' 
and imtype = 'N' 
and imcost > 0 
order by imstk# desc


select ionval 
from inpoptf f 
inner join inpoptd d on f.ioco# = d.idco# 
--  and f.iovin = inp.imvin 
  and f.ioseq# = d.idseq# 
  and iddesc = 'Internet Price'
*/

/*
advantages for dealer.com to change
1. no superfluous characters in stocknumber, best price, price, cost
2. new vehicles only
3. only agreed upon relevant fields

*/

select 'RY1' as Company, inp.imvin as VIN,  '# ' || trim(inp.imstk#) as "Stock Number", inp.imtype as Type,
  inp.imvcod as "Vehicle Code", inp.imyear as "Year", inp.immake as Make, inp.immodl as Model,
  inp.immcode as "Model Code", inp.imbody as Body, inp.imcolr as Color, inp.imccode as "Color Code",
  inp.imtrim as Trim, inp.imfuel as Fuel, inp.immpg as MPG, inp.imcyl as Cylinders, inp.imtrkc as Truck,
  inp.im4wd as "4WD", inp.imturb as Turbo, inp.imecode as Engine, inp.imtcode as Transmission,
  inp.imodom as Odometer, inp.imlocn as Location, inp.imdinv as "Date in Inventory", '' as "Warranty Months",
  '' as "Warranty Miles", inp.impric as Price, inp.imcost as Cost, 0 as "Work-in-Process", 
  '' as Options, '' as "License #"
  
from inpmast inp
--where inp.imvin = '1G4GA5EC4BF212729'
where inp.imstat = 'I'
and inp.imtype = 'N'

select * from inpmast where imvin = '1G4HP52K24U122251'

select distinct ivoptcost from inpvoptd

/*******************************************************************************************************************/

-- 7/11/11
-- attempting to configure a manual new car feed for dealertrack
-- this is the basic new inventory
SELECT trim(IMSTK#) as StockNumber, IMPRIC as ListPrice, IMYear as Yr, IMMake as Make, 
  IMMODL as Model, IMFUEL, IMTRCK
-- select count(*)
FROM INPMAST i
inner join (
  select gtctl#, sum(gttamt)as net
  from rydedata.glptrns
  where trim(gtacct) in (-- Inventory accounts only
    '123100', -- Chev
    '123101', -- Cad
    '123105', -- Buick
    '123700', -- Chev Lt Trk
    '123701', -- Cad Trk
    '123705', -- Buick Trk
    '123706', -- GMC Trk
    '223000', -- Honda Trk
    '223100', -- Honda
    '223105', -- Nissan
    '223700', -- Nissan Trk
    '323102', -- Buick
    '323702', -- Buick Trk
    '323703') -- GMC Trk) 
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by gtctl#
    having sum(coalesce(gttamt,0)) > 1) ia on trim(i.imstk#) = trim(ia.gtctl#)
WHERE IMSTAT = 'I'
and IMType = 'N'
and left(trim(imstk#),1) <> 'H'
order by immodl

-- based on what's currently in arkona, only silverados and sierras are being marked as trucks
-- (in the IMFUEL field)
select immodl, count(*)
FROM INPMAST i
inner join (
  select gtctl#, sum(gttamt)as net
  from rydedata.glptrns
  where trim(gtacct) in (-- Inventory accounts only
    '123100', -- Chev
    '123101', -- Cad
    '123105', -- Buick
    '123700', -- Chev Lt Trk
    '123701', -- Cad Trk
    '123705', -- Buick Trk
    '123706', -- GMC Trk
    '223000', -- Honda Trk
    '223100', -- Honda
    '223105', -- Nissan
    '223700', -- Nissan Trk
    '323102', -- Buick
    '323702', -- Buick Trk
    '323703') -- GMC Trk) 
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by gtctl#
    having sum(coalesce(gttamt,0)) > 1) ia on trim(i.imstk#) = trim(ia.gtctl#)
WHERE IMSTAT = 'I'
and IMType = 'N'
and left(trim(imstk#),1) <> 'H'
and length(trim(imfuel)) <> 0
group by immodl


-- with truck decoding
SELECT trim(imvin) as vin, trim(IMSTK#) as stocknumber, IMYear as year, IMMake as make, 
  IMMODL as model, imtrim as trimlevel, IMFUEL, IMTRCK, IMPRIC as MSRP,
  case
    when immodl like '%SIERRA%' then 'Y'
    when immodl like '%SILVERADO%' then 'Y'
    else 'N'
  end as truck,
  imigkc as transmission,
  coalesce((select ionval from inpoptf f inner join inpoptd d on f.ioco# = d.idco# and f.iovin = i.imvin and f.ioseq# = d.idseq# and iddesc = 'Internet Price'), 0) as "Best Price" -- opt field Internet Price
-- select count(*)
FROM INPMAST i
inner join (
  select gtctl#, sum(gttamt)as net
  from rydedata.glptrns
  where trim(gtacct) in (-- Inventory accounts only
    '123100', -- Chev
    '123101', -- Cad
    '123105', -- Buick
    '123700', -- Chev Lt Trk
    '123701', -- Cad Trk
    '123705', -- Buick Trk
    '123706', -- GMC Trk
    '223000', -- Honda Trk
    '223100', -- Honda
    '223105', -- Nissan
    '223700', -- Nissan Trk
    '323102', -- Buick
    '323702', -- Buick Trk
    '323703') -- GMC Trk) 
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by gtctl#
    having sum(coalesce(gttamt,0)) > 1) ia on trim(i.imstk#) = trim(ia.gtctl#)
WHERE IMSTAT = 'I'
and IMType = 'N'
--and left(trim(imstk#),1) <> 'H'
order by immodl

-- trimlevel doesn't come from inpmast
select * from inpmast where trim(imstk#) = '11819'

select * from inpmast where trim(imstk#) = 'C4462'

