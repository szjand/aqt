Select trim(a.gtctl#) as stock_number, a.gttamt as amount, b.inventory_account, trim(gtacct) as account
from RYDEDATA.GLPTRNS a
left join rydedata.inpmast b on trim(a.gtctl#) = trim(b.inpmast_Stock_number)
where a.gtdate between '06/01/2019' and '06/30/2019'
and trim(a.gtacct) in ('21301D','21310D','21302C')  
  and b.status = 'I'
  and a.gtpost = 'Y'