select ymco#, ymname, ymempn, ymss# as "Goofy Social" 
from RYDEDATA.pymast where ymss# in (
  Select ymss# 
  from RYDEDATA.PYMAST 
  group by ymss#
    having count(*) > 1)
order by ymss#