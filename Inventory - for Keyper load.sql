
-- Stock numer, Year, make , model, ext color, int color, vin, preferably in excel
-- revised slightly 10/21/11 for RY1 import
SELECT trim(IMSTK#) as StockNumber, IMMake as Make, IMMODL as Model, IMYear as Year, 
  IMCOLR as "EXT COLOR", '*', IMTRIM as "INT COLOR", IMVIN as Vin 
-- select count(*)
FROM INPMAST i
inner join (
  select gtctl#, sum(gttamt)as net
  from rydedata.glptrns
  where trim(gtacct) in (-- Inventory accounts only
    '123100', -- Chev
    '123101', -- Cad
    '123105', -- Buick
    '123700', -- Chev Lt Trk
    '123701', -- Cad Trk
    '123705', -- Buick Trk
    '123706', -- GMC Trk
    '223000', -- Honda Trk
    '223100', -- Honda
    '223105', -- Nissan
    '223700', -- Nissan Trk
    '323102', -- Buick
    '323702', -- Buick Trk
    '323703', -- GMC Trk 
    '124000', -- Used  
    '124100',
    '224000',
    '224100',
    '324000',
    '324100') 
  and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by gtctl#
    having sum(coalesce(gttamt,0)) > 1) ia on trim(i.imstk#) = trim(ia.gtctl#)
WHERE IMSTAT = 'I'
and IMType IN ('N', 'U')
and SUBSTRING(TRIM(IMSTK#), 1, 1) not in  ('H', 'C')


--select imtype, count(*) from inpmast group by imtype