Select iemodl, count(*) 
from RYDEDATA.INPVIN
group by iemodl
order by iemodl

select distinct length(trim(ievkey))
from inpvin

-- 484 ANY models with no corresponding records in INPVIN
select count(*)
from inpmast i
where immodl like '%ANY%'
and immodl not like '%CANYON%'


-- 470 ANY models with no corresponding records in INPVIN
select left(trim(i.imvin),8), i.immake, i.immodl, i.imstat, v.*
-- select count(*)
from inpmast i
left join inpvin v on trim(v.ievkey) = left(trim(i.imvin),9)
where immodl like '%ANY%'
and immodl not like '%CANYON%'
and v.ievkey is not null
order by left(trim(i.imvin),8)


select '''' || left(trim(i.imvin),8) || '''' || ',' as vin
from inpmast i
left join inpvin v on trim(v.ievkey) = left(trim(i.imvin),9)
where immodl like '%ANY%'
and immodl not like '%CANYON%'
and v.ievkey is null
order by vin


select left(trim(i.imvin),8), i.immake
from inpmast i
left join inpvin v on trim(v.ievkey) = left(trim(i.imvin),9)
where immodl like '%ANY%'
and immodl not like '%CANYON%'
and v.ievkey is null
order by left(trim(i.imvin),8)


select *
from inpvin
where trim(ievkey) = '1G6KD57Y8'