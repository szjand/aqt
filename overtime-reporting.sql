select account_number, department, account_desc
from rydedata.glpmast
where year = 2018
and trim(account_number) in (
Select trim(gross_expense_act_) as account from RYDEDATA.PYACTGR
union
Select trim(overtime_act_) from RYDEDATA.PYACTGR
union
Select trim(vacation_expense_act_) from RYDEDATA.PYACTGR
union
Select trim(holiday_Expense_act_) from RYDEDATA.PYACTGR
union
Select trim(sick_leave_expense_act_) from RYDEDATA.PYACTGR
union
Select trim(retire_expense_act_) from RYDEDATA.PYACTGR
union
Select trim(emplr_fica_expense) from RYDEDATA.PYACTGR
union
Select trim(emplr_med_expense) from RYDEDATA.PYACTGR
union
Select trim(federal_un_emp_act_) from RYDEDATA.PYACTGR
union
Select trim(state_unemp_act_) from RYDEDATA.PYACTGR)


select a.company_number, a.dist_code, a.seq_number, a.description, 
  a.gross_dist, b.account_number, b.department, b.account_desc
from rydedata.pyactgr a
left join rydedata.glpmast b on trim(a.gross_expense_act_) = b.account_number
  and b.year = 2018
where a.company_number in ('RY1','RY2')
order by b.department, a.dist_code, a.seq_number


select a.company_number, a.dist_code, a.seq_number, a.description, a.gross_dist, 
  b.employee_name, b.pymast_employee_number, b.department_code,
  c.account_number, c.department, c.account_desc  
from rydedata.pyactgr a
left join rydedata.pymast b on a.dist_code = b.distrib_code
  and b.active_code <> 'T'
left join rydedata.glpmast c on trim(a.gross_expense_act_) = c.account_number
  and c.year = 2018
where a.company_number in ('RY1','RY2')


