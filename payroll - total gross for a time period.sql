so, for these folks, what was the total gross pay from 12/1/12 thru 11/30/13
-- here is basic total gross for each employee with real dates
select a.ybco#, a.ypbcyy, a.payroll_run_number, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp,
  cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
  cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
from rydedata.pyptbdta a
inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.payroll_run_number = b.ypbnum

select distinct startdate,enddate
from (
select a.ybco#, a.ypbcyy, a.payroll_run_number, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp,
  cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
  cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
from rydedata.pyptbdta a
inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.payroll_run_number = b.ypbnum
where a.payroll_cen_year = 116) a


select ymname as "name", sum(totalgross) as "total gross"
from (-- here is basic total gross for each employee with real dates
  select a.ybco#, a.ypbcyy, a.payroll_run_number, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp,
    cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
    cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
  cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
  from rydedata.pyptbdta a
  inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.payroll_run_number = b.ypbnum) a
inner join (
  select a.ymempn, a.ymname
  from rydedata.pymast a
  where ymdist = 'SALE'
    and ymco# = 'RY1'
    and ymactv <> 'T'
    and ymname <> 'ADRIAN, JOEL S'
    and ymname not like 'DALEN%'
    and ymname not like 'KACH%'
  and ymname not like 'DULAN%'
  and ymname not like 'PEARSON, ROB%'
  and ymname not like 'ROEM%'
  and ymname not like 'KUHN%'
  and ymname not like 'HOGEN%') b on trim(a.yhdemp) = trim(b.ymempn)
where a.checkdate between '12/01/2012' and '11/30/2013'
group by ymname


-- 1/28, for mike writers and bev, total gross 2012, 2013

select ymname as "name", 
  sum(case when year(checkdate) = 2012 then totalgross end) as " 2012 total gross",
  sum(case when year(checkdate) = 2013 then totalgross end) as " 2013 total gross"
from (-- here is basic total gross for each employee with real dates
  select a.ybco#, a.ypbcyy, a.payroll_run_number, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp,
    cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
    cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
  cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
  from rydedata.pyptbdta a
  inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.payroll_run_number = b.ypbnum) a
inner join (
  select a.ymempn, a.ymname
  from rydedata.pymast a
  where trim(ymempn) in ('121362','123525','140790','1140870','1142827','187675')) b on trim(a.yhdemp) = trim(b.ymempn)
where a.checkdate between '01/01/2012' and '12/31/2013'
group by ymname


-- 6/5/ cahalan needs team pay techs, total gros 1/1/13 -> 5/31/13 and 1/1/14 -> 5/31/14

-- here is basic total gross for each employee with real dates
select 2013, yhdemp, sum(totalgross) 
from (
  select a.ybco#, a.ypbcyy, a.payroll_run_number, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp,
    cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
    cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
  cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
  from rydedata.pyptbdta a
  inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.payroll_run_number = b.ypbnum
  where trim(yhdemp) in (
  '116185',
  '131370',
  '136170',
  '152235',
  '152410',
  '161120',
  '164015',
  '167580',
  '179750',
  '194675',
  '1106399',
  '1107950',
  '1124436',
  '1133500',
  '1134850',
  '1135410',
  '1137590',
  '1146989',
  '1149500')) g
where checkdate between '01/01/2013' and '05/31/2013'
group by yhdemp


-- 12/31/14 mark wants the same thing for body shop techs, 2013 vs 2014


-- 6/5/ cahalan needs team pay techs, total gros 1/1/13 -> 5/31/13 and 1/1/14 -> 5/31/14

-- here is basic total gross for each employee with real dates
select 2013, g.yhdemp, g.employee_name, sum(totalgross) 
from (
  select a.ybco#, a.ypbcyy, a.payroll_run_number, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp, c.employee_name,
    cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
    cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
  cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
  from rydedata.pyptbdta a
  inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.payroll_run_number = b.ypbnum
  inner join rydedata.pymast c on a.ybco# = c.pymast_company_number 
    and trim(b.yhdemp) = trim(c.pymast_employee_number)
    and c.department_code = '05'
    and c.distrib_code = 'BTEC') g
where checkdate between '01/01/2013' and '12/31/2013'
group by g.yhdemp, g.employee_name

select 2014, g.yhdemp, g.employee_name, sum(totalgross) 
from (
  select a.ybco#, a.ypbcyy, a.payroll_run_number, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp, c.employee_name,
    cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
    cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
  cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
  from rydedata.pyptbdta a
  inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.payroll_run_number = b.ypbnum
  inner join rydedata.pymast c on a.ybco# = c.pymast_company_number 
    and trim(b.yhdemp) = trim(c.pymast_employee_number)
    and c.department_code = '05'
    and c.distrib_code = 'BTEC') g
where checkdate between '01/01/2014' and '12/31/2014'
group by g.yhdemp, g.employee_name

-- using employeenumbers generated from scotest
select 2014, g.yhdemp, sum(totalgross) 
from (
  select a.ybco#, a.ypbcyy, a.payroll_run_number, yhdtgp AS TotalGross, trim(yhdemp) as yhdemp, c.employee_name,
    cast(left(digits(ybstart), 4) || '-' || substr(digits(ybstart), 5, 2) || '-' || substr(digits(ybstart), 7, 2) as date) as startDate,
    cast(left(digits(ybend), 4) || '-' || substr(digits(ybend), 5, 2) || '-' || substr(digits(ybend), 7, 2) as date) as endDate,
  cast(left(digits(ybchkd), 4) || '-' || substr(digits(ybchkd), 5, 2) || '-' || substr(digits(ybchkd), 7, 2) as date) as checkDate
  from rydedata.pyptbdta a
  inner join rydedata.pyhshdta b on a.ybco# = b.yhdco# and a.ypbcyy = b.ypbcyy and a.payroll_run_number = b.ypbnum
  inner join rydedata.pymast c on a.ybco# = c.pymast_company_number 
    and trim(b.yhdemp) = trim(c.pymast_employee_number)
    and trim(c.pymast_employee_number) in(
      '135770',   
      '150105',   
      '171055',   
      '174130',   
      '191350',   
      '1106400',  
      '1109852',  
      '1110650',  
      '1114120',  
      '1118722',  
      '1118780',  
      '1125565',  
      '1126040',  
      '1146991',  
      '1147061',  
      '186210')) g
where checkdate between '01/01/2014' and '12/31/2014'
group by g.yhdemp

