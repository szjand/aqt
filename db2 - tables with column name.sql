select  *
from sysibm.columns
where table_schema = 'RYDEDATA'
  and (column_name = 'PART_NUMBER'
  or column_name like '%RIM%')
order by  table_name

select * 
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and column_name = 'PART_NUMBER'
order by  table_name

-- has column rim_controlled, does not appear to be used
select * 
from rydedata.part_Transaction_Detail
where part_number in ('13544900','12657093')

-- has column rim_controlled, does not appear to be used
select * 
from rydedata.pdltdex
where part_number in ('13544900','12657093')
order by transaction_date desc

-- this looks like it
select aa.type, aa.part_number, bb.*
from (
select 'rim' as type, a.*
from rydedata.pdpmast a
where part_number in ('12657093','12677002','12677004','12681993','19192673')
union
select 'non_rim' as type, b.*
from rydedata.pdpmast b
where part_number in ('13544900','12637060','19420236','22921944','23282834','22816982')) aa
left join rydedata.ry1rimsnd bb on aa.part_number = bb.part_number
order by aa.type, aa.part_number

select count(*) from rydedata.ry1rimsnd  -- 9839