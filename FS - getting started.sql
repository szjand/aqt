-- factory accounts
Select fxfact, count(*) 
from RYDEDATA.FFPXREFDTA
group by fxfact
order by fxfact


Select f.fxfact as "Fact Acct", f.fxgact as "GL Acct", count(*) 
from RYDEDATA.FFPXREFDTA f
where fxco# = 'RY1'
group by f.fxfact, f.fxgact
order by f.fxfact, f.fxgact

select gtacct, sum(gttamt) 
from rydedata.glptrns
where gtco# = 'RY1'
  and gtdate > curdate() - 7 days
group by gtacct
order by gtacct


select * from ffpxrefd26 order by fxfp01 desc

select distinct fxfp01 from ffpxrefd14


select distinct vpage, vline
from rydedata.ffpvtot
order by vpage, vline


select distinct fxcyy from ffpxrefdta

select *
from (
select trim(gtacct) as account, sum(gttamt) as amount
from rydedata.glptrns
where gtdate <= '01/31/2014'
  and length(trim(gtacct)) = 6
  and left(trim(gtacct), 2) = '12'
group by trim(gtacct)) a

select fxfact, sum(amount)
from (
select *
from (
select trim(gmacct) as account, gmat01 as amount
from rydedata.glpmast 
where gmyear = 2014 
  and gmtype in ('1','2','3')) a
left join rydedata.ffpxrefdta b on a.account = trim(b.fxgact)  
where b.fxconsol = ''
  and b.fxcyy = 2014
) x group by fxfact
order by fxfact

select *
from eisglobal.sypffxmst
where fxmcyy = 2015
  and trim(fxmcde)  = 'GM'
  and left(trim(fxmact), 1) <> '<'
  and left(trim(fxmact), 1) <> '&'
  and left(trim(fxmact), 1) <> '*'
  and fxmpge = 1
  and fxmlne in (26, 27)
  
select *
from (
  select trim(fxfact) as factAcct, amount
  from (
    select trim(gmacct) as account, gmat01 as amount
    from rydedata.glpmast 
    where gmyear = 2014 
      and gmtype in ('1','2','3')) a
  left join rydedata.ffpxrefdta b on a.account = trim(b.fxgact)  
  where b.fxconsol = ''
    and b.fxcyy = 2014) m
left join eisglobal.sypffxmst n on m.factAcct = trim(n.fxmact) 
where n.fxmcyy = 2014    
  and trim(n.fxmcde) = 'GM'
  
-- this query seems to match the statement !  
select fxmpge, fxmlne, fxmcol, sum(amount) as amount
from (
  select trim(fxfact) as factAcct, amount
  from (
    select trim(gmacct) as account, gmat01 as amount
    from rydedata.glpmast 
    where gmyear = 2014
      and gmstyp = 'A') a 
      --and gmtype in ('1','2','3')) a
  left join rydedata.ffpxrefdta b on a.account = trim(b.fxgact)  
  where b.fxconsol = ''
    and b.fxcyy = 2014) m
left join eisglobal.sypffxmst n on m.factAcct = trim(n.fxmact) 
where n.fxmcyy = 2014    
  and trim(n.fxmcde) = 'GM'  
group by fxmpge, fxmlne, fxmcol  
order by fxmpge, fxmlne, fxmcol  

select * from rydedata.glpmast where gmyear = 2014 and gmstyp = 'B'

select * from rydedata.glptrns
  
-- jan 2014 bal
    select sum(gmat01) as amount
    from rydedata.glpmast 
    where gmyear = 2014  
    and trim(gmtype) = '3'
    and gmstyp = 'A'
    
    group by gmacct
    order by gmacct
  
 select * 
    from rydedata.glpmast 
    where gmyear = 2014  
    and trim(gmtype) = '1'
    and gmstyp = 'A'  
    and trim(gmacct) = '120300'
  
  
  select *
  from rydedata.glptrns
  where abs(gttamt) between 10930 and 10970
  
  
  
  
select fxfact, sum(amount) as amount
from (
  select trim(fxfact) as fxfact,amount
  from (
    select trim(gmacct) as account, gmtype, gmat01 as amount
    from rydedata.glpmast 
    where gmyear = 2014
      and gmstyp = 'A') a 
  left join rydedata.ffpxrefdta b on a.account = trim(b.fxgact)  
  where b.fxconsol = ''
    and b.fxcyy = 2014) m
--left join eisglobal.sypffxmst n on m.factAcct = trim(n.fxmact) 
--where n.fxmcyy = 2014    
--  and trim(n.fxmcde) = 'GM'  
group by fxfact
order by fxfact

select * from rydedata.glpmast

select * from rydedata.ffpxrefdta

select gmtype, sum(gmat01) from rydedata.glpmast where gmstyp = 'A' and gmyear = 2014 group by gmtype

select gmtype, sum(gmat01)
from (
select gmtype, trim(gmacct) as gmaccct, gmat01, b.*
from rydedata.glpmast a
inner join rydedata.ffpxrefdta b on trim(a.gmacct) = trim(b.fxgact)
where a.gmstyp = 'A' 
  and a.gmyear = 2014 
  and b.fxcyy = 2014    
  and trim(b.fxcode) = 'GM') c
group by gmtype   


select *
from (
select gmtype, sum(gmat01) from rydedata.glpmast where gmstyp = 'A' and gmyear = 2014 group by gmtype) x
left join (

select gmtype, sum(gmat01)
from (
  select  gmtype, trim(gmacct) as gmacct, gmat01, b.*
  from rydedata.glpmast a
  inner join (
    select distinct fxgact
    from rydedata.ffpxrefdta
    where fxcyy = 2014
      and trim(fxcode) = 'GM') b on trim(a.gmacct) = trim(b.fxgact)
  where a.gmstyp = 'A' 
    and a.gmyear = 2014) c
    --and b.fxcyy = 2014    
    --and trim(b.fxcode) = 'GM') c
group by gmtype ) y on x.gmtype = y.gmtype

select fxgact
from rydedata.ffpxrefdta b 
where b.fxcyy = 2014    
    and trim(b.fxcode) = 'GM'
group by fxgact having count(*) > 1    


select * from rydedata.ffpxrefdta where fxcyy = 2014 and trim(fxgact) in ('144500', '146500')

select * from rydedata.glpmast where
trim(gmacct) = '1657001'



select * from eisglobal.sypffxmst where fxmcyy = 2014 and fxmcde = 'GM' and fxmpge = 7 order by fxmlne
-- this query seems to match the statement !  
select fxmpge, fxmlne, fxmcol, sum(amount) as amount
from (
  select trim(fxfact) as factAcct, amount
  from (
    select trim(gmacct) as account, gmat01 as amount
    from rydedata.glpmast 
    where gmyear = 2014
      and gmstyp = 'A') a 
      --and gmtype in ('1','2','3')) a
  left join rydedata.ffpxrefdta b on a.account = trim(b.fxgact)  
  where b.fxconsol = ''
    and b.fxcyy = 2014) m
left join eisglobal.sypffxmst n on m.factAcct = trim(n.fxmact) 
where n.fxmcyy = 2014    
  and trim(n.fxmcde) = 'GM'  
group by fxmpge, fxmlne, fxmcol  
order by fxmpge, fxmlne, fxmcol  