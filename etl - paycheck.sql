select yhdco#, yhdemp, max(x.ymname), 
  sum(yhcrtm) as total, -- 17 Emplr Curr Ret
  sum(yhdhrs) as Hours,  -- 7 Reg Hours
  sum(yhdtgp) as "1 Total Gross",
  sum(yhdtga) as "2 Total Adj Gross",
  sum(yhcfed) as "3 Curr Federal Tax",
  sum(yhcsse) as "4 Curr FICA",
  sum(yhcssm) as "5 Curr Emplr FICA",
  sum(yhcmde) as "6 Curr Employee Medicare",
  sum(yhcmdm) as "7 Curr Employer Medicare",
  sum(yhceic) as "8 YTD EIC Payments",
  sum(yhcst) as "9 Curr State Tax",
  sum(yhcsde) as "10 Curr Employee SDI",
  sum(yhcsdm) as "11 Curr Employer SDI",
  sum(yhccnt) as "12 Curr County Tax",
  sum(yhccty) as "13 Curr City Tax",
  sum(yhcfuc) as "14 Curr FUTA",
  sum(yhcsuc) as "15 Curr SUTA",
  sum(yhccmp) as "16 Workmans Comp",
  sum(yhcrte) as "17 Emply Curr Ret",
  sum(yhcrtm) as "18 Emplr Curr Ret",
  sum(yhdota) as "19 Overtime Amount",
  sum(yhdded) as "20 Total Deduction",
  sum(yhcopy) as "21 Total Other Pay",
  sum(yhcnet) as "22 Current Net",
  sum(yhctax) as "23 Current Tax Tot",
  sum(yhafed) as "1 Curr Adj Fed",
  sum(yhass) as "2 Curr Adj SS",
  sum(yhafuc) as "3 Curr Adj FUTA",
  sum(yhast) as "4 Curr Adj State",
  sum(yhacn) as "5 Curr Adj Cnty",
  sum(yhamu) as "6 Curr Adj City",
  sum(yhasuc) as "7 Curr Adj SUTA",
  sum(yhacm) as "8 Curr Adj Comp",
  sum(yhdvac) as "1 Vacation Taken",
  sum(yhdhol) as "2 Holday Taken",
  sum(yhdsck) as "3 Sick Leave Taken",
  sum(yhavac) as "4 Vacation Acc",
  sum(yhahol) as "5 Holiday Acc",
  sum(yhasck) as "6 Sick Leave Acc",
  sum(yhdhrs) as "7 Reg Hours",
  sum(yhdoth) as "8 Overtime Hours",
  sum(yhdahr) as "9 Alt Pay Hours",
  sum(yhvacd) as "10 Def Vac Hrs",
  sum(yhhold) as "11 Def Hol Hrs"
from rydedata.pyhshdta w-- Payroll Header transaction file
left join rydedata.pymast x on trim(w.yhdemp) = trim(x.ymempn) 
  and w.yhdco# = x.ymco# 
where ypbcyy = '112' -- payroll cen + year
and trim(yhdemp) in ('145840','1103050','196341')
group by yhdco#, yhdemp


/*********** PYHSCDTA Payroll Code Transaction File *****************/

select yhcemp, yhccde, yhccds, sum(yhccam), sum(yhccea)
from rydedata.pyhscdta
where ypbcyy = '112'
and trim(yhcemp) in ('145840','1103050','196341')
group by yhcemp, yhccde, yhccds
order by yhcemp, yhccde

-- fucking multiple duplicate yhccde with different yhccds (RY1 83: 1 ACCTS REC & AR
-- RY1 227: 12 UNIFORMS & ROTH AFTER TAX 
select yhcco#, yhccde, yhccds
from rydedata.pyhscdta
where ypbcyy = '112'
group by yhcco#, yhccde, yhccds
order by yhcco#, yhccde

-- 111 is both 7 DENTAL PREM & ROTH AFTER TAX,
-- so, i'm thinking, can't use just the code, but must include the description as well
-- must use both
select *
from rydedata.pyhscdta
where trim(yhccde) = '111'
-- must use both
select yhcco#, yhccds
from rydedata.pyhscdta
group by yhcco#, yhccds
  having count(*) > 1


/*********** PYHSCDTA Payroll Code Transaction File *****************/


--PYHSCDTA - Payroll Code Transaction File

select *
from rydedata.pyhscdta
where trim(yhcemp) = '16250'

-- 1 row per yhcemp/ypbnum/yhccde
select yhccco, yhcemp, ypbnum, yhccde, yhccsq, ypbcyy, yhccfx, yhctyp
from rydedata.pyhscdta 
group by yhccco, yhcemp, ypbnum, yhccde, yhccsq, ypbcyy, yhccfx, yhctyp
  having count(*) > 1

-- finally fucking unique record
select p.*
from rydedata.pyhscdta p
inner join (
    select yhccco, yhcemp, ypbnum, yhccde, yhccsq, ypbcyy, yhccfx, yhctyp
    from rydedata.pyhscdta 
    group by yhccco, yhcemp, ypbnum, yhccde, yhccsq, ypbcyy, yhccfx, yhctyp
      having count(*) > 1) x 
  on p.yhcemp = x.yhcemp
    and p.ypbnum = x.ypbnum
    and p.yhccde = x.yhccde
    and p.yhccsq = x.yhccsq
    and p.ypbcyy = x.ypbcyy
    and p.yhccfx = x.yhccfx
    and p.yhccco = x.yhccco
    and p.yhctyp = x.yhctyp
order by yhcemp, ypbnum, yhccde, yhccsq, ypbcyy, yhccfx

-- whats the difference between pyhscdta (payroll code transaction file) and pydeduct (employee deduction control file)
-- pydeduct is the deduction amt per check

----PYDEDUCT

-- PYHSHDTA -- paychec/employee
select *
from rydedata.pyhshdta


select *
-- yhdbsp: base total
-- yhdtp: Curr Gross Pay
-- yhcfed: Curr Fed Tax
-- yhdhrs: Base Hours
-- yhdoth: Overtime Hours
from rydedata.pyhshdta
where trim(yhdemp) = '16250'
and trim(yhdref) = '17807'

select *
-- yhdbsp: base total
-- yhdtp: Curr Gross Pay
-- yhcfed: Curr Fed Tax
-- yhdhrs: Base Hours
-- yhdoth: Overtime Hours
-- yhdhol: Holiday Hours
-- yhdsck: PTO Hours
from rydedata.pyhshdta
where trim(yhdemp) = '1110425'
and trim(yhdref) = '16151'

select *
-- yhdbsp: base total
-- yhdtp: Curr Gross Pay
-- yhcfed: Curr Fed Tax
-- yhdhrs: Base Hours
-- yhdoth: Overtime Hours
-- yhdhol: Holiday Hours
-- yhdsck: PTO Hours
-- yhdvac: Vacation Hours
from rydedata.pyhshdta
where trim(yhdemp) = '119180'
and trim(yhdref) = '17044'

select *
from rydedata.pydeduct
where trim(ydempn) = '16250'

select * -- brenda w/vac & ot period end: 10/8/11, chedate: 10/14/11, check#: 17044
from rydedata.pyhscdta
where trim(yhcemp) = '119180'
  and trim(ypbnum) = '1014001' -- batchnumber

select * -- brian p. w/holiday, ot, pto
/*
yhccde   yhccea
VAC      hours on this check
OVT      hours on this check
HOL      hours on this check
PTO      hours on this check
*/
from rydedata.pyhscdta
where trim(yhcemp) = '1110425'
  and trim(ypbnum) = '916000'

select yhcco#, ypbcyy, ypbnum, yhcemp, yhccde
from rydedata.pyhscdta
where trim(yhccde) in ('VAC','OVT','HOL','PTO')
group by yhcco#, ypbcyy, ypbnum, yhcemp, yhccde
  having count(*) > 1

select *
from rydedata.pyhscdta
where ypbcyy = 111
and ypbnum = 429000
and trim(yhcemp) = '1148065'

select *
from rydedata.pyhshdta
where ypbnum = 429000
and trim(yhdemp) = '1148065'

select yhcco#, ypbcyy, ypbnum, yhcemp, yhccde, sum(yhccea) as hours
from rydedata.pyhscdta
where trim(yhccde) in ('VAC','OVT','HOL','PTO')
  and trim(yhcemp) = '1110425'
group by yhcco#, ypbcyy, ypbnum, yhcemp, yhccde

/*********** 1/13 relationship between pyhshdta / pyhscdta *****************************/
select * 
from rydedata.pyhshdta
where ypbnum = 1209000
  and trim(yhdemp) = '115255'

select ymname, yhdhrs as "YHDHRS-REGhrs", yhdoth as "YHDOTH-OThrs",
  yhdhol as "YHDHOL-HOLhrs", yhdsck as "YHDSCK-PTOhrs",
  yhdvac as "YHDVAC-VAChors"
-- yhdbsp: base total
-- yhdtp: Curr Gross Pay
-- yhcfed: Curr Fed Tax
-- yhdhrs: Base Hours
-- yhdoth: Overtime Hours
-- yhdhol: Holiday Hours
-- yhdsck: PTO Hours
-- yhdvac: Vacation Hours
from rydedata.pyhshdta
where ypbnum = 1209000
  and trim(yhdemp) = '115255'


select yhcco#, yhcemp, max(z.ymname), yhccde as "YHCCDE-Code", 
  yhccds as "YHCCDS-Desc", sum(yhccam) as Amount, sum(yhccea) as Hours
from rydedata.pyhscdta y 
left join rydedata.pymast z on trim(yhcemp) = trim(z.ymempn)
where ypbcyy = '111'  
  and trim(yhcemp) in ('145840','1103050','196341')
  and trim(yhccde) in ('VAC','OVT','HOL','PTO')
group by yhcco#, yhcemp, yhccde, yhccds
order by yhcemp, yhccde

/*********** 1/13 relationship between pyhshdta / pyhscdta *****************************/
select *
from rydedata.pyhscdta
where ypbnum = 1209000
  and trim(yhcemp) = '115255'

select *
/*
yhccde   yhccea
VAC      hours on this check
OVT      hours on this check
HOL      hours on this check
PTO      hours on this check
*/
from rydedata.pyhscdta
where ypbnum = 1209000
  and trim(yhcemp) = '115255'

-- from greg
select yhcco#, yhcemp, ymname, ymdept, sum(case when yhccde = 'VAC' then yhccea else 0 end) as Vac, 
  sum(case when yhccde = 'PTO' then yhccea else 0 end) as PTO, ymrate
from rydedata.pyhscdta va
left join rydedata.pymast py on py.ymempn = va.yhcemp
where  ypbcyy in (111, 112)
  and yhccde in ('VAC', 'PTO')
group by yhcco#, yhcemp, ymname, ymdept, ymrate
order by yhcco#, ymname

/**********************/
select *
from  rydedata.pyactocd
where ytaco# = 'RY1'

select *
from rydedata.pyactgr
where ytaco# = 'RY1'


select ymdist, count(*)
from rydedata.pymast
group by ymdist


select distinct ymdvac, ymdhol, ymdsck, ymdvyr, ymdhyr, ymdsyr
from rydedata.pymast
where ymname like '%BRIE%'

-- 8/26/12
select yhcco#, ypbcyy, yhccde, count(*)
from rydedata.pyhscdta
group by yhcco#, ypbcyy, yhccde
order by yhccde

-- yhccde (code id) and yhccds (description)
-- multiple descriptions per code (for the same store & year)
select yhcco#, ypbcyy, yhccde
from (
  select yhcco#, ypbcyy, yhccde, yhccds
  from rydedata.pyhscdta
  group by yhcco#, ypbcyy, yhccde, yhccds) a
group by yhcco#, ypbcyy, yhccde
having count(*) > 1
order by yhcco#, yhccde, ypbcyy
-- eg
select *
from (
  select yhcco#, ypbcyy, yhccde, yhccds
  from rydedata.pyhscdta
  group by yhcco#, ypbcyy, yhccde, yhccds) a
where yhcco# = 'RY1'
  and ypbcyy = 112
  and trim(yhccde) = '287'

select *
from rydedata.pyhscdta
where yhcco# = 'RY1'
  and ypbcyy = 112
  and trim(yhccde) = '287'
order by yhccds

select *
from rydedata.pypcodes
order by ytddcd, ytdco#



select *
from rydedata.pyhscdta
where yhcco# = 'RY1'
  and ypbcyy = 112
  and trim(yhccde) = '119'
order by yhccds


select *
from rydedata.pyhscdta
where yhccds like '%ROTH%'

