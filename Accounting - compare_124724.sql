select a.gtdate, a.gttamt, a.gtctl#, b.employee_name
from rydedata.glptrns a
left join rydedata.pymast b on trim(a.gtctl#) = trim(b.pymast_employee_number)
where gtdate between '11/01/2017' and '12/31/2017'
  and trim(gtacct) = '124724'
  
  
select b.pymast_employee_number, b.employee_name,
  sum(case when a.gtdate between '11/01/2017' and '11/30/2017' then a.gttamt end) as november,
  sum(case when a.gtdate between '12/01/2017' and '12/31/2017' then a.gttamt end) as december
from rydedata.glptrns a
left join rydedata.pymast b on trim(a.gtctl#) = trim(b.pymast_employee_number)
where gtdate between '11/01/2017' and '12/31/2017'
  and trim(gtacct) = '124724'  
  and b.employee_name is not null  
  and a.gtpost = 'Y'
group by b.pymast_employee_number, b.employee_name  