-- is there any use for records with:
--   no ro
--   no date
--   ptlhrs + ptlamt = 0
--   non RY1/2/3 company

Column: PTLTYP
Values:                 A is a Header Record
                        I  Internal Deductible
                        L   Labor Line                           
                        M  Paint/Materials
                        N  Sublet Line
                        Q  Discount
                        W  Hazardous Materials

Column:  PTCODE
Values:                 SC  Service Contract Info Line
                        SL  Sublet Sale Type
                        PM  Paint/Materials Line
                        PO Purchase Order Line                         
                        CP  Customer Pay Info Line
                        PR  Estimate Data
                        CR  Correction Data
                        WS Warranty Info Pay Line
                        IS  Internal Flag Info          
                        TT  Tech Time Flag
                        HZ  Hazardous Materials Line


select * from sdprdet
select count(*) from rydedata.sdprdet -- 3,561,371
select count(*) from rydedata.sdprdet where length(trim(ptro#)) < 6 -- 360200
select * from sdprdet where length(trim(ptro#)) < 6 order by ptro# desc
select * from sdprdet where length(trim(ptro#)) < 6 and ptdate <> 0 order by ptro# desc
select max(ptdate), min(ptdate) from sdprdet where length(trim(ptro#)) < 6

select count(*) from rydedata.sdprdet where ptdate = 0 -- 2,159,895

select distinct length(trim(ptro#)) from  sdprdet where ptdate = 0

select * from sdprdet where ptdate = 0 and length(trim(ptro#)) = 8

select * from sdprdet where trim(ptro#) = '16001051'

select * from sdprdet where trim(ptco#) not in ('RY1','RY2','RY3')

select ptltyp, ptcode, count(*) from sdprdet group by ptltyp, ptcode order by ptltyp, ptcode


select left(trim(ptro#),4) as RO, count(*)
from rydedata.sdprdet
group by left(trim(ptro#),4)
order by left(trim(ptro#),4)

select ptco#, ptro#, ptline, ptseq#, sum
from sdprdet 
group by ptco#, ptro#, ptline, ptseq# 
having count(*) > 1

select ptro#, sum(ptlhrs), sum(ptlamt)
from sdprdet
group by ptro#

select ptro#
from(
select ptro#, sum(ptlhrs) as ptlhrs, sum(ptlamt)as ptlamt
from sdprdet
group by ptro#) s
where (ptlhrs = 0 and ptlamt = 0)

select * 
from sdprdet
where ptro# in (
select ptro#
from(
select ptro#, sum(ptlhrs) as ptlhrs, sum(ptlamt)as ptlamt
from sdprdet
group by ptro#) s
where (ptlhrs = 0 and ptlamt = 0))
and trim(ptco#) = 'RY1'
order by ptro#

select count(*) from sdprdet -- 3,561,052
where ptro# not in ( --3,527,134
select ptro#
from(
select ptro#, sum(ptlhrs) as ptlhrs, sum(ptlamt)as ptlamt
from sdprdet
group by ptro#) s
where (ptlhrs = 0 and ptlamt = 0))

select *
from (
  select ptco#, ptro#, ptline, ptseq# 
  from sdprdet 
  group by ptco#, ptro#, ptline, ptseq# 
  having count(*) > 1) a
left join sdprdet b on a.ptco# = b.ptco#
  and a.ptro# = b.ptro#
  and a.ptline = b.ptline
  and a.ptseq# = b.ptseq#


select * from sdpxtim  where trim(ptro#) = '16001028'


-- nope it's not all crookston
select ptco#, count(*)
from (
  select ptco#, ptro#, ptline, ptseq# 
  from sdprdet 
  group by ptco#, ptro#, ptline, ptseq# 
  having count(*) > 1) a
group by ptco#


select a.ptco#, a.ptro#, a.ptline, a.ptseq#, b.* 
from (
  select ptco#, ptro#, ptline, ptseq# 
  from sdprdet 
  where ptro# not in ( 
    select ptro#
    from (
      select ptro#, sum(ptlhrs) as ptlhrs, sum (ptlamt) as ptlamt
      from sdprdet
      group by ptro#) s
    where s.ptlhrs = 0 and s.ptlamt = 0)
  group by ptco#, ptro#, ptline, ptseq# 
  having count(*) > 1) a
left join sdprdet b on a.ptco# = b.ptco#
  and a.ptro# = b.ptro#
  and a.ptline = b.ptline
  and a.ptseq# = b.ptseq#
where b.ptro# not in ( 
  select ptro#
  from (
    select ptro#, sum(ptlhrs) as ptlhrs, sum (ptlamt) as ptlamt
    from sdprdet
    group by ptro#) s
  where s.ptlhrs = 0 and s.ptlamt = 0)


-- 3/12/12
-- back to looking for a sane initial load grouping
select count(*)
from rydedata.sdprdet
where ptco# in ('RY1','RY2','RY3')
  and trim(ptro#) <> ''
  and ptdate <> 0

select left(trim(ptro#),2) as RO, count(*)
from rydedata.sdprdet
where ptco# in ('RY1','RY2','RY3')
  and trim(ptro#) <> ''
  and ptdate = 0
group by left(trim(ptro#),2)
order by left(trim(ptro#),2)


select left(trim(ptro#),3) as RO, count(*)
from rydedata.sdprdet
where ptco# in ('RY1','RY2','RY3')
  and trim(ptro#) <> ''
  and left(trim(ptro#), 2) in ('16','18','19','26','36')
group by left(trim(ptro#),3)
order by left(trim(ptro#),3)

-- fuck, it's a one time thing, bite the bullet and let it run to completion
select count(*) from(
select left(trim(ptro#),4) as RO, count(*)
from rydedata.sdprdet
where ptco# in ('RY1','RY2','RY3')
  and trim(ptro#) <> ''
group by left(trim(ptro#),4)
order by left(trim(ptro#),4))x


