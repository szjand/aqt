SELECT distinct a.ptro#
  (SELECT thedate FROM day WHERE datekey = a.opendatekey) AS opendate,
  (SELECT thedate FROM day WHERE datekey = a.closedatekey) AS closedate,
  (SELECT thedate FROM day WHERE datekey = a.finalclosedatekey) AS finalclosedate, 
  b.gtdate
FROM factro a
LEFT JOIN stgArkonaGLPTRNS b ON a.ro = b.gtdoc#
WHERE ro IN (
    '16101200','16101126','16101038','16101182','16101199','16100330','16101195','16101170',
    '16100119','18019020','16101163','16100977','16100930','16101161','16100929','16101192',
    '16101196','16101023','16101169','16101194','16101167','16101191','16101198','16101075',
    '16101132','16101165','16101070','16100941','16101151','16101128','16101197','2670384',
    '2670450','2670366','2669832','2670406','2670411','2670237','2670415','2670387','2670382',
    '2670358','2669150','2670239','2670386','2670238','36013863','36013840','36013856','36013864')
--  AND b.gtdoc# IS NULL
ORDER BY ro 


select distinct a.ptro#, a.ptdate, a.ptcdat, a.ptfcdt, b.gtdate, gtacct
from rydedata.sdprhdr a
left join rydedata.glptrns b on trim(a.ptro#) = trim(gtdoc#)
where trim(ptro#) in (
    '16101200','16101126','16101038','16101182','16101199','16100330','16101195','16101170',
    '16100119','18019020','16101163','16100977','16100930','16101161','16100929','16101192',
    '16101196','16101023','16101169','16101194','16101167','16101191','16101198','16101075',
    '16101132','16101165','16101070','16100941','16101151','16101128','16101197','2670384',
    '2670450','2670366','2669832','2670406','2670411','2670237','2670415','2670387','2670382',
    '2670358','2669150','2670239','2670386','2670238','36013863','36013840','36013856','36013864')
order by ptfcdt


select *
from rydedata.sdprhdr a
where ptdate between 20121001 and 20121031
  and ptfcdt <> 0
  and not exists (
    select 1
    from rydedata.glptrns 
    where trim(gtdoc#) = trim(a.ptro#))