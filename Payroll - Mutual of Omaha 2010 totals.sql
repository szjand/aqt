-- whats the difference between pyhscdta (payroll code transaction file) and pydeduct (employee deduction control file)
-- pydeduct is the deduction amt per check
select p.ymco# as Company, p.Ymempn as "Employee #", p.ymname as Name, p.ymrate as Rate, g.Gross,
  case 
    when cast(right(trim(ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as "Hire Date"
-- d.*
from pymast p
--inner join (
--select distinct yhcemp, 
--  coalesce((select sum(yhccam) from pyhscdta where yhcemp = pyh.yhcemp and yhccde = '107'),0) as Health,
--  coalesce((select sum(yhccam) from pyhscdta where yhcemp = pyh.yhcemp and yhccde = '111'),0) as Dental,
--  coalesce((select sum(yhccam) from pyhscdta where yhcemp = pyh.yhcemp and yhccde = '115'),0) as Daycare,
--  coalesce((select sum(yhccam) from pyhscdta where yhcemp = pyh.yhcemp and yhccde = '119'),0) as MedFlex,
--  coalesce((select sum(yhccam) from pyhscdta where yhcemp = pyh.yhcemp and yhccde = '293'),0) as CriticalCare,
--  coalesce((select sum(yhccam) from pyhscdta where yhcemp = pyh.yhcemp and yhccde = '299'),0) as Cancer,
--  coalesce((select sum(yhccam) from pyhscdta where yhcemp = pyh.yhcemp and yhccde = '91'),0) as "401K",
--  coalesce((select sum(yhccam) from pyhscdta where yhcemp = pyh.yhcemp and yhccde = '98'),0) as ToolChex
--from pyhscdta pyh
--where ypbcyy = '0110'
--and trim(yhccde) in ('107','111','115','119','293','299','91','98')) d on p.ymempn = d.yhcemp

/*
left join (
  select 
    yhdemp,
    sum(yhdtgp) as Gross -- 1 total gross pay
  from pyhshdta -- Payroll Header transaction file
  where ypbcyy = '0110'
  group by yhdemp)g on p.ymempn = g.yhdemp
*/




inner join (
  select yhdemp, 
    sum(yhacm) as Gross -- 8 Curr Adj Comp
  from pyhshdta -- Payroll Header transaction file
  where ypbcyy = '110' -- payroll cen + year
  group by yhdemp) g on p.ymempn = g.yhdemp

order by p.ymco#, p.ymname

/*
-- scratch pad for 2010 total pay
select 
  yhdemp,
  sum(yhdtgp)  -- 1 total gross pay
from pyhshdta -- Payroll Header transaction file
where ypbcyy = '0110'
group by yhdemp

-- assuming void payroll = yhdseq = '0J'
select yhdseq, count(*)
from pyhshdta -- Payroll Header transaction file
where ypbcyy = '0110'
group by yhdseq
-- and it backs out a previous payroll, therefore sum all regardless of yhdseq
select *
from pyhshdta
where trim(yhdemp) = '34059'

*/


