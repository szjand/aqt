Service appointments pushed into arkona by xtime

Select distinct ptapdt from RYDEDATA.PDPPHDR

select *
from rydedata.pdpphdr
where ptvin = 'JN8AF5MV2BT017443'

select *
from rydedata.pdpphdr
where ptdtyp = 'RO'
and ptdoc# = ''

select ptttyp, count(*)
from rydedata.pdpphdr
group by ptttyp


select *
from rydedata.pdppdet
where ptpkey = 169253


select *
from rydedata.pdhludt
--where phpndky = 169253
order by phpndky desc


select count(*)
from (
select phpndky
from rydedata.pdhludt
group by phpndky) a


select b.ptdtyp, b.ptdoc#, a.ptpkey, a.ptline, a.ptcode, a.ptltyp, a.ptdate, a.ptdspadate, 
  a.ptdspatime, a.ptdupdate

select a.ptco#, a.ptpkey, a.ptline, a.ptltyp, a.ptseq#, a.ptcode, a.ptdate, a.ptcmnt, a.ptsvctyp, a.ptlpym,
  a.ptlopc, a.ptdisstrz, a.ptdspadate, a.ptdspatime, a.ptdspmdate, a.ptdspmtime, a.ptdupdate, 
  b.ptckey, b.ptcnam, b.ptphon, b.ptswid, b.pttest, b.ptvin, b.ptodom, b.ptpdtm, b.ptcreate, b.ptcthold
-- select * 
from rydedata.pdppdet a
left join rydedata.pdpphdr b on a.ptpkey = b.ptpkey
where a.ptdspatime <> '00:00:00'
order by a.ptpkey, a.ptline

/*
1/3/14
looking at xtime (xtime.com  rnokelby/Service2) looks like xtime does not push the date/time of the appointment
into Arkona, just the the creation date/time
oops
promise time is in pdpphdr.ptpdtm
*/
select a.ptco#, a.ptpkey, a.ptline, a.ptltyp, a.ptseq#, a.ptcode, a.ptdate, a.ptcmnt, a.ptsvctyp, a.ptlpym,
  a.ptlopc, a.ptdisstrz, a.ptdspadate, a.ptdspatime, a.ptdspmdate, a.ptdspmtime, a.ptdupdate, 
  b.ptckey, b.ptcnam, b.ptphon, b.ptswid, b.pttest, b.ptvin, b.ptodom, b.ptpdtm, b.ptcreate, b.ptcthold
-- select * 
from rydedata.pdppdet a
left join rydedata.pdpphdr b on a.ptpkey = b.ptpkey
where exists (
  select 1
  from rydedata.pdppdet
  where ptpkey = a.ptpkey
    and ptdupdate like 'XT%')
order by ptdspadate desc     
order by a.ptpkey, a.ptline

select * from rydedata.pdppdet where ptpkey = 183512

select * from rydedata.pdpphdr where ptpkey = 183512


-- what about the stuff that is showing up as RY1 appointments (ro list)
-- an appt made by aubrey on 12/30 for an inventory vehicle, 1GNFK26399R242948
-- row in pdpphdr only
select * from rydedata.pdpphdr where trim(ptvin) = '1GNFK26399R242948'

select * from rydedata.pdpphdr where ptdtyp = 'RO' and ptdoc# = ''

-- back to honda
-- Katherine Emmons appt made on 1/9/14 for 1/20 @ 9:30
-- ok, this is a little confusing, on this appointment, pdpphdr.ptdtm shows 201401200930
-- arkona display says that is promise time
-- BUT in xtime, that is the appt time, promise time is 1/20 6PM
-- in db2 field desc, ptpdtm: Promised Date/Time, ptapdt: Appointment Date/Time
-- in Arkona, the promise datetime of 1/20 6PM does not show anywhere, the 1/20 9:30 shows on ro list only (main ro screen)  
select a.ptco#, a.ptpkey, a.ptline, a.ptltyp, a.ptseq#, a.ptcode, a.ptdate, a.ptcmnt, a.ptsvctyp, a.ptlpym,
  a.ptlopc, a.ptdisstrz, a.ptdspadate, a.ptdspatime, a.ptdspmdate, a.ptdspmtime, a.ptdupdate, 
  b.ptckey, b.ptcnam, b.ptphon, b.ptswid, b.pttest, b.ptvin, b.ptodom, b.ptpdtm, b.ptcreate, b.ptcthold, ptapdt
-- select * 
from rydedata.pdppdet a
left join rydedata.pdpphdr b on a.ptpkey = b.ptpkey
where exists (
  select 1
  from rydedata.pdppdet
  where ptpkey = a.ptpkey
    and ptdupdate like 'XT%')
and a.ptpkey = 184380
order by a.ptpkey, a.ptline

select * from rydedata.pdpphdr where ptapdt <> 0


-- 2/20/15 lear's email
select * from (
select a.ptco#, a.ptpkey, a.ptline, a.ptltyp, a.ptseq#, a.ptcode, a.ptdate, a.ptcmnt, a.ptsvctyp, a.ptlpym,
  a.ptlopc, a.ptdisstrz, a.ptdspadate, a.ptdspatime, a.ptdspmdate, a.ptdspmtime, a.ptdupdate, 
  b.ptckey, b.ptcnam, b.ptphon, b.ptswid, b.pttest, b.ptvin, b.ptodom, b.ptpdtm, b.ptcreate, b.ptcthold
-- select * 
from rydedata.pdppdet a
left join rydedata.pdpphdr b on a.ptpkey = b.ptpkey
where exists (
  select 1
  from rydedata.pdppdet
  where ptpkey = a.ptpkey
    and ptdupdate like 'XT%')
) x
where lower(ptcnam) like '%raate%'  



select a.ptco#, b.ptdtyp, b.ptdoc#, a.ptpkey, a.ptline, a.ptltyp, a.ptseq#, a.ptcode, a.ptdate, a.ptcmnt, a.ptsvctyp, a.ptlpym,
  a.ptlopc, a.ptdisstrz, a.ptdspadate, a.ptdspatime, a.ptdspmdate, a.ptdspmtime, a.ptdupdate, 
  b.ptckey, b.ptcnam, b.ptphon, b.ptswid, b.pttest, b.ptvin, b.ptodom, b.ptpdtm, b.ptcreate, b.ptcthold
-- select * 
from rydedata.pdppdet a
left join rydedata.pdpphdr b on a.ptpkey = b.ptpkey
where exists (
  select 1
  from rydedata.pdppdet
  where ptpkey = a.ptpkey
    and ptdupdate like 'XT%')
order by ptdate desc, ptpkey,ptline    
    
select * from (
select a.ptco#, a.ptpkey, a.ptline, a.ptltyp, a.ptseq#, a.ptcode, a.ptdate, a.ptcmnt, a.ptsvctyp, a.ptlpym,
  a.ptlopc, a.ptdisstrz, a.ptdspadate, a.ptdspatime, a.ptdspmdate, a.ptdspmtime, a.ptdupdate, 
  b.ptckey, b.ptcnam, b.ptphon, b.ptswid, b.pttest, b.ptvin, b.ptodom, b.ptpdtm, b.ptcreate, b.ptcthold
-- select * 
from rydedata.pdppdet a
left join rydedata.pdpphdr b on a.ptpkey = b.ptpkey
where exists (
  select 1
  from rydedata.pdppdet
  where ptpkey = a.ptpkey
    and ptdupdate like 'XT%')
) x
where lower(ptcnam) like '%holle%'      



-- anything like appointment
-- aha, sdprhdr as ptapdt (date_time_of_appointment)

select distinct table_name, column_text
-- select *
from sysibm.sqlcolumns
where table_schem = 'RYDEDATA'
  and table_name not like '$%'
  and lower(column_text) like '%point%'
order by table_name

-- ah fuck, no data
select distinct ptapdt from rydedata.sdprhdr

-- so, it looks like, the appt shows up in pdpphdr, but once that record  moves to
-- sdprhdr, the appointment info is gone

select record_status, count(*) from rydedata.pdpphdr where document_type = 'RO' group by record_status

select *
from rydedata.pdpphdr
where lower(cust_name) like 'henegar%'

select ro_status, count(*) from rydedata.pdpphdr where document_type = 'RO' group by ro_status

select * from rydedata.pdpphdr where ro_status = 'A'

-- all ro rows are of transaction_type = 3
select transaction_type, count(*) from rydedata.pdpphdr where document_type = 'RO' group by transaction_type

select document_type, transaction_type, count(*) from rydedata.pdpphdr group by document_type, transaction_type

-- 2/25/15 ok, quit fucking around, what do i know for sure
-- sarah vogel
has an appt in xtime for 10AM on 2/27/15, made via web on 2/24/15
xtime creation time -> pdpphdr.ptdate
xtime cust# 1092918 is the arkona customer_key
xtime: promise time = 2/27/15 6:00PM 
xtime dms details: appt 230151 = arkona ptpkey (pending_key)

arkona shows an open appointment, does not show anywhere the promise time
on the open ro page, shows 2/27 10AM (promised_date_time)


select *
from rydedata.pdpphdr
where lower(cust_name) like 'bachmei%'

select * from rydedata.pdppdet where ptpkey = 230151

select * from rydedata.pdpphdr where trim(document_number) = '2712837'
select * from rydedata.pdppdet where ptpkey = 230253

rows in pdpphdr where promised_date_time_after today
turns out, these are all ry2 appointments for future dates
select * from rydedata.pdpphdr where promised_date_time > 201502259999

-- 5/1/15 need to start scraping xtime appt info from arkona into rydellservice

select * from rydedata.pdpphdr where trim(document_number) = '2716024'

select * from rydedata.pdpphdr where customer_key = 323002

select * from rydedata.pdppdet where ptpkey = 237089

select * from rydedata.pdppdet 
where ptpkey in (
  select ptpkey
  from rydedata.pdppdet
  where trim(ptdupdate) = 'XT-APTADD') 
order by ptpkey desc, ptline asc, ptltyp asc 


-- this is the query to populate rydellservice.ext_xtime_appointments
-- ron stores create of his appointments as timestamps
-- db2 stores the timestamp as:  2014-03-27-09.24.14.634803 which does not decode in advantage, 
-- so, format as string, then ads can cast that string as a timestamp
select a.company_number, a.pending_key, 
  varchar_format(a.doc_Create_timestamp, 'YYYY-MM-DD HH24:MI:SS') as doc_create_timestamp,
  a.cust_name, a.cust_phone_no,
  vin, promised_date_time,
  b.ptline, b.ptltyp, b.ptseq# as ptseq, b.ptcmnt, c.name
from rydedata.pdpphdr a
left join rydedata.pdppdet b on a.pending_key = b.ptpkey
left join rydedata.sdpswtr c on a.service_writer_id = c.id
where a.ptpkey in (
  select ptpkey
  from rydedata.pdppdet
  where trim(ptdupdate) = 'XT-APTADD')
-- order by a.promised_date_time desc, a.pending_key, b.ptline, b.ptseq#  


select a.company_number, a.pending_key, 
  varchar_format(a.doc_Create_timestamp, 'YYYY-MM-DD HH24:MI:SS') as doc_create_timestamp,
  a.cust_name, a.cust_phone_no,
  vin, promised_date_time,
  b.ptline, b.ptltyp, b.ptseq# as ptseq, b.ptcmnt
from rydedata.pdpphdr a
left join rydedata.pdppdet b on a.pending_key = b.ptpkey
where a.ptpkey = 196091


/*
(timestamp_exp, 
fmt_str) 

Converts timestamp_exp to a string in the format specified by the fmt-string. 

timestamp-exp - is an expression that results in a timestamp. The argument must be a timestamp or a string representation of a timestamp that is neither a CLOB nor a LONG VARCHAR. The string expression returns a CHAR or a VARCHAR value whose maximum length is not greater than 254.
 
fmt-string - is a character constant that contains a template for how the result is to be formatted. The length of the format string must not be greater than 254. The content of format-string can be specified only in full case.
 
Format string must be following: 

'YYYY-MM-DD HH24:MI:SS' 


select a.doc_Create_timestamp, varchar_format(a.doc_Create_timestamp, 'YYYY-MM-DD HH24:MI:SS')
from rydedata.pdpphdr a
where a.ptpkey in (
  select ptpkey
  from rydedata.pdppdet
  where trim(ptdupdate) = 'XT-APTADD')
*/

select a.doc_Create_timestamp, varchar_format(a.doc_Create_timestamp, 'YYYY-MM-DD HH24:MI:SS')
from rydedata.pdpphdr a
where a.ptpkey in (
  select ptpkey
  from rydedata.pdppdet
  where trim(ptdupdate) = 'XT-APTADD')
  
select 1 + 2 from sysibm.sysdummy1

select date(now()) from sysibm.sysdummy1


select promised_date_time, date(promised_date_time),
  c
from rydedata.pdpphdr a
where a.ptpkey in (
  select ptpkey
  from rydedata.pdppdet
  where trim(ptdupdate) = 'XT-APTADD')
--and date(promised_date_time) >= curdate()
order by promised_date_time desc 


  
select *
from rydedata.pdpphdr a
left join rydedata.pdppdet b on a.pending_key = b.ptpkey
where trim(ptdoc#) = '2716282'


select a.company_number, a.pending_key, 
  varchar_format(a.doc_Create_timestamp, 'YYYY-MM-DD HH24:MI:SS') as doc_create_timestamp,
  a.cust_name, a.cust_phone_no,
  vin, promised_date_time,
  b.ptline, b.ptltyp, b.ptseq# as ptseq, b.ptcmnt, c.name
-- select *  
from rydedata.pdpphdr a
left join rydedata.pdppdet b on a.pending_key = b.ptpkey
left join rydedata.sdpswtr c on a.service_writer_id = c.id
where a.ptpkey in (
  select ptpkey
  from rydedata.pdppdet
  where trim(ptdupdate) = 'XT-APTADD')
and promised_date_time > 201505000000  


select * from rydedata.bopname where bopname_record_key = 1093079

select * from rydedata.pdpphdr where pending_key = 237926
  
select * from rydedata.inpmast where imvin = '19XFB2E53DE090620'  


-- this is the query to populate rydellservice.ext_xtime_appointments
-- ron stores create of his appointments as timestamps
-- db2 stores the timestamp as:  2014-03-27-09.24.14.634803 which does not decode in advantage, 
-- so, format as string, then ads can cast that string as a timestamp
-- 5/14/15: add vehicle description, customer address
select a.company_number, a.pending_key, 
  varchar_format(a.doc_Create_timestamp, 'YYYY-MM-DD HH24:MI:SS') as doc_create_timestamp,
  a.cust_name, a.cust_phone_no,
  vin, promised_date_time,
  b.ptline, b.ptltyp, b.ptseq# as ptseq, b.ptcmnt, c.name,
  d.address_1, d.city, d.state_code, d.zip_code,
  e.year, e.make, e.model
from rydedata.pdpphdr a
left join rydedata.pdppdet b on a.pending_key = b.ptpkey
left join rydedata.sdpswtr c on a.service_writer_id = c.id
left join rydedata.bopname d on a.customer_key = d.bopname_record_key
left join rydedata.inpmast e on a.vin = e.imvin
where a.ptpkey in (
  select ptpkey
  from rydedata.pdppdet
  where trim(ptdupdate) = 'XT-APTADD')
-- order by a.promised_date_time desc, a.pending_key, b.ptline, b.ptseq#  
and promised_date_time > 201505000000  
--and a.pending_key = 238564


