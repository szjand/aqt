/* 1/8/13
kim needs the vacation and pto paid out in 2012 for each employee
on the check detail screen (2012), those amounts show up as negative values under earned vac & pto
in pyshdta those columns are yhvacd * yhsckd
so i need the last check paid to each employee in 2012
is ypbnum sequential?
*/
Select * from RYDEDATA.PYHSHDTA where ypbnum = 1221000 and trim(yhdemp) = '11650'

Select ypbnum, yhdemp, yhdecc, yhdeyy, yhdemm, yhdedd, yhvacd, yhsckd from RYDEDATA.PYHSHDTA
where ypbcyy = 112
and trim(yhdemp) = '11650'
  and ypbnum = (
    select max(ypbnum)
    from rydedata.pyhshdta
    where ypbcyy = 112)
    and trim(yhdemp) = '11650')


select trim(yhdemp) as yhdemp, max(ypbnum) as batch
from rydedata.pyhshdta
where ypbcyy = 112
group by trim(yhdemp)


Select c.ymco#, c.ymname, c.ymactv, c.ymtdte, a.ypbnum, a.yhdemp, yhdecc, yhdeyy, yhdemm, yhdedd, yhvacd, yhsckd 
-- select *
from RYDEDATA.PYHSHDTA a
inner join ( -- last check of 2012
  select trim(yhdemp) as yhdemp, max(ypbnum) as batch
  from rydedata.pyhshdta
  where ypbcyy = 112
  group by trim(yhdemp)) b on trim(a.yhdemp) = trim(b.yhdemp) and a.ypbnum = b.batch
inner join rydedata.pymast c on trim(a.yhdemp) = trim(c.ymempn)
where abs(yhvacd) + abs(yhsckd) <> 0
  and ymtdte = 0
order by c.ymco#, c.ymname

-- so this is all looking fine and dandy until i get to dale axtman, 18005
-- 5250 -136 / -48
-- query - 120 / -48
-- fields in query do not include the values for the current paycheck
Select * from RYDEDATA.PYHSHDTA where ypbnum = 1221000 and trim(yhdemp) = '18005'

-- so instead of using the accumulated columns, use the current paid columns
-- yhdvac & yhdsck

select b.ymco#, b.ymname, a.yhdemp, sum(yhdvac), sum(yhdsck)
from rydedata.pyhshdta a
inner join rydedata.pymast b on trim(a.yhdemp) = trim(b.ymempn)
  and b.ymactv <> 'T'
where ypbcyy = 112
group by b.ymco#, b.ymname, a.yhdemp
order by b.ymco#, b.ymname

-- which should have worked, but axtman comes up with 120, not 136

Select yhdemp, yhdvac, yhdsck, yhvacd, yhsckd from RYDEDATA.PYHSHDTA where ypbcyy = 112 and trim(yhdemp) = '18005'
p
-- which takes me back, do i do the original query and simply add the values from the last check?

Select c.ymco#, c.ymname, c.ymactv, c.ymtdte, a.ypbnum, a.yhdemp, yhdecc, yhdeyy, yhdemm, yhdedd, yhvacd, yhsckd, yhdvac, yhdsck, 
  yhvacd - yhdvac as "Vac Pd", yhsckd - yhdsck as "PTO Pd" 
-- select *
from RYDEDATA.PYHSHDTA a
inner join ( -- last check of 2012
  select trim(yhdemp) as yhdemp, max(ypbnum) as batch
  from rydedata.pyhshdta
  where ypbcyy = 112
  group by trim(yhdemp)) b on trim(a.yhdemp) = trim(b.yhdemp) and a.ypbnum = b.batch
inner join rydedata.pymast c on trim(a.yhdemp) = trim(c.ymempn)
where abs(yhvacd) + abs(yhsckd) <> 0
  and ymtdte = 0
  and ymclas = 'H'
order by c.ymco#, c.ymname

-- getting better, but now mark bjornseth show +177.8468 of yhsckd
-- but 5250 shows 187.08 hours of paid vacation
Select yhdemp, yhdvac, yhdsck, yhvacd, yhsckd from RYDEDATA.PYHSHDTA where ypbcyy = 112 and trim(yhdemp) = '115255'

-- not an issue, mark is salaried, so, per kim, i'll limit the output to hourly folks only, and we should be good
select distinct ymclas
from rydedata.pymast

Select c.ymco#, c.ymname, yhvacd - yhdvac as "Vac Pd", yhsckd - yhdsck as "PTO Pd" 
-- select *
from RYDEDATA.PYHSHDTA a
inner join ( -- last check of 2012
  select trim(yhdemp) as yhdemp, max(ypbnum) as batch
  from rydedata.pyhshdta
  where ypbcyy = 112
  group by trim(yhdemp)) b on trim(a.yhdemp) = trim(b.yhdemp) and a.ypbnum = b.batch
inner join rydedata.pymast c on trim(a.yhdemp) = trim(c.ymempn)
where abs(yhvacd) + abs(yhsckd) <> 0
  and ymtdte = 0
  and ymclas = 'H'
  -- and yhvacd - yhdvac < 1
order by c.ymco#, c.ymname
-- order by yhvacd - yhdvac

-- 3 anomalies vac pd & pto pd are positive nimbers
-- michael schwan, john gardner, josh syverson

Select yhdemp, yhdvac, yhdsck, yhvacd, yhsckd from RYDEDATA.PYHSHDTA where ypbcyy = 112 and trim(yhdemp) = '1124436'
