
-- When you get a �negative scale� error just wrap it with float().


Select ymname as "Name", case when ymrate = ymsaly then 'Hourly' else 'Salary' end as "Pay Type", ymsaly as "Rate",
  case 
    when cast(right(trim(ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(ymhdte))
          when 5 then  '20'||substr(trim(ymhdte),4,2)||'-'|| '0' || left(trim(ymhdte),1) || '-' ||substr(trim(ymhdte),2,2)
          when 6 then  '20'||substr(trim(ymhdte),5,2)||'-'|| left(trim(ymhdte),2) || '-' ||substr(trim(ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(ymhdte))
          when 5 then  '19'||substr(trim(ymhdte),4,2)||'-'|| '0' || left(trim(ymhdte),1) || '-' ||substr(trim(ymhdte),2,2)
          when 6 then  '19'||substr(trim(ymhdte),5,2)||'-'|| left(trim(ymhdte),2) || '-' ||substr(trim(ymhdte),3,2)
        end as date) 
  end as "Hire Date",
(select sum(yhdtgp)
  from rydedata.pyhshdta
  where yhdemp = py.ymempn and yhdeyy = 11) as "YTD Gross",
  round((select sum(yhdtgp)
  from rydedata.pyhshdta
  where yhdemp = py.ymempn and yhdeyy = 11) / float(dayofyear((select date(trim(2000 + yhdeyy) || '-' || trim(yhdemm) || '-' || trim(yhdedd))
 from rydedata.pyhshdta
  where yhdemp = py.ymempn
  order by date(trim(2000 + yhdeyy) || '-' || trim(yhdemm) || '-' || trim(yhdedd)) desc
  fetch first row only)) / 365.0), 2) as "Pace Gross"
from rydedata.pymast as py
where ymco# = 'RY1' 
  and trim(ymdept) in ('06')
  and ymactv <> 'T'
--  and (select sum(yhdtgp) from rydedata.pyhshdta where yhdemp = py.ymempn and yhdeyy = 11) > 0
order by ymdept, ymrate DESC
