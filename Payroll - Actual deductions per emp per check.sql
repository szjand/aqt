/*
Hi Jon  could I  get a payroll report that I could run every month?
The info I would like on it is
Employee name,  department,  and then it would be deduction  fields that come out of each paycheck with the $ amount
( 87, 107, 111, 281, 287, 293, 299, 295, 296, 297, 298, 300, 301, 302)

What I would like  is a spread sheet with a column for each deduction field
No rush.   let me know thanks

I thought of one more field that I would need in that report it is pay period (B) or (S)
*/
Select * from RYDEDATA.PYDEDUCT
where trim(employee_number) = '16425'

select * from rydedata.pypcodes where ytdco# = 'RY1' and ytddcd in ( '87', '107', '111', '281', '287', '293', '299', '295', '296', '297', '298', '300', '301', '302') order by ytddcd

-- paycheck header, doesn't look like i need it
select * from rydedata.pyhshdta
where trim(yhdemp) = '16425'
  and ypbnum_ = 719130

-- this looks like what i need
select * from rydedata.pyhscdta
where trim(yhcemp) = '16425'
  and ypbnum = 719130

-- USE YHCCEA not yhccam
select b.ymname, a.* from rydedata.pyhscdta a left join rydedata.pymast b on trim(a.yhcemp) = trim(ymempn) where yhccam <> yhccea and yhcco# <> 'RY3' 
  AND yhccde in ( '87', '107', '111', '281', '287', '293', '299', '295', '296', '297', '298', '300', '301', '302') order by ypbcyy desc


select yhcemp
  round(coalesce(sum(case when yhccde = '87' then yhccea end),0),2) as "87",
  coalesce(sum(case when yhccde = '107' then yhccea end),0) as "107",
  coalesce(sum(case when yhccde = '111' then yhccea end),0) as "111",
  coalesce(sum(case when yhccde = '281' then yhccea end),0) as "281",
  coalesce(sum(case when yhccde = '287' then yhccea end),0) as "287",  
  coalesce(sum(case when yhccde = '293' then yhccea end),0) as "293",
  coalesce(sum(case when yhccde = '295' then yhccea end),0) as "295",
  coalesce(sum(case when yhccde = '296' then yhccea end),0) as "296",
  coalesce(sum(case when yhccde = '297' then yhccea end),0) as "297",
  coalesce(sum(case when yhccde = '298' then yhccea end),0) as "298",
  coalesce(sum(case when yhccde = '299' then yhccea end),0) as "299",
  coalesce(sum(case when yhccde = '300' then yhccea end),0) as "300",
  coalesce(sum(case when yhccde = '301' then yhccea end),0) as "301",
  coalesce(sum(case when yhccde = '302' then yhccea end),0) as "302"
from rydedata.pyhscdta
where ypbnum = 719130
group by yhcemp

do a join to pyhshdta grouped on ypbnum, ypbcyy, yhdcyy, yhdcmm, yhdcdd to get date of check
select ypbnum, ypbcyy, yhdcyy, yhdcmm, yhdcdd 
from rydedata.pyhshdta 
where ypbcyy = 113 
group by ypbnum, ypbcyy, yhdcyy, yhdcmm, yhdcdd

select d.checkdate, e.ymname, e.ymdept, e.ympper, b.* 
--select e.ymname, e.ymdept, e.ympper, d.checkdate,  
from ( -- actual deduction amounts for each employee/batch
  select a.ypbnum, a.yhcemp, 
    coalesce(sum(case when a.yhccde = '87' then a.yhccea end),0) as "87",
    coalesce(sum(case when a.yhccde = '107' then a.yhccea end),0) as "107",
    coalesce(sum(case when a.yhccde = '111' then a.yhccea end),0) as "111",
    coalesce(sum(case when a.yhccde = '281' then a.yhccea end),0) as "281",
    coalesce(sum(case when a.yhccde = '287' then a.yhccea end),0) as "287",  
    coalesce(sum(case when a.yhccde = '293' then a.yhccea end),0) as "293",
    coalesce(sum(case when a.yhccde = '295' then a.yhccea end),0) as "295",
    coalesce(sum(case when a.yhccde = '296' then a.yhccea end),0) as "296",
    coalesce(sum(case when a.yhccde = '297' then a.yhccea end),0) as "297",
    coalesce(sum(case when a.yhccde = '298' then a.yhccea end),0) as "298",
    coalesce(sum(case when a.yhccde = '299' then a.yhccea end),0) as "299",
    coalesce(sum(case when a.yhccde = '300' then a.yhccea end),0) as "300",
    coalesce(sum(case when a.yhccde = '301' then a.yhccea end),0) as "301",
    coalesce(sum(case when a.yhccde = '302' then a.yhccea end),0) as "302"
  from rydedata.pyhscdta a
  group by a.ypbnum, a.yhcemp) b
inner join ( -- check date for each batch
  select ypbnum, 
    cast (
      case
        when yhdcmm between 1 and 9 then '0'||trim(char(yhdcmm))
        else trim(char(yhdcmm))
      end
      ||'/'||
      case
        when yhdcdd between 1 and 9 then '0'||trim(char(yhdcdd))
        else trim(char(yhdcdd))
      end
      ||'/'||
      case
        when yhdcyy between 1 and 9 then '200'||trim(char(yhdcyy))
        else '20'||trim(char(yhdcyy))
      end as date) as CheckDate
  from (
    select ypbnum, ypbcyy, yhdcyy, yhdcmm, yhdcdd 
    from rydedata.pyhshdta 
    where ypbcyy = 113 
    group by ypbnum, ypbcyy, yhdcyy, yhdcmm, yhdcdd) c ) d on b.ypbnum = b.ypbnum
left join rydedata.pymast e on trim(b.yhcemp) = trim(e.ymempn)
where checkdate > curdate() - 30 day

select yhcseq, count(*) yhcseq from rydedata.pyhscdta group by yhcseq

-- 8/5 way too many rows based on batch, make paycheck the base table



select d.checkdate, e.ymname, e.ymdept, e.ympper, b.*
from (
  select ypbnum, 
    cast (
      case
        when yhdcmm between 1 and 9 then '0'||trim(char(yhdcmm))
        else trim(char(yhdcmm))
      end
      ||'/'||
      case
        when yhdcdd between 1 and 9 then '0'||trim(char(yhdcdd))
        else trim(char(yhdcdd))
      end
      ||'/'||
      case
        when yhdcyy between 1 and 9 then '200'||trim(char(yhdcyy))
        else '20'||trim(char(yhdcyy))
      end as date) as CheckDate
  from (
    select ypbnum, ypbcyy, yhdcyy, yhdcmm, yhdcdd 
    from rydedata.pyhshdta 
    where ypbcyy = 117 
    group by ypbnum, ypbcyy, yhdcyy, yhdcmm, yhdcdd) c) d
left join (
  select a.ypbnum, a.yhcemp, 
    coalesce(sum(case when a.yhccde = '87' then a.yhccea end),0) as "87",
    coalesce(sum(case when a.yhccde = '107' then a.yhccea end),0) as "107",
    coalesce(sum(case when a.yhccde = '111' then a.yhccea end),0) as "111",
    coalesce(sum(case when a.yhccde = '281' then a.yhccea end),0) as "281",
    coalesce(sum(case when a.yhccde = '287' then a.yhccea end),0) as "287",  
    coalesce(sum(case when a.yhccde = '293' then a.yhccea end),0) as "293",
    coalesce(sum(case when a.yhccde = '295' then a.yhccea end),0) as "295",
    coalesce(sum(case when a.yhccde = '296' then a.yhccea end),0) as "296",
    coalesce(sum(case when a.yhccde = '297' then a.yhccea end),0) as "297",
    coalesce(sum(case when a.yhccde = '298' then a.yhccea end),0) as "298",
    coalesce(sum(case when a.yhccde = '299' then a.yhccea end),0) as "299",
    coalesce(sum(case when a.yhccde = '300' then a.yhccea end),0) as "300",
    coalesce(sum(case when a.yhccde = '301' then a.yhccea end),0) as "301",
    coalesce(sum(case when a.yhccde = '302' then a.yhccea end),0) as "302"
  from rydedata.pyhscdta a
  group by a.ypbnum, a.yhcemp) b on d.ypbnum = b.ypbnum
left join rydedata.pymast e on trim(b.yhcemp) = trim(e.ymempn)
where checkdate > curdate() - 60 day


select * from rydedata.sdprdet where trim(ptro#) = '19131062'