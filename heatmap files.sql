Select address,city,state,left(zip, 5), count(*) as value
from heatmap_new
group by address,city,state,left(zip, 5)

select trim(trim(address) + ' ' + trim(city) + ' ' + trim(state) + ' ' + trim(zip)) as address, value
from (
Select address,city,state,left(zip, 5) as zip, count(*) as value
from heatmap_new
group by address,city,state,left(zip, 5)) x

Select left(zip, 5) as zip, count(*) as value
from heatmap_new
group by left(zip, 5)


select 

select storecode, count(*)
from heatmap_new
group by storecode


select trim(trim(address) + ' ' + trim(city) + ' ' + trim(state) + ' ' + trim(zip)) as address, 1
from (
  Select address,city,state,left(zip, 5) as zip
  from heatmap_new
  group by address,city,state,left(zip, 5)) x

select trim(trim(address) + ' ' + trim(city) + ' ' + trim(state) + ' ' + trim(zip)) as address, 1
from (
  Select address,city,state,left(zip, 5) as zip
  from heatmap_used
  group by address,city,state,left(zip, 5)) x

select trim(trim(address) + ' ' + trim(city) + ' ' + trim(state) + ' ' + trim(zip)) as address, 1
from (
  select address,city,state,left(zip, 5) as zip
  from heatmap_all
  where area in ('new','used')
  group by address,city,state,left(zip, 5)) x

select trim(trim(address) + ' ' + trim(city) + ' ' + trim(state) + ' ' + trim(zip)) as address, 1
from (
  select address,city,state,left(zip, 5) as zip
  from heatmap_all
  where area not in ('new','used')
  group by address,city,state,left(zip, 5)) x
  
  
select count(*)
from (
  select address,city,state,left(zip, 5) as zip
  from heatmap_all
  where area not in ('new','used')
    and address not like '%po%'
    and zip <> '0'
    and thedate between '08/01/2014' and '09/30/2014'
  group by address,city,state,left(zip, 5)) x
  
select trim(trim(address) + ' ' + trim(city) + ' ' + trim(state) + ' ' + trim(zip)) as address, 1 as value
from (
  select address,city,state,left(zip, 5) as zip
  from heatmap_all
  where area not in ('new','used')
    and address not like '%po%'
    and zip <> '0'
    and thedate between '08/01/2014' and '09/30/2014'
  group by address,city,state,left(zip, 5)) x
  
  select month(thedate), count(*)
  from heatmap_all
  where area not in ('new','used')
group by month(thedate)
  
    and thedate between '10/01/2014' and curdate()
  
  
  
select count(*)
from (
  select address,city,state,left(zip, 5) as zip, max(month(thedate))
  from heatmap_all
  where area not in ('new','used')
    and address not like '%po%'
    and zip <> '0'
    and thedate between '08/01/2014' and '09/30/2014'
  group by address,city,state,left(zip, 5)) x  
