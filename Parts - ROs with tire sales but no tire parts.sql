/*
Jon- Can you do me a huge favor??  We did tire inventory, and are off quite a bit.  
Is it possible to run a report from Dec 2012 to current that shows the 
Labor Op-  1NT, 2NT, 3NT, 4NT, 5NT, and 6NT, and see if tires are billed out on these particular ROs?  
We are considering the possibility of some rubber not getting billed out
*/
select 
  cast(left(digits(a.ptdate), 4) || '-' || substr(digits(a.ptdate), 5, 2) || '-' || substr(digits(a.ptdate), 7, 2) as date),
  trim(a.ptro#) as ptro, a.ptline, a.ptlopc--, a.ptsvctyp, a.ptlpym
--  b.ptpart, b.ptsgrp, b.ptqty, b.ptnet,
--  d.ptswid
from rydedata.sdprdet a
left join rydedata.pdptdet b on trim(a.ptro#) = trim(b.ptinv#) 
  and a.ptline = b.ptline
left join rydedata.sdprhdr d on trim(a.ptro#) = trim(d.ptro#)
where a.ptdate > 20121130
  and a.ptco# = 'RY1'
  and b.ptinv# is null
  and trim(ptlopc) in ('1NT', '2NT', '3NT', '4NT', '5NT', '6NT')



select *
from rydedata.sdprhdr