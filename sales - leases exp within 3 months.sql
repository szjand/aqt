Select bopmast_stock_number, leasing_source, payment_frequency, lease_term,
  date_approved, date_Capped, delivery_date,
  date_1st_payment, date_last_payment
-- select *
from RYDEDATA.BOPMAST
where sale_type = 'L'
order by date_capped desc



select x.*, date_approved + lease_term month
from (
  Select trim(bopmast_stock_number), lease_term,
    date_approved, date_Capped, delivery_date,
    date_1st_payment, date_last_payment
  -- select *
  from RYDEDATA.BOPMAST
  where sale_type = 'L') x
where date_approved + lease_term month between curdate() and curdate() + 3 month  
order by date_capped desc
  

-- spreadsheet for trent
-- include customer, vehicle, salesman
-- last payment date within 6 months
select x.*
-- select count(*)
from (
  Select trim(bopmast_stock_number) as stocknumber, lease_term,
    date_Capped,
    cast(left(trim(cast(date_last_payment as char(12))), 4) 
      || '-' || substr(trim(cast(date_last_payment as char(12))), 5, 2) 
      || '-' || substr(trim(cast(date_last_payment as char(12))), 7, 2) as date) as LastPaymentDate
  -- select *
  from RYDEDATA.BOPMAST
  where sale_type = 'L'
    and bopmast_company_number = 'RY1'
    and bopmast_stock_number <> '') x
where LastPaymentDate between curdate() and curdate() + 6 month  
order by lastpaymentdate

  