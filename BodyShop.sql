select j.JobID, c.LastName as Customer, ltrim(v.VehicleYear) + ' ' + ltrim(v.VehicleModel) as Vehicle, JobClass, wtf.PromiseDate,
   ScheduleDate, APlus, 
  case when j.StatusCode in (300, 350, 400, 440, 450, 500, 600) then 'Yes' else 'No' end as Arrived,
  case when j.StatusCode in (400, 440, 450, 500, 600) then 'Yes' else 'No' end as Released,
  case when RepairsComplete is not null then 'Yes' else 'No' end as Done,
  j.StatusCode,
  j.EstimatedCost, j.BodyLaborHours, j.PaintLaborHours, j.ActualPaintDate, 
  j.RepairsBegin, j.RepairsComplete, FinalCost, FrameLaborHours, 
  ClosedDate, EstimateDate, CustomerAuthorization, TotalLoss, BluePrintType,
  ProjectedStart, ProjectedCompletion, ProjectedProcessStart, FileHandler, ScheduledEstimate,
(select top 1 ronumber from jobinsurance where jobid = j.jobid order by ronumber desc) as Ronumber
from Job j
join Customer c on j.JobID = c.JobID
join Vehicle v on j.JobID = v.JobID
left outer join (select JobID, max(DateSubmited) DateSubmitted, max(PromiseDate) PromiseDate
from JobPromiseDates
group by JobID) wtf on j.JobID = wtf.JobID
where (j.StatusCode in (100, 200, 300, 350, 400, 440, 450, 500, 600)) 
  or (j.StatusCode not in (300, 350, 400, 440, 450, 500, 600) and datediff(day, coalesce(j.PromiseDate, j.ProjectedDelivery), getdate()) <= 60) 
and j.statuscode <> 1100 
or (StatusCode = 200 and ProjectedDelivery is not null)
order by coalesce(j.PromiseDate, j.ProjectedDelivery)