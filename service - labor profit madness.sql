Select * from RYDEDATA.GLPTRNS where trim(gtdoc#) = '16135551'


select * from rydedata.sdprhdr where trim(ptro#) in ('16135224','16117935','16118311','16119597')

select a.ptro#, ptdate, ptcdat, ptfcdt
from rydedata.sdprhdr a
where ptcdat between 20130519 and 20131102
  and ptco# = 'RY1'
  and exists (
   select 1
   from rydedata.sdprdet 
    where trim(ptro#) = trim(a.ptro#)
    and ptsvctyp in ('AM','MR'))
  and ((
    ptfcdt > 20131102
    or
    ptfcdt < 20130519)
    or 
    (ptcdat < 20130519 and ptfcdt between 20130519 and 20131102)) 


select a.ptro#, ptdate, ptcdat, ptfcdt
from rydedata.sdprhdr a
where ptcdat between 20130519 and 20131102
  and ptco# = 'RY1'
  and exists (
   select 1
   from rydedata.sdprdet 
    where trim(ptro#) = trim(a.ptro#)
    and ptsvctyp in ('AM','MR'))
  and ptcdat < 20130519 
  and ptfcdt between 20130519 and 20131102


select * from rydedata.sdprhdr where trim(ptro#) = '16119597'

select * from rydedata.sdprdet where trim(ptro#) = '16130131' and pttech = '577'
select * from rydedata.sdprhdr where trim(ptro#) = '16130131'


select * from rydedata.sdprhdr where trim(ptvin) = '1GNDT13S922354450' order by ptdate


SELECT *
FROM rydedata.SDPXTIM  
WHERE trim(ptro#) = '16130849'

select * from rydedata.sdprdet where ptdate > 20130611 and ptpadj <> ''

select *
from rydedata.glptrns
where trim(gtdoc#) = '16130131'
order by gtacct




select ptline, ptcode, count(*)
from rydedata.sdprdet
where ptdbas = 'V'
  and ptdate > 20130000
group by  ptline, ptcode

select ptltyp, ptcode, ptpadj, ptdbas, count(*)
from rydedata.sdprdet
group by ptltyp, ptcode, ptpadj, ptdbas 

select ptpadj, ptdbas, count(*)
from rydedata.sdprdet
group by ptpadj, ptdbas 


select ptro#, ptline, ptltyp, ptcode, pttech, ptlhrs
from rydedata.sdprdet
where ptpadj = 'X'
  and ptdbas = 'V'
  and ptco# = 'RY1'
  and ptdate > 20121231

select 'XTIM' as source, z.*
from (
  select ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, sum(ptlhrs) as hours
  from rydedata.sdpxtim
  --where ptco# = 'RY1' 
  group by ptco#, ptro#, ptline, pttech) z
where hours <> 0

select *
from rydedata.sdprdet
where ptco# = 'RY1'
  and ptdate > 20121231
  and ptpadj <> ''

select ptltyp, ptcode, count(*)
from rydedata.sdprdet
where ptlhrs <> 0
group by ptltyp, ptcode

select ptltyp, ptcode, ptdbas, count(*)
from rydedata.sdprdet
where ptlhrs <> 0
group by ptltyp, ptcode, ptdbas

select  'ADJ' as source, a.*
from (
  select cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date),
    ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, sum(ptlhrs) as hours
  from rydedata.sdprdet
  --where ptpadj = 'X'
  where ptpadj <> ''
--    and ptdbas = 'V'
--    and ptco# = 'RY1'
    and ptdate > 20111231
    and ptltyp = 'L'
    and ptcode = 'TT'
  group by cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date), ptco#, ptro#, ptline, pttech) a
where hours <> 0


problems
only one for october
ro 16134106 tech 577, adj hours shows 9.8 but lab/prof does not
select *
from rydedata.sdprdet
where trim(ptro#) in ('16134106','16130131')
order by ptro#, ptline

jul-sep
select *
from rydedata.sdprdet
where trim(ptro#) = '19147659' and pttech = '536'

-- neg does not seem to be accurate
select 'NEG' as source, ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, ptlhrs as hours
from rydedata.sdprdet
where ptdate > 20111231
  and ptlhrs < 0

select *
from rydedata.sdprdet
where trim(ptro#) = '16132044'
and pttech = '566'

select *
from rydedata.sdpxtim
where trim(ptro#) = '16132044'


select *
from rydedata.sdprdet
where ptdbas = 'V'
  AND ptdate > 20121231
  and pttech = '566'

select *
from rydedata.sdprdet
where ptdate > 20111231
  and ptlhrs < 0  
  and ptdbas <> 'V'
  and pttech = '566'

--ok, let us try this
select 'NEG' as source,  cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date),
ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, ptlhrs as hours
from rydedata.sdprdet
where ptdate > 20111231
  and ptlhrs < 0
  and ptdbas <> 'V'




select count(*) from (
select 'XTIM' as source, z.*
from (
  select ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, sum(ptlhrs) as hours
  from rydedata.sdpxtim
  --where ptco# = 'RY1' 
  group by ptco#, ptro#, ptline, pttech) z
where hours <> 0
union 
select  'ADJ' as source, a.*
from (
  select ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, sum(ptlhrs) as hours
  from rydedata.sdprdet
  --where ptpadj = 'X'
  where ptpadj <> ''
--    and ptdbas = 'V'
--    and ptco# = 'RY1'
    and ptdate > 20111231
    and ptltyp = 'L'
    and ptcode = 'TT'
  group by ptco#, ptro#, ptline, pttech) a
where hours <> 0
union 
--ok, let us try this
select 'NEG' as source, ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, ptlhrs as hours
from rydedata.sdprdet
where ptdate > 20111231
  and ptlhrs < 0
  and ptdbas <> 'V'
) x



-- recreate sep labor prof
select a.ptco#, a.ptro#, a. ptcdat, a.ptfcdt, b.ptline, b.pttech, c.stname, b.ptlhrs, b.ptpadj, b.ptdbas
from rydedata.sdprhdr a
inner join rydedata.sdprdet b on trim(a.ptro#) = trim(b.ptro#)
left join rydedata.sdptech c on a.ptco# = c.stco# 
  and b.pttech = c.sttech
where a.ptfcdt between 20130901 and 20130930
  and exists (
    select 1
    from rydedata.sdprdet
    where trim(ptro#) = trim(a.ptro#) 
      and ptline = b.ptline
      and ptsvctyp in ('AM','MR'))
  and b.ptlhrs <> 0
  and a.ptco# = 'RY1'
  and b.ptcode = 'TT'
  and b.ptltyp = 'L'
--and b.ptpadj <> ''
--and left(trim(a.ptro#),2) = '18'


select stname, sum(ptlhrs)
from (
  select a.ptco#, a.ptro#, a. ptcdat, a.ptfcdt, b.ptline, b.pttech, c.stname, b.ptlhrs, b.ptpadj, b.ptdbas
  from rydedata.sdprhdr a
  inner join rydedata.sdprdet b on trim(a.ptro#) = trim(b.ptro#)
  left join rydedata.sdptech c on a.ptco# = c.stco# 
    and b.pttech = c.sttech
  where a.ptfcdt between 20130901 and 20130930
    and exists (
      select 1
      from rydedata.sdprdet
      where trim(ptro#) = trim(a.ptro#) 
        and ptline = b.ptline
        and ptsvctyp in ('AM','MR'))
    and b.ptlhrs <> 0
    and a.ptco# = 'RY1'
    and b.ptcode = 'TT'
    and b.ptltyp = 'L') w
group by stname
order by stname

select f.*, coalesce(g.hours, 0) as xHours from (
  select a.ptco#, a.ptro#, a.ptcdat, a.ptfcdt, b.ptline, b.pttech, c.stname, b.ptlhrs, b.ptpadj, b.ptdbas
  from rydedata.sdprhdr a
  inner join rydedata.sdprdet b on trim(a.ptro#) = trim(b.ptro#)
  left join rydedata.sdptech c on a.ptco# = c.stco# 
    and b.pttech = c.sttech
  where a.ptfcdt between 20130901 and 20130930
    and exists (
      select 1
      from rydedata.sdprdet
      where trim(ptro#) = trim(a.ptro#) 
        and ptline = b.ptline
        and ptsvctyp in ('AM','MR'))
    and b.ptlhrs <> 0
    and a.ptco# = 'RY1'
    and b.ptcode = 'TT'
    and b.ptltyp = 'L') f
  left join (
    select ptco#, trim(ptro#) as ptro#, pttech, sum(ptlhrs) as hours
    from rydedata.sdpxtim
    --where ptco# = 'RY1' 
    group by ptco#, ptro#, pttech) g on f.ptco# = g.ptco# and trim(f.ptro#) = trim(g.ptro#) and f.pttech = g.pttech
where f.pttech = '574'
order by f.ptro#


select * 
from rydedata.sdpxtim
where trim(ptro#) = '16128998'

    select ptco#, trim(ptro#) as ptro#, pttech, sum(ptlhrs) as hours
    from rydedata.sdpxtim
    where ptdate between 20130901 and 20130903
    group by ptco#, ptro#, pttech



select gtdoc#, gttrn#, gtseq#, gtpost, gtjrnl, gtdate, gtrdate, gtacct, gtdesc, gttamt 
-- select *
from rydedata.glptrns where trim(gtdoc#) = '16119570' 
and gtacct in (
'146000',    
'146001',    
'146007',    
'146100',    
'146101',    
'146201',    
'146202',    
'146204',    
'146301',    
'146304',    
'146305',    
'146320',    
'146401',    
'146402',    
'146404')
order by gtacct, gtdate


select * from rydedata.sdprhdr where trim(ptro#) = '16129010'

select * from rydedata.sdprdet where trim(ptro#) = '19151671'

select * from rydedata.glptrns where trim(gtdoc#) = '16127601' order by gtacct, gtdate

select * from rydedata.glptrns where trim(gtdoc#) = '16116971' order by gtacct, gtdate

select pttech, sum(ptlhrs) from rydedata.sdprdet where trim(ptro#) = '16116971' and ptcode = 'TT' and ptltyp = 'L' group by pttech

select * from rydedata.sdprhdr where trim(ptro#) = '16129935'

select * from rydedata.sdprdet where trim(ptro#) = '16135402' and ptcode = 'TT' and ptltyp = 'L' order by pttech

select sum(ptlamt) from rydedata.sdprdet where trim(ptro#) = '16133579' and ptcode = 'TT' and ptltyp = 'L' 

select * from rydedata.sdpxtim where trim(ptro#) = '16129010'

select gtdoc#, gttrn#, gtseq#, gtpost, gtjrnl, gtdate, gtacct, gtdesc, gttamt, b.*
from rydedata.glptrns a
left join rydedata.GLPDTIM b on a.gttrn# = gqtrn# 
where trim(gtdoc#) = '16136626'  /*and trim(gtacct) = '146000'*/ order by gtacct, gtdate


select spsvctyp, splpym, spfran, spsvco, spclac, sptlac
from rydedata.sdpprice
where spco# = 'RY1'
  and spsvctyp in ('AM','MR')
order by spclac


-- car and truck accounts are the same
select *
from (
select spsvctyp, splpym, spfran, spsvco, spclac as account
from rydedata.sdpprice
where spco# = 'RY1'
  and spsvctyp in ('AM','MR'))a
inner join (
select spsvctyp, splpym, spfran, spsvco, sptlac as account
from rydedata.sdpprice
where spco# = 'RY1'
  and spsvctyp in ('AM','MR')) b on a.spsvctyp = b.spsvctyp and a.splpym = b.splpym and a.spfran = b.spfran and a.spsvco = b.spsvco and a.account <> b.account


select spsvctyp, splpym, spfran, spsvco, spclac
from rydedata.sdpprice
where spco# = 'RY1'
  and spsvctyp in ('AM','MR')


select spsvctyp, splpym, spclac
from rydedata.sdpprice
where spco# = 'RY1'
  and spsvctyp in ('AM','MR','EM')
  and trim(spclac) not in ('146306','BLANK')
group by spsvctyp, splpym, spclac
order by spsvctyp, splpym, spclac
  
-- this accoun last used 10/2009
select * 
from rydedata.glptrns
where trim(gtacct) = '146305'




SELECT * FROM rydedata.glptrns a left join rydedata.GLPDTIM b on a.gttrn# = gqtrn# where trim(gtdoc#) = '16134547' ORDER BY gtacct



select * from rydedata.sdprdet where trim(ptro#) = '16106442'

