--BOPNAME
select bntype, count(*)
from bopname
group by bntype

--BOPVREF
select bvtype, count(*)
from rydedata.bopvref
group by bvtype

--INPMAST
select imtype, count(*)
from inpmast
group by imtype

select imstat, count(*)
from inpmast
group by imstat /*Status*/

-- GLPDTIM  Date/Timestamp for Transaction Apply
select gqfunc, count(*)
from glpdtim
group by gqfunc /*Apply Function*/

-- GLPTRNS
select 'GTDTYP' as Field, 'Doc Type' as "Field Def", gtdtyp as Value, count(*) as Count
from rydedata.glptrns
where gtco# in ('RY1','RY2','RY3')
group by gtdtyp
union
select 'GTTYPE' as Field, 'Record Type' as "Field Def", gttype as Value, count(*) as Count
from rydedata.glptrns
where gtco# in ('RY1','RY2','RY3')
group by gttype
union
select 'GTPOST' as Field, 'Post Status' as "Field Def", gtpost as Value, count(*) as Count
from rydedata.glptrns
where gtco# in ('RY1','RY2','RY3')
group by gtpost
union
select 'GTRDTYP' as Field, 'Recon Doc Type' as "Field Def", gtrdtyp as Value, count(*) as Count
from rydedata.glptrns
where gtco# in ('RY1','RY2','RY3')
group by gtrdtyp
order by Field, Value


-- BOPMAST
select 'BMSTAT' as Field, 'Record Status' as "Field Def", bmstat as Value, count(*) as Count
from bopmast
where bmco# in ('RY1','RY2','RY3')
group by bmstat
union
select 'BMTYPE' as Field, 'Record Type' as "Field Def", bmtype as Value, count(*) as Count
from bopmast
where bmco# in ('RY1','RY2','RY3')
group by bmtype
union
select 'BMWHSL' as Field, 'Sale Type' as "Field Def", BMWHSL as Value, count(*) as Count
from bopmast
where bmco# in ('RY1','RY2','RY3')
group by BMWHSL
union
select 'BMVTYP' as Field, 'Vehicle Type' as "Field Def", BMVTYP as Value, count(*) as Count
from bopmast
where bmco# in ('RY1','RY2','RY3')
group by BMVTYP
order by Field, Value

-- INPMAST
select 'IMSTAT' as Field, 'Status' as "Field Def", IMSTAT as Value, count(*) as Count
from INPMAST
where IMCO# in ('RY1','RY2','RY3')
group by IMSTAT
union
select 'IMGTRN' as Field, 'G/L Applied' as "Field Def", IMGTRN as Value, count(*) as Count
from INPMAST
where IMCO# in ('RY1','RY2','RY3')
group by IMGTRN
union
select 'IMOSTAT' as Field, 'Ordered Status' as "Field Def", IMOSTAT as Value, count(*) as Count
from INPMAST
where IMCO# in ('RY1','RY2','RY3')
group by IMOSTAT
union
select 'IMSRC' as Field, 'Sale' as "Field Def", IMSRC as Value, count(*) as Count
from INPMAST
where IMCO# in ('RY1','RY2','RY3')
group by IMSRC
order by Field, Value





