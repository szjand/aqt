-- basic concept
Select gtctl#, in.imstat, in.imtype, sum(gttamt) as Cost
from glptrns gl
  left join inpmast in on gl.gtctl# = in.imstk#
where trim(gtacct) in ('123000', '123100', '123101', '123105', '123700', '123701', '123705', '123706')
  and in.imstat = 'I' -- Inventory
  and in.imtype = 'N' -- New
  and gl.gtpost <> 'V' -- Void
group by gtctl#, in.imstat, in.imtype
having sum(gttamt) = 0
order by gtctl#

-- Inventory Accounts
select distinct gmacct, gmdesc
from glpmast
where gmdesc like 'INV%'
and gmdesc not like '%PARTS%'
and gmdesc not like '%GAS%'
and gmdesc not like '%PAINT%'
and gmdesc not like '%SUBLET%'
and gmdesc not like '%WIP%'
and gmdesc not like 'INVEST%'
and gmdesc not like '%OTHER AUTOMOTIVE%'
and gmdesc not like '%ACCESS%'
and gmdesc not like '%PROCESS%'
and gmdesc not like '%MISC%'
and gmdesc not like '%TIRES%'
and gmdesc not like '%G.O%'
and trim(gmdesc) <> 'INV-OTHER'
order by gmacct

-- basic concept, with all inventory accounts
Select gtctl#, in.imstat, in.imtype, sum(gttamt) as Cost
from glptrns gl
left join inpmast in on gl.gtctl# = in.imstk#
where trim(gtacct) in (
  select distinct gmacct
  from glpmast
  where gmdesc like 'INV%'
  and gmdesc not like '%PARTS%'
  and gmdesc not like '%GAS%'
  and gmdesc not like '%PAINT%'
  and gmdesc not like '%SUBLET%'
  and gmdesc not like '%WIP%'
  and gmdesc not like 'INVEST%'
  and gmdesc not like '%OTHER AUTOMOTIVE%'
  and gmdesc not like '%ACCESS%'
  and gmdesc not like '%PROCESS%'
  and gmdesc not like '%MISC%'
  and gmdesc not like '%TIRES%'
  and gmdesc not like '%G.O%'
  and trim(gmdesc) <> 'INV-OTHER')
and in.imstat = 'I' -- Inventory
and in.imtype = 'N' -- New
and gl.gtpost <> 'V' -- Void
group by gtctl#, in.imstat, in.imtype
having sum(gttamt) = 0
order by trim(gtctl#)
  
-- imkey (BOPNAME Key) shows 0
select imvin, imkey
from inpmast
where trim(imstk#) = '98928'

select *
from bopname
where bnsnam like 'DEALER%'

-- but it appears that a record was created in bopvref
select *
from bopvref
where trim(bvvin) = '3GNFK22039G249602'


-- include all stores, new and used 252 vehicles
Select trim(gl.gtctl#), max(i.imvin), i.imstat, i.imtype, sum(gttamt) as Cost, max(gl.gtacct)
from glptrns gl
left join inpmast i on gl.gtctl# = i.imstk#
where trim(gl.gtacct) in ('123000', '123100', '123101', '123103', '123104', '123105', '123700', '123701', '123703', '123704', '123705', '123706', -- RY1 New
                       '124000', '124100',  -- RY1 Used
                       '223000', '223100', '223105', '223700', -- RY2 New
                       '224000', '224005', '224006', '224100', '224105', '224106', -- RY2 Used 
                       '323101', '323102', '323701', '323702', '323703', -- RY3 New
                       '324100', '324100') --RY3 Used 
and i.imstat = 'I' -- Inventory
--  and i.imtype = 'N' -- New
and gl.gtpost <> 'V' -- Void
group by gtctl#, i.imstat, i.imtype
having sum(gttamt) = 0
order by trim(gtctl#)

-- are there deals on these cars?
-- yep, 4 of them
select wtf.*, b.bmkey
from (
  Select gl.gtctl#, i.imstat, i.imtype, sum(gttamt) as Cost
  from glptrns gl
  left join inpmast i on gl.gtctl# = i.imstk#
  where trim(gtacct) in ('123000', '123100', '123101', '123103', '123104', '123105', '123700', '123701', '123703', '123704', '123705', '123706', -- RY1 NC
                         '124000', '124100',  -- RY1 Used
                         '223000', '223100', '223105', '223700', -- RY2 New
                         '224000', '224005', '224006', '224100', '224105', '224106', -- RY2 Used 
                         '323101', '323102', '323701', '323702', '323703', -- RY3 New
                         '324100', '324100') --RY3 Used 
  and i.imstat = 'I' -- Inventory
  --  and i.imtype = 'N' -- New
  and gl.gtpost <> 'V' -- Void
  group by gtctl#, i.imstat, i.imtype
  having sum(gttamt) = 0) wtf
left join bopmast b on b.bmstk# = wtf.gtctl#
where bmkey is not null  


-- are there deals on these cars? yep, 4 of them
-- what about current owners from bopvref? -- yep 2, neither of which have deals
select wtf.gtctl# as StockNumber, wtf.vin, b.bmkey, bv.bvkey, bv.bvsdat, bv.bvedat
from (
  Select gl.gtctl#, max(i.imvin) as VIN, i.imstat, i.imtype, sum(gttamt) as Cost
  from glptrns gl
  left join inpmast i on gl.gtctl# = i.imstk#
  where trim(gtacct) in ('123000', '123100', '123101', '123103', '123104', '123105', '123700', '123701', '123703', '123704', '123705', '123706', -- RY1 NC
                         '124000', '124100',  -- RY1 Used
                         '223000', '223100', '223105', '223700', -- RY2 New
                         '224000', '224005', '224006', '224100', '224105', '224106', -- RY2 Used 
                         '323101', '323102', '323701', '323702', '323703', -- RY3 New
                         '324100', '324100') --RY3 Used 
  and i.imstat = 'I' -- Inventory
  --  and i.imtype = 'N' -- New
  and gl.gtpost <> 'V' -- Void
  group by gtctl#, i.imstat, i.imtype
  having sum(gttamt) = 0) wtf
left join bopmast b on b.bmstk# = wtf.gtctl#
left join bopvref bv on bv.bvvin = wtf.vin
where bv.bvedat = 0

-- what about current owners from bopvref? -- yep 2, neither of which have deals
-- so who are those 2 current owners
select wtf.gtctl# as StockNumber, wtf.vin, b.bmkey, bv.bvkey, bv.bvsdat, bv.bvedat, bn.bnsnam
from (
  Select gl.gtctl#, max(i.imvin) as VIN, i.imstat, i.imtype, sum(gttamt) as Cost
  from glptrns gl
  left join inpmast i on gl.gtctl# = i.imstk#
  where trim(gtacct) in ('123000', '123100', '123101', '123103', '123104', '123105', '123700', '123701', '123703', '123704', '123705', '123706', -- RY1 NC
                         '124000', '124100',  -- RY1 Used
                         '223000', '223100', '223105', '223700', -- RY2 New
                         '224000', '224005', '224006', '224100', '224105', '224106', -- RY2 Used 
                         '323101', '323102', '323701', '323702', '323703', -- RY3 New
                         '324100', '324100') --RY3 Used 
  and i.imstat = 'I' -- Inventory
  --  and i.imtype = 'N' -- New
  and gl.gtpost <> 'V' -- Void
  group by gtctl#, i.imstat, i.imtype
  having sum(gttamt) = 0) wtf
left join bopmast b on b.bmstk# = wtf.gtctl#
left join bopvref bv on bv.bvvin = wtf.vin
left join bopname bn on bn.bnkey = bv.bvkey
where bv.bvedat = 0

-- List for Marlee 1/18/2010
Select gl.gtctl# as Stock#, max(i.imvin) as VIN, max(i.imyear) as Year, max(i.immake) as Make, max(immodl) as Model
from rydedata.glptrns gl
left join rydedata.inpmast i on gl.gtctl# = i.imstk#
left join rydedata.bopmast b on b.bmstk# = gl.gtctl#
where trim(gtacct) in ('123000', '123100', '123101', '123103', '123104', '123105', '123700', '123701', '123703', '123704', '123705', '123706', -- RY1 NC
                       '124000', '124100',  -- RY1 Used
                       '223000', '223100', '223105', '223700', -- RY2 New
                       '224000', '224005', '224006', '224100', '224105', '224106', -- RY2 Used 
                       '323101', '323102', '323701', '323702', '323703', -- RY3 New
                       '324100', '324100') --RY3 Used 
and i.imstat = 'I' -- Inventory
--  and i.imtype = 'N' -- New
and gl.gtpost <> 'V' -- Void
and b.bmkey is null -- leave out records for which there is a car deal
group by gtctl#, i.imstat, i.imtype
having sum(gttamt) = 0