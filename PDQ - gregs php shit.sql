-- PDQDetailsAll
select substr(digits(a.PTDATE), 5, 2) || '/' || substr(digits(a.PTDATE), 7, 2)  || '/' || left(digits(a.PTDATE), 4) as TheDate,
  a.ptco# as StoreCode,
  sum(case when a.ptco# = 'RY1' and ptlopc in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDEL1', 'PDQDP') then 1
           when a.ptco# = 'RY2' and ptlopc in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S') then 1
      else 0 end) as OilChanges,
  sum(case when a.ptco# = 'RY1' and ptlopc in ('PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQDP') then 1
           when a.ptco# = 'RY2' and ptlopc in ('LOFD', 'LOF0205', 'MSS', 'M1S') then 1
      else 0 end) as BOCs,
  sum(case when a.ptco# = 'RY1' and ptlopc in ('13A') then 1
           when a.ptco# = 'RY2' and ptlopc in ('RAF') then 1
      else 0 end) as AirFilters,
  sum(case when a.ptco# in 'RY1' and ptlopc in ('ROT', 'FREEROT', 'ROTEMP') then 1
           when a.ptco# in 'RY2' and ptlopc in ('FROT', 'ROT') then 1
      else 0 end) as Rotates,
  sum(case when ptlopc in ('NT2', 'NT4') then 1 else 0 end) as Tires
from rydedata.sdprhdr a
left join rydedata.sdprdet b on a.ptco# = b.ptco# and a.ptro# = b.ptro#
where a.ptro# in
   (select distinct b.ptro#
  from rydedata.sdprdet a
  left join rydedata.sdprhdr b on a.ptco# = b.ptco# and a.ptro# = b.ptro#
    where
      (((a.ptco# = 'RY1') and (ptlopc in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDP', '13A', 'ROT', 'FREEROT', 'ROTEMP', 'NT2', 'NT4'))
        )
       or
       ((a.ptco# = 'RY2') and (ptlopc in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S', 'LOFD', 'LOF0205', 'MSS', 'M1S', 'FROT', 'ROT', 'RAF', 'NT2', 'NT4'))
        ))
    and b.ptdate >= 20110101)
group by a.ptdate, a.ptco#
order by a.ptdate desc, a.ptco#

-- PDQDetailsMonth
select substr(digits(a.PTDATE), 5, 2) || '/' || substr(digits(a.PTDATE), 7, 2)  || '/' || left(digits(a.PTDATE), 4) as TheDate,
  a.ptco# as StoreCode,
  sum(case when a.ptco# = 'RY1' and ptlopc in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDEL1', 'PDQDP') then 1
           when a.ptco# = 'RY2' and ptlopc in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S') then 1
      else 0 end) as OilChanges,
  sum(case when a.ptco# = 'RY1' and ptlopc in ('PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQDP') then 1
           when a.ptco# = 'RY2' and ptlopc in ('LOFD', 'LOF0205', 'MSS', 'M1S') then 1
      else 0 end) as BOCs,
  sum(case when a.ptco# = 'RY1' and ptlopc in ('13A') then 1
           when a.ptco# = 'RY2' and ptlopc in ('RAF') then 1
      else 0 end) as AirFilters,
  sum(case when a.ptco# in 'RY1' and ptlopc in ('ROT', 'FREEROT', 'ROTEMP') then 1
           when a.ptco# in 'RY2' and ptlopc in ('FROT', 'ROT') then 1
      else 0 end) as Rotates,
  sum(case when ptlopc in ('NT2', 'NT4') then 1 else 0 end) as Tires
from rydedata.sdprhdr a
left join rydedata.sdprdet b on a.ptco# = b.ptco# and a.ptro# = b.ptro#
where a.ptro# in
   (select distinct b.ptro#
  from rydedata.sdprdet a
  left join rydedata.sdprhdr b on a.ptco# = b.ptco# and a.ptro# = b.ptro#
    where
      (((a.ptco# = 'RY1') and (ptlopc in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDP', '13A', 'ROT', 'FREEROT', 'ROTEMP', 'NT2', 'NT4'))
        )
       or
       ((a.ptco# = 'RY2') and (ptlopc in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S', 'LOFD', 'LOF0205', 'MSS', 'M1S', 'FROT', 'ROT', 'RAF', 'NT2', 'NT4'))
        ))
    and b.ptdate >= year(curdate() - 45 days) * 10000 + (Month(curdate() - 45 days) * 100) + dayofmonth(curdate() - 45 days))
group by a.ptdate, a.ptco#
order by a.ptdate desc, a.ptco#

-- PDQDetailsToday
select substr(digits(a.PTDATE), 5, 2) || '/' || substr(digits(a.PTDATE), 7, 2)  || '/' || left(digits(a.PTDATE), 4) as TheDate,
  a.ptco# as StoreCode,
  sum(case when a.ptco# = 'RY1' and ptlopc in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDEL1', 'PDQDP') then 1
           when a.ptco# = 'RY2' and ptlopc in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S') then 1
      else 0 end) as OilChanges,
  sum(case when a.ptco# = 'RY1' and ptlopc in ('PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQDP') then 1
           when a.ptco# = 'RY2' and ptlopc in ('LOFD', 'LOF0205', 'MSS', 'M1S') then 1
      else 0 end) as BOCs,
  sum(case when a.ptco# = 'RY1' and ptlopc in ('13A') then 1
           when a.ptco# = 'RY2' and ptlopc in ('RAF') then 1
      else 0 end) as AirFilters,
  sum(case when a.ptco# in 'RY1' and ptlopc in ('ROT', 'FREEROT', 'ROTEMP') then 1
           when a.ptco# in 'RY2' and ptlopc in ('FROT', 'ROT') then 1
      else 0 end) as Rotates,
  sum(case when ptlopc in ('NT2', 'NT4') then 1 else 0 end) as Tires
from rydedata.sdprhdr a
left join rydedata.sdprdet b on a.ptco# = b.ptco# and a.ptro# = b.ptro#
where a.ptro# in
   (select distinct b.ptro#
  from rydedata.sdprdet a
  left join rydedata.sdprhdr b on a.ptco# = b.ptco# and a.ptro# = b.ptro#
    where
      (((a.ptco# = 'RY1') and (ptlopc in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDP', '13A', 'ROT', 'FREEROT', 'ROTEMP', 'NT2', 'NT4'))
        )
       or
       ((a.ptco# = 'RY2') and (ptlopc in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S', 'LOFD', 'LOF0205', 'MSS', 'M1S', 'FROT', 'ROT', 'RAF', 'NT2', 'NT4'))
       ))
    and b.ptdate >= year(curdate()) * 10000 + (Month(curdate()) * 100) + dayofmonth(curdate()))
group by a.ptdate, a.ptco#
order by a.ptdate desc, a.ptco#

-- PDQTicketsAll
select char(gtdate, USA) as TheDate, ptco# as StoreCode, count(distinct gtctl#) as Tickets, sum(case when gmtype = 4 then gttamt else 0 end)* -1 as Sales,
  sum(case when gmtype = 4 then gttamt else 0 end) * -1 - sum(case when gmtype = 5 then gttamt else 0 end) as Gross,
  case when count(distinct gtctl#) <> 0 then (sum(case when gmtype = 4 then gttamt else 0 end)* -1) / count(distinct gtctl#) else 0 end as AvgTicket 
from rydedata.glptrns a
left join rydedata.glpmast b on a.gtacct = b.gmacct and gmyear = year(gtdate)
inner join rydedata.sdprhdr c on ptcdat <> 0 and a.gtctl# = c.ptro# and a.gtdate = substr(digits(c.PTCDAT), 5, 2) || '/' || substr(digits(c.PTCDAT), 7, 2)  || '/' || left(digits(c.PTCDAT), 4) and ptco# in ('RY1', 'RY2')
where gtctl# in
  (select distinct b.ptro#
  from rydedata.sdprdet a
  left join rydedata.sdprhdr b on a.ptco# = b.ptco# and a.ptro# = b.ptro#
  where
      (((a.ptco# = 'RY1') and (ptlopc in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDP', '13A', 'ROT', 'FREEROT', 'ROTEMP', 'NT2', 'NT4'))
        )
       or
       ((a.ptco# = 'RY2') and (ptlopc in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S', 'LOFD', 'LOF0205', 'MSS', 'M1S', 'FROT', 'ROT', 'RAF', 'NT2', 'NT4'))
       ))
    and b.ptdate >= 20110101
  )
  and gmtype not in ('1', '2') 
  and gtdate >= '1/1/2011'
group by gtdate, ptco#

-- PDQTicketsMonth
select char(gtdate, USA) as TheDate, ptco# as StoreCode, count(distinct gtctl#) as Tickets, sum(case when gmtype = 4 then gttamt else 0 end)* -1 as Sales,
  sum(case when gmtype = 4 then gttamt else 0 end) * -1 - sum(case when gmtype = 5 then gttamt else 0 end) as Gross,
  case when count(distinct gtctl#) <> 0 then (sum(case when gmtype = 4 then gttamt else 0 end)* -1) / count(distinct gtctl#) else 0 end as AvgTicket 
from rydedata.glptrns a
left join rydedata.glpmast b on a.gtacct = b.gmacct and gmyear = year(gtdate)
inner join rydedata.sdprhdr c on ptcdat <> 0 and a.gtctl# = c.ptro# and a.gtdate = substr(digits(c.PTCDAT), 5, 2) || '/' || substr(digits(c.PTCDAT), 7, 2)  || '/' || left(digits(c.PTCDAT), 4) and ptco# in ('RY1', 'RY2')
where gtctl# in
  (select distinct b.ptro#
  from rydedata.sdprdet a
  left join rydedata.sdprhdr b on a.ptco# = b.ptco# and a.ptro# = b.ptro#
  where
      (((a.ptco# = 'RY1') and (ptlopc in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDP', '13A', 'ROT', 'FREEROT', 'ROTEMP', 'NT2', 'NT4'))
        )
       or
       ((a.ptco# = 'RY2') and (ptlopc in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S', 'LOFD', 'LOF0205', 'MSS', 'M1S', 'FROT', 'ROT', 'RAF', 'NT2', 'NT4'))
       ))
    and b.ptdate >= year(curdate() - 45 days) * 10000 + (Month(curdate() - 45 days) * 100) + dayofmonth(curdate() - 45 days)
  )
  and gmtype not in ('1', '2') 
  and gtdate >= curdate() - 45 days
group by gtdate, ptco#


-- PDQTicketsToday
select char(gtdate, USA) as TheDate, ptco# as StoreCode, count(distinct gtctl#) as Tickets, sum(case when gmtype = 4 then gttamt else 0 end)* -1 as Sales,
  sum(case when gmtype = 4 then gttamt else 0 end) * -1 - sum(case when gmtype = 5 then gttamt else 0 end) as Gross,
  case when count(distinct gtctl#) <> 0 then (sum(case when gmtype = 4 then gttamt else 0 end)* -1) / count(distinct gtctl#) else 0 end as AvgTicket 
from rydedata.glptrns a
left join rydedata.glpmast b on a.gtacct = b.gmacct and gmyear = year(gtdate)
inner join rydedata.sdprhdr c on ptcdat <> 0 and a.gtctl# = c.ptro# and a.gtdate = substr(digits(c.PTCDAT), 5, 2) || '/' || substr(digits(c.PTCDAT), 7, 2)  || '/' || left(digits(c.PTCDAT), 4) and ptco# in ('RY1', 'RY2')
where gtctl# in
  (select distinct b.ptro#
  from rydedata.sdprdet a
  left join rydedata.sdprhdr b on a.ptco# = b.ptco# and a.ptro# = b.ptro#
  where
      (((a.ptco# = 'RY1') and (ptlopc in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDP', '13A', 'ROT', 'FREEROT', 'ROTEMP', 'NT2', 'NT4'))
        )
       or
       ((a.ptco# = 'RY2') and (ptlopc in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S', 'LOFD', 'LOF0205', 'MSS', 'M1S', 'FROT', 'ROT', 'RAF', 'NT2', 'NT4'))
       ))
    and b.ptdate >= year(curdate() - 45 days) * 10000 + (Month(curdate() - 45 days) * 100) + dayofmonth(curdate() - 45 days)
  )
  and gmtype not in ('1', '2') 
  and gtdate >= curdate() - 45 days
group by gtdate, ptco#
