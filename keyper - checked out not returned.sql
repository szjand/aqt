
-- 9/21/17
-- transaction_date: 2017-09-21 12:22:25.937, this was run on 9/21 @7:22 AM
-- looks like transactin_Date is UTC

-- duh, default value of asset_transactions.trasaction_date & asset.created_date, updated_date, checkout_date is getutcdate()
-- kc wants, for ashley, list of keys checked out after 5PM previous day and not returned 
select *
from dbo.asset_transactions 
order by transaction_Date desc

select message, count(*)
from dbo.asset_transactions
group by message

select *
from dbo.asset_transactions 
where message = 'Removed asset|TAKE TO KELLY F'

table system
system_id: 27
name: KEY-Used-Car

select message, count(*)
from dbo.asset_transactions
where system_id = 27
group by message

select description, count(*)
from dbo.user_transactions group by description

select top 100 c.name, c.asset_status, b.last_name, b.first_name, a.*, dateadd(hour, -5, a.transaction_date)
from dbo.asset_Transactions a
left join dbo.[user] b on a.user_id = b.user_id 
left join dbo.asset c on a.asset_id = c.asset_id
where a.system_id = 27
  and a.asset_status = 'Out' -- removed
  and c.asset_Status = 'Out' -- and not yet returned
  --and a.asset_id = 44098
  and c.name is not null
order by a.transaction_date desc

select *
from dbo.asset_transactions
where asset_id = 44113
order by transaction_date

want time style 100
ok, define what i want

vehicles for which the keys were checked out of the used car keyper after 5 pm yesterday and that are still out
vehicle (stocknumber): asset_transactions.asset_name
still out: asset.asset_status = 'Out'
used car keyper: system_id = 27
checked out: asset_transactions.asset_status = 'Out'
after 5PM yesterday: asset_transactions.transaction_date
extra:
checked out by whom: asset_transactions.user_id <-> user.last_name, user.first_name
issue reason

after 5PM yesterday
depends if script is run before or after midnight, lets assume before midnight

and for the purpose of testing, keys checked out after 9AM this morning and still out
(assumes the checkout is the last transaction, if still checked out)
!!!!!!!!! remember, transaction_date is UTC !!!!!!!!!!!!!!!!!!!

select --top 300  convert(nvarchar(MAX), getdate(), 101),
  --convert(datetime, convert(nvarchar(MAX), getdate(), 101) + ' 2:00:00 PM'),
  -- c.name, c.asset_status, b.last_name, b.first_name, a.*,
  -- coalesce(d.name, 'unknown') as issue_reason,
  a.asset_name, b.last_name, b.first_name, convert(nvarchar(MAX), dateadd(hour, -5, a.transaction_date), 22),
  coalesce(d.name, 'unknown') as issue_reason
from dbo.asset_transactions a
left join dbo.[user] b on a.user_id = b.user_id 
left join dbo.asset c on a.asset_id = c.asset_id
left join dbo.issue_reason d on a.issue_reason_id = d.issue_reason_id
where transaction_date > convert(datetime, convert(nvarchar(MAX), getdate(), 101) + ' 2:00:00 PM')
  and a.system_id = 27 -- used car keyper
  and a.asset_status = 'Out' -- removed
  and c.asset_Status = 'Out' -- and not yet returned
order by a.transaction_date


-- this looks pretty good
-- ashley doesn't want reason
select 'stock #','checked out by', 'checked out at'
union
select a.asset_name as stock_number, b.first_name + ' ' + b.last_name as "checked out by", 
  convert(nvarchar(MAX), dateadd(hour, -5, a.transaction_date), 100) as "checked out at"
from dbo.asset_transactions a
left join dbo.[user] b on a.user_id = b.user_id 
left join dbo.asset c on a.asset_id = c.asset_id
left join dbo.issue_reason d on a.issue_reason_id = d.issue_reason_id
where transaction_date > convert(datetime, convert(nvarchar(MAX), getdate(), 101) + ' 10:00:00 PM') -- 5P< CST
  and a.system_id = 27 -- used car keyper
  and a.asset_status = 'Out' -- removed
  and c.asset_Status = 'Out' -- and not yet returned



-- yesterday
select 'stock #','checked out by', 'checked out at'
union
select a.asset_name as stock_number, b.first_name + ' ' + b.last_name as "checked out by", 
  convert(nvarchar(MAX), dateadd(hour, -5, a.transaction_date), 100) as "checked out at"
from dbo.asset_transactions a
left join dbo.[user] b on a.user_id = b.user_id 
left join dbo.asset c on a.asset_id = c.asset_id
left join dbo.issue_reason d on a.issue_reason_id = d.issue_reason_id
where transaction_date > convert(datetime, convert(nvarchar(MAX), getdate() - 1 , 101) + ' 10:00:00 AM') -- 5PM CST
  and a.system_id = 27 -- used car keyper
  and a.asset_status = 'Out' -- removed
  and c.asset_Status = 'Out' -- and not yet returned


select  convert(datetime, convert(nvarchar(MAX), getdate() - 1, 101))

10/17/17

Do you think you could do me a favor?? I would like to know the names � if any � that have checked out stock number 31143 out of any keeper and the dates it took place. No biggie if you can�t get it, but someone like your smart self can do anything, right? 


select a.asset_name as stock_number, b.first_name + ' ' + b.last_name as "checked out by", 
  convert(nvarchar(MAX), dateadd(hour, -5, a.transaction_date), 100) as "checked out at"
from dbo.asset_transactions a
left join dbo.[user] b on a.user_id = b.user_id 
-- left join dbo.asset c on a.asset_id = c.asset_id
-- left join dbo.issue_reason d on a.issue_reason_id = d.issue_reason_id
where a.asset_name = '31146'


