/*
01/10/2022 see python_projects\ext_arkona\sql\reports\vendor_spend_report.sql

*/


Select /*trns.gtco# as Company,*/ trns.gtacct as Account, mast.gmdesc as Description, 
  cust.gcsnam as VendorName, name.bnadr1 as Address1, name.bnadr2 as Address2, 
  name.bncity as City, name.bnstcd as State, name.bnzip as Zip, sum(trns.gttamt) as Amount
from rydedata.glptrns trns
  left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
  left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
  left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(gtvnd#) <> ''
  and gtdate between '2013-01-01' and '2013-12-31'
  and gtacct in (select gmacct from rydedata.glpmast where gmtype = '8')
  and not gcsnam is null
group by trns.gtco#, trns.gtacct, mast.gmdesc, cust.gcsnam, name.bnadr1, name.bnadr2, name.bncity, name.bnstcd, name.bnzip 
having sum(trns.gttamt) <> 0
order by trns.gtacct
-- order by gmdesc
--order by cust.gcsnam

-- 1/20/14
take zip out of the grouping
-- wait a minute, do not need account info, just vendornumber & name
Select /*trns.gtco# as Company,*/ trns.gtacct as Account, mast.gmdesc as Description, cust.gcsnam as VendorName, 
  name.bnadr1 as Address1, name.bnadr2 as Address2, name.bncity as City, name.bnstcd as State, 
  max(name.bnzip) as Zip, sum(trns.gttamt) as Amount
from rydedata.glptrns trns
  left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
  left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
  left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(gtvnd#) <> ''
  and gtdate between '2013-01-01' and '2013-12-31'
  and gtacct in (select gmacct from rydedata.glpmast where gmtype = '8')
  and not gcsnam is null
group by /*trns.gtco#,*/ trns.gtacct, mast.gmdesc, cust.gcsnam, name.bnadr1, name.bnadr2, name.bncity, name.bnstcd
having sum(trns.gttamt) <> 0
--order by trns.gtacct
order by cust.gcsnam


-- wait a minute, do not need account info, just vendornumber & name
Select /*trns.gtco# as Company, trns.gtacct as Account, mast.gmdesc as Description,*/ 
  trim(trns.gtvnd#) as VendorNumber, cust.gcsnam as VendorName, 
  name.bnadr1 as Address1, name.bnadr2 as Address2, name.bncity as City, name.bnstcd as State, 
  max(name.bnzip) as Zip, sum(trns.gttamt) as Amount
from rydedata.glptrns trns
  left join rydedata.glpcust cust on trns.gtco# = cust.gcco# and trns.gtvnd# = cust.gcvnd#
  left join rydedata.bopname name on cust.gckey = name.bnkey and cust.gcco# = name.bnco#
  left join rydedata.glpmast mast on trns.gtacct = mast.gmacct and trns.gtco# = mast.gmco#
where trim(gtvnd#) <> ''
  and gtdate between '2013-01-01' and '2013-12-31'
  and gtacct in (select gmacct from rydedata.glpmast where gmtype = '8')
  and not gcsnam is null
group by /*trns.gtco#,*/ trim(trns.gtvnd#), cust.gcsnam, name.bnadr1, name.bnadr2, name.bncity, name.bnstcd
having sum(trns.gttamt) <> 0
--order by trns.gtacct
order by cust.gcsnam

-- don't know, use glpcust as the main table
select trim(a.gcvnd#) as VendorNumber, a.gcsnam as VendorName, 
  c.bnadr1 as Address1, c.bnadr2 as Address2, c.bncity as City, c.bnstcd as State, 
  max(c.bnzip) as Zip
from rydedata.glpcust a
inner join rydedata.glptrns b on trim(a.gcvnd#) = trim(b.gtvnd#)
left join rydedata.bopname c on a.gckey = c.bnkey and a.gcco# = c.bnco
where b.gtdate between '2013-01-01' and '2013-12-31'
group by trim(a.gcvnd#), a.gcsnam,
  c.bnadr1, c.bnadr2, c.bncity, c.bnstcd


select *
from rydedata.glptrns
where gtdate between '2013-12-01' and '2013-12-31'
  and trim(gtacct) = '120300'
and trim(gtctl#) <> trim(gtvnd#)

select *
from rydedata.glpcust
where trim(gcvnd#) = '114399'

select gtacct, 
-- select
  sum(case when gttamt < 0 then gttamt end) as lessThan,
  sum(case when gttamt > 0 then gttamt end) as moreThan, 
  sum(gttamt)
from rydedata.glptrns
where trim(gtvnd#) = '14704'
  and gtdate between '01/01/2013' and '12/31/2013'
group by gtacct

select * from rydedata.glpcust where trim(gcvnd#) = '14704'

select gtacct, 
-- select
  sum(case when gttamt < 0 then gttamt end) as lessThan,
  sum(case when gttamt > 0 then gttamt end) as moreThan, 
  sum(gttamt)
from rydedata.glptrns
where trim(gtvnd#) = '117326'
  and gtdate between '01/01/2013' and '12/31/2013'
group by gtacct

select gtacct, 
-- select
  sum(case when gttamt < 0 then gttamt end) as lessThan,
  sum(case when gttamt > 0 then gttamt end) as moreThan, 
  sum(gttamt)
from rydedata.glptrns
where trim(gtvnd#) = '1115'
  and gtdate between '01/01/2013' and '12/31/2013'
group by gtacct


select *
from rydedata.glptrns
where trim(gtvnd#) = '14704'
  and gtdate between '01/01/2013' and '12/31/2013'


select trim(gtvnd#) as gtvnd#, sum(gttamt)
from rydedata.glptrns
where trim(gtacct) = '120300'
  and gtdate between '01/01/2013' and '12/31/2013'
  and gttamt < 0
  and gtvnd# <> ''
group by gtvnd#
order by trim(gtvnd#) 

select count(*) from (
select trim(gtvnd#) as gtvnd#, sum(gttamt)
from rydedata.glptrns
where trim(gtacct) = '120300'
  and gtdate between '01/01/2013' and '12/31/2013'
  and gttamt < 0
  and gtvnd# <> ''
group by gtvnd#
) x

select * from (
select trim(gtvnd#) as gtvnd#, sum(gttamt) as gttamt
from rydedata.glptrns
where trim(gtacct) = '120300'
  and gtdate between '01/01/2013' and '12/31/2013'
  and gttamt < 0
  and gtvnd# <> ''
group by gtvnd#) x where abs(gttamt) > 599
order by trim(gtvnd#) 


select a.*, b.gcsnam, b.gcftid, b.gckey, 
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
  c.*
from (
  select trim(gtvnd#) as gtvnd#, sum(gttamt)
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200','320200')
    and gtdate between '01/01/2013' and '12/31/2013'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null 

-- 1/21/14
was concerned abt the large result set, jeri says ok,
she would like the stores (acct) separated

select 
  case a.gtacct
    when '120300' then 'RY1'
    when '220200' then 'RY2'
    when '320200' then 'RY3'
  end as Store,
a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, abs(a.gttamt) as Amount,
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip
from (
  select trim(gtvnd#) as gtvnd#, trim(gtacct) as gtacct, sum(gttamt) as gttamt
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200','320200')
    and gtdate between '01/01/2013' and '12/31/2013'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#, gtacct) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null 

-- 1 row per vendor
select a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount", 
  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip,
b.*
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount,
  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200','320200')
    and gtdate between '01/01/2013' and '12/31/2013'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null 


-- 7/14/14 june needs the same thing for 2014 ytd

-- 1 row per vendor
select a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
--  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
--  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200') --,'320200')
    and gtdate between '01/01/2014' and curdate()
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null 


/*
1/20/2015
Good Afternoon Jonny!

I need you to run the vendor spend report again for me through December 2014. Please� and Thank You! You�re the best!
*/

select a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
--  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
--  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200') --,'320200')
    and gtdate between '01/01/2014' and '12/31/2014'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null 


select corporation_1099_, count(*) from rydedata.glpcust group by corporation_1099_


/* 12/7/15
Good Afternoon! 

Approaching the end of the year and I need a favor�..
The last couple of years we have had you run a vendor spend report� 
I need an updated one of those � right now mostly just looking for tax id�s for the vendors we are using, 
not as interested at this point in the amount of money spent yet this year. Could ya make this happen for us?

*/

select a.gtvnd# as VendorNumber, b.gcsnam as Name,  
  b.gcftid as TaxID --, 
  --coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
  --case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
  --c.bncity as City, c.bnstcd as State, c.bnzip as zip
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200')
    and gtdate between '01/01/2015' and '12/31/2015'
    and gttamt < 0
    and gtvnd# <> ''
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null


/*
1/13/16
In the past you have prepared the vendor spend report for both stores for me� can you do this again for me, with totals paid to each vendor as of 12/31/2015 � pretty please? Let me know if you have any questions! Thanks jonny!
*/

select a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
--  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
--  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200') --,'320200')
    and gtdate between '01/01/2015' and '12/31/2015'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
where b.gcvnd# is not null 


/*
1-11-17
Good afternoon Jon!

I was hoping you could pull together the vendor spend report for me, as you have in previous years? 
This time I would like to add "vendor type" as a field. We would like to have this report reviewed and 
forwarded to Brady's within a week. Please send to this email as I don't have my Rydell email handy. 

Sorry I didn't mean vendor type I meant "1099 vendor" field
*/

DECLARE GLOBAL TEMPORARY TABLE  SESSION.TEMP_1099_TYPES (
  code  CHAR(2),
  type char(26)); 

insert into session.temp_1099_types(code, type)
select * from table(
  values
    ('1','Rent'),
    ('2','Royalties'),  
    ('3','Other Income'),
    ('6','Medical & Health Care'),
    ('7','Nonemployee Compensation'),
    ('8','Interest Income'),
    ('S','Sole Proprietor'),
    ('14','Attorney'),
    ('','Not 1099 Vendor')) x  
    
      

select * from session.temp_1099_types

select a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
--  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip, d.type as "1099 Vendor Type"
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
--  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200') --,'320200')
    and gtdate between '01/01/2016' and '12/31/2016'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
left join SESSION.TEMP_1099_TYPES d on b.corporation_1099_ = d.code
where b.gcvnd# is not null 


-- 9/18/17 kim asked for top 10 vendor spends for 2017 (for brian)
-- the temp table stuff does not seem to be working today, 
-- don't need it anyway

select a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  cast(coalesce(RY1Amount, 0) as integer) as "RY1 Amount", -- coalesce(RY2Amount, 0) as "RY2 Amount",
--  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip -- , d.type as "1099 Vendor Type"
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
--  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where trim(gtacct) in( '120300','220200') --,'320200')
    and gtdate between '01/01/2017' and '12/31/2017'
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
-- left join SESSION.TEMP_1099_TYPES d on b.corporation_1099_ = d.code
where b.gcvnd# is not null 
order by coalesce(RY1Amount, 0) desc 
limit 40


/* 1-16-18
Good Afternoon, 

It is that time of the year again � I have to prepare a report for Brady�s 
on which Vendors that we are paying. I have attached a prior year report 
that you have run for me, and I was wondering if you could run the same 
report for both stores for 2017 spending ? I will have to have this 
reviewed and turned into Bradys by mid-week, so the sooner I can get it, the better. ?  
You�re the best!

June Gagnon
*/

-- drop table SESSION.TEMP_1099_TYPES

DECLARE GLOBAL TEMPORARY TABLE  SESSION.TEMP_1099_TYPES (
  code  CHAR(2),
  type char(26)); 

insert into session.temp_1099_types(code, type)
select * from table(
  values
    ('1','Rent'),
    ('2','Royalties'),  
    ('3','Other Income'),
    ('6','Medical & Health Care'),
    ('7','Nonemployee Compensation'),
    ('8','Interest Income'),
    ('S','Sole Proprietor'),
    ('14','Attorney'),
    ('','Not 1099 Vendor')) x  
    
      

select * from session.temp_1099_types

select gtctl#, a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
--  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip, d.type as "1099 Vendor Type"
from (
  select trim(gtvnd#) as gtvnd#, trim(gtctl#) as gtctl#,
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
--  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where gtpost = 'Y'
    and trim(gtacct) in( '120300','220200') --,'320200')
    and gtdate between '01/01/2019' and '12/31/2019' ---------------------------------------
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#, gtctl#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
left join SESSION.TEMP_1099_TYPES d on b.corporation_1099_ = d.code
where b.gcvnd# is not null 
order by trim(gtctl#)

select * from rydedata.glptrns where gtdate > current date - 2 days and gtvnd# <> ''

-- page 3 line 26
select * 
from rydedata.glptrns 
where trim(gtacct) in ('16801','16802W','16802')
  and gtdate between '2018-01-01' and '2018-01-31'
  
  
select gtacct, sum(gttamt), count(*) 
-- select sum(gttamt)
from rydedata.glptrns 
where gtpost = 'Y'
  and trim(gtacct) in ('16801','16802W','16802')
  and gtdate between '2018-01-01' and '2018-01-31'
group by gtacct  
  
  
/*
Good Morning! 

I am hoping that you can run the current year vendor spend report (which you typically do for me in January). 
I am also wanting to see if you can include the contents of the �Remarks�  field. 
I have attached the 2016 report for your reference. Thanks!
June Gagnon
Office Manager
*/

/**********************************************************************************************************/
10/10/18

moved this to python and postgres
E:\python projects\ext_arkona\vendor_spend_report.py
E:\python projects\ext_arkona\sql\reports\vendor_report
run the report from db2 in python, populate a postgres table



generate the spreadsheet from the postgres data

1/11/2021
last year and now this year, the python script to run this query fails 
with [<class 'decimal.ConversionSyntax'>]
have no idea what that means

run the python script in the W10 dream

select b.record_key, gtctl#, a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
  c.bncity as City, c.bnstcd as State, c.bnzip as zip, d.type as "1099 Vendor Type"
from (
  select trim(gtvnd#) as gtvnd#, trim(gtctl#) as gtctl#,
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
  from rydedata.glptrns
  where gtpost = 'Y'
    and trim(gtacct) in( '120300','220200') --,'320200')
    and gtdate between '01/01/2020' and  '12/31/2020' ---------------------------------------
    and gttamt < 0
    and gtvnd# <> ''
  group by gtvnd#, gtctl#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
left join temp_1099_TYPES d on b.corporation_1099_ = d.code
where b.gcvnd# is not null 
order by name


  
10/10/18 could not get listagg to work, definitely some sort of truncation issue
-- ok, the issue is this gives me both remarks for 1066207 but only one for 203625
select name_Record_key, listagg (trim(remarks), '---') within group(order by sequence_number)
from rydedata.glpcrmk
where name_record_key in (1066207,203625, 217634)
group by name_record_key

-- nope
select name_Record_key, cast(listagg (all trim(remarks), '---')  as varchar(4000))
from rydedata.glpcrmk
where name_record_key in (1066207,203625, 217634)
group by name_record_key
  
/*
trying to run this caused AQT to immediately shut down  
SELECT  name_record_key, 
        SUBSTR(xmlserialize(xmlagg(xmltext(CONCAT( ', ',remarks))) as VARCHAR(1024)), 3)
FROM    rydedata.glpcrmk
GROUP   BY name_record_key
yikes;
*/


-- looks like a truncation issue when i order by remarks
select name_Record_key, listagg(trim(remarks), '---') within group(order by remarks) 
from rydedata.glpcrmk
where name_record_key in (1066207,203625, 217634)
group by name_record_key

-- casting does not seem to help
select name_Record_key, cast(listagg(trim(cast(remarks as varchar(100))), '---')  within group(order by name_record_key) as varchar(1000))
from rydedata.glpcrmk
where name_record_key in (1066207,203625, 217634)
group by name_record_key


select name_record_key, listagg(trim(remarks), '-*-') within group(order by sequence_number)
from (
  select name_Record_key,sequence_number, remarks, note_date
  from rydedata.glpcrmk
  where name_record_key in (1066207,203625, 217634)) x
group by name_record_key, note_date















