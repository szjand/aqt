Select pmpart as "Part #", pmdesc as Description, pmsgrp as "Stock Group", 
  pmstat as Status, pmonhd as "On Hand"
from RYDEDATA.PDPMAST
where pmsgrp <> '122'
  and pmco# = 'RY1'
  and pmppac = 'M'
--  and pmstat = 'A'

select distinct pmstat from rydedata.pdpmast -- nope

select distinct pmprdc from rydedata.pdpmast -- nope

select distinct pmgrpc from rydedata.pdpmast -- nope

select distinct pmrtrn from rydedata.pdpmast -- nope

select distinct pmarrv from rydedata.pdpmast -- nope

select distinct pmstko from rydedata.pdpmast -- nope

select distinct pmspoc from rydedata.pdpmast -- nope

select distinct pmppac from rydedata.pdpmast -- yep