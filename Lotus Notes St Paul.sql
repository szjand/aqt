select Segment, StockNumber,Location, Make, Model, TrimLevel, Color, Miles, 
         VIN, 
        "Year", SalesStatus,  DateAcquired

      from frmStoreInventory
where SalesStatus not in ('Sold','Wholesaled')


select SalesStatus, Location, Count(*)
      from frmStoreInventory
where SalesStatus not in ('Sold','Wholesaled')
group by SalesStatus, Location

select SalesStatus, Count(*)
      from frmStoreInventory
where SalesStatus not in ('Sold','Wholesaled')
group by SalesStatus


select Location, SalesStatus, Segment, BodyStyle, Count(Location)
      from frmStoreInventory
where SalesStatus not in ('Sold','Wholesaled')
group by Location, SalesStatus, Segment, BodyStyle

select Segment, StockNumber,Location, Make, Model, TrimLevel, Color, Miles, 
         VIN, 
        "Year", SalesStatus,  DateAcquired

      from frmStoreInventory
where SalesStatus = 'Recon Buffer'
order by StockNumber


select Location, SalesStatus, Segment, BodyStyle, Count(Location)
      from frmStoreInventory
where SalesStatus not in ('Sold','Wholesaled')
group by Location, SalesStatus, Segment, BodyStyle

select Segment, StockNumber,Location, Make, Model, TrimLevel, Color, Miles, 
         VIN, 
        "Year", SalesStatus,  DateAcquired

      from frmStoreInventory
where SalesStatus = 'Available'
order by StockNumber


select StockNumber,Location, Make, Model, 
         VIN, 
        "Year", SalesStatus,  DateAcquired

      from frmStoreInventory
where SalesStatus not in ('Sold','Wholesaled')

select Segment, StockNumber,Location, Make, Model, TrimLevel, Color, Miles, 
         VIN, 
        "Year", SalesStatus,  DateAcquired, DateSold

      from Store_Inventory
where SalesStatus not in ('Sold','Wholesaled')

select SalesStatus, Count(*)
      from Store_Inventory
where SalesStatus not in ('Sold','Wholesaled')
group by SalesStatus


select StockNumber, "Year", Make, Model, BodyStyle, TrimLevel, Glump, Segment, 
  VIN, SalesStatus,  DateAcquired, DateSold, 'SP-St. Paul'
from frmStoreInventory
where DateAcquired > '2008-01-01'
and Glump is null 
and DateSold is not null
order by DateAcquired desc
--and StockNumber = '63622B'
--where ((SalesStatus not in ('Sold','Wholesaled')) or (DateSold > '2011-01-01'))

-- stocknumbers from ageing
select StockNumber, "Year", Make, Model, BodyStyle, TrimLevel, Glump, Segment, 
  VIN, SalesStatus,  DateAcquired, DateSold, Location, 'SP-St. Paul'
from frmStoreInventory
where StockNumber in (
'59400PG',         
'59561P',          
'59641PG',         
'59780PA',         
'59797PB',         
'59963PA',         
'60395XB',         
'60443X',          
'59997PC',         
'60680XG',         
'60830A',          
'60377PB',         
'60638',           
'61331P',          
'61177XB',         
'60803XA',         
'61336A',          
'60477X',          
'60946BA',         
'61160X',          
'60850P',          
'61393XA',         
'61887X',          
'61679PA',         
'67937X',          
'61737XG',         
'61737XB',         
'62157A',          
'62326',           
'62468A',          
'62652X',          
'62663197P',       
'62985X',          
'62369XA',         
'62743P',          
'0VE 1.20-1',      
'62821P',          
'62719A',          
'62245A',          
'62474XA')         




select StockNumber, "Year", Make, Model, BodyStyle, Glump, Segment, 
  VIN, SalesStatus,  DateAcquired, DateSold
from frmStoreInventory
where StockNumber = '63478X'


select StockNumber, "Year", Make, Model, BodyStyle, TrimLevel, Glump, Segment, 
  VIN, SalesStatus,  DateAcquired, DateSold, 'SP-St. Paul'
from frmStoreInventory
where DateAcquired > '2008-01-01'
and Glump is null 
and DateSold is not null
order by DateSold desc

--10/22/11
select StockNumber, "Year", Make, Model, BodyStyle, Glump, Segment, 
  VIN, SalesStatus,  DateAcquired, DateSold
from frmStoreInventory
where Market = 'SP-St. Paul'
and SalesStatus not in ('Sold','Wholesaled')

select StockNumber, "Year", Make, Model, BodyStyle, Glump, Segment, 
  VIN, SalesStatus,  DateAcquired, DateSold
from frmStoreInventory
where StockNumber = '63785P

-- the non glump is an issue now for recently sold vehicles
select StockNumber, "Year", Make, Model, BodyStyle, Glump, Segment, 
  VIN, SalesStatus,  DateAcquired, DateSold, SoldAmount
from frmStoreInventory
where DateSold > '2011-07-31'
and Glump is null 
order by StockNumber

-- expeditions
select StockNumber, "Year", Make, Model, BodyStyle, Glump, Segment, 
  VIN, SalesStatus,  DateAcquired, DateSold, SoldAmount
from frmStoreInventory
where Model = 'Expedition'
and DateSold is not null
and SoldAmount < 2000



-- 10/23, vehicles still miscategorized on the email report
/*
63979B 04 Tahoe   BS: SUV    Segment: Midsize   Glump: SUVs - Medium
*/

select StockNumber, "Year", Make, Model, BodyStyle, Glump, Segment, 
  VIN, SalesStatus,  DateAcquired, DateSold, SoldAmount
from frmStoreInventory
where StockNumber = '63979B'

-- for export to ads
-- table StPaulAvailable
-- current inventory + last 2 1/2 months of sales
select StockNumber, "Year", Make, Model, BodyStyle, Glump, Segment, 
  VIN, SalesStatus
from frmStoreInventory
where ((DateSold > '2011-07-31') or (SalesStatus not in ('Sold','Wholesaled')))



