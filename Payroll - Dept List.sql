select ymname, 
  case 
    when cast(right(trim(ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as "Hire Date",
  p.ymdept,
  j.yrtext as Occupation
from pymast p
left join pyprhead ph on ph.yrempn = p.ymempn
left join (
  select distinct yrjobd, yrtext
  from pyprjobd) j on j.yrjobd = ph.yrjobd 
    and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') -- job descriptions
where ymactv in ('A','P')
and trim(p.ymname) <> 'TEST'
and ymco# = 'RY1'
order by ymdept, ymname