-- 19210283 shows 5 should be ~7200 (possible coincidence, 5 is the number in process)
-- 19177982 shows 37 should be 0
-- a stocked part is indicated by a non null bin location
select pmgrpc as Group, pmpart as "Part #", pmonhd as "On Hand", pmbinl as "Bin", pmdesc as Description,
-- this is what's currntly being used and generating some bad results
  (select coalesce(sum(ptqty), 0) from pdptdet where ptpart = p.pmpart and ptdate > '20080901') as "Qty Sold last 12 Months"
from pdpmast p
where trim(pmgrpc) = '07831'
AND pmonhd > 0
and p.pmdesc like 'FASCIA%'
and trim(pmco#) = 'RY1'
order by (select coalesce(sum(ptqty), 0) from pdptdet where ptpart = p.pmpart and ptdate > '20080901') desc
-----------------------------------------------------
select * from rydedata.pdpmast where trim(pmpart) = '19210283'

select pmgrpc as Group, pmpart as "Part #", pmonhd as "On Hand", pmbinl as "Bin", pmdesc as Description,
-- this is what's currntly being used and generating some bad results
  (select coalesce(sum(ptqty), 0) from pdptdet where ptpart = p.pmpart and ptdate > '20080901') as "Qty Sold last 12 Months"
-- select *
from pdpmast p
where trim(pmpart) = '19210283'


select pmpart, pmdesc, pmstat, pmonhd, pmordr, pmbcko, 
  (select coalesce(sum(ptqty), 0) from rydedata.pdptdet where ptpart = pm.pmpart and ptcode = 'SA') as sales, 
  pmbinl, pmoprt 
from rydedata.pdpmast pm 
where trim(pmpart) = '19210283'
and trim(pmgrpc) in ('01835', '01836', '03410') and pmco# = 'RY1' and pmstat = 'A'

select *
from pdptdet where trim(ptpart) = '19210283'
-- old and new part nos, still 5000 short
select sum(ptqty)
from pdptdet where trim(ptpart) in ('19210283','PF46','25014748')
and trim(ptco#) = 'RY1'

-- old and new part nos, still 5000 short
select sum(ptqty)
from pdptdet where trim(ptpart) in ('19210283','PF46','25014748')
and trim(ptco#) = 'RY1'
and ptdate >= 20090801
and ptdate <= 20090831


select ptdate, sum(ptqty)
from rydedata.pdptdet
where trim(ptpart) = '19210283' 
group by ptdate
order by ptdate desc

select *
from pdpmrpl
where trim(pfaprt) = '19210283' 

select sum(ptqty) from pdptdet where trim(ptpart) = '19210283' 

-- filter order
select pmpart, pmdesc, pmstat, pmonhd, pmordr, pmbcko,
  (select coalesce(sum(ptqty), 0) from rydedata.pdptdet where ptpart = pm.pmpart/* and ptcode = 'SA'*/) as sales, 
  pmbinl, pmoprt
from rydedata.pdpmast pm 
where trim(pmgrpc) in ('01835', '01836', '03410') 
and trim(pmpart) = '19210283'
and pmco# = 'RY1' 
and pmstat = 'A'

select * from pdpmast where trim(pmpart) = '19210283'

select substr(ptdate,5,2)||'-'||left(ptdate,4), sum(ptqty)
from pdptdet
where trim(ptpart) = '19210283'
and trim(ptco#) = 'RY1'
group by substr(ptdate,5,2)||'-'||left(ptdate,4)

select sum(ptqty)
from pdptdet
where trim(ptpart) = '19210283'
and ptdate >= 20090801
and ptdate <= 20090831

-- Chemical Order
-- 11/24/09 Yearly sales is badly fucked up
-- this is much better
-- but way too slow  EXISTS: 152 sec
--                   Original (IN) 143 sec

select pmpart as "Part #", pmpack, pmdesc as Description,
  pmstat as Status, pmonhd as "On Hand", pmordr as "Qty on Order",
  pmbcko as "Qty on Back Order",
  (SELECT COALESCE(SUM(PTQTY), 0) 
  FROM rydedata.PDPTDET 
  WHERE (PTPART = PM.PMPART OR PTPART IN ((SELECT PFOPRT FROM rydedata.PDPMRPL WHERE PFAPRT = PM.PMPART))) 
    AND PTCODE IN (
      'CP', /*RO CUSTOMER PAY SALE*/
      'IS', /*RO INTERNAL SALE*/ 
      'SA', /*COUNTER SALE*/
      'WS', /*RO WARRANTY SALE*/
      'SC', /*RO SERVICE CONTRACT SALE*/ 
      'SR', /*RO RETURN*/ 
      'CR', /*RO CORRECTION*/ 
      'RT', /*COUNTER RETURN*/ 
      'FR') /*FACTORY RETURN*/ 
    AND PTCO# = PM.PMCO#
    AND PTDATE > (YEAR(CURDATE()) - 1) * 10000 + MONTH(CURDATE()) * 100 + DAY(CURDATE())) AS "YEARLY SALES", 
  pmbinl as Bin, pmoprt as "Old Part #"
from rydedata.pdpmast pm
where trim(pmgrpc) = '08800' 
 and pmco# = 'RY1'
 and pmstat = 'A'
 
--   EXISTS: 152 sec
--   Original (IN) 143 sec 
select pmpart as "Part #", pmpack, pmdesc as Description,
  pmstat as Status, pmonhd as "On Hand", pmordr as "Qty on Order",
  pmbcko as "Qty on Back Order",
         (select coalesce(sum(ptqty), 0) 
          from pdptdet pd
          where (ptpart = pm.pmpart or ptpart in ((select pfoprt from pdpmrpl where pfaprt = pm.pmpart))) 
--          where (pd.ptpart = pm.pmpart or exists (
--            select 1
--            from pdpmrpl 
--            where pfoprt = pd.ptpart
--            and pfaprt = pm.pmpart))
          and ptcode in (
            'CP', /*RO Customer Pay Sale*/
            'IS', /*RO Internal Sale*/ 
            'SA', /*Counter Sale*/
            'WS', /*RO Warranty Sale*/
            'SC', /*RO Service Contract Sale*/ 
            'SR', /*RO Return*/ 
            'CR', /*RO Correction*/ 
            'RT', /*Counter Return*/ 
            'FR') /*Factory Return*/ 
          and ptco# = pm.pmco#
          and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())) as "Yearly Sales", 
  pmbinl as Bin, pmoprt as "Old Part #"
from pdpmast pm
--where trim(pmgrpc) = '08800' 
where pm.pmpart = '19210283'
 and pmco# = 'RY1'
 and pmstat = 'A'
 
-- Try 2 separate subqueries in select
-- 76 sec
select pmpart as "Part #",pmpack as "Pack Qty", pmdesc as Description, pmonhd as "On Hand",
  pmordr as "Qty on Order", pmbcko as "Qty on Back Order", Sales1 + Sales2 as YearlySales,
  pmbinl as Bin, pmoprt as "Old Part #"
from(
  select pmpart, pmpack, pmdesc, pmstat, pmonhd, pmordr, pmbcko,  
     (select coalesce(sum(ptqty), 0) 
       from pdptdet pd
       where ptpart = pm.pmpart
       and ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
       and ptco# = pm.pmco#
       and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())) as Sales1, 
     (select coalesce(sum(ptqty), 0) 
       from pdptdet pd
       where exists(
         select 1
         from pdpmrpl 
         where pfoprt = pd.ptpart
         and pfaprt = pm.pmpart)   
       and ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
       and ptco# = pm.pmco#
       and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())) as Sales2,           
    pmbinl, pmoprt
  from pdpmast pm
  where trim(pmgrpc) = '08800' 
--  where pm.pmpart = '19210283'
   and pmco# = 'RY1'
   and pmstat = 'A' ) as wtf
  
select coalesce(sum(ptqty), 0) 
       from pdptdet pd
       where exists(
         select 1
         from pdpmrpl 
         where pfoprt = pd.ptpart
         and pfaprt = '19210283'
       and ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
       and ptco# = 'RY1'
       and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate()))  
  
  
select coalesce(sum(ptqty), 0) 
from pdptdet 
where (ptpart ='19210283' or ptpart in ((select pfoprt from pdpmrpl where pfaprt = '19210283'))) 
and ptcode in (
  'CP', /*RO Customer Pay Sale*/
  'IS', /*RO Internal Sale*/ 
  'SA', /*Counter Sale*/
  'WS', /*RO Warranty Sale*/
  'SC', /*RO Service Contract Sale*/ 
  'SR', /*RO Return*/ 
  'CR', /*RO Correction*/ 
  'RT', /*Counter Return*/ 
  'FR') /*Factory Return*/ 
and ptco# = 'RY1'
and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())

-- by source
select pmpart as "Part #",pmdesc as Description, pmonhd as "On Hand",
  Sales1 + Sales2 as YearlySales, pmsgrp

from(
  select pmpart, pmpack, pmdesc, pmstat, pmonhd, pmordr, pmbcko, pmsgrp,  
     (select coalesce(sum(ptqty), 0) 
       from pdptdet pd
       where ptpart = pm.pmpart
       and ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
       and ptco# = pm.pmco#
       and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())) as Sales1, 
     (select coalesce(sum(ptqty), 0) 
       from pdptdet pd
       where exists(
         select 1
         from pdpmrpl 
         where pfoprt = pd.ptpart
         and pfaprt = pm.pmpart)   
       and ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
       and ptco# = pm.pmco#
       and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())) as Sales2,           
    pmbinl, pmoprt
  from pdpmast pm
  where trim(pmsgrp) in ('505', '100') -- source
   and pmco# = 'RY1'
   and pmstat = 'A' ) as wtf
   order by pmsgrp, yearlysales desc

-- 8/14/12 mark wants a filter (stocking group = 3410) report showing sales for the last year
-- a stocked part is indicated by a non null bin location
select pmgrpc as Group, pmpart as "Part #", pmonhd as "On Hand", pmbinl as "Bin", pmdesc as Description,
-- this is what's currntly being used and generating some bad results
  (select coalesce(sum(ptqty), 0) from pdptdet where ptpart = p.pmpart and ptdate > '20080901') as "Qty Sold last 12 Months"
from pdpmast p
where trim(pmgrpc) = '07831'
AND pmonhd > 0
and p.pmdesc like 'FASCIA%'
and trim(pmco#) = 'RY1'
order by (select coalesce(sum(ptqty), 0) from pdptdet where ptpart = p.pmpart and ptdate > '20080901') desc

select a.pmco#, a.pmpart, a.pmdesc, a.pmsgrp, a.pmcost, a.pmlist, a.pmonhd, a.pmordr, a.pmbcko
select *

select c.*, d.pmonhd
from (
  select a.pmco#, a.pmpart, a.pmdesc, sum(b.ptqty)
  from rydedata.pdpmast a
  left join rydedata.pdptdet b on a.pmco# = b.ptco#
    and trim(a.pmpart) = trim(b.ptpart)
  where trim(pmgrpc) = '03410'
    and b.ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())
  group by a.pmco#, a.pmpart, a.pmdesc) c
left join rydedata.pdpmast d on c.pmco# = d.pmco#
  and c.pmpart = d.pmpart




 