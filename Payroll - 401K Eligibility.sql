/*
Payroll Class (ymclas)
  H - Hourly
  S - Salary
  C - Commission
  E - Executive
*/
select company, name, birthdate, hiredate, ymclas, TCHrs2010,  
  year(now()) - year(birthdate) -
    case 
      when
        month(BirthDate)*100 + day(BirthDate) > month(CurDate())*100 + day(CurDate()) then 1
      else 0
    end as Age,
  case ymclas
    when 'H' then 0
    else
      case year(hiredate)
        when 2010 then (week(curdate()) - week(hiredate))*40
        else week(curdate())* 40
      end
  end as EstHours
from (
select p.ymco# as Company, p.ymname as Name, 
  cast(
    case length(trim(p.ymbdte))
      when 5 then  '19'||substr(trim(p.ymbdte),4,2)||'-'|| '0' || left(trim(p.ymbdte),1) || '-' ||substr(trim(p.ymbdte),2,2)
      when 6 then  '19'||substr(trim(p.ymbdte),5,2)||'-'|| left(trim(p.ymbdte),2) || '-' ||substr(trim(p.ymbdte),3,2)
    end as date) as BirthDate,
  case 
    when cast(right(trim(ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as HireDate,
  case ymclas
    when 'H' then
      (select sum(
        case 
          when yiclkind=yiclkoutd then 
            case
              when yiclkoutt <> '00:00:00'  then hour(yiclkoutt) - hour(yiclkint)
              when yiclkoutt = '00:00:00'  then 24 - hour(yiclkint)
            end
          when yiclkoutd > yiclkind then (hour(yiclkoutt)+24) - hour(yiclkint)
        end)
      from rydedata.pypclockin
      where yiemp# = p.ymempn
      and year(yiclkind) > 2016
      and ymclas = 'H')
    else 0
  end as TCHrs2010,
  ymclas
from rydedata.pymast as p
where length(trim(p.ymname)) > 4 
and ymactv in ('A','P')) wtf


