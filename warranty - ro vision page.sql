-- don't have to worry about the date, all the data is here
select * from (
Select control_number, account_number, min(transaction_date) as min_date, max(transaction_date) as max_date, sum(transaction_amount) as bal 
from RYDEDATA.GLPTRNS 
where post_status = 'Y'
--  and transaction_date > '01/01/2022'
  and trim(account_number) in ('126300', '226300', '226301', '2200','2255')
group by control_number, account_number
  having sum(transaction_amount) <> 0
) x order by trim(control_number) -- min_date  

select aa.*, bb.close_date
from (
Select control_number, account_number, sum(transaction_amount) as bal,
  listagg(distinct trim(description), ' | ')
from RYDEDATA.GLPTRNS 
where post_status = 'Y'
  and trim(account_number) in ('126300', '226300', '226301', '2200','2255')
group by control_number, account_number
  having sum(transaction_amount) <> 0) aa
left join rydedata.sdprhdr bb on trim(aa.control_number) = trim(bb.ro_number)  
  
  
select * 
from rydedata.sdprhdr
order by close_date desc limit 10

select left(trim(ro_number),4), count(*)
from rydedata.sdprhdr
where company_number = 'RY8'
group by left(trim(ro_number),4)
order by count(*) desc

need to do something about the bunch of old toyota stuff with no ro


select aa.store, aa.control_number as ro, aa.account, aa.balance, aa.description, 
  cast(left(digits(bb.close_date), 4) || '-' || substr(digits(bb.close_date), 5, 2) || '-' || substr(digits(bb.close_date), 7, 2)  as date) as close_date
from (
  Select 'Toyota' as store, control_number, account_number as account, sum(transaction_amount) as balance,
    listagg(distinct trim(description), ' | ') as description
  from RYDEDATA.GLPTRNS 
  where post_status = 'Y'
    and trim(account_number) in ('2200','2255')
    and trim(control_number) like '86%'
  group by control_number, account_number
    having sum(transaction_amount) <> 0
  union
  Select 'Honda Nissan' as store, control_number, account_number as account, sum(transaction_amount) as balance,
    listagg(distinct trim(description), ' | ') as description
  from RYDEDATA.GLPTRNS 
  where post_status = 'Y'
    and trim(account_number) in ('226300', '226301')
  group by control_number, account_number
    having sum(transaction_amount) <> 0  
  union
  Select 'GM' as store, control_number, account_number as account, sum(transaction_amount) as balance,
    listagg(distinct trim(description), ' | ') as description
  from RYDEDATA.GLPTRNS 
  where post_status = 'Y'
    and trim(account_number) = '126300'
  group by control_number, account_number
    having sum(transaction_amount) <> 0) aa
left join rydedata.sdprhdr bb on trim(aa.control_number) = trim(bb.ro_number); 

   
