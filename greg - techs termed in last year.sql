-- Find Main Shop Technicians terminated within the last year
select t.sttech as "Tech#", p.ymname as "Name", 
  cast(case 
    when length(trim(p.ymhdte)) = 5 and integer(substr(trim(p.ymhdte),4,2)) >= 30 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
    when length(trim(p.ymhdte)) = 5 and integer(substr(trim(p.ymhdte),4,2)) < 30  then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
    when length(trim(p.ymhdte)) = 6 and integer(substr(trim(p.ymhdte),5,2)) >= 30 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
    when length(trim(p.ymhdte)) = 6 and integer(substr(trim(p.ymhdte),5,2)) < 30  then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
  end as date) as "Hired", ymdept as "Dept", ymdist as "Dist", ymrate as "Pay",
  cast(case 
    when length(trim(p.ymtdte)) = 5 and integer(substr(trim(p.ymtdte),4,2)) >= 30 then  '19'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
    when length(trim(p.ymtdte)) = 5 and integer(substr(trim(p.ymtdte),4,2)) < 30  then  '20'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
    when length(trim(p.ymtdte)) = 6 and integer(substr(trim(p.ymtdte),5,2)) >= 30 then  '19'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
    when length(trim(p.ymtdte)) = 6 and integer(substr(trim(p.ymtdte),5,2)) < 30  then  '20'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
  end as date) as "Term"
from rydedata.sdptech t
  inner join rydedata.pymast p on ymco# = 'RY1' and t.stpyemp# = p.ymempn
where t.stco# = 'RY1'
  and p.ymdist in ('STEC', 'PR/S') -- proper distribution codes in payroll
  and cast(case 
    when length(trim(p.ymtdte)) = 5 and integer(substr(trim(p.ymtdte),4,2)) >= 30 then  '19'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
    when length(trim(p.ymtdte)) = 5 and integer(substr(trim(p.ymtdte),4,2)) < 30  then  '20'||substr(trim(p.ymtdte),4,2)||'-'|| '0' || left(trim(p.ymtdte),1) || '-' ||substr(trim(p.ymtdte),2,2)
    when length(trim(p.ymtdte)) = 6 and integer(substr(trim(p.ymtdte),5,2)) >= 30 then  '19'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
    when length(trim(p.ymtdte)) = 6 and integer(substr(trim(p.ymtdte),5,2)) < 30  then  '20'||substr(trim(p.ymtdte),5,2)||'-'|| left(trim(p.ymtdte),2) || '-' ||substr(trim(p.ymtdte),3,2)
  end as date) >= curdate() - 1 year
  and not left(sttech, 1) in ('H', 'M') -- fake stalls -- not tech numbers
order by t.sttech


