SELECT *
FROM rydedata.SDPRDET a
left join rydedata.pdptdet b on trim(a.ptro#) = trim(b.ptinv#)
WHERE trim(ptlopc) LIKE 'PDQ%'
  AND a.ptdate > 20120831
  and b.ptinv# is null 

select *
from rydedata.pdptdet a

-- all oil and air filter part numbers
select trim(pmpart) as pmpart, 
  case trim(pmgrpc)
    when '03410' then 'Air'
    else 'Oil'
  end as filter
from rydedata.pdpmast
where trim(pmgrpc) in ('01835', '01836', '03410')
  and pmco#= 'RY1'

-- this could be it
-- ro's since 08/01/12 with labor of of pdq or 13A or lof, with no filter parts transaction
SELECT a.ptro#, a.ptdate, d.ptfcdt, a.ptlopc, c.*
-- select a.ptro# as RO, a.ptlopc as OpCode
FROM rydedata.SDPRDET a
left join rydedata.sdprhdr d on trim(a.ptro#) = trim(d.ptro#)
left join (
  select a.ptinv#, a.ptdate, a.ptpart,
    case trim(b.pmgrpc)
      when '03410' then 'Air'
      else 'Oil'
    end as filter
  from rydedata.pdptdet a 
  inner join rydedata.pdpmast b on trim(a.ptpart) = trim(b.pmpart)
    and trim(b.pmgrpc) in ('01835', '01836', '03410')
  where a.ptco# = 'RY1') c on trim(a.ptro#) = trim(c.ptinv#)
WHERE (trim(a.ptlopc) LIKE 'PDQ%' or trim(a.ptlopc) = '13A' or trim(a.ptlopc) like 'LOF%')
  AND a.ptdate > 20120800
  and c.ptinv# is null 
  and a.ptco# = 'RY1'

R85358
select *
from rydedata.sdprdet a
where left(trim(ptro#), 2) = '19'
  and ptdate > 20120731
  and not exists (
    select 1
    from rydedata.sdprdet
    where trim(ptro#) = trim(a.ptro#)
    and trim(ptlopc) like 'PDQ%')

select *
from rydedata.pdpmast
where trim(pmpart) = 'R85358'

select *
from rydedata.glptrns
where trim(gtdoc#) in (
SELECT trim(ptro#)
FROM rydedata.SDPRDET a
left join rydedata.pdptdet b on trim(a.ptro#) = trim(b.ptinv#)
WHERE trim(ptlopc) LIKE 'PDQ%'
  AND a.ptdate > 20120831
  and b.ptinv# is null)


-- parts sales accounts
select *
from rydedata.glpmast
where gmstyp = 'A'
  and gmyear = 2012
  and gmtype = '4'
  and gmdept = 'PD'


select *
from rydedata.sdprhdr
where ptco# = 'RY1'
  and ptdate > 20120831  
  and ptfcdt <> 0 

select *
from rydedata.sdprdet 
where trim(ptlopc) like 'PDQ%'
  and ptdate > 2120831

-- all gl parts sales transactions for 09/12
SELECT a.gmacct, a.gmdesc, b.gtdoc#, b.gttamt 
FROM rydedata.GLPMAST a
INNER JOIN rydedata.GLPTRNS b ON trim(a.gmacct) = trim(b.gtacct)
WHERE a.gmyear = 2012
  AND a.gmtype = '4'
  AND a.gmdept = 'PD'
  and a.gmstyp = 'A' -- ry1
  and b.gtdate between '09/01/2012' and curdate()
  and b.gtdtyp = 'S'

-- all sept ros with a pdq labor op and no parts transactions
select a.ptro# as RO--, b.*
from rydedata.sdprhdr a
inner join rydedata.sdprdet b on trim(a.ptro#) = trim(b.ptro#)
  and trim(b.ptlopc) like 'PDQ%'
left join (-- all gl parts sales transactions for 09/12
  SELECT a.gmacct, a.gmdesc, b.gtdoc#, b.gttamt 
  FROM rydedata.GLPMAST a
  INNER JOIN rydedata.GLPTRNS b ON trim(a.gmacct) = trim(b.gtacct)
  WHERE a.gmyear = 2012
    AND a.gmtype = '4' -- sales
    AND a.gmdept = 'PD' -- parts
    and a.gmstyp = 'A' -- ry1
    and b.gtdate between '09/01/2012' and curdate()
    and b.gtdtyp = 'S') c on trim(a.ptro#) = trim(c.gtdoc#)
where a.ptco# = 'RY1'
  and a.ptdate > 20120831  
  and a.ptfcdt <> 0 
  and c.gtdoc# is null 


-- all sept ros with a air filter labor op and no parts transactions
select a.ptro#, b.*
from rydedata.sdprhdr a
inner join rydedata.sdprdet b on trim(a.ptro#) = trim(b.ptro#)
  and trim(b.ptlopc) like '13A%'
left join (-- all gl parts sales transactions for 09/12
  SELECT a.gmacct, a.gmdesc, b.gtdoc#, b.gttamt 
  FROM rydedata.GLPMAST a
  INNER JOIN rydedata.GLPTRNS b ON trim(a.gmacct) = trim(b.gtacct)
  WHERE a.gmyear = 2012
    AND a.gmtype = '4'
    AND a.gmdept = 'PD'
    and a.gmstyp = 'A' -- ry1
    and b.gtdate between '09/01/2012' and curdate()
    and b.gtdtyp = 'S') c on trim(a.ptro#) = trim(c.gtdoc#)
where a.ptco# = 'RY1'
  and a.ptdate > 20120831  
  and a.ptfcdt <> 0 
  and c.gtdoc# is null 







  