
/*
We are wanting to send out a mailer to anyone that has purchased a Cadillac from us or have serviced a Cadillac with us.

Could I get:
First Name
Last Name
Street Address
City
State
Zip 
Email Address


For anyone that has had a Cadillac service with us in the last 5 years and anyone that has purchased a Cadillac (new and used) from us between the dates of 1/1/2010 and 1/30/2016.

Please let me know what other information you may need from me.

Thank you,
Morgan 

Hi Jon,

Would it be possible to also get the phone numbers for these records?

Morgan 

Morgan Hibma


*/

--select * from rydedata.bopname limit 10

select c.first_name, c.last_company_name, c.address_1, c.city, c.state_code, c.zip_code, c.email_address,
  c.phone_number, c.business_phone, c.cell_phone
-- select *
from rydedata.sdprhdr a
inner join rydedata.inpmast b on a.vin = b.inpmast_vin
  and trim(b.make) = 'CADILLAC'
inner join rydedata.bopname c on a.customer_key = c.bopname_Record_key
where c.first_name <> ''
  and a.open_date > 20130301
  and c.address_1 <> ''
group by c.first_name, c.last_company_name, c.address_1, c.city, c.state_code, c.zip_code, c.email_address,
  c.phone_number, c.business_phone, c.cell_phone
union
select c.first_name, c.last_company_name, c.address_1, c.city, c.state_code, c.zip_code, c.email_address,
  c.phone_number, c.business_phone, c.cell_phone
-- select *
from rydedata.bopmast a
inner join rydedata.inpmast b on a.bopmast_vin = b.inpmast_vin
  and trim(b.make) = 'CADILLAC'
inner join rydedata.bopname c on a.buyer_number = c.bopname_record_key  
where a.record_status = 'U'
  and a.date_capped between '01/01/2010' and '01/30/2016'
  and c.first_name <> ''
  and c.address_1 <> ''
group by c.first_name, c.last_company_name, c.address_1, c.city, c.state_code, c.zip_code, c.email_address,
  c.phone_number, c.business_phone, c.cell_phone













