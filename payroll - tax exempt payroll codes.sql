for unum, kim needs the W2 line 1 total for 2019 for all currently employed folks
that total is total gross wages minus the tax exempt payroll codes

-- initiall kim thought these were the codes
Select * 
from RYDEDATA.PYPCODES 
where trim(ded_pay_code) in ('107','111','295','96','91B','91C') 
  and company_number in ('RY1','RY2')
order by trim(ded_pay_code)

Select * 
from RYDEDATA.PYPCODES 
where exempt_1_fed_tax_ = 'Y'
  and company_number in ('RY1','RY2')
order by trim(ded_pay_code)


select a.pymast_company_number, trim(a.pymast_employee_number) as employee_number, a.employee_name,
  b.ded_pay_code, c.description, c.exempt_1_fed_tax_
from rydedata.pymast a
left join rydedata.pydeduct b on trim(a.pymast_employee_number) = trim(b.employee_number)
--  and b.code_type = '1'
left join rydedata.pypcodes c on b.company_number = c.company_number
  and trim(b.ded_pay_code) = trim(c.ded_pay_code)  
  and c.exempt_1_fed_tax_ = 'Y'
where a.pymast_company_number in ('RY1','RY2')
  and a.active_code <> 'T'
--  and trim(a.pymast_employee_number) = '17534'
  and c.exempt_1_fed_tax_ = 'Y'
order by a.pymast_company_number, a.employee_name

-- here are all the actual tax exempt codes 
select distinct a.pymast_company_number, b.ded_pay_code, c.description
from rydedata.pymast a
left join rydedata.pydeduct b on trim(a.pymast_employee_number) = trim(b.employee_number)
--  and b.code_type = '1'
left join rydedata.pypcodes c on b.company_number = c.company_number
  and trim(b.ded_pay_code) = trim(c.ded_pay_code)  
  and c.exempt_1_fed_tax_ = 'Y'
where a.pymast_company_number in ('RY1','RY2')
  and a.active_code <> 'T'
--  and trim(a.pymast_employee_number) = '17534'
  and c.exempt_1_fed_tax_ = 'Y'
order by a.pymast_company_number, b.ded_pay_code