-- 9/27/11
/*
-- whats the difference between pyhscdta (payroll code transaction file) and pydeduct (employee deduction control file)
-- pydeduct is the deduction amt per check
Name, birth, age, gender, salary, hire date, pay period (biweekly or semi-monthly), health 107, dental 111, disability 87, usable 281, life 287, critical care 293 gross pay (ytd)

format birthdate and empdate as dates

had a lot of null Gross : needed to change hyear is gross query agains pyhsdta
oops, left out salary

10/10/11
  added address and occupation
  verified that Hourly Rate is ok

10/10/12 -- see below
-- must set year - yhdcyy 

  need to est gross for entire year of 2012
    if hire date is before 1/1/12 then ytd + ytd/3
    else ytd + (ytd/(9 - month(hiredate))* 3
*/
Select distinct p.ymco# as Company, /*trim(p.ymempn) as EmpNo,*/ ymname as Name, b.Birthdate, 
  year(now()) - year(b.birthdate) -
    case 
      when
        month(b.BirthDate)*100 + day(b.BirthDate) > month(CurDate())*100 + day(CurDate()) then 1
      else 0
    end as Age,
  ymsex as Gender,
--  ymmari as"Marital Status", 
  ymstre as Address, ymcity as City, ymstat as State, ymzip as zip,  
  case 
    when cast(right(trim(ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as "Hire Date",
  case ympper
    when 'B' then 'Bi-Weekly'
    when 'S' then 'Semi-Monthly'
  end as "Pay Period",
  case ymactv
    when 'A' then 'Full'
    when 'P' then 'Part'
  end as "Full or Part",
  p.ymrate as "Hourly Rate",
  j.yrtext as Occupation,
  coalesce((select yddamt
    from rydedata.pydeduct -- employee deduction control file
    where ydempn = p.ymempn
    and yddcde = '107'), 0) as Health,
  coalesce((select yddamt
    from rydedata.pydeduct -- employee deduction control file
    where ydempn = p.ymempn
    and yddcde = '111'), 0) as "Dental",
  coalesce((select yddamt
    from rydedata.pydeduct
    where ydempn = p.ymempn
    and yddcde = '87'), 0) as Disability, -- "VST/VLTD",  
  coalesce((select yddamt
    from rydedata.pydeduct -- employee deduction control file
    where ydempn = p.ymempn
    and yddcde in ('281', '299')), 0) as UsAble, -- cancer insurance, ry1 & ry2 codes are different
  coalesce((select yddamt
    from rydedata.pydeduct
    where ydempn = p.ymempn
    and yddcde = '287'), 0) as Life, -- "EE VTL/SP VTL", 
  coalesce((select yddamt
    from rydedata.pydeduct
    where ydempn = p.ymempn
    and yddcde = '293'), 0) as "Critical Care", -- "CH VTL",
  (select sum(yhdtgp) -- must set year - yhdcyy
    from rydedata.pyhshdta
    where yhdemp = p.ymempn
    and yhdcyy = 12) as "YTD Gross"     
from rydedata.pymast p
left join rydedata.pyprhead ph on ph.yrempn = p.ymempn
left join rydedata.pyprjobd j on j.yrjobd = ph.yrjobd and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') -- job descriptions, eliminate dups
-- where trim(ymco#) = 'RY1'
left join ( -- generate birth date
  select ymempn, 
    case 
      when cast(right(trim(ymbdte),2) as integer) < 20 then 
        cast (
          case length(trim(ymbdte))
            when 5 then  '20'||substr(trim(ymbdte),4,2)||'-'|| '0' || left(trim(ymbdte),1) || '-' ||substr(trim(ymbdte),2,2)
            when 6 then  '20'||substr(trim(ymbdte),5,2)||'-'|| left(trim(ymbdte),2) || '-' ||substr(trim(ymbdte),3,2)
          end as date) 
      else  
        cast (
          case length(trim(ymbdte))
            when 5 then  '19'||substr(trim(ymbdte),4,2)||'-'|| '0' || left(trim(ymbdte),1) || '-' ||substr(trim(ymbdte),2,2)
            when 6 then  '19'||substr(trim(ymbdte),5,2)||'-'|| left(trim(ymbdte),2) || '-' ||substr(trim(ymbdte),3,2)
          end as date) 
      end as BirthDate
  from rydedata.pymast
  where ymactv <> 'T'
  and length(trim(ymname)) > 4) b on p.ymempn = b.ymempn
where ymactv in ('A','P')
and trim(p.ymname) <> 'TEST'
order by ymco#, ymName

/*
Usable? 
select * from pydeduct where yddcde = '281'

select p.ymname, d.*
from pymast p
inner join pydeduct d on p.ymempn = d.ydempn
where ymactv in ('A','P')
and trim(p.ymname) <> 'TEST'
and yddcde = '281'


select yddamt
    from pydeduct
     
    where yddcde = '281'
*/

/* 
10/10/12
  need to est gross for entire year of 2012
    if hire date is before 1/1/12 then ytd + ytd/3
    else ytd + (ytd/(9 - month(hiredate))* 3


10/11/12
YTD Gross is subtracting AR and shouldn't

AR comes from pyhscdta where yhccde = 83

-- exasmples from kim: shane anderson & dale axtman
select yhdemp, sum(yhdtgp) - coalesce(sum(b.yhccam), 0)
-- select *
from rydedata.pyhshdta a
left join rydedata.pyhscdta b on a.yhdemp = b.yhcemp
  and a.ypbnum = b.ypbnum
  and trim(b.yhccde) = '83'
where trim(yhdemp) in ('16298', '18005')
    and a.yhdcyy = 12
    --and a.ypbnum = 928000
group by yhdemp
*/

select a.*, 
  case 
    when name = 'MOLSTAD,BRANDON L' then "YTD Gross" + "YTD Gross"/3
    when year("Hire Date") < 2012 then "YTD Gross" + "YTD Gross"/3
    when year("Hire Date") = 2012 and month("Hire Date") = 9 then "YTD Gross" + "YTD Gross"*3
    when year("Hire Date") = 2012 and month("Hire Date") = 10 then 0
    else "YTD Gross" + ("YTD Gross"/(9 - month("Hire Date")))* 3
  end as "Est 2012 Total Gross"
from (
Select distinct p.ymco# as Company, /*trim(p.ymempn) as EmpNo,*/ ymname as Name, b.Birthdate, 
  year(now()) - year(b.birthdate) -
    case 
      when
        month(b.BirthDate)*100 + day(b.BirthDate) > month(CurDate())*100 + day(CurDate()) then 1
      else 0
    end as Age,
  ymsex as Gender,
--  ymmari as"Marital Status", 
  ymstre as Address, ymcity as City, ymstat as State, ymzip as zip,  
  case 
    when cast(right(trim(ymhdte),2) as integer) < 20 then 
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.ymhdte))
          when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
          when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
        end as date) 
    end as "Hire Date",
  case ympper
    when 'B' then 'Bi-Weekly'
    when 'S' then 'Semi-Monthly'
  end as "Pay Period",
  case ymactv
    when 'A' then 'Full'
    when 'P' then 'Part'
  end as "Full or Part",
  p.ymrate as "Hourly Rate",
  j.yrtext as Occupation,
  coalesce((select yddamt
    from rydedata.pydeduct -- employee deduction control file
    where ydempn = p.ymempn
    and yddcde = '107'), 0) as Health,
  coalesce((select yddamt
    from rydedata.pydeduct -- employee deduction control file
    where ydempn = p.ymempn
    and yddcde = '111'), 0) as "Dental",
  coalesce((select yddamt
    from rydedata.pydeduct
    where ydempn = p.ymempn
    and yddcde = '87'), 0) as Disability, -- "VST/VLTD",  
  coalesce((select yddamt
    from rydedata.pydeduct -- employee deduction control file
    where ydempn = p.ymempn
    and yddcde in ('281', '299')), 0) as UsAble, -- cancer insurance, ry1 & ry2 codes are different
  coalesce((select yddamt
    from rydedata.pydeduct
    where ydempn = p.ymempn
    and yddcde = '287'), 0) as Life, -- "EE VTL/SP VTL", 
  coalesce((select yddamt
    from rydedata.pydeduct
    where ydempn = p.ymempn
    and yddcde = '293'), 0) as "Critical Care", -- "CH VTL",
  coalesce((
    select sum(yhdtgp) - coalesce(sum(b.yhccam), 0)
    from rydedata.pyhshdta a
    left join rydedata.pyhscdta b on a.yhdemp = b.yhcemp
      and a.ypbnum = b.ypbnum
      and trim(b.yhccde) = '83'
    where yhdemp = p.ymempn
    and a.yhdcyy = 12), 0) as "YTD Gross"  -- must set year - yhdcyy 
from rydedata.pymast p
left join rydedata.pyprhead ph on ph.yrempn = p.ymempn
left join rydedata.pyprjobd j on j.yrjobd = ph.yrjobd and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') -- job descriptions, eliminate dups
-- where trim(ymco#) = 'RY1'
left join ( -- generate birth date
  select ymempn, 
    case 
      when cast(right(trim(ymbdte),2) as integer) < 20 then 
        cast (
          case length(trim(ymbdte))
            when 5 then  '20'||substr(trim(ymbdte),4,2)||'-'|| '0' || left(trim(ymbdte),1) || '-' ||substr(trim(ymbdte),2,2)
            when 6 then  '20'||substr(trim(ymbdte),5,2)||'-'|| left(trim(ymbdte),2) || '-' ||substr(trim(ymbdte),3,2)
          end as date) 
      else  
        cast (
          case length(trim(ymbdte))
            when 5 then  '19'||substr(trim(ymbdte),4,2)||'-'|| '0' || left(trim(ymbdte),1) || '-' ||substr(trim(ymbdte),2,2)
            when 6 then  '19'||substr(trim(ymbdte),5,2)||'-'|| left(trim(ymbdte),2) || '-' ||substr(trim(ymbdte),3,2)
          end as date) 
      end as BirthDate
  from rydedata.pymast
  where ymactv <> 'T'
  and length(trim(ymname)) > 4) b on p.ymempn = b.ymempn
where ymactv in ('A','P')
and trim(p.ymname) <> 'TEST') a
order by company, name



-- 9/25/13
Hi Jon would you be able to do a report with this info on it that I could send to Ken (Mutual of Omaha) by tomorrow afternoon?
Employee name, by  store,  hire date, part or full time, rate of pay, income, biweekly/monthly

select a.*
from (
  Select distinct p.ymco# as Store, /*trim(p.ymempn) as EmpNo,*/ ymname as Name, 
    case 
      when cast(right(trim(ymhdte),2) as integer) < 20 then 
        cast (
          case length(trim(p.ymhdte))
            when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
            when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
          end as date) 
      else  
        cast (
          case length(trim(p.ymhdte))
            when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
            when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
          end as date) 
      end as "Hire Date",
    case ymactv
      when 'A' then 'Full'
      when 'P' then 'Part'
    end as "Full or Part",
    p.ymrate as "Hourly Rate",
    coalesce((
      select sum(yhdtgp) - coalesce(sum(b.yhccam), 0)
      from rydedata.pyhshdta a
      left join rydedata.pyhscdta b on a.yhdemp = b.yhcemp
        and a.ypbnum = b.ypbnum
        and trim(b.yhccde) = '83'
      where yhdemp = p.ymempn
      and a.yhdcyy = 13), 0) as "YTD Gross",  -- must set year - yhdcyy 
    case ympper
      when 'B' then 'Bi-Weekly'
      when 'S' then 'Semi-Monthly'
    end as "Pay Period"
  from rydedata.pymast p
  where ymactv in ('A','P')
  and trim(p.ymname) <> 'TEST') a
order by store, name

-- 9/26/13
Thanks Jon I missed a few fields I also need date of birth, male/female, and job title
I looked at the first few people on the report for the ytd gross  Dharma I show 24868.83,  Krishna I show 7205.00, and  Sabitra is 16852.63 .  that one matches what you pulled
But the other 2 do not
Added the missing fields, removed subtracting AR from YTD Gross

select a.*
from (
  Select  distinct p.ymco# as Store, /*trim(p.ymempn) as EmpNo,*/ ymname as Name, ymsex as Gender, b.birthdate,
    case 
      when cast(right(trim(ymhdte),2) as integer) < 20 then 
        cast (
          case length(trim(p.ymhdte))
            when 5 then  '20'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
            when 6 then  '20'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
          end as date) 
      else  
        cast (
          case length(trim(p.ymhdte))
            when 5 then  '19'||substr(trim(p.ymhdte),4,2)||'-'|| '0' || left(trim(p.ymhdte),1) || '-' ||substr(trim(p.ymhdte),2,2)
            when 6 then  '19'||substr(trim(p.ymhdte),5,2)||'-'|| left(trim(p.ymhdte),2) || '-' ||substr(trim(p.ymhdte),3,2)
          end as date) 
      end as "Hire Date",
    j.yrtext as"Job Title",
    case ymactv
      when 'A' then 'Full'
      when 'P' then 'Part'
    end as "Full or Part",
    p.ymrate as "Hourly Rate",
-- do not subtract AR
--    coalesce((
--      select sum(yhdtgp) - coalesce(sum(b.yhccam), 0)
--      from rydedata.pyhshdta a
--      left join rydedata.pyhscdta b on a.yhdemp = b.yhcemp
--        and a.ypbnum = b.ypbnum
--        and trim(b.yhccde) = '83'
--      where yhdemp = p.ymempn
--      and a.yhdcyy = 13), 0) as "YTD Gross",  -- must set year - yhdcyy 
    coalesce((
      select sum(yhdtgp)
      from rydedata.pyhshdta a
      where yhdemp = p.ymempn
      and a.yhdcyy = 13), 0) as "YTD Gross",  -- must set year - yhdcyy 
    case ympper
      when 'B' then 'Bi-Weekly'
      when 'S' then 'Semi-Monthly'
    end as "Pay Period"
  from rydedata.pymast p
left join rydedata.pyprhead ph on ph.yrempn = p.ymempn
left join rydedata.pyprjobd j on j.yrjobd = ph.yrjobd and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') -- job descriptions, eliminate dups
-- where trim(ymco#) = 'RY1'
left join ( -- generate birth date
  select ymempn, 
    case 
      when cast(right(trim(ymbdte),2) as integer) < 20 then 
        cast (
          case length(trim(ymbdte))
            when 5 then  '20'||substr(trim(ymbdte),4,2)||'-'|| '0' || left(trim(ymbdte),1) || '-' ||substr(trim(ymbdte),2,2)
            when 6 then  '20'||substr(trim(ymbdte),5,2)||'-'|| left(trim(ymbdte),2) || '-' ||substr(trim(ymbdte),3,2)
          end as date) 
      else  
        cast (
          case length(trim(ymbdte))
            when 5 then  '19'||substr(trim(ymbdte),4,2)||'-'|| '0' || left(trim(ymbdte),1) || '-' ||substr(trim(ymbdte),2,2)
            when 6 then  '19'||substr(trim(ymbdte),5,2)||'-'|| left(trim(ymbdte),2) || '-' ||substr(trim(ymbdte),3,2)
          end as date) 
      end as BirthDate
  from rydedata.pymast
  where ymactv <> 'T'
  and length(trim(ymname)) > 4) b on p.ymempn = b.ymempn
  where ymactv in ('A','P')
  and trim(p.ymname) <> 'TEST') a
order by store, name


