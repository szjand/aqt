select *
from rydedata.pyhshdta
where trim(yhdemp)= '1150510'

select *
from rydedata.glptrns
where trim(gtctl#) = '1150510'

@fromDate = '07/14/2013';
@thrudate = '07/27/2013';

select *
from rydedata.pypclockin
where trim(yiemp#)= '18005'
  and yiclkind between '07/14/2013' and '07/27/2013'

-- pull the actual check amounts to check the accrual amounts
-- pyactgr is fucked up (dist_code CALC)

select b.ymname, a.yhdbsp as gross, curr_fica_tax as FICA, curr_emplr_medicr, emplr_curr_ret
-- select *
from rydedata.pyhshdta a
left join rydedata.pymast b on trim(a.yhdemp) = trim(b.ymempn)
where ypbcyy = 113
  and ypbnum_ = 621130
  --and trim(yhdemp)= '141069'
  AND b.ymname in (
'ARRINGTON,TONY',           
'BERRY,ALAN S',             
'BJORNSETH, MARK L.',       
'BLUMHAGEN, KATHERINE M.',  
'CAHALAN,BENJAMIN C',       
'DANGERFIELD,JOEL',         
'DELOHERY,MICHAEL J',       
'EULISS,NED',               
'EVAVOLD,DANIEL J',         
'FOERSTER,KELLY J.',        
'GUSTAFSON,JUNE',           
'HANSON, KEVIN W',          
'HILL,BRIAN',               
'HUOT,MICHAEL',             
'LEAR,MICHAEL ALLEN',       
'LIND,KATHERINE',           
'LONGORIA,BEVERLEY ANNE',   
'LUEKER,DAVID C',           
'LYNCH,JAMES J',            
'MORRIS,GREG',              
'NEUMANN, ANDREW D',        
'OLSON,CHAD',               
'OSTLUND,ANNE B',           
'PEDERSON,DAVID A',         
'PETROTTO,MARYANN',         
'RYAN, ANTONIO R',          
'RYDELL,BRIAN L.',          
'SCHMIESS PENAS,JERI L',    
'STEINKE,MARK',             
'STINAR,DANIEL M',          
'YEM, RITHY MICHAEL R')
order by b.ymname

select b.ymname, a.yhdbsp as gross, curr_fica_tax as FICA, curr_emplr_medicr, emplr_curr_ret
-- select b.ymname, a.*
from rydedata.pyhshdta a
left join rydedata.pymast b on trim(a.yhdemp) = trim(b.ymempn)
where ypbcyy = 113
  --and ypbnum_ = 802130
  --and trim(yhdemp)= '141069'
  AND b.ymname =  'STEINKE,MARK'     

select * from rydedata.pyhscdta where ypbnum = 802130 and trim(yhcemp) = '11650'


select b.ymname, a.yhrate, reg_hours as RegHrs, a.yhdbsp as gross, OVertime_hours as OTHrs, 
overtime_amount as ot, 
cast(round(sick_leave_taken,2) as decimal(6,2)) as PTOHrs, 
cast(round(sick_leave_taken*yhrate,2) as decimal(6,2)) as PTO, 
yhdvac as VacHrs, cast(round(yhdvac*yhrate,2) as decimal(6,2)) as Vac, 
holiday_taken as HolHrs, 
holiday_taken*yhrate as Hol, curr_fica_tax as FICA, curr_emplr_medicr as Med, 
emplr_curr_ret + coalesce(c.yhccam, 0) as Retire
-- select b.ymname, a.*
from rydedata.pyhshdta a
left join rydedata.pymast b on trim(a.yhdemp) = trim(b.ymempn)
left join rydedata.pyhscdta c on a.ypbnum = c.ypbnum and a.ypbcyy = c.ypbcyy and trim(a.yhdemp) = trim(c.yhcemp)
  and trim(c.yhccde) = '99' and yhctyp = '5'
where a.ypbcyy = 113
  and a.ypbnum_ = 802130
  --and trim(yhdemp)= '141069'
  AND b.ymname in (

'ACHARYA, DHARMA',          
'ACHARYA, KRISHNA',         
'ACHARYA,SABITRA',          
'ADAMS, JOSHUA D',          
'ADHIKARI, CHOMA',          
'AHMED,AHMED O',            
'AMUNDSON,ELIZABETH M',     
'ANDREWS,DALE R',           
'ANFILOFIEFF, IVAN M',      
'AXTMAN,DALE M.',           
'BEAR,JEFFERY L',           
'BERG,STEPHEN R.',          
'BESTUL, KIMBERLY A',       
'BHATTARAI, GOPAL',         
'BHATTARAI, MON B',         
'BHATTARAI, SUK BAHADUR',   
'BJERKE, KRISTIN L',        
'BOHM,DOUGLAS',             
'BRAATEN,DEAN T',           
'BRAGUNIER, KYLE D',        
'BREKKE, DEVON Y',          
'BROUILLET,CHRISTOPHER',    
'BURNETT, NICKOLAS D',      
'BURSINGER,TRAVIS C',       
'CARLSON,KENNETH A',        
'COCHRAN,MICHELLE C.L.',    
'COLLINGS,ETHAN F',         
'COLLINS, BRIAN J',         
'COPELAND, BRANDON J',      
'CRANE, AUBREY LYNN',       
'DAVID,GORDON',             
'DAVIDSON,GILFORD',         
'DELEON, CORTEZ',           
'DOERR, ROY',               
'DRISCOLL,TERRANCE',        
'DUCKSTAD,JARED J.',        
'DVORAK, JULIA M',          
'EGELAND, TALON M',         
'EGSTAD,KEITH R',           
'EICHHORST,NOEL E.',        
'ENNO, JORDAN E',           
'ESPELIEN,TYLER T',         
'ESPELUND,KENNETH MIKE',    
'EVENSON,GARRETT',          
'FEIST, DAKOTA J',          
'FEIST,LUDWIG H',           
'FLATIN, JERRY C',          
'FLIKKA,MATTHEW G',         
'FRANKS,RAYMOND',           
'FRESHLEY, FREDERICK A',    
'FRUETEL, STEVEN R',        
'GAGNON,TRACI L',           
'GARDNER, JOHN T',          
'GARDNER,CHAD A',           
'GERSZEWSKI, NICK L',       
'GIRODAT,JEFF S.',          
'GRAY,NATHAN J',            
'HAGER, SARA E',            
'HAMMAN, CALEB L',          
'HARMEL, CADE M',           
'HASTINGS,DAWN',            
'HEFFERNAN, JOSH',          
'HELGESON, JEFFREY S.',     
'HEMINGTON,AARON',          
'HEPOLA,JAMES W',           
'HILLEBRAND,BROCK D',       
'HOLTER,WENDY A',           
'HOLWERDA,KIRBY W',         
'HOMMERDING,AARON',         
'HOPKINS,AMANDA J',         
'HULST, MICHELLE M',        
'HUNTER, JASON W',          
'IVERSON,ARNOLD W.',        
'JACOBSON,PETER A',         
'JAMA,JAMA O',              
'JERSTAD,KENNETH',          
'JOHNKE,JEFFREY W',         
'JOHNSON, CODY',            
'JOHNSON, JOSEPH',          
'JOHNSON, ROBERT C',        
'JOHNSON,BRANDON J',        
'JOHNSTON, NICHOLAS E',     
'KARTES,MACKENZIE C',       
'KAZMIERCZAK,JOHNATHON J',  
'KEMPERT, ADAM E',          
'KILMER,JUSTIN',            
'KNUDSON, KENNETH',         
'KOENEN,DEBRA M',           
'KOLLER,MARK D.',           
'KOLSTAD,KIRK R.',          
'KREWSON, ANTHONY D',       
'KUEFLER, JAMES',           
'LARSON,JONATHAN L',        
'LAUGHLIN,LARRY J',         
'LEGACIE, BROOKE K',        
'LINDQUIST, ADAM J',        
'LUCERO, PATRICIA F',       
'MAGNUSON,TYLER D',         
'MANGAN,RICHARD',           
'MAREK,DAYTON C',           
'MATHISEN,LAURIE S.',       
'MAVITY,ROBERT',            
'MCCOWAN, JACOB D',         
'MCCOY, BLAKE R',           
'MCKAY, DAVID B',           
'MCNAMEE, MATTHEW L',       
'MCVEIGH,DENNIS',           
'METZGER,JUSTIN',           
'MILLER, MICHAEL T',        
'MILLER,KIM M',             
'MOHAMED, MOHAMED M',       
'MORLOCK,BRUCE A',          
'MORROW,KYLE G',            
'MOULDS,CRYSTAL D',         
'MUTZENBERGER, LUCAS T',    
'NORD, SKYLAR',             
'OHLSON, KENNETH R',        
'OLDENBURG,ERIKA L',        
'OLSON, JUSTIN S',          
'OLSON,DEREK A',            
'OLSON,JACOB',              
'OLSON,JAY P.',             
'ORTIZ, GUADALUPE',         
'OTTO,GORDON REED',         
'PASCHKE,MATTHEW G',        
'PETERSEN,RYAN C',          
'PETERSON,BRIAN D.',        
'PETERSON,MAVRIK',          
'PETROTTO, BRANDON M',      
'POKHREL, LEKH N',          
'POWELL,GAYLA R',           
'RADKE,DUANE',              
'RAMBERG,GARY',             
'RAMIREZ, MATTHEW L',       
'RATH, ALEXANDER M',        
'RECTOR, NOLAN K',          
'RICHARDSON,DARRICK J',     
'ROBLES,RUDOLPH',           
'ROGERS, MITCH A',          
'ROGNE,CRAIG A.',           
'ROSE,CORY',                
'ROSENAU,JEREMY',           
'ROSENTHAL, DAVID B',       
'RUD,JOSEPH A',             
'RUDE,ALEX J',              
'RYGG, KEN C',              
'SANCHEZ, ROGELIO',         
'SANNES, GUNNER L',         
'SANNES,MATTHEW',           
'SARKELA, STEVEN J',        
'SATTLER, NICHOLAS G',      
'SCHAFER, MYLES E',         
'SCHWAN,MICHAEL',           
'SEEBA,BRADLEY F.',         
'SEVIGNY,SCOTT',            
'SHERECK,LOREN',            
'SHROYER, RYAN G',          
'SHULER, JOHN L',           
'SHUMAKER, MARTIN D',       
'SIERACKI, ZACHARY F',      
'SLAMA,HEIDI S.',           
'SMOOT, DEREK W',           
'SOBOLIK,PAUL A.',          
'SORUM, JAYSON R',          
'STAFFORD, KEVIN C',        
'STALLARD, NICK D',         
'STALLMO,RICHARD D',        
'STEFFENS,DARRIN J',        
'STEIN, DONALD W',          
'STINAR,CORY',              
'STRANDELL,TRAVIS J',       
'STREETER,TIMOTHY',         
'STREITZ,DANIELLE M',       
'STROPNICKY,FRANK J.',      
'STRYKER, DAVID T',         
'SUBEDI,PARSU R',           
'SUNDBY, ROBERT A',         
'SWIFT, KENNETH E',         
'SWIFT, THOMAS C',          
'SYMONS,STEVEN',            
'SYVERSON,JOSHUA LEE',      
'TAMANG, CHATUR S',         
'TELKEN,WYATT',             
'THOMPSON,WYATT',           
'TROFTGRUBEN,RODNEY',       
'TYRRELL,JOHN H.',          
'UHRICH, TYLER J',          
'VACURA,SCOTT',             
'VANHESTE II,FRED E',       
'VIDA,SCOTT A',             
'WALDBAUER,ALEX',           
'WALDEN,CHRIS W.',          
'WALIOR,SUSAN E.',          
'WALKER, JOHNATHAN W',      
'WALTON,JOSHUA',            
'WARSAME,MAHAMED',          
'WELLI,GULLED',             
'WESTERHAUSEN,JEFFERY A',   
'WIEBUSCH, DANIEL R',       
'WIMBERLY, GREGORY',        
'WINFIELD,RYAN J.',         
'WINZER,CHRISTOPHER L',     
'YANISH,JORDAN',            
'YOUNG, JACOB A')     
order by b.ymname     
   

