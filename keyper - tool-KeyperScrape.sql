select
RTRIM(LTRIM(a.name)) as name,
case
  when asset_status = 'In' then coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet') 
  when asset_status = 'Out' then
    case  
      when a.user_id is null then 'Out to ??????'
      else 'Out to ' +  left(RTRIM(LTRIM(d.first_name)),3) + ' ' + RTRIM(LTRIM(d.last_name))
    end
end as KeyStatus 
from dbo.asset a
left join dbo.cabinet b on a.cabinet_id = b.cabinet_id 
left join dbo.system c on b.system_id = c.system_id  
left join dbo.[user] d on a.user_id = d.user_id 
where a.deleted = 0
and a.name like 'T10794GA'
--and a.name like 'g4418%'
--or a.name in ('G44209B','G44345B','G44183B','G44536P')

select * from dbo.cabinet


select a.name, a.asset_status, a.cabinet_id,
  b.system_id,
  c.name,
case
  when asset_status = 'In' then coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet') 
  when asset_status = 'Out' then
    case  
      when a.user_id is null then 'Out to ??????'
      else 'Out to ' +  left(RTRIM(LTRIM(d.first_name)),3) + ' ' + RTRIM(LTRIM(d.last_name))
    end
end as KeyStatus   
-- select count(*)
from dbo.asset a
left join dbo.cabinet b on a.cabinet_id = b.cabinet_id 
left join dbo.system c on b.system_id = c.system_id  
left join dbo.[user] d on a.user_id = d.user_id 
where a.deleted = 0
  and a.name = 'G47794a'
 



-- this is the python/pg query used for Vision
                select a.asset_name, 
                  convert(nvarchar(MAX), dateadd(hour, -5, a.transaction_date), 22) as transaction_date,
                  a.message, a.asset_status, 
                  convert(nvarchar(MAX), b.created_date, 101) as created_date,
                  coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet') as cabinet,
                  d.first_name, d.last_name, coalesce(e.name, 'none') as issue_reason
-- select count(*)                  
                from dbo.asset_transactions a
                inner join dbo.asset b on a.asset_id = b.asset_id
                inner join dbo.system c on a.system_id = c.system_id
                inner join dbo.[user] d on a.user_id = d.user_id
                left join dbo.issue_reason e on a.issue_reason_id = e.issue_reason_id 
                where a.asset_name is not null
                  and b.deleted = 0
                  and a.transaction_date > '12/31/2022 01:00:00'
                  -- and a.asset_name = 'G47260A'
                  and a.asset_name like '2023 G%'

select left(name, 20) from dbo.asset where name like '2023%'

select left(RTRIM(LTRIM(a.name)), 20), a.* from dbo.asset a where name like '2023%'


