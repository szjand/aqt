-- 7/12/18 i think this is good enough
Select upper(a.name) as stock_number, convert(nvarchar(MAX), a.created_date, 101) as created_date,
  coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet') as cabinet, 
  d.first_name, d.last_name, 
  e.transaction_date, e.message, e.asset_status, f.name as issue_reason
from dbo.asset a
left join dbo.cabinet b on a.cabinet_id = b.cabinet_id
left join dbo.system c on b.system_id = c.system_id
left join dbo.asset_transactions e on a.asset_id = e.asset_id
left join dbo.[user] d on e.user_id = d.user_id
left join dbo.issue_reason f on e.issue_reason_id = f.issue_reason_id
where a.deleted = 0
  and e.transaction_Date > dateadd(DD, -365, getdate())

--turns out vins are inadequately recorded to be of use
--1GKS2HKJ1JR106563 
--
--select *
--from dbo.attribute_single_value
--where rtrim(ltrim(value)) = '1GKS2HKJ1JR106563' 
--
--select *
--from dbo.asset
--where asset_id = 8615
--
--select * 
--from dbo.attribute
--where id = 7

Select upper(a.name) as stock_number, convert(nvarchar(MAX), a.created_date, 101) as created_date,
  coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet') as cabinet, 
  d.first_name, d.last_name, 
  e.transaction_date, e.message, e.asset_status, f.name as issue_reason
from dbo.asset a
left join dbo.cabinet b on a.cabinet_id = b.cabinet_id
left join dbo.system c on b.system_id = c.system_id
left join dbo.asset_transactions e on a.asset_id = e.asset_id
left join dbo.[user] d on e.user_id = d.user_id
left join dbo.issue_reason f on e.issue_reason_id = f.issue_reason_id
where a.deleted = 0
  and e.transaction_Date > dateadd(DD, -365, getdate())
  -- and e.asset_status = 'Out'
order by a.name, e.transaction_date

---< ----------------------------------------------------------------------------------------------
-- is cabinet an attribute of transaction or asset
-- this seems odd to me, same cabinet entire life of asset? ? ? ?
select stock_number
from (
select stock_number, cabinet
from (
Select upper(a.name) as stock_number, convert(nvarchar(MAX), a.created_date, 101) as created_date,
  coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet') as cabinet, 
  d.first_name, d.last_name, 
  e.transaction_date, e.message, e.asset_status, f.name as issue_reason
from dbo.asset a
left join dbo.cabinet b on a.cabinet_id = b.cabinet_id
left join dbo.system c on b.system_id = c.system_id
left join dbo.asset_transactions e on a.asset_id = e.asset_id
left join dbo.[user] d on e.user_id = d.user_id
left join dbo.issue_reason f on e.issue_reason_id = f.issue_reason_id
where a.deleted = 0
  and e.transaction_Date > dateadd(DD, -365, getdate())) x
where stock_number not like 'U%'
group by stock_number, cabinet
having count(*) > 1) xx
group by stock_number
having count(*) > 1

-- actually, some, but not all that many
select name
from (
select name, cabinet_id
from dbo.asset
group by name, cabinet_id) x
group by name
having count(*) > 1

-- in every case, one of the cabinets is null
select distinct name, cabinet_id
from dbo.asset
where name in (
select name
from (
select name, cabinet_id
from dbo.asset
group by name, cabinet_id) x
group by name
having count(*) > 1)
order by name

-- but what about transactions, may cabinet gets assigned to asset and that never changes
-- ahh yes many many
select asset_name
from (
select asset_name, cabinet_id
from dbo.asset_transactions
where transaction_Date > dateadd(DD, -365, getdate())
group by asset_name, cabinet_id) x
group by asset_name 
having count(*) > 1

---/> ----------------------------------------------------------------------------------------------

-- missing asset_name from dbo.asset_Transactions hasn't happend since 2013
select a.transaction_date, a.asset_name, b.*
from dbo.asset_transactions a
left join dbo.asset b on a.asset_id = b.asset_id
where a.asset_name is null
order by a.transaction_date desc 

-- asset creation_date vs min transaction_date
-- asset creation does not generate an asset-transaction row
-- this feels like a better foundation for the all data scrape
select a.name, a.created_date, b.*
from dbo.asset a
left join dbo.asset_transactions b on a.asset_id = b.asset_id
where a.created_date > dateadd(DD, -5, getdate())
order by a.name, b.transaction_date desc

-- already see a bad actor, g33390a, keys out since 6/16
-- feeling good about how this is progressing
-- go with this one
select a.asset_name, a.transaction_date, a.message, a.asset_status, 
  convert(nvarchar(MAX), b.created_date, 101) as created_date,
  coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet') as cabinet,
  d.first_name, d.last_name, e.name as issue_reason
from dbo.asset_transactions a
inner join dbo.asset b on a.asset_id = b.asset_id
inner join dbo.system c on a.system_id = c.system_id
inner join dbo.[user] d on a.user_id = d.user_id
left join dbo.issue_reason e on a.issue_reason_id = e.issue_reason_id
where b.created_date > dateadd(DD, -365, getdate())


--pk, this will work
select asset_name, transaction_date from (
select a.asset_name, a.transaction_date, a.message, a.asset_status, 
  convert(nvarchar(MAX), b.created_date, 101) as created_date,
  coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet') as cabinet,
  d.first_name, d.last_name, e.name as issue_reason
-- select a.*  
from dbo.asset_transactions a
inner join dbo.asset b on a.asset_id = b.asset_id
inner join dbo.system c on a.system_id = c.system_id
inner join dbo.[user] d on a.user_id = d.user_id
left join dbo.issue_reason e on a.issue_reason_id = e.issue_reason_id
where transaction_Date > dateadd(DD, -1000, getdate())
) x group by asset_name, transaction_date having count(*) > 1



!!!!!!!!! remember, transaction_date is UTC !!!!!!!!!!!!!!!!!!!

select a.asset_name, a.transaction_date, convert(nvarchar(MAX), dateadd(hour, -5, a.transaction_date), 22), a.message, a.asset_status, 
  convert(nvarchar(MAX), b.created_date, 101) as created_date,
  coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet') as cabinet,
  d.first_name, d.last_name, e.name as issue_reason
from dbo.asset_transactions a
inner join dbo.asset b on a.asset_id = b.asset_id
inner join dbo.system c on a.system_id = c.system_id
inner join dbo.[user] d on a.user_id = d.user_id
left join dbo.issue_reason e on a.issue_reason_id = e.issue_reason_id
where transaction_Date > dateadd(DD, -1, getdate())
order by transaction_date desc 
