/*
2-23-18
Vendor categorization and rules for Jeri
these first queries are from accounting - vendor spend report
*/

DECLARE GLOBAL TEMPORARY TABLE  SESSION.TEMP_1099_TYPES (
  code  CHAR(2),
  type char(26)); 

insert into session.temp_1099_types(code, type)
select * from table(
  values
    ('1','Rent'),
    ('2','Royalties'),  
    ('3','Other Income'),
    ('6','Medical & Health Care'),
    ('7','Nonemployee Compensation'),
    ('8','Interest Income'),
    ('S','Sole Proprietor'),
    ('14','Attorney'),
    ('','Not 1099 Vendor')) x;
    
      

select * from session.temp_1099_types;

select a.gtvnd# as VendorNumber, b.gcsnam as Name,  b.gcftid as TaxID, 
  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
--  coalesce(RY3Amount, 0) as "RY3 Amount",
  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
c.bncity as City, c.bnstcd as State, c.bnzip as zip, d.type as "1099 Vendor Type"
from (
  select trim(gtvnd#) as gtvnd#, 
  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
--  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
  from rydedata.glptrns
  where gtpost = 'Y'
    and trim(gtacct) in( '120300','220200') --,'320200')
    and gtdate between '01/01/2017' and current date ---------------------------------------
    and gttamt < 0
    and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
  group by gtvnd#) a
inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
left join rydedata.bopname c on b.gckey = c.bnkey
  and c.bntype = 'C'
left join SESSION.TEMP_1099_TYPES d on b.corporation_1099_ = d.code
where b.gcvnd# is not null 

-- issue #1 glptrns.gtvnd: not in fact_gl

select trim(gtvnd#) as gtvnd#, gtctl#, gtdoc#, gtref#,
sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount,
sum(case when trim(gtvnd#) = trim(gtctl#) then 1 else 0 end) as matched
--  sum(case when trim(gtacct) = '320200' then abs(gttamt) end) as RY3Amount
from rydedata.glptrns
where trim(gtacct) in( '120300','220200') --,'320200')
  and gtdate between '01/01/2018' and current date ---------------------------------------
  and gttamt < 0
  and gtvnd# <> ''
--and left(trim(gtvnd#), 2) = '11'
group by gtvnd#, gtctl#, gtdoc#, gtref#
  
  
select *
from rydedata.glptrns
where trim(gtacct) in( '120300','220200') --,'320200')
  and gtpost = 'Y'
  and gtdate between '01/01/2018' and current date ---------------------------------------
  and gttamt < 0
  and gtvnd# <> ''
  and trim(gtvnd#) <> trim(gtctl#)
  
select *
from rydedata.glpcust
where trim(vendor_number) = '1135'
  
 
select active, count(*)
from rydedata.glpcust  
group by active

-- arkona vendor report: 2976 rows, query: 2592 rows
-- ok, limited report to active, now 2582, close enough
select trim(vendor_number), search_name, record_key, active, customer_number, customer_type, vendor_type, last_invoice_date
-- select count(*)
from rydedata.glpcust
where active in ('V','B')
order by search_name
limit 200 offset 201

select * 
from rydedata.glpcust
where trim(vendor_number) = '1135'


select *
from rydedata.glptrns
where trim(gtvnd#) = '26330'
  and gtdate > '2017-01-01'

select *
from rydedata.glptrns
where trim(gtref#) = '515518'

select * from rydedata.glpcust limit 100

select *
from rydedata.glptrns a
where gtpost = 'Y'
  and trim(gtvnd#) = '26330'
  and gtvnd# <> ''
  and gtdate > '2017-12-31'
  and exists (
    select 1
    from rydedata.glpcust
    where active in ('B','V')
      and trim(vendor_number) = trim(a.gtvnd#))


select * from rydedata.glptrns where gttrn# = 3854243





  