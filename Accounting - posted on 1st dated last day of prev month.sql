select a.journal, a.transaction_date, b.gqdate, trim(a.account_number),
  a.transaction_amount, aa.department
from rydedata.glptrns a
join rydedata.glpmast aa on trim(a.account_number) = trim(aa.account_number)
  and department = 'NC'
  and year = 2020
join RYDEDATA.GLPDTIM b on a.transaction_number = b.gqtrn#
  and gqdate = 20200601
--  and gqco# <> 'RY5'
where a.transaction_Date = '05/31/2020'
  and a.company_number in ('RY1','RY2')
  and a.post_Status = 'Y'
  
  
select trim(a.account_number), sum(a.transaction_amount), listagg(distinct gquser)
-- select a.*
from rydedata.glptrns a
join rydedata.glpmast aa on trim(a.account_number) = trim(aa.account_number)
  and department = 'NC'
  and year = 2020
join RYDEDATA.GLPDTIM b on a.transaction_number = b.gqtrn#
  and gqdate = 20200601
--  and gqco# <> 'RY5'
where a.transaction_Date = '05/31/2020'
  and a.company_number in ('RY1','RY2')
  and a.post_Status = 'Y'  
group by trim(a.account_number)  
order by trim(a.account_number)  


select *
from rydedata.glpmast
where year = 2020
  and department = 'NC'
  and active = 'Y'
  and (
    account_Desc like 'CST%'
    or
    account_desc like 'SLS%')
  
  
select a.journal, trim(a.control_number) as control, trim(a.account_number) as account,
  a.transaction_amount, gquser
from rydedata.glptrns a
join rydedata.glpmast aa on trim(a.account_number) = trim(aa.account_number)
  and aa.year = 2020
  and aa.department = 'NC'
  and aa.active = 'Y'
  and (
    aa.account_Desc like 'CST%'
    or
    aa.account_desc like 'SLS%')  
join RYDEDATA.GLPDTIM b on a.transaction_number = b.gqtrn#
  and gqdate = 20200601
--  and gqco# <> 'RY5'
where a.transaction_Date = '05/31/2020'
  and a.company_number in ('RY1','RY2')
  and a.post_Status = 'Y'
   
   
select a.journal, trim(a.control_number) as control, trim(a.account_number) as account,
  a.transaction_amount, gquser, a.transaction_Date, b.gqdate
from rydedata.glptrns a
join rydedata.glpmast aa on trim(a.account_number) = trim(aa.account_number)
  and aa.year = 2020
  and aa.department = 'NC'
  and aa.active = 'Y'
  and (
    aa.account_Desc like 'CST%'
    or
    aa.account_desc like 'SLS%')  
join RYDEDATA.GLPDTIM b on a.transaction_number = b.gqtrn#
  and gqdate = 20200601
--  and gqco# <> 'RY5'
where a.transaction_Date between '05/01/2020' and  '05/31/2020'
  and a.company_number in ('RY1','RY2')
  and a.post_Status = 'Y'
      
      
select a.journal, trim(a.control_number) as control, trim(a.account_number) as account,
  a.transaction_amount, gquser, a.transaction_Date, b.gqdate
from rydedata.glptrns a
join rydedata.glpmast aa on trim(a.account_number) = trim(aa.account_number)
  and aa.year = 2020
  and aa.department = 'NC'
  and aa.active = 'Y'
  and (
    aa.account_Desc like 'CST%'
    or
    aa.account_desc like 'SLS%')  
join RYDEDATA.GLPDTIM b on a.transaction_number = b.gqtrn#
  and gqdate = 20200601
--  and gqco# <> 'RY5'
where a.transaction_Date between '05/01/2020' and  '05/31/2020'
  and a.company_number in ('RY1','RY2')
  and a.post_Status = 'Y'      
   


      
select aa.department, a.journal, trim(a.control_number) as control, trim(a.account_number) as account,
  a.transaction_amount, gquser, a.transaction_Date, b.gqdate
from rydedata.glptrns a
join rydedata.glpmast aa on trim(a.account_number) = trim(aa.account_number)
  and aa.year = 2020
--  and aa.department = 'NC'
  and aa.active = 'Y'
--  and (
--    aa.account_Desc like 'CST%'
--    or
--    aa.account_desc like 'SLS%')  
join RYDEDATA.GLPDTIM b on a.transaction_number = b.gqtrn#
  and gqdate = 20200531
--  and gqco# <> 'RY5'
-- where a.transaction_Date between '05/01/2020' and  '05/31/2020'
where a.company_number in ('RY1','RY2')
  and a.post_Status = 'Y'      
  
select *
from rydedata.glpdtim
where gqdate = 20200531  

select *
from rydedata.glptrns
where transaction_number between 4795430 and 479544


select *
from rydedata.glptrns
where transaction_date = '05/31/2020'
  and trim(account_number) = '1628001'
  
  

select journal, control_number, transaction_amount, trim(account_number), b.make
from rydedata.glptrns a
left join rydedata.inpmast b on trim(a.control_number) = trim(b.inpmast_Stock_number)
where transaction_date = '05/31/2020'  
  and trim(document_number) = 'SFE0520'
  and journal = ''
  