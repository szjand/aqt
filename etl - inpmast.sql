Select * 
from RYDEDATA.inpmast

Select count(*) --115649
from RYDEDATA.inpmast

select imco#, count(*)
from RYDEDATA.inpmast
group by imco#

select *
from rydedata.inpmast
where trim(imvin) in (
  select trim(imvin)
  from rydedata.inpmast
  where imco# = 'RY2')
order by imvin

-- all non RY1 records, have an RY1 record for the same vin
select *
from rydedata.inpmast i
where imco# <> 'RY1'
  and not exists (
    select 1
    from rydedata.inpmast
    where imvin = imvin
    and imco# = 'RY1')


-- select count(*) -- 115630
from rydedata.inpmast
where imco# = 'RY1'

-- natural key
select imstk#, imvin
from rydedata.inpmast
where imco# = 'RY1'
group by imstk#, imvin
  having count(*) > 1

-- base query
select 
  imco#, imvin, imstk#, imdoc#, imstat, 
  imgtrn, imtype, imfran, imyear, immake, 
  immcode, immodl, imbody, imcolr, imodom, 
  imdinv, imdsvc, imddlv, imdord, imsact, 
  imiact, imlic#, imkey, imodoma, imkey
from rydedata.inpmast
where imco# = 'RY1'
    

IMYEAR
IMODOM
IMDINV
IMDSVC
IMDDLV
IMDORD
IMKEY

select min(IMKEY), max(IMKEY)
from rydedata.inpmast
where imco# = 'RY1'



-- final query for code
select 
  imco#, imvin, imstk#, imdoc#, imstat, 
  imgtrn, imtype, imfran, imyear, immake, 
  immcode, immodl, imbody, imcolr, imodom, 
  case 
    when imdinv = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(imdinv), 4) || '-' || substr(digits(imdinv), 5, 2) || '-' || substr(digits(imdinv), 7, 2) as date)   
  end as imdinv,
  case 
    when imdsvc = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(imdsvc), 4) || '-' || substr(digits(imdsvc), 5, 2) || '-' || substr(digits(imdsvc), 7, 2) as date) 
  end as imdsvc,
  case 
    when imddlv = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(imddlv), 4) || '-' || substr(digits(imddlv), 5, 2) || '-' || substr(digits(imddlv), 7, 2) as date) 
  end as imddlv,
  case 
    when imdord = 0 then cast('9999-12-31' as date) 
    else cast(left(digits(imdord), 4) || '-' || substr(digits(imdord), 5, 2) || '-' || substr(digits(imdord), 7, 2) as date) 
  end as imdord,
  imsact, imiact, imlic#, imkey, imodoma, imkey
from rydedata.inpmast
where imco# = 'RY1'

select imstk#, imvin
from rydedata.inpmast
where imco# = 'RY1'
group by imstk#, imvin
  having count(*) > 1


/*
      OpenQuery(AdoQuery, 'select ' +
          '  imco#, imvin, imstk#, imdoc#, imstat, ' +
          '  imgtrn, imtype, imfran, imyear, immake, ' +
          '  immcode, immodl, imbody, imcolr, imodom, ' +
          '  case ' +
          '    when imdinv = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(imdinv), 4) || ''-'' || substr(digits(imdinv), 5, 2) || ''-'' || substr(digits(imdinv), 7, 2) as date) ' +
          '  end as imdinv, ' +
          '  case ' +
          '    when imdsvc = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(imdsvc), 4) || ''-'' || substr(digits(imdsvc), 5, 2) || ''-'' || substr(digits(imdsvc), 7, 2) as date)  ' +
          '  end as imdsvc, ' +
          '  case ' +
          '    when imddlv = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(imddlv), 4) || ''-'' || substr(digits(imddlv), 5, 2) || ''-'' || substr(digits(imddlv), 7, 2) as date) ' +
          '  end as imddlv, ' +
          '  case ' +
          '    when imdord = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(imdord), 4) || ''-'' || substr(digits(imdord), 5, 2) || ''-'' || substr(digits(imdord), 7, 2) as date) ' +
          '  end as imdord, ' +
          '  imsact, imiact, imlic#, imkey, imodoma, imkey ' +
          'from rydedata.inpmast ' +
          'where imco# = ''RY1''');
*/


select imcost, count(*)
from rydedata.inpmast
where imstat = 'I'
group by imcost
order by count(*) desc

select *
from rydedata.inpmast
where imstat = 'I'
  and imcost = 0


-- 7/28/31 dimVehicle

select * from rydedata.inpmast order by imupdts desc

select imvin from rydedata.inpmast group by imvin having count(*) > 1

-- limit to RY1, vin is now unique
select imvin from rydedata.inpmast where inpmast_company_number = 'RY1' group by imvin having count(*) > 1

select *
from rydedata.inpmast
where trim(imvin) in (
  select trim(imvin) 
  from rydedata.inpmast
  group by trim(imvin)
  having count(*) > 1)
order by imvin


-- ok, crts = updts
select * from rydedata.inpmast where year(imupdts) >= 2012 and imcrtts <> imupdts
select * from rydedata.inpmast where imcrtts <> imupdts

-- this makes no sense: only 49 vehicles added to inpmast in june 2013???
-- trying to trim down the size of the nightly scrape, but i just don't know if this makes sense
select month(imcrtts) as month, year(imcrtts) as year, count(*)
from rydedata.inpmast
group by month(imcrtts), year(imcrtts)
order by year(imcrtts), month(imcrtts)

-- vin added via service on 7/28/13, fucking ts fies are 01-01-0001
select * from rydedata.inpmast where trim(imvin) = '1GTR2WE77DZ125573'

select * from rydedata.sdprhdr where trim(ptvin) = '1GTR2WE77DZ125573'


select count(*) from rydedata.inpmast -- 133590

select distinct imccode, imecode, imtcode, imigkc, imtrkc,imkylc,imradc, imdlrc
from rydedata.inpmast

select imccode, min(imcrtts), max(imcrtts), count(*)
from rydedata.inpmast
group by imccode
order by count(*) desc 

select imchromeid, min(imcrtts), max(imcrtts), count(*)
from rydedata.inpmast
group by imchromeid
order by count(*) desc 


-- for now, fuck it, the initial cut of dimVehicle is for service,
-- i'll add columns as there is a need


-- real time ros need vehicle info
-- current dimVehicle whacks and replaces the entire fucking inpmast (stgArkonaINPMAST)
-- that takes something like 12 minutes, way too long for real time shit
-- this cuts it down significantly
select *
from rydedata.inpmast
where trim(imvin) in (
  select trim(ptvin)
  from rydedata.pdpphdr
  where ptdtyp = 'RO' and ptdoc# <> ''
  and cast(left(digits(PTDATE), 4) || '-' || substr(digits(PTDATE), 5, 2) || '-' || substr(digits(PTDATE), 7, 2) as date) = curdate()
  union
  select trim(ptvin)
  from rydedata.sdprhdr
  where ptco# in ('RY1','RY2') 
  and trim(ptro#) <> ''
  and (  
    cast(left(digits(PTDATE), 4) || '-' || substr(digits(PTDATE), 5, 2) || '-' || substr(digits(PTDATE), 7, 2) as date) = curdate() 
    or ( 
      ptcdat <> 0 
      and  
      cast(left(digits(PTCDAT), 4) || '-' || substr(digits(PTCDAT), 5, 2) || '-' || substr(digits(PTCDAT), 7, 2) as date) = curdate()) 
    or (
      ptfcdt <> 0 
      and  
      cast(left(digits(PTFCDT), 4) || '-' || substr(digits(PTFCDT), 5, 2) || '-' || substr(digits(PTFCDT), 7, 2) as date) = curdate())))

select *
from rydedata.bopname where date(bnupdts) = curdate()

       