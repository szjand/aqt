
/*select distinct p.ymco# as "Company", p.ymname as "Name", 
  case length(trim(p.ymbdte))
    when 5 then '0' || left(trim(p.ymbdte),1) || '/' || substr(trim(p.ymbdte),2,2) || '/' || substr(trim(p.ymbdte),4,2)
    when 6 then left(trim(p.ymbdte),2) || '/' || substr(trim(p.ymbdte),3,2) || '/' || substr(trim(p.ymbdte),5,2)
    else ''
  end as "Birth Date",
  p.ymdriv as "Driv Lic", p.ymstat as" State", j.yrtext as" Title",
  case length(trim(p.ymhdte))
    when 5 then '0' || left(trim(p.ymhdte),1) || '/' || substr(trim(p.ymhdte),2,2) || '/' || substr(trim(p.ymhdte),4,2)
    when 6 then left(trim(p.ymhdte),2) || '/' || substr(trim(p.ymhdte),3,2) || '/' || substr(trim(p.ymhdte),5,2)
    else ''
  end as "Hire Date",
  case length(trim(p.ymtdte))
    when 5 then '0' || left(trim(p.ymtdte),1) || '/' || substr(trim(p.ymtdte),2,2) || '/' || substr(trim(p.ymtdte),4,2)
    when 6 then left(trim(p.ymtdte),2) || '/' || substr(trim(p.ymtdte),3,2) || '/' || substr(trim(p.ymtdte),5,2)
    else ''
  end as "Term Date",
  p.ymhdte

from rydedata.pymast p
left join rydedata.pyprhead ph on ph.yrempn = p.ymempn
left join rydedata.pyprjobd j on j.yrjobd = ph.yrjobd and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') 
where length(trim(p.ymname)) > 4 -- garbage data

and (
  CAST ( -- since 6/1/2011
    CASE LENGTH(TRIM(p.YMTDTE))
      WHEN 5 THEN  '20'||SUBSTR(TRIM(p.YMTDTE),4,2)||'-'|| '0' || LEFT(TRIM(p.YMTDTE),1) || '-' ||SUBSTR(TRIM(p.YMTDTE),2,2)
      WHEN 6 THEN  '20'||SUBSTR(TRIM(p.YMTDTE),5,2)||'-'|| LEFT(TRIM(p.YMTDTE),2) || '-' ||SUBSTR(TRIM(p.YMTDTE),3,2)
    END AS DATE) between cast('2011-09-01' as date) and cast('2011-09-30' as date))
order by p.ymco#, p.ymName
*/

/*
11/15/11 from Kim
We did a report last year for federated insurance that I need again but we have a few changes
The report needs to be from 9-1-10 to 9-1-11
We need company,  name,  birth date,  driv lic,  state, title, hire date,  term date,   and total gross pay  (this is not on last years report)
I need that for company 1, 2  and 3
Also just on company 3  they want how much over time $  paid out
*/

/*
anyone that was employed between 9/1/10 and 9/1/11
so, what that looks like is any employee (not terminated before 9/1/10 or not terminated) and hired before 9/1/11
ok, got the dates
now need total gross for 9/1/10 to 9/1/11
*/

/*
I need a report of all recon staff and their salary along with hire date.  Then the other report I need all employees in car wash and recon with their hire date and driver license number.  Thank you I appreciate it.  

Maryann Petrotto, PHR

-- change it to current employees
*/
select distinct p.ymco# as "Company", p.ymname as "Name", 
  trim(p.ymdriv) as "Driv Lic", p.ymstat as" State", p.ymdept as Dept, c.yicdesc as "Dept Descr", j.yrtext as "Job Title",
  case length(trim(p.ymhdte))
    when 5 then '0' || left(trim(p.ymhdte),1) || '/' || substr(trim(p.ymhdte),2,2) || '/' || substr(trim(p.ymhdte),4,2)
    when 6 then left(trim(p.ymhdte),2) || '/' || substr(trim(p.ymhdte),3,2) || '/' || substr(trim(p.ymhdte),5,2)
    else ''
  end as "Hire Date",
  p.ymrate as Salary
from rydedata.pymast p
left join rydedata.pyprhead ph on ph.yrempn = p.ymempn
  and trim(ph.yrjobd) <> '' -- Dale Andrews
left join rydedata.pyprjobd j on j.yrjobd = ph.yrjobd 
  and j.yrtext not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') 
left join rydedata.pypclkctl c on p.ymdept = c.yicdept 
where length(trim(p.ymname)) > 4 -- garbage data
  and p.ymdept in ('27', '24')
  and ymactv <> 'T'
order by p.ymName




