/*
11/04/2022
For each of the below accounts

124200
124201
124205
124219
124299
124300
124400

1�Start with current balance in each account
2�Back out any transactions with actual post dates after 10/22/22 at 3:25 pm
�	Any credit postings should be added back
�	Any debit postings should be subtracted
3�This should give an ending balance as of the inventory date

If I�m not clear on any of this, give me and call.

Jeri

*/
select transaction_date, account_number, control_number,
  document_number, reference_number, description, transaction_amount, b.gqdate, b.gqtime, b.gquser
from rydedata.glptrns a
join rydedata.glpdtim b on a.transaction_number = b.gqtrn#
where gqdate > 20221022
  and trim(account_number) in ('124219','124200','124201','124205','124299','124300','124400')
order by a.account_number

-- the account balances
select account_number, nov_balance11
from rydedata.glpmast
where year = 2022
   and trim(account_number) in ('124219','124200','124201','124205','124299','124300','124400')