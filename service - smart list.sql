-- service
select count(*)  -- 184650
from rydedata.sdprhdr a
inner join rydedata.inpmast b on a.vin = b.inpmast_vin
where b.year between 2014 and 2018

-- sales
select count(*) -- 17800
from rydedata.bopmast a
inner join rydedata.inpmast b on a.bopmast_vin = b.inpmast_vin
where b.year between 2014 and 2018



select count(*) -- 17073
-- select count(distinct a.bopmast_vin)
from rydedata.bopmast a
inner join rydedata.inpmast b on a.bopmast_vin = b.inpmast_vin
where b.year between 2014 and 2018
  and trim(lower(b.make)) in ('chevrolet','buick','gmc','cadillac','honda','nissan')
  and date_capped is not null


select a.bopmast_vin, a.record_key, a.date_capped, a.bopmast_search_name, b.make, b.year
-- select count(*) -- 14426
from rydedata.bopmast a
inner join rydedata.inpmast b on a.bopmast_vin = b.inpmast_vin
where b.year between 2014 and 2018
  and trim(lower(b.make)) in ('chevrolet','buick','gmc','cadillac','honda','nissan')
  and a.date_capped = (
    select max(date_capped)
    from rydedata.bopmast
    where bopmast_vin = a.bopmast_vin)
  and a.record_status = 'U'

 
 
select a.ro_number, a.vin, a.cust_name, a.customer_key, close_date, b.make, b.year
-- select count(*) -- 19453
from rydedata.sdprhdr a
inner join rydedata.inpmast b on a.vin = b.inpmast_vin
where b.year between 2014 and 2018
  and trim(lower(b.make)) in ('chevrolet','buick','gmc','cadillac','honda','nissan')
  and a.close_date = (
    select max(close_date)
    from rydedata.sdprhdr
    where vin = a.vin)
  and cust_name not like '%VOIDED%'
  and cust_name not like 'INVENTORY%'  
  and cust_name not like 'NATIONAL%'
  and length(trim(ro_number)) > 6
  
    
-- getting about half the number in postgres    

select left(vin, 8), count(*) from (      
select a.ro_number, a.vin, a.cust_name, a.customer_key, close_date, b.make, b.year
-- select count(*) -- 19453
from rydedata.sdprhdr a
inner join rydedata.inpmast b on a.vin = b.inpmast_vin
where b.year between 2014 and 2018
  and trim(lower(b.make)) in ('chevrolet','buick','gmc','cadillac','honda','nissan')
  and a.close_date = (
    select max(close_date)
    from rydedata.sdprhdr
    where vin = a.vin)
  and cust_name not like '%VOIDED%'
  and cust_name not like 'INVENTORY%'  
  and cust_name not like 'NATIONAL%'
  and length(trim(ro_number)) > 6        
) x 
where left(vin, 1) in ('1','2','3','4')
group by left(vin,8)
order by left(vin,8)       
            
              
select a.ro_number, a.vin, a.cust_name, a.customer_key, close_date, b.make, b.year
-- select count(*) -- 19453
from rydedata.sdprhdr a
inner join rydedata.inpmast b on a.vin = b.inpmast_vin
where b.year between 2014 and 2018
  and trim(lower(b.make)) in ('chevrolet','buick','gmc','cadillac','honda','nissan')
  and a.close_date = (
    select max(close_date)
    from rydedata.sdprhdr
    where vin = a.vin)
  and cust_name not like '%VOIDED%'
  and cust_name not like 'INVENTORY%'  
  and cust_name not like 'NATIONAL%'
  and length(trim(ro_number)) > 6   
  and left(vin,4) = '1GAZ'                
                  
                      