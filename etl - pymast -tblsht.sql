select YMCO#, YMEMPN, YMACTV, 
--  YMDEPT, YMSTCD, 
  YMNAME, -- YMSTRE, 
--  YMCITY, YMSTAT, YMZIP, YMAREA, YMPHON, YMSS#,
--  YMDRIV, 
  case 
    when cast(right(trim(p.YMBDTE),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMBDTE))
          when 5 then  '20'||substr(trim(p.YMBDTE),4,2)||'-'|| '0' || left(trim(p.YMBDTE),1) || '-' ||substr(trim(p.YMBDTE),2,2)
          when 6 then  '20'||substr(trim(p.YMBDTE),5,2)||'-'|| left(trim(p.YMBDTE),2) || '-' ||substr(trim(p.YMBDTE),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.YMBDTE))
          when 5 then  '19'||substr(trim(p.YMBDTE),4,2)||'-'|| '0' || left(trim(p.YMBDTE),1) || '-' ||substr(trim(p.YMBDTE),2,2)
          when 6 then  '19'||substr(trim(p.YMBDTE),5,2)||'-'|| left(trim(p.YMBDTE),2) || '-' ||substr(trim(p.YMBDTE),3,2)
        end as date) 
  end as YMBDTE,
  case 
    when cast(right(trim(p.YMHDTE),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMHDTE))
          when 5 then  '20'||substr(trim(p.YMHDTE),4,2)||'-'|| '0' || left(trim(p.YMHDTE),1) || '-' ||substr(trim(p.YMHDTE),2,2)
          when 6 then  '20'||substr(trim(p.YMHDTE),5,2)||'-'|| left(trim(p.YMHDTE),2) || '-' ||substr(trim(p.YMHDTE),3,2)
        end as date) 
    else  
      cast (
        case length(trim(p.YMHDTE))
          when 5 then  '19'||substr(trim(p.YMHDTE),4,2)||'-'|| '0' || left(trim(p.YMHDTE),1) || '-' ||substr(trim(p.YMHDTE),2,2)
          when 6 then  '19'||substr(trim(p.YMHDTE),5,2)||'-'|| left(trim(p.YMHDTE),2) || '-' ||substr(trim(p.YMHDTE),3,2)
        end as date) 
  end as YMHDTE,
  case 
    when p.YMHDTO = 0 then date('9999-12-31')
    when cast(right(trim(p.YMHDTO),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMHDTO))
          when 5 then  '20'||substr(trim(p.YMHDTO),4,2)||'-'|| '0' || left(trim(p.YMHDTO),1) || '-' ||substr(trim(p.YMHDTO),2,2)
          when 6 then  '20'||substr(trim(p.YMHDTO),5,2)||'-'|| left(trim(p.YMHDTO),2) || '-' ||substr(trim(p.YMHDTO),3,2)
        end as date) 
    when cast(right(trim(p.YMHDTO),2) as integer) >= 20 then  
      cast (
        case length(trim(p.YMHDTO))
          when 5 then  '19'||substr(trim(p.YMHDTO),4,2)||'-'|| '0' || left(trim(p.YMHDTO),1) || '-' ||substr(trim(p.YMHDTO),2,2)
          when 6 then  '19'||substr(trim(p.YMHDTO),5,2)||'-'|| left(trim(p.YMHDTO),2) || '-' ||substr(trim(p.YMHDTO),3,2)
        end as date) 
  end as YMHDTO,
  case 
    when p.YMRDTE = 0 then date('9999-12-31')
    when cast(right(trim(p.YMRDTE),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMRDTE))
          when 5 then  '20'||substr(trim(p.YMRDTE),4,2)||'-'|| '0' || left(trim(p.YMRDTE),1) || '-' ||substr(trim(p.YMRDTE),2,2)
          when 6 then  '20'||substr(trim(p.YMRDTE),5,2)||'-'|| left(trim(p.YMRDTE),2) || '-' ||substr(trim(p.YMRDTE),3,2)
        end as date) 
    when cast(right(trim(p.YMRDTE),2) as integer) >= 20 then  
      cast (
        case length(trim(p.YMRDTE))
          when 5 then  '19'||substr(trim(p.YMRDTE),4,2)||'-'|| '0' || left(trim(p.YMRDTE),1) || '-' ||substr(trim(p.YMRDTE),2,2)
          when 6 then  '19'||substr(trim(p.YMRDTE),5,2)||'-'|| left(trim(p.YMRDTE),2) || '-' ||substr(trim(p.YMRDTE),3,2)
        end as date) 
  end as YMRDTE,
  case 
    when p.YMTDTE = 0 then date('9999-12-31')
    when cast(right(trim(p.YMTDTE),2) as integer) < 20 then 
      cast (
        case length(trim(p.YMTDTE))
          when 5 then  '20'||substr(trim(p.YMTDTE),4,2)||'-'|| '0' || left(trim(p.YMTDTE),1) || '-' ||substr(trim(p.YMTDTE),2,2)
          when 6 then  '20'||substr(trim(p.YMTDTE),5,2)||'-'|| left(trim(p.YMTDTE),2) || '-' ||substr(trim(p.YMTDTE),3,2)
        end as date) 
    when cast(right(trim(p.YMTDTE),2) as integer) >= 20 then  
      cast (
        case length(trim(p.YMTDTE))
          when 5 then  '19'||substr(trim(p.YMTDTE),4,2)||'-'|| '0' || left(trim(p.YMTDTE),1) || '-' ||substr(trim(p.YMTDTE),2,2)
          when 6 then  '19'||substr(trim(p.YMTDTE),5,2)||'-'|| left(trim(p.YMTDTE),2) || '-' ||substr(trim(p.YMTDTE),3,2)
        end as date) 
  end as YMTDTE, 
  YMSEX, YMMARI, YMFDEX, YMFDAD, 
  YMFDAP, YMCLAS, YMEEIC, YMECLS, YMPPER, YMSALY, YMRATE, YMDIST, 
  YMRETE, YMSTTX, YMCNTX, YMSTEX, YMSBKI, YMAR#, YMDVST, YMFDST, YMSTST
from rydedata.pymast p
where trim(ymname) in ('SCHLENKER,GENE','SEGALL,BRAD')

select count(*) from rydedata.ffpxrefdta

