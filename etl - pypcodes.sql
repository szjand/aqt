Select * from RYDEDATA.PYPCODES where ytdco# = 'RY1' and trim(ytddcd) = '91'

-- fica exempt
Select * from RYDEDATA.PYPCODES where ytdco# = 'RY1' and ytdex2 = 'Y'

-- medicare exempt
Select * from RYDEDATA.PYPCODES where ytdco# = 'RY1' and ytdex6 = 'Y'

-- ytdtyp req'd for unique row, each store as ytdtyp 1 & 2 for 83 (AR)
select ytdco#, ytddcd, ytdtyp
from rydedata.pypcodes
group by ytdco#, ytddcd, ytdtyp
having count(*) > 1

select *
from rydedata.pydeduct
where trim(ydempn) = '11650'

select ydco#, ydempn, yddcde
from rydedata.pydeduct
group by ydco#, ydempn, yddcde
having count(*) > 1

select *
from rydedata.pydeduct
where ydempn = '115