---- limit to inventory in april (date_in_invent)
---- 05/01/18 i am going brain dead, what date do i put in the where clause?
---- well, 11/01/2018, for oct month end date_in_invent < 20181101
--
--select trim(a.inpmast_stock_number) as stock_number, a.inpmast_vin as vin, 
--  b.num_field_value as ad_pack_amount\
---- select count(*)  
--from rydedata.inpmast a 
--inner join rydedata.inpoptf b on a.inpmast_vin = b.vin_number
--inner join rydedata.inpoptd c on b.company_number = c.company_number
--  and b.seq_number = c.seq_number
--where a.status = 'I'
--  and b.seq_number = 14
--  and b.num_field_value <> 0
--and  date_in_invent < 20191101 ------------------------------
--
--
--201802: 474 $116021.03  239.71 per
--201803: 407 $ 98973.52  243.18 per
--201804: 351 $ 83017.53  236.52 per
--208105: 372 $ 87698.80  235.75 per
--201806: 395 $ 92350.91  233.80 per
--201810: 375 $ 93850.92  250.26 per
--201812: 411 $ 92242.08  224.43 per
--
--select sum(b.num_field_value)  as total, count(*), sum(b.num_field_value)/count(*)
--from rydedata.inpmast a 
--inner join rydedata.inpoptf b on a.inpmast_vin = b.vin_number
--inner join rydedata.inpoptd c on b.company_number = c.company_number
--  and b.seq_number = c.seq_number
--where a.status = 'I'
--  and b.seq_number = 14
--  and b.num_field_value <> 0
--  and  date_in_invent < 20191101
--
--
--
--
--select a.control_number, b.inpmast_vin, a.transaction_amount
--from rydedata.glptrns a
--join rydedata.inpmast b on trim(a.control_number) = trim(b.inpmast_stock_number)
--  and b.status = 'I'
--  and b.type_n_u = 'N'
--where a.post_status = 'Y'
--  and trim(a.account_number) = '133211'
--
--  
--  
--select trim(a.inpmast_Stock_number) as stock_number, trim(a.inpmast_vin) as vin, -b.transaction_amount as ad_pack_amount
---- select count(*)
--from rydedata.inpmast a
--join rydedata.glptrns b on trim(a.inpmast_stock_number) = trim(b.control_number)
--  and trim(b.account_number) = '133211'
--  and b.post_status = 'Y'
--where a.status = 'I'
--  and a.type_n_u = 'N'  
--  and left(trim(a.inpmast_Stock_number), 1) <> 'H'
--  and right(trim(a.inpmast_Stock_number), 1) <> 'R'
--
--union
--select trim(a.inpmast_stock_number) as stock_number, a.inpmast_vin as vin, 
--  b.num_field_value as ad_pack_amount
---- select count(*)  
--from rydedata.inpmast a 
--inner join rydedata.inpoptf b on a.inpmast_vin = b.vin_number
--inner join rydedata.inpoptd c on b.company_number = c.company_number
--  and b.seq_number = c.seq_number
--where a.status = 'I'
--  and b.seq_number = 14
--  and b.num_field_value <> 0
--  and  date_in_invent < 20200201-----------------------------
--  and a.inpmast_stock_number like '%R'
--
--  
---- new car inventory accounts  
--Select * 
--from RYDEDATA.GLPMAST a
--where a.year = 2020
--  and a.account_type = '1'
--  and a.department = 'NC'
--  and a.typical_balance = 'D'
--  and trim(a.account_number) <> '126104'
--  and trim(a.account_number) not like '3%'
--order by account_number  
--  
--  
---- R units in inventory  
--
--select trim(a.control_number), left(trim(a.control_number), 6)
--from rydedata.glptrns a
--join rydedata.glpmast b on trim(a.account_number) = trim(b.account_number)
--  and b.year = 2020
--  and b.account_type = '1'
--  and b.department = 'NC'
--  and b.typical_balance = 'D'
--  and trim(b.account_number) <> '126104'
--  and trim(b.account_number) not like '3%'
--where a.control_number like '%R'  
--  and a.post_status = 'Y'
--group by (a.control_number) 
--having sum(a.transaction_amount) > 10000  
--
--
--
---- current new car inventory
--select trim(a.control_number) as control_number, left(trim(a.control_number), 6), min(c.inpmast_vin) as vin
--from rydedata.glptrns a
--join rydedata.glpmast b on trim(a.account_number) = trim(b.account_number)
--  and b.year = 2020
--  and b.account_type = '1'
--  and b.department = 'NC'
--  and b.typical_balance = 'D'
--  and trim(b.account_number) <> '126104'
--  and trim(b.account_number) not like '3%'
----where a.control_number like '%R'  
--join rydedata.inpmast c on trim(a.control_number) = trim(c.inpmast_stock_number)
--where a.post_status = 'Y'
--group by (a.control_number)
--having sum(a.transaction_amount) > 10000  
--
--






-------------------------------------------------------------------------
--< 02/10/20 

/*DECLARE GLOBAL TEMPORARY TABLE  SESSION.adpack_1 (
  control_number  CHAR(17),
  vin char(17));    
    
drop table SESSION.adpack_1    

select * from SESSION.adpack_1    
    
insert into session.adpack_1 
select trim(a.control_number),  min(c.inpmast_vin)
from rydedata.glptrns a
join rydedata.glpmast b on trim(a.account_number) = trim(b.account_number)
  and b.year = 2020
  and b.account_type = '1'
  and b.department = 'NC'
  and b.typical_balance = 'D'
  and trim(b.account_number) <> '126104'
  and trim(b.account_number) not like '3%'
--where a.control_number like '%R'  
join rydedata.inpmast c on trim(a.control_number) = trim(c.inpmast_stock_number)
where a.post_status = 'Y'
group by (a.control_number)
having sum(a.transaction_amount) > 10000  

select a.control_number, vin, -b.transaction_amount as ad_pack_amount
-- select count(*)
from session.adpack_1 a
join rydedata.glptrns b on left(trim(a.control_number), 6) = trim(b.control_number)
  and trim(b.account_number) = '133211'
  and b.post_status = 'Y'
where left(trim(a.control_number), 1) <> 'H'
  and right(trim(a.control_number), 1) <> 'R'
union
select a.control_number, vin, -b.transaction_amount as ad_pack_amount
-- select count(*)
from session.adpack_1 a
join rydedata.glptrns b on trim(a.control_number) = trim(b.control_number)
  and trim(b.account_number) = '133211'
  and b.post_status = 'Y'
where right(trim(a.control_number), 1) = ORDER BY 'R'
*/ 

-- actuall this works just the same, no need to deal with temp tables
select a.stock_number, vin, -b.transaction_amount as ad_pack_amount 
from ( -- this gives me stock number and vin of all nc currently in inventory
  select trim(a.control_number) as stock_number,  min(c.inpmast_vin) as vin
  from rydedata.glptrns a
  join rydedata.glpmast b on trim(a.account_number) = trim(b.account_number)
    and b.year = 2023
    and b.account_type = '1'
    and b.department = 'NC'
    and b.typical_balance = 'D'
    and trim(b.account_number) <> '126104'
    and trim(b.account_number) not like '3%' --OR
    -- and a.transaction_date < '01/01/2021'  -- add date for jeri 1/13/21
  --where a.control_number like '%R'  
  join rydedata.inpmast c on trim(a.control_number) = trim(c.inpmast_stock_number)
  where a.post_status = 'Y'
  group by (a.control_number)
  having sum(a.transaction_amount) > 10000) a  
join rydedata.glptrns b on left(trim(a.stock_number), 6) = trim(b.control_number)
  and trim(b.account_number) = '133211'
  and b.post_status = 'Y'  
  -- and b.transaction_date < '01/01/2021'  -- add date for jeri 1/13/21
 
-- 05/01/20 355 @ 242.78 per (86.187.47) 
-- 06/01/20 279 @ 235.29 per
-- 07/01/20 205 @ 233.28 per
-- 08/01/20 203 @ 236.97 per
-- 09/01/20 204 @ 224.95 per (45890.68)
-- 10/01/20 220 @ 232.54 per (51159.14)
-- 11/01/20 169 @ 228.13 per (38555.09)
-- 12/01/20 178 @ 284.50 per (44233.94)
-- 02/01/2023 296 @ 302.31 per (89486.25)
-- 04/01/2023 306 @ 277.76 per (845995.94)
select count(*), sum(-b.transaction_amount), sum(-b.transaction_amount)/count(*)
from ( -- this gives me stock number and vin of all nc currently in inventory
  select trim(a.control_number) as stock_number,  min(c.inpmast_vin) as vin
  from rydedata.glptrns a
  join rydedata.glpmast b on trim(a.account_number) = trim(b.account_number)
    and b.year = 2023
    and b.account_type = '1'
    and b.department = 'NC'
    and b.typical_balance = 'D'
    and trim(b.account_number) <> '126104'
    and trim(b.account_number) not like '3%' --OR
    and a.transaction_date < '04/01/2023'  -- add date for jeri 1/13/21
  --where a.control_number like '%R'  
  join rydedata.inpmast c on trim(a.control_number) = trim(c.inpmast_stock_number)
  where a.post_status = 'Y'
  group by (a.control_number)
  having sum(a.transaction_amount) > 10000) a  
join rydedata.glptrns b on left(trim(a.stock_number), 6) = trim(b.control_number)
  and trim(b.account_number) = '133211'
  and b.post_status = 'Y'   
  and b.transaction_date < '04/01/2023'  -- add date for jeri 1/13/21
  
  
select * from rydedata.glptrns limit 10  
  