select distinct ry3.pmpart, ry3.pmdesc, ry3.pmonhd
from (

Select pmpart, pmdesc, pmonhd, pmdtls, pmbinl, pmcost
from PDPMAST
where pmco# = 'RY3'
and pmdtls < 20090325
and pmonhd > 0) RY3

inner join (

Select pmpart, pmonhd, pmdtls, pmbinl, pmcost
from PDPMAST
where pmco# = 'RY1'
and pmdtls > 20090325) RY1 on ry1.pmpart = ry3.pmpart

left join (

select pmpart, Sales1 + Sales2 as YearlySales, pmoprt as "Old Part #"
from(
	select pmpart, pmpack, pmdesc, pmstat, pmonhd, pmordr, pmbcko, pmco#,  
      (select coalesce(sum(ptqty), 0) 
	   from pdptdet pd
	   where ptpart = pm.pmpart
	   and ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
	   and ptco# = 'RY1'
	   and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())) as Sales1, 
	   (select coalesce(sum(ptqty), 0) 
	    from pdptdet pd
		where exists(
		  select 1
		  from pdpmrpl 
		  where pfoprt = pd.ptpart
		  and pfaprt = pm.pmpart)   
		and ptcode in ('CP','IS','SA','WS','SC','SR','CR','RT','FR') 
		and ptco# = 'RY1'
		and ptdate > (year(curdate()) - 1) * 10000 + month(curdate()) * 100 + day(curdate())) as Sales2,           
	  pmbinl, pmoprt
	from pdpmast pm) as wtf1) as wtf on wtf.pmpart = ry1.pmpart

order by ry3.pmcost desc