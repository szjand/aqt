Select * 
from RYDEDATA.SDPRDET 
order by ptdate desc

select count(*) from rydedata.sdprdet

Select ptco#, ptro#, ptline, count(*) 
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3')
group by ptco#, ptro#, ptline

select ptco#, count(*)
from rydedata.sdprdet
group by ptco#

Select length(trim(ptro#)), count(*)
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
group by length(trim(ptro#))

Select ptco#, ptro#, ptline, count(*) 
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
group by ptco#, ptro#, ptline
  having count(*) > 1

select s.*
from rydedata.sdprdet s
inner join (
    Select ptco#, ptro#, ptline, count(*) 
    from RYDEDATA.SDPRDET 
    where ptco# in ('RY1', 'RY2','RY3')
      and ptro# <> ''
    group by ptco#, ptro#, ptline
      having count(*) > 1) x on s.ptco# = x.ptco#
  and s.ptro# = x.ptro#
  and s.ptline = x.ptline


-- count of ptltyp
select ptltyp, count(*)
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
group by ptltyp

-- add ptltyp to grouping
select s.*
from rydedata.sdprdet s
inner join (
    Select ptco#, ptro#, ptline, ptltyp, count(*) 
    from RYDEDATA.SDPRDET 
    where ptco# in ('RY1', 'RY2','RY3')
      and ptro# <> ''
    group by ptco#, ptro#, ptline, ptltyp
      having count(*) > 1) x on s.ptco# = x.ptco#
  and s.ptro# = x.ptro#
  and s.ptline = x.ptline
  and s.ptltyp = x.ptltyp

-- add ptseq# to grouping
-- hmm, offsetting ptlamt values appear
select s.*
from rydedata.sdprdet s
inner join (
    Select ptco#, ptro#, ptline, ptltyp, ptseq#, count(*) 
    from RYDEDATA.SDPRDET 
    where ptco# in ('RY1', 'RY2','RY3')
      and ptro# <> ''
    group by ptco#, ptro#, ptline, ptltyp, ptseq#
      having count(*) > 1) x on s.ptco# = x.ptco#
  and s.ptro# = x.ptro#
  and s.ptline = x.ptline
  and s.ptltyp = x.ptltyp
  and s.ptseq# = x.ptseq#

-- add ptlamt to grouping
-- hmmm, even adding the > 2011, lots of exact duplicates
-- uh oh, i'm getting nervous about my understanding of inner join 
select s.*
from rydedata.sdprdet s
inner join (
    Select ptco#, ptro#, ptline, ptltyp, ptseq#, ptlamt, count(*) 
    from RYDEDATA.SDPRDET 
    where ptco# in ('RY1', 'RY2','RY3')
      and ptro# <> ''
    group by ptco#, ptro#, ptline, ptltyp, ptseq#, ptlamt
      having count(*) > 1) x on s.ptco# = x.ptco#
--  and s.ptro# = x.ptro#
--  and s.ptline = x.ptline
-- and s.ptltyp = x.ptltyp
-- and s.ptseq# = x.ptseq#
--  and s.ptlamt = x.ptlamt
where ptdate > 20110000

-- so let's go the RO in route
select *
from rydedata.sdprdet
where trim(ptro#) in (
  Select trim(ptro#) 
  from RYDEDATA.SDPRDET 
  where ptco# in ('RY1', 'RY2','RY3')
    and ptro# <> ''
  group by ptco#, ptro#, ptline, ptltyp, ptseq#, ptlamt
    having count(*) > 1) 
and ptdate > 20110000
order by ptco#, ptro#, ptline, ptltyp, ptseq#, ptlamt

-- ro 16040393, the dup would be removed by adding ptlhrs and or ptcost
-- so lets add ptlhrs
-- yep eliminates 16040393
select *
from rydedata.sdprdet
where trim(ptro#) in (
  Select trim(ptro#) 
  from RYDEDATA.SDPRDET 
  where ptco# in ('RY1', 'RY2','RY3')
    and ptro# <> ''
  group by ptco#, ptro#, ptline, ptltyp, ptseq#, ptlamt, ptlhrs
    having count(*) > 1) 
and ptdate > 20110000
order by ptco#, ptro#, ptline, ptltyp, ptseq#, ptlamt, ptlhrs

-- for this one the duplicate line has all 0's for ptlhrs, ptlamt and ptcost
select *
from rydedata.sdprdet
where trim(ptro#) = '16042331'
  and ptline = 3
  and ptltyp = 'L'
  and ptseq# = 41

-- a whole shit load of pdq
select *
from rydedata.sdprdet
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptlhrs + ptlamt + ptcost = 0
  and ptdate > 20110000
order by ptdate desc
  
-- and ptdate <> 0  -- this looks like junk data to me


-- shit is going nowhere, let's see if there are ros in det that are not in hdr
select *
from rydedata.sdprdet d
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptdate <> 0
  and not exists (
    select 1
    from rydedata.sdprhdr
    where trim(ptro#) = trim(d.ptro#))

-- holy shit, 2169694 records with ptdate = 0
select count(*)
from rydedata.sdprdet
where ptdate = 0
order by ptro# desc

select *
from rydedata.sdprdet
where ptdate = 0
  and length(trim(ptro#)) = 8
  and ptco# = 'RY1'
  and left(trim(ptro#), 2) = '16'
order by ptro# desc

select *
from rydedata.sdprhdr
where trim(ptro#)= '16079688'


select *
from rydedata.sdprhdr h
left join rydedata.sdprdet d on trim(h.ptro#) = trim(d.ptro#)
where h.ptdate > 20111200
  and d.ptdate = 0

select *
from rydedata.sdprdet
where trim(ptro#) = '19086296'

select 
  min(ptdate), max(ptdate)
from rydedata.sdprdet
where ptdate <> 0
-- 4 honda ros with ptline = 0
select *
from rydedata.sdprdet
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptline = 0

-- find a reasonable chunk size for scraping
-- date won't work, unless i do ptdate = 0 separately
select count(*)
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptdate = 0

select *
from rydedata.sdprdet 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptdate > 20111100
order by ptdate desc

select left(trim(ptro#), 4), count(*) as howmany
from rydedata.sdprdet 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
--  and ptdate > 20111100
group by left(trim(ptro#), 4)
order by count(*) desc

select count(howmany)
from (
select left(trim(ptro#), 4), count(*) as howmany
from rydedata.sdprdet 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
--  and ptdate > 20111100
group by left(trim(ptro#), 4)) x


select count(howmany)
from (
select ptline, ptltyp, ptcode, count(*) as howmany
from rydedata.sdprdet 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
--  and ptdate > 20111100
group by ptline, ptltyp, ptcode) x

-- ok 1 chunk of ptdate = 0
-- and about thirty runs of 50K each (month)/year)
select left(digits(ptdate),4), substr(digits(ptdate), 5, 2), count(*)
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptdate <> 0
group by left(digits(ptdate),4), substr(digits(ptdate), 5, 2)

select left(digits(min(ptdate)),4) as minYear, substr(digits(min(ptdate)), 5, 2) as minMonth,
  left(digits(max(ptdate)),4) as maxYear, substr(digits(max(ptdate)), 5, 2) as maxMonth
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptdate <> 0


select left(digits(min(ptdate)),4) as minYear,
       substr(digits(min(ptdate)), 5, 2) as minMonth, 
       left(digits(max(ptdate)),4) as maxYear, 
       substr(digits(max(ptdate)), 5, 2) as maxMonth 
       from RYDEDATA.SDPRDET
        where ptco# in ('RY1', 'RY2','RY3')
       and ptro# <> '' and ptdate <> 0

-- looks like 2mil where ptdate = 0 is too big
select left(trim(ptro#), 3), count(*)
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
--  and ptdate = 0
  and left(trim(ptro#), 2) like '26%'
group by left(trim(ptro#), 3)
order by count(*) desc
order by left(trim(ptro#), 4)

select left(trim(ptro#), 2), count(*)
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
--  and ptdate = 0
--  and left(trim(ptro#), 3) > '1670'
group by left(trim(ptro#), 2)
order by count(*) desc

select count(*) 
from (
select left(trim(ptro#), 3), count(*)
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
--  and ptdate = 0
  --and left(trim(ptro#), 4) > '6999'
group by left(trim(ptro#), 3))x

select left(trim(ptro#), 3), count(*)
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
--  and ptdate = 0
  --and left(trim(ptro#), 4) > '6999'
group by left(trim(ptro#), 3)
order by count(*) desc
--12/11
-- turns out that simple grouping by ro works "best"
-- sweet spot seems to be abt 100k records
-- the problem with the ro grouping is 1000s of group with < 1000 records, so the hit is in
-- requerying for each of those groups


-- groups based on record count?
group by left 2 except

1901 - 1908 left 4
      
1600 - 1607 left 4

260 - 265 left 3

/**********************************************************************************************/
/*        SDPRHDR          */ -- updated 3/14/12, 
/**********************************************************************************************/
select *
from rydedata.sdprhdr

select count(*)
from rydedata.sdprhdr

select ptco#, count(*)
from rydedata.sdprhdr
group by ptco#

-- compare ptdate to ptcreate
select ptro#, ptdate, ptcreate, date(ptcreate) 
from rydedata.sdprhdr
where ptco# in ('RY1',' RY2','RY3')
  and ptdate > 20090900
  and cast(ptcreate as date) <> '0001-01-01'
  and date(ptcreate) <> cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date)
order by ptdate desc

select ptcreate, cast(ptcreate as date)
select min(ptdate), max(ptdate)
from rydedata.sdprhdr
where cast(ptcreate as date) = '0001-01-01'


select *
from rydedata.sdprhdr
where trim(ptro#) = '16072130'

select *
from rydedata.sdprdet
where trim(ptro#) = '16072130'

select count(*) from rydedata.sdprhdr

-- min/max on decimal columns
select min(ptcreate),max(ptcreate)
from rydedata.sdprhdr
where cast(ptcreate as date) <> '0001-01-01'

select ptodom, ptmilo
from rydedata.sdprhdr
where ptodom > 2000000

select distinct ptapdt, ptdtcm, ptdtpi
from rydedata.sdprhdr

-- unique rows ? hahahahahaha
-- no dup ro# 11/2011, 2 in 2011

select trim(ptro#)
from rydedata.sdprhdr
where ptco# in ('RY1',' RY2','RY3')
  and ptdate > 20100000
group by trim(ptro#)
  having count(*) > 1

-- ptco# to grouping
-- BINGO, only 2 dup ros
-- and those are justification in ptro# differences,
-- shit it looks like _160... is the correct one, has a final close date
select *
from rydedata.sdprhdr
where trim(ptro#) in (
  select trim(ptro#)
  from rydedata.sdprhdr
  where ptco# in ('RY1',' RY2','RY3')
--    and ptdate > 20110000
  group by trim(ptro#), trim(ptco#)
    having count(*) > 1)
order by trim(ptro#)

-- close date
select count(*) -- only 1 ?
from rydedata.sdprhdr
where ptco# in ('RY1',' RY2','RY3')
  and ptcdat = 0

-- final close date
select count(*) -- 70
from rydedata.sdprhdr
where ptco# in ('RY1',' RY2','RY3')
  and ptfcdt = 0
-- aha, the only ones are
select *
from rydedata.sdprhdr
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and trim(ptro#) in (
    select ptro#
    from rydedata.sdprhdr
    where ptfcdt = 0)

-- "date" fields
select ptdate, ptcdat, ptfcdt, ptapdt, ptdtcm, ptdtpi,
case
  when ptcdat = 0 then cast('9999-12-31' as date)
  else cast(left(digits(ptcdat), 4) || '-' || substr(digits(ptcdat), 5, 2) || '-' || substr(digits(ptcdat), 7, 2) as date)
end as ptdate
from rydedata.sdprhdr
where ptco# in ('RY1',' RY2','RY3')
  and ptfcdt = 0
  and ptcdat <> 0

select PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, PTARCK, PTPMTH,
case  
  when PTDATE = 0 then cast('9999-12-31' as date)
  else cast(left(digits(PTDATE), 4) || '-' || substr(digits(PTDATE), 5, 2) || '-' || substr(digits(PTDATE), 7, 2) as date)
end as PTDATE,
case  
  when PTCDAT = 0 then cast('9999-12-31' as date)
  else cast(left(digits(PTCDAT), 4) || '-' || substr(digits(PTCDAT), 5, 2) || '-' || substr(digits(PTCDAT), 7, 2) as date)
end as PTCDAT,
case  
  when PTFCDT = 0 then cast('9999-12-31' as date)
  else cast(left(digits(PTFCDT), 4) || '-' || substr(digits(PTFCDT), 5, 2) || '-' || substr(digits(PTFCDT), 7, 2) as date)
end as PTFCDT,
PTVIN, PTTEST, PTSVCO, PTSVCO2, PTDEDA, PTDEDA2, PTWDED, PTFRAN, PTODOM, PTMILO, 
PTCHK#, PTPO#, PTREC#, PTPTOT, PTLTOT, PTSTOT, PTDEDP, PTSVCT, PTSPOD, PTCPHZ, 
PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, PTWAST3, PTWAST4, PTINST, 
PTINST2, PTINST3, PTINST4, PTSCST, PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, 
PTINSS, PTSCSS, PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTAUTH, PTTAG#, 
case  
  when PTAPDT = 0 then cast('9999-12-31' as date)
  else cast(left(digits(PTAPDT), 4) || '-' || substr(digits(PTAPDT), 5, 2) || '-' || substr(digits(PTAPDT), 7, 2) as date)
end as PTAPDT,
case  
  when PTDTCM = 0 then cast('9999-12-31' as date)
  else cast(left(digits(PTDTCM), 4) || '-' || substr(digits(PTDTCM), 5, 2) || '-' || substr(digits(PTDTCM), 7, 2) as date)
end as PTDTCM,
PTDTPI, PTTIIN, PTCREATE, PTDSPTEAM
from rydedata.sdprhdr 
where ptco# in ('RY1','RY2','RY3') 
  and ptfcdt = 0 
  and ptcdat <> 0


select count(*) from rydedata.sdprdet where ptco# in ('RY1','RY2','RY3') and trim(ptro#) <> '' and ptdate <> 0




select left(digits(min(ptdate)),4) as minYear, left(digits(max(ptdate)),4) as maxYear from RYDEDATA.SDPRDET 

where ptco# in ('RY1', 'RY2','RY3') and ptro# <> ''

select ptcreate, 
case 
  when cast(PTCREATE as date) = '0001-01-01' then cast('9999-12-31 00:00:00' as timestamp) 
  else PTCREATE 
end as PTCREATE1
from rydedata.sdprhdr
where cast(PTCREATE as date) = '0001-01-01'

select *
from rydedata.sdprhdr
where ptco# in ('RY1','RY2','RY3') and ptfcdt = 0 and cast(ptcreate as date) = '0001-01-01'


select left(trim(ptro#), 3), count(*)
from rydedata.sdprhdr
where ptco# in ('RY1','RY2','RY3')
  and trim(ptro#) <> ''
group by left(trim(ptro#), 3)
order by left(trim(ptro#), 3)
order by count(*) desc

select count(*)
from (
select left(trim(ptro#), 4), count(*)
from rydedata.sdprhdr
group by left(trim(ptro#), 4)) x

-- 12/11 what to update
-- ptcreate will give me new records, but can existing records change, 
-- and if so, what fields
select max(ptcreate)
from rydedata.sdprhdr

select *
from rydedata.sdprhdr
where ptcreate > timestamp('2011-12-10 16:53:28')

select ymco#, ymempn, ymactv, ymname, ymhdte, ymhdto, ymrdte, ymtdte, ympper, 
  ymclas, ymsaly, ymrate, ymeeoc
from rydedata.pymast
order by ymname

select *
from rydedata.pymast
order by ymname

-- 12/18 
-- a reduced subset of sdprhdr, starting at 1/1/2010

select count(*) -- 726119  -- 195706
from rydedata.sdprhdr
where ptdate > 20100000

select count(distinct ptro#) -- 711162 -- 195706
from rydedata.sdprhdr
where ptdate > 20100000

select left(trim(ptro#), 2), count(*)
from rydedata.sdprhdr
where ptco# in ('RY1','RY2','RY3')
  and trim(ptro#) <> ''
  --and ptdate > 20100000
group by left(trim(ptro#), 2)
order by left(trim(ptro#), 2)

select min(ptdtcm), min(ptdtpi), max(ptdtcm), max(ptdtpi)
from rydedata.sdprhdr
where ptco# in ('RY1','RY2','RY3')
  and trim(ptro#) <> ''
  and ptdate > 20100000

-- 3/14 looking for the nightly chunk
-- what about an ro opened 60 days ago that closed yesterday
-- include all for any that opened, closed or final closed in last 45 days


select count(*) -- 10861, -- 11330, -- 11446
from rydedata.sdprhdr
where ptco# in ('RY1','RY2','RY3')
  and trim(ptro#) <> ''
  and (
    cast(left(digits(PTDATE), 4) || '-' || substr(digits(PTDATE), 5, 2) || '-' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 45 days
    or (
      ptcdat <> 0
      and
      cast(left(digits(PTCDAT), 4) || '-' || substr(digits(PTCDAT), 5, 2) || '-' || substr(digits(PTCDAT), 7, 2) as date) > curdate() - 45 days)
    or (
      ptfcdt <> 0
      and 
      cast(left(digits(PTFCDT), 4) || '-' || substr(digits(PTFCDT), 5, 2) || '-' || substr(digits(PTFCDT), 7, 2) as date) > curdate() - 45 days))



select count(*) -- 82712, that's a digestible chunk
from rydedata.sdprdet
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptdbas <> 'V'
and trim(ptro#) in (
select trim(ptro#) -- 10861, -- 11330, -- 11446
from rydedata.sdprhdr
where ptco# in ('RY1','RY2','RY3')
  and trim(ptro#) <> ''
  and (
    cast(left(digits(PTDATE), 4) || '-' || substr(digits(PTDATE), 5, 2) || '-' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 45 days
    or (
      ptcdat <> 0
      and
      cast(left(digits(PTCDAT), 4) || '-' || substr(digits(PTCDAT), 5, 2) || '-' || substr(digits(PTCDAT), 7, 2) as date) > curdate() - 45 days)
    or (
      ptfcdt <> 0
      and 
      cast(left(digits(PTFCDT), 4) || '-' || substr(digits(PTFCDT), 5, 2) || '-' || substr(digits(PTFCDT), 7, 2) as date) > curdate() - 45 days)))


/************ 3/12/12 sdprdet, records to exclude ***************************************/


select count(*)
from rydedata.sdprdet 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptdbas <> 'V'


select count(*)
from rydedata.sdprdet 
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptdbas = 'V'

select *
from rydedata.sdptech
where trim(sttech) = '139'

select count(*)
from rydedata.pdptdet

select *
from rydedata.pdptdet
where trim(ptinv#) = '16065259'

select *
from rydedata.pdpthdr
where trim(ptinv#) = '16065259'



-- compbinations of ptltyp and ptcode
SELECT ptltyp, ptcode, COUNT(*)
FROM rydedata.sdprdet
GROUP BY ptltyp, ptcode

-- ro with lots of lines

SELECT *
FROM rydedata.sdprdet
WHERE TRIM(ptro#) = '16063942'


-- records to exclude
-- whoa, including glptrns <> V cuts the count down to 1924398
select count(*)
from rydedata.sdprdet d
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptdbas <> 'V'
and exists ( -- eliminate voids 
select 1 
from rydedata.glptrns 
where trim(gtdoc#) = trim(d.ptro#) 
and gtpost <> 'V') 


select *
from rydedata.glptrns
where trim(gtdoc#) = '16074737'



-- records to exclude
-- whoa, including glptrns <> V cuts the count down to 1924398
select *
from rydedata.sdprdet
where trim(ptro#) not in (
select trim(ptro#) as ptro#
from rydedata.sdprdet d
where ptco# in ('RY1', 'RY2','RY3')
  and ptro# <> ''
  and ptdbas <> 'V'
and exists ( -- eliminate voids 
select 1 
from rydedata.glptrns 
where trim(gtdoc#) = trim(d.ptro#) 
and gtpost <> 'V'))
order by ptdate desc


/* 3/14/ */
select count(*) -- 8953
-- select *
from rydedata.sdprhdr
where cast(PTCREATE as date) = '0001-01-01'

select min(ptdate), max(ptdate)
from rydedata.sdprhdr
where cast(PTCREATE as date) = '0001-01-01'

select *
from rydedata.sdprhdr
where ptdate = 0

select distinct ptdtcm
from rydedata.sdprhdr


select * 
from rydedata.sdprhdr
where trim(ptro#) = '18013475'



/*********** 3/17/12  sdprdet unique rows *****************************/

select min(ptpadj), max(ptpadj)
from rydedata.sdprdet


SELECT *
FROM rydedata.GLPTRNS
WHERE trim(gtdoc#) = '16082718'

select count(*)
from rydedata.SDPRDET
where trim(ptro#) in (

select trim(ptro#) -- 10861, -- 11330, -- 11446
from rydedata.sdprhdr
where ptco# in ('RY1','RY2','RY3')
  and trim(ptro#) <> ''
  and (
    cast(left(digits(PTDATE), 4) || '-' || substr(digits(PTDATE), 5, 2) || '-' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 45 days
    or (
      ptcdat <> 0
      and
      cast(left(digits(PTCDAT), 4) || '-' || substr(digits(PTCDAT), 5, 2) || '-' || substr(digits(PTCDAT), 7, 2) as date) > curdate() - 45 days)
    or (
      ptfcdt <> 0
      and 
      cast(left(digits(PTFCDT), 4) || '-' || substr(digits(PTFCDT), 5, 2) || '-' || substr(digits(PTFCDT), 7, 2) as date) > curdate() - 45 days)))
--  and trim(ptcnam) = '*VOIDED REPAIR ORDER*')


/* 6/15/2012 */
select min(ptptaxamt), max(ptptaxamt), min(ptktxamt), max(ptktxamt)
from rydedata.sdprdet

Select * from RYDEDATA.SDPRDET WHERE trim(ptro#) IN ('2681118', '2681127', '2682382') AND ptline = 1

--7/2/13  ptdbas <> V is filtering out ptltyp = A on these 3 ros line 1  ptdbas = Coupon Disc Basis, and each of these 3 had a free oil change punch card
-- so what is the downside of leaving out that filter?
select PTCO#, PTRO#, PTLINE, PTLTYP, PTSEQ#, PTCODE, 
case 
  when ptdate = 0 then cast('9999-12-31' as date) 
else 
  cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date) 
end as PTDATE, 
PTLPYM, PTSVCTYP, PTPADJ, PTTECH, PTLOPC, PTCRLO, PTLHRS, PTLAMT, PTCOST, PTARTF,
PTFAIL, PTSLV#, PTSLI#, PTDCPN, PTDBAS, PTVATCODE, PTVATAMT, PTDISPTY1, PTDISPTY2,
PTDISRANK1, PTDISRANK2 
from RYDEDATA.SDPRDET 
where ptco# in ('RY1', 'RY2','RY3') --and ptdbas <> 'V' 

  
    and trim(ptro#) in (
      select trim(ptro#) from rydedata.sdprhdr 
      where ptco# in ('RY1','RY2','RY3')
        and trim(ptro#) <> ''
        and (  
          cast(left(digits(PTDATE), 4) || '-' || substr(digits(PTDATE), 5, 2) || '-' || substr(digits(PTDATE), 7, 2) as date) > curdate() - 45 days        
          or (
            ptcdat <> 0
            and  
            cast(left(digits(PTCDAT), 4) || '-' || substr(digits(PTCDAT), 5, 2) || '-' || substr(digits(PTCDAT), 7, 2) as date) > curdate() - 45 days) 
          or (
            ptfcdt <> 0 
            and 
            cast(left(digits(PTFCDT), 4) || '-' || substr(digits(PTFCDT), 5, 2) || '-' || substr(digits(PTFCDT), 7, 2) as date) > curdate() - 45 days)))
and trim(ptro#) = '2681118' and ptline = 1


  select PTCO#, PTRO#, PTLINE, PTLTYP, PTSEQ#, PTCODE, 
  case 
    when ptdate = 0 then cast('9999-12-31' as date) 
  else 
    cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date) 
  end as PTDATE, 
  PTLPYM, PTSVCTYP, PTPADJ, PTTECH, PTLOPC, PTCRLO, PTLHRS, PTLAMT, PTCOST, PTARTF,
  PTFAIL, PTSLV#, PTSLI#, PTDCPN, PTDBAS, PTVATCODE, PTVATAMT, PTDISPTY1, PTDISPTY2,
  PTDISRANK1, PTDISRANK2 
  from RYDEDATA.SDPRDET 
  where ptco# in ('RY1', 'RY2','RY3') 
--and ptdbas <> 'V'
  
and trim(ptro#) IN ('2681118', '2681127', '2682382', '16106124')

-- ok, here's the conclusion, when ptcode = CP then ptdbas = V signifies a coupon
-- the rest of the time, it is reversed tech time
-- so, in the scrape, will remove the ptdbas filter, then delete the TT rows later
select ptline, ptcode, count(*)
from rydedata.sdprdet
where ptdbas = 'V'
  and ptdate > 20130000
group by  ptline, ptcode

