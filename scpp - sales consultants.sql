Select a.company_number, a.sales_person_id,a.sales_person_name, a.employee_number,
  substr(a.sales_person_name, position(' ' in a.sales_person_name) + 1) as last_name,
  b.*
from RYDEDATA.BOPSLSP a
left join (
  select employee_name, left(employee_name, position(',' in employee_name) - 1) as last_name, 
    trim(pymast_employee_number) as employee_number,
    active_code
  -- select *
  from rydedata.pymast
  where trim(distrib_code) = 'SALE'
    and active_code <> 'T') b on substr(a.sales_person_name, position(' ' in a.sales_person_name) + 1) = b.last_name
where a.active <> 'N' 
and a.company_number in ('RY1','RY2') 
and a.sales_person_type = 'S' 
order by sales_person_name


Select a.company_number, a.sales_person_id,a.sales_person_name, a.employee_number,
  substr(a.sales_person_name, position(' ' in a.sales_person_name) + 1) as last_name
from RYDEDATA.BOPSLSP a
where a.active <> 'N' 
and a.company_number in ('RY1','RY2') 
and a.sales_person_type = 'S' 
order by sales_person_name, company_number


Select a.company_number, a.sales_person_id,a.sales_person_name, a.employee_number,
  substr(a.sales_person_name, position(' ' in a.sales_person_name) + 1) as last_name
--  b.*
from RYDEDATA.BOPSLSP a
left join (
  select employee_name, left(employee_name, position(',' in employee_name) - 1) as last_name, 
--  trim(substring(employee_name, position(',' in employee_name) + 1)) as first_name,
  trim(pymast_employee_number) as employee_number
--  termination_date, active_code
  -- select *
  from rydedata.pymast
  where trim(distrib_code) = 'SALE') b on substr(a.sales_person_name, position(' ' in a.sales_person_name) + 1) = b.last_name
--    and active_code <> 'T') b on substr(a.sales_person_name, position(' ' in a.sales_person_name) + 1) = b.last_name
--where a.active <> 'N' 
where a.company_number in ('RY1','RY2') 
and a.sales_person_type = 'S' 
order by sales_person_name



select left(employee_name, position(',' in employee_name) - 1) as last_name, trim(pymast_employee_number) as employee_number
-- select *
from rydedata.pymast
where trim(distrib_code) = 'SALE'
  and active_code <> 'T'



select bopmast_company_number, year(date_capped)*100 + month(date_capped),
      trim(primary_salespers), trim(secondary_slspers2),
      trim(bopmast_Stock_number), date_capped,
      trim(bopmast_search_name)
--      trim(b.year), trim(b.make), trim(b.model)
from rydedata.bopmast
where primary_salespers = 'HUM'
  and left(trim(bopmast_stock_number), 1) = 'H'



Select a.deal_key, a.company_number, a.sales_person_id, a.sales_person_name, a.sale_date,
      record_key, bopmast_company_number, year(date_capped)*100 + month(date_capped),
      trim(primary_salespers) as prim_sc, trim(secondary_slspers2) as sec_sc,
      trim(bopmast_Stock_number), date_capped,
      trim(bopmast_search_name) 
from RYDEDATA.BOPSLSS a
inner join rydedata.bopmast b on a.deal_key = b.record_key and a.company_number = b.bopmast_company_number
where a.sales_person_type = 'S'
  and a.company_number in ('RY1', 'RY2')
  and a.sale_date > 20149999
  and b.date_capped is not null 
  and a.sales_person_id <> primary_salespers
